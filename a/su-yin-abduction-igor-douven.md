# 溯因 abduction (Igor Douven)

_首次发表于 2011 年 3 月 9 日星期三；实质性修订于 2021 年 5 月 18 日星期二_

在哲学文献中，“溯因”一词有两个相关但不同的含义。在这两个含义中，该术语指的是某种形式的解释推理。然而，在历史上首次出现的含义中，它指的是解释推理在_生成_假设中的位置，而在现代文献中最常见的含义中，它指的是解释推理在_证明_假设中的位置。在后一种意义上，溯因也经常被称为“最佳解释推断”。

本条目专门讨论了现代意义上的溯因，尽管还有一个关于历史意义上的溯因的补充，它起源于查尔斯·桑德斯·皮尔斯的工作——请参见

> [补充：皮尔斯论溯因](https://plato.stanford.edu/entries/abduction/peirce.html).

另请参阅有关[科学发现](https://plato.stanford.edu/entries/scientific-discovery/index.html#DiscAbdu)的条目，特别是有关发现作为溯因的部分。

大多数哲学家都同意，溯因（在推理至最佳解释的意义上）是一种经常被使用的推理类型，无论是在日常生活中还是在科学推理中以某种形式。然而，溯因的确切形式以及规范地位仍然存在争议。本文对比了溯因与其他类型的推理；指出了它在哲学内外的突出用途；考虑了对它的各种或多或少精确的陈述；讨论了它的规范地位；并突出了溯因与贝叶斯证实理论之间可能的联系。

* [1. 溯因：概念](https://plato.stanford.edu/entries/abduction/#AbdGenIde)
* [1.1 演绎、归纳、溯因](https://plato.stanford.edu/entries/abduction/#DedIndAbd)
* [1.2 溯因的普遍性](https://plato.stanford.edu/entries/abduction/#UbiAbd)
* [2. 阐释溯因](https://plato.stanford.edu/entries/abduction/#ExpAbd)
* [3. 溯因的地位](https://plato.stanford.edu/entries/abduction/#StaAbd)
* [3.1 批评](https://plato.stanford.edu/entries/abduction/#Cri)
* [3.2 辩护](https://plato.stanford.edu/entries/abduction/#Def)
* [4. 溯因 versus 贝叶斯证实理论](https://plato.stanford.edu/entries/abduction/#AbdVerBayConThe)
* [参考文献](https://plato.stanford.edu/entries/abduction/#Bib)
* [学术工具](https://plato.stanford.edu/entries/abduction/#Aca)
* [其他互联网资源](https://plato.stanford.edu/entries/abduction/#Oth)
* [相关条目](https://plato.stanford.edu/entries/abduction/#Rel)

***

## 1. 溯因：概念

你碰巧知道蒂姆和哈里最近发生了一场可怕的争吵，结束了他们的友谊。现在有人告诉你，她刚看到蒂姆和哈里一起慢跑。你能想到的最好解释是他们和好了。你得出结论他们再次成为朋友。

一天早晨，你走进厨房发现桌子上有一个盘子和杯子，上面有面包屑和一小块黄油，周围是一罐果酱、一包糖和一个空的牛奶盒。你得出结论是你的室友之一在夜里起来做宵夜，太累了没清理桌子。你认为，这是最能解释你面对的场景的方式。当然，也可能是有人闯入房子，趁机吃了一口，或者是室友把东西摆在桌子上，并没有吃宵夜，只是让你相信有人吃了宵夜。但是，你觉得这些假设提供的解释比你推断出的那个要牵强得多。

在沙滩上散步时，你看到沙地上好像有温斯顿·丘吉尔的画像。可能就像希拉里·普特南（Hilary Putnam）1981 年的书《理由、真理和历史》开头所描述的那样，你看到的实际上是一只蚂蚁在沙滩上爬行留下的痕迹。更简单，因此（你认为）更好的解释是有人有意在沙滩上画了一幅丘吉尔的画像。无论如何，这就是你得出的结论。

在这些例子中，结论并不从前提中逻辑地得出。例如，从他们发生了一场可怕的争吵结束了友谊，他们刚刚被看到一起慢跑这两个前提并不能逻辑地得出蒂姆和哈里又成为朋友了；我们可以假设，即使从你对蒂姆和哈里的所有信息中也不能得出这个结论。关于友谊、可怕的争吵和慢跑者的任何有用的统计数据都不能证明从你对蒂姆和哈里的信息得出他们再次成为朋友的结论，甚至不能证明他们很可能再次成为朋友。导致你得出这个结论的是什么，根据许多哲学家的看法，也可能支持这个结论的是，蒂姆和哈里再次成为朋友，如果是真的，将最好地解释他们刚刚被看到一起慢跑这一事实。（假设一个假设要解释任何事情，就必须是真实的这一前提从这里开始被视为已知。）类似的评论也适用于其他两个例子。这里展示的推理类型被称为溯因，或者如今更常见的是最佳解释推理。

### 1.1 演绎、归纳、溯因

溯因通常被认为是三种主要推理类型之一，另外两种是演绎和归纳。演绎、归纳和溯因之间的区别对应着必然和非必然推理之间的区别。在演绎推理中，如果推断的前提为真，则推断出的内容是_必然_为真；也就是说，前提的真实性_保证_了结论的真实性。一个熟悉的例子是实例化模式的推理。

> 所有的 _A_ 都是 _B_。

> _a_ 是一个 _A_。

> 因此，_a_ 是 _B_。

但并非所有的推理都是这种类型。例如，考虑从“John 住在切尔西”和“大多数住在切尔西的人都很富有”推导出“John 很富有”的推理。在这里，第一句的真实性并不是由第二和第三句的联合真实性所保证（只是变得可能）。换句话说，前提为真并不一定意味着结论也为真：在前提为真的情况下，John 是切尔西非富有居民中的一员是与之逻辑相容的。关于你根据 Tim 和 Harry 一起慢跑的信息推断他们再次成为朋友的推理也是类似的。也许 Tim 和 Harry 是前商业伙伴，他们仍然有一些财务事项需要讨论，无论他们多么想避免这种情况，并决定将这与他们的日常锻炼结合起来；这与他们坚决决定永不和解是相容的。

这是将非必要推论分为归纳和 溯因 推论的标准做法。归纳推论形成了一个有些异质的类别，但就目前的目的而言，它们可以被描述为那些仅基于统计数据的推论，比如在给定人口中观察到某一特征发生的频率。这样一个推论的例子是这样的：

> 96%的佛兰芒大学生会说荷兰语和法语。

> Louise 是一名佛兰芒大学生。

> 因此，Louise 会说荷兰语和法语。

然而，相关的统计信息也可能给出得更模糊，就像前提中所说的，“住在切尔西的大多数人都很富有。”（关于归纳论证的结论是否可以纯粹定性地陈述，或者是否应该是定量的问题有很多讨论——例如，它具有 0.96 的概率表明路易丝既会讲荷兰语又会讲法语——或者它有时可以用定性的方式陈述——例如，如果其真实概率足够高——有时则不行。关于这些和其他归纳相关问题，请参阅 Kyburg 1990（第 4 章）。还应该提到 Harman（1965）将归纳看作是一种特殊类型的溯因。另请参阅 Weintraub 2013 进行讨论。）

仅仅基于统计数据的推断并不足以将其归类为归纳推理。你可能观察到许多灰色大象而没有看到非灰色的，然后推断所有大象都是灰色的，因为这样可以最好地解释为什么你观察到这么多灰色大象而没有看到非灰色的。这将是一种溯因推理的实例。这表明区分归纳和溯因的最佳方法是：两者都是扩大性的，意味着结论超出了前提中（逻辑上）包含的内容（这就是它们为什么是非必要推理），但在溯因中有暗示或明示的解释考虑，而在归纳中没有；在归纳中，只有对观察频率或统计数据的呼应。（我强调“只有”，因为在溯因中也可能会对频率或统计数据进行呼应，就像关于大象的例子展示的那样。）

一个值得注意的 溯因 的特点是，它与归纳推理共享，但与演绎推理不同的是，它违反了_单调性_，这意味着可能可以从一组前提的_子集_中 溯因 推断出某些结论，而这些结论不能从整个集合_S_中 溯因 推断出来。例如，假设将 Tim 和 Harry 是前商业伙伴，他们仍然有一些财务事项要讨论这一前提添加到了一段时间前他们发生了严重争吵并且刚刚被看到一起慢跑的前提中，即使——让我们假设——仅仅最后两个前提就足以推断他们再次成为朋友，现在也不再能够这样做。原因在于，在添加了他们是有财务事项要讨论的前商业伙伴这一信息后，根据最初的前提，对 Tim 和 Harry 一起慢跑的最佳解释可能不再成立。

### 1.2 溯因的普遍性

在本条目开头描述的案例中所体现的推理类型，大多数人会觉得非常熟悉。哲学家和心理学家一般都同意，溯因经常被用于日常推理。有时我们对溯因推理的依赖是相当明显和明确的。但在一些日常实践中，它可能如此常规和自动化，以至于很容易被忽视。一个例子可能是我们对他人证词的信任，据说这种信任是建立在溯因推理的基础上的；参见 Harman 1965，Adler 1994，Fricker 1994 和 Lipton 1998，以支持这一说法。例如，根据 Jonathan Adler（1994 年，274 页），“\[t] 信息提供者声称 _P_ 的最佳解释通常是…他有充分负责任的理由相信它，并且…他打算我也相信它”，这就是为什么我们通常有理由相信信息提供者的证词。即使在开始相信一个人的证词时，人们通常似乎并没有意识到自己的头脑中正在进行任何溯因推理，这也可能是正确的。类似的评论也适用于一些人认为溯因在语言实践中发挥了进一步、甚至可能更为基础的作用，即在确定说话者通过话语表达的含义时的作用。具体来说，有人认为解码话语是推断为什么某人在话语所在的语境中说了什么的最佳解释的问题。更具体地说，从事语用学研究的作者们提出，当话语的语义内容对于谈话目的来说信息不足、信息过多、离题、不太可能，或者其他方面不寻常或不恰当时，听话者会援引格莱斯会话准则来帮助他们找出说话者话语的最佳解释；例如，参见 Bach 和 Harnish 1979（92 页），Dascal 1979（167 页）和 Hobbs 2004。与依赖说话者证词的情况一样，所需的溯因推理通常似乎是在潜意识层面上进行的。

溯因推理并不局限于日常背景。相反：科学哲学家们认为，溯因是科学方法论的基石；例如，Boyd 1981, 1984，Harré 1986, 1988，Lipton 1991, 2004 和 Psillos 1999。根据 Timothy Williamson（2007），“\[t] 溯因方法论是科学提供的最佳方法”，Ernan McMullin（1992）甚至认为溯因是“推动科学前进的推理”。为了说明溯因在科学中的运用，我们考虑两个例子。

在十九世纪初，人们发现天王星的轨道与根据艾萨克·牛顿的普遍引力理论和辅助假设（即太阳系中没有其他行星）预测的轨道有所偏离。一个可能的解释当然是，牛顿的理论是错误的。鉴于其在当时两个多世纪以来取得的巨大经验成功，这似乎不是一个很好的解释。两位天文学家约翰·库奇·亚当斯和乌尔班·勒维里耶尔则独立提出（但几乎同时）另一种可能性，即太阳系中存在第八颗尚未被发现的行星；他们认为，这是对天王星轨道偏离的最佳解释。不久之后，这颗行星被发现，现在被称为“海王星”。

第二个例子涉及英国物理学家约瑟夫·约翰·汤姆逊现在普遍认为是发现电子的发现。汤姆逊进行了有关阴极射线的实验，以确定它们是否是带电粒子流。他得出结论，认为它们确实是，推理如下：

> 由于阴极射线携带负电荷，受静电力偏转，就好像它们被负电荷电气化一样，并且受到磁力的作用，就像这种力会作用在沿着这些射线路径运动的带负电荷的物体上一样，我认为无法逃脱这样的结论：它们是由物质粒子携带的负电荷。 (汤姆逊, 引自阿钦斯坦 2001, 17)

从报道的实验结果中逻辑上并不能得出阴极射线由带负电荷的粒子组成的结论，汤姆逊也无法依据任何相关的统计数据得出这一结论。然而，他仍然能够“无法逃脱这一结论”，我们可以安全地假设，这是因为这个结论是他能想到的最好的解释，即使在这种情况下，这可能是唯一合理的解释。

许多其他关于溯因科学运用的例子已经在文献中讨论过；例如，参见 Harré 1986 年，1988 年以及 Lipton 1991 年，2004 年。据说溯因也是医学诊断中占主导地位的推理方式：医生倾向于选择最能解释患者症状的假设（参见 Josephson 和 Josephson（编）1994 年，9-12 页；另请参见 Dragulinescu 2016 年关于医学背景下溯因推理的内容）。

最后，溯因在一些重要的哲学辩论中扮演着核心角色。参见 Shalkowski 2010 年关于溯因在形而上学中的地位（还有 Bigelow 2010 年），Krzyżanowska，Wenmackers 和 Douven 2014 年以及 Douven 2016a 年关于溯因在条件语句语义中可能扮演的角色，以及 Williamson 2017 年关于溯因在逻辑哲学中的应用。然而，可以说，溯因在认识论和科学哲学中发挥着最显著的哲学作用，在这些领域中，人们经常援引溯因来反驳所谓的不确定性论证。不确定性论证通常从这样一个前提开始，即一些给定的假设在经验上是等效的，这些作者认为这意味着证据——实际上，我们可能获得的任何证据——无法支持其中任何一个而不是其他的假设。由此，我们应该得出结论，即一个人永远不能有理由相信任何一个特定的假设。（这只是一个概括，但对目前的目的足够了；更详细的不确定性论证内容请参见 Douven 2008 年和 Stanford 2009 年。）这种类型论证的一个著名例子是笛卡尔全局怀疑论证，根据这一论证，关于现实更多或更少符合我们通常认为的方式的假设在经验上等效于各种所谓的怀疑性假设（例如，我们被邪恶的恶魔欺骗，或者我们是连接到超级计算机的罐子里的大脑；例如，参见 Folina 2016 年）。类似的论证也支持科学反实在主义，根据这一理论，我们永远不会有理由在关于构成现实可观部分的背后的等效竞争对手之间做出选择（van Fraassen 1980 年）。

对这些论点的回应通常指出，所涉及的经验等价概念过分忽视了解释考虑因素，例如，通过严格以假设产生相同预测来定义这一概念。 回应者随后认为，即使有些假设确实产生完全相同的预测，其中的一个仍然可能是对所预测现象更好的解释。 因此，如果解释考虑因素在决定我们被授权做出哪些推断时起到作用——正如溯因的辩护者所认为的那样——那么我们仍然有理由相信某个假设的真实性（或可能的真实性，或类似的，取决于所假定的溯因版本，如下文所见）。 许多认识论者在反对笛卡尔怀疑论时引用了伯特兰德·罗素（1912 年，第 2 章）的观点，他们的关键主张是，尽管怀疑性假设与现实更多或更少与我们通常理解的方式相符的假设产生相同的预测，但它们并非同样好的解释所预测的内容；特别是，据说怀疑性假设要比“普通世界”假设复杂得多。 请参阅哈曼 1973 年（第 8 章和第 11 章）、戈德曼 1988 年（205 页）、莫泽 1989 年（161 页）和沃格尔 1990 年、2005 年；请参阅帕杰特 1984 年，针对其他心灵怀疑提出了一种溯因回应。 同样，科学哲学家们认为，我们有理由相信相对于洛伦兹的以太理论，特殊相对论更为合理。 即使这些理论产生相同的预测，前者在解释上仍优于后者。（对于这一主张提出的大多数论据归结为特殊相对论在本体上比其竞争对手更为简约，后者假定了以太的存在。 请参阅詹森 2002 年，了解科学哲学家们支持爱因斯坦理论胜过洛伦兹理论的各种原因的优秀讨论。）

## 2. 阐明溯因

精确说明溯因的内容在溯因文献中并不常见。（皮尔斯确实提出了至少相当精确的说明；但正如本词条的补充部分所解释的那样，它并没有捕捉到大多数人如今对溯因的理解。）人们经常说它的核心思想是解释性考虑具有证实论上的重要性，或者解释的成功是真理的（不一定是不会失败的）标志。然而，显然，这些表述充其量只是口号，很容易看出它们可以用各种看似合理的方式来兑现。在这里，我们将考虑许多这样可能的阐释，从可能称之为“溯因教科书版本”的开始，正如将要看到的那样，这明显是有缺陷的，然后继续考虑对它的各种可能的完善。这些版本共同之处在于它们都是推理规则，需要包含解释性考虑的前提，并产生一个关于假设真理的陈述的结论。差异在于所需的前提，或者我们究竟被允许从中推断出什么（或两者兼有）。

在认识论或科学哲学的教科书中，人们经常会遇到类似以下内容作为溯因的表述：

ABD1

鉴于证据 _E_ 和候选解释 _H_1,…, _H\*\*n_ 对 _E_ 的推断，推断出最能解释 _E_ 的 _H\*\*i_ 的真实性。

经常对这一规则提出的观察指出了可能存在的问题，即它预设了候选解释和最佳解释的概念，这两者都没有直接的解释。虽然一些人仍然希望前者可以用纯逻辑或至少纯形式化的术语来阐明，但人们经常说后者必须诉诸所谓的理论优点，比如简单性、普适性和与成熟理论的一致性；然后，最佳解释将是在这些优点方面总体上表现最好的假设。（例如，参见 Thagard 1978 和 McMullin 1996。）问题在于目前没有一个所说的优点特别被充分理解。（Giere，在 Callebaut（ed.）1993（232）中，甚至做出激进的主张，即理论优点缺乏实质内容，在科学中仅起修辞作用。鉴于最近关于简单性和一致性的形式化工作，例如 Forster 和 Sober 1994，Li 和 Vitanyi 1997 以及 Sober 2015，关于简单性的 Bovens 和 Hartmann 2003 和 Olsson 2005，关于一致性的 Schupbach 和 Sprenger（2011）直接用概率术语提出了解释优势的解释。心理证据对该主张的第二部分提出了质疑；例如，参见 Lombrozo 2007，关于简单性在人们对解释优势的评估中的作用以及 Koslowski _et al_. 2008，关于与背景知识的一致性在这些评估中的作用。）

此外，许多认为溯因走在正确方向上的人认为它太过强大。有些人认为溯因仅支持对最佳解释的_可能_真实性进行推断，另一些人认为它仅支持对最佳解释的_近似_真实性进行推断，还有一些人认为它仅支持对_可能_的_近似_真实性进行推断。

然而，溯因的真正问题比这更深刻。因为溯因是扩张性的——正如前面解释的那样——无论溯因被解释得多么准确，它都不会成为严格逻辑意义上的正确推理规则。然而，只要前提为真，它仍然可以是_可靠_的，因为它在大多数情况下会导致一个真实的结论。ABD1 能够在这种意义上是可靠的一个明显的必要条件是，_大多数情况下_，当_H_最好地解释了_E_，而_E_为真时，_H_也为真（或_H_近似为真，或可能为真，或可能近似为真）。但这对于 ABD1 来说并不足够。因为 ABD1 的前提仅是某个假设是最佳解释证据的_相对于给定集合中的其他假设_。因此，如果这个规则要是可靠的，它必须保持，至少通常情况下，相对于我们考虑的假设集合，最佳解释也将被认为是最佳的，与我们可能构想的任何其他假设相比（但由于缺乏时间或才智，或出于其他原因，我们没有构想）。换句话说，它必须保持，至少通常情况下，证据的_绝对_最佳解释可以在我们提出的候选解释中找到，否则 ABD1 很可能会让我们相信“众坏中的精”（van Fraassen 1989, 143）。

多么合理地假设这种额外要求通常会被满足呢？很可能根本不会。要相信相反的情况，我们必须假设我们在考虑数据的可能解释时，在某种程度上有一种特权，即我们在某种程度上预先设定了，我们会在这些数据的绝对最佳解释中达成一致。毕竟，我们几乎永远不会考虑过，或者甚至不可能考虑_所有_潜在的解释。正如范弗拉森（1989 年，144 页）指出的那样，认为我们具有这种特权是_a priori_相当不可信的。

作为对此的回应，有人可能会主张，要证明最佳解释总是或大多数情况下都在考虑的假设之中，可以在不必假设某种特权的情况下实现（有关不同回应，请参见 Schupbach 2014，有关讨论，请参见 Dellsén 2017）。因为鉴于我们已经设法提出的假设，我们总是可以生成一组共同穷尽逻辑空间的假设。假设_H_1，...，_H\*\*n_是我们迄今为止能够构想出的候选解释。然后简单地定义_H_n+1 := ¬_H_1 ∧ … ∧ ¬_H\*\*n_，并将这个新假设作为我们已经拥有的候选解释之外的另一个候选解释。显然，集合{_H_1，...，_H_n+1}是穷尽的，因为它的元素之一必须为真。遵循这个本身简单的程序似乎足以确保我们永远不会错过绝对最佳的解释。（有关这方面提议，请参见 Lipton 1993。）

哎呀，这里有一个问题。即使可能存在许多假设_H\*\*j_暗示_H_n+1，并且如果它们被提出来，会被评估为比我们最初列出的候选解释中最好的解释更好，_H_n+1 本身通常并不具有信息量；实际上，通常甚至不清楚它的经验后果是什么。例如，假设我们有作为竞争解释的狭义相对论和洛伦兹的以太理论。然后，根据上述建议，我们可以将这两个理论都不正确的假设添加到我们的候选解释中。但是这个进一步的假设肯定会被排名很低作为解释——如果它被排名的话，这似乎是值得怀疑的，因为它的经验后果完全不清楚。这并不是说建议的程序永远不起作用。关键在于通常情况下，它很少能确保最好的解释在我们考虑的候选解释中。

对上述“坏批次论证”的更有前途的回应始于观察到该论证利用了 ABD1 中的一种特殊不对称性或不一致性。该规则允许根据一个比较前提——即特定假设相对于其他可用的假设是证据的最佳解释这一事实得出一个绝对结论，即给定假设为真（参见 Kuipers 2000, 171）。通过用“可能为真”或“近似真”替换“真”并不能避免这种不一致性。为了避免这种情况，有两个一般选项。

第一种选择是修改规则，使其要求一个绝对前提。例如，可以遵循 Alan Musgrave（1988）或 Peter Lipton（1993）的观点，要求被推断为真的假设不仅是可用潜在解释中最好的，而且还要是“令人满意”（Musgrave）或“足够好”（Lipton），从而产生 ABD1 的以下变体：

ABD2

鉴于证据_E_和候选解释_H_1，...，_H\*\*n_，推断解释_E_最佳的_H\*\*i_的真实性，前提是_H\*\*i_在解释方面是令人满意/足够好的。

毋庸置疑，ABD2 需要补充一个关于解释的令人满意或足够好的标准，然而，我们目前仍然缺乏这一点。

其次，人们可以通过制定一个对称或一致的溯因版本来使其在给定比较前提的情况下只支持一个比较结论；这个选择也可以以多种方式实现。以下是一个方法，已经在西奥·凯珀斯（例如，凯珀斯 1984 年，1992 年，2000 年）的著作中提出并得到辩护。

ABD3

给定证据 _E_ 和候选解释 _H_1,…, _H\*\*n_ 对 _E_ 的解释，如果 _H\*\*i_ 比其他假设更好地解释了 _E_，则推断 _H\*\*i_ 比其他假设更接近真相。

显然，溯因需要一个关于接近真相的解释，但今天有许多这样的解释（参见，例如，Niiniluoto 1998）。

一种值得注意的特征是，这里考虑的溯因的合理版本并不依赖于推理者在某种特权的假设上，正如我们看到的，ABD1 隐含地依赖于这一点。另一个特征是，如果一个人可以确定，无论数据的候选解释有多少个被忽略了，都没有一个等同于那些他_已经_考虑过的最好的解释，那么这些合理版本就会授权与 ABD1 完全相同的推理（假设如果后者甚至不令人满意或足够好，那么一个人就不会确定没有潜在的解释与他所考虑的最佳解释一样好）。

正如前面提到的，人们普遍认为人们经常依赖溯因推理。人们究竟依赖于上述规则中的哪一条？或者可能还有一些进一步的规则是他们依赖的吗？或者在某些情境下他们依赖一种版本，在其他情境下依赖另一种（Douven 2017，即将发表）？哲学论证无法回答这些问题。近年来，实验心理学家开始关注人类在推理中赋予解释考虑的作用。例如，Tania Lombrozo 和 Nicholas Gwynne（2014）报告了一些实验，显示我们如何解释某一类事物的属性——无论是通过机械方式，参考部件和过程，还是功能方式，参考功能和目的——对我们将该属性推广到其他类事物的可能性有影响（另见 Sloman 1994 和 Williams 和 Lombrozo 2010）。Igor Douven 和 Jonah Schupbach（2015a），（2015b）提供了实验证据，表明人们的概率更新往往受到解释考虑的影响，使其偏离严格的贝叶斯更新（见下文）。Douven（2016b）表明，在上述实验中，更重视解释考虑的参与者往往更准确，这是根据标准评分规则确定的。（有关与解释和推理相关的最新实验工作的有用概述，请参见 Lombrozo 2012 和 2016。）Douven 和 Patricia Mirabile（2018）发现了一些证据表明，人们在某些情境下依赖类似 ABD2 的东西，但在大多数情况下，关于上述问题的实证研究还是缺乏的。

关于我们_应该_依赖哪些先前规则的规范问题（如果我们应该依赖任何形式的溯因），哲学论证应该能够提供帮助，情况几乎没有好转。鉴于坏一批的论证，ABD1 看起来并不好。声称反对溯因的其他论点与规则的确切阐释无关；在下文中，这些论点将被发现有缺陷。另一方面，已经提出支持溯因的论点——其中一些也将在下文讨论——并没有区分具体版本。因此，假设人们确实通常依赖溯因，必须考虑一个悬而未决的问题，即他们依赖哪个版本的溯因。同样，假设人们依赖溯因是理性的，必须考虑一个悬而未决的问题，即他们应该依赖哪个版本，或者也许是哪些版本的溯因，或者至少被允许依赖哪个版本。

## 3. 溯因的地位

即使我们通常依赖于溯因推理这一事实是真实的，人们仍然可以问到这种做法是否是理性的。例如，实验研究表明，当人们能够想出某种可能事件的解释时，他们往往会高估这种事件实际发生的可能性。 （有关其中一些研究的概述，请参阅 Koehler 1991；另请参阅 Brem 和 Rips 2000。）更有意义的是，Lombrozo（2007）表明，在某些情况下，人们倾向于极大地高估简单解释相对于更复杂解释的概率。 尽管这些研究并不直接涉及到迄今为止所讨论的任何形式的溯因，但它们仍然表明，在推理中考虑解释性因素可能并不总是有益的。（值得注意的是，Lombrozo 的实验\*直接涉及到一些已经提出的用贝叶斯框架阐明溯因的建议；请参阅第 4 节。）然而，迄今为止关于溯因规范地位的最相关评论可以在哲学文献中找到。 本节讨论了针对溯因提出的主要批评，以及为其辩护提出的最有力论据。

### 3.1 批评

我们已经遇到了所谓的坏手牌论证，我们看到，这种论证对于溯因的批评是有效的，但对各种（我们所说的）协调规则的溯因却无能为力。我们在这里考虑两个旨在更为一般的反对意见。第一个甚至声称挑战溯因背后的核心思想；第二个则不那么一般，但仍旨在削弱一大类候选的溯因解释。这两个反对意见都是基于 Bas van Fraassen 的观点。

第一个反对意见的前提是，“解释”的含义之一是，如果一个理论比另一个更具解释性，那么前者必须比后者更具信息量（见，例如，van Fraassen 1983 年，第 2 节）。然后所谓的问题是，“更具信息量的理论不可能更有可能是真实的\[因此]试图通过需要信息的特征（如‘最佳解释推理’）来描述归纳或证据支持，要么自相矛盾，要么含糊其辞”（van Fraassen 1989, 192）。这个基本逻辑观点被认为是“最\[明显]的...在一个理论是另一个理论的延伸的典型情况中：显然，延伸有更多的错误方式”（van Fraassen 1985, 280）。

重要的是要注意，然而，在除了“典范”案例之外的任何其他情况下，假定的基本要点根本不明显。例如，在什么意义上相对论“有更多错误方式”比洛伦兹的以太理论完全不清楚，因为它们做出相同的预测。然而前者通常被认为在解释上优于后者。

第二个反对意见是在范弗拉森 1989 年（第 6 章）提出的，针对溯因的概率版本。该反对意见是这样的规则要么等同于贝叶斯定律，因此是多余的，要么与之相悖，但然后，根据刘易斯的动态荷兰书论证（如泰勒 1973 年所述），在概率上是不一致的，这意味着它们可能导致评估一些共同确保财务损失的赌注是公平的；而且，范弗拉森认为，遵循具有这一特征的规则是不理性的。

然而，这一反对意见并不比第一个更好。首先，正如帕特里克·马赫（1992）和布莱恩·斯科姆斯（1993）所指出的，某种方面的损失可能会被另一方面的好处所抵消。例如，可能有一些概率版本的溯因在某种程度上比贝叶斯法则要好得多，至少在我们的世界中是这样，因为从平均意义上讲，它更快地接近真相，即更快地为真假设分配高概率（理解为高于某个阈值的概率）（参见 Douven 2013, 2020，以及 Douven 和 Wenmackers 2017；有关讨论，请参见 Climenhaga 2017）。如果是这样，那么遵循那个规则而不是贝叶斯法则可能具有优势，这些优势也许不能用金钱来表达，但在决定遵循哪个规则时应该被考虑。简言之，遵循一个概率上不一致的规则是否必然是非理性的并不那么清楚。

另一方面，Douven（1999）认为，概率规则是否连贯这个问题不能独立于考虑与之一起使用的其他认识论和决策理论规则而解决；连贯性应该被理解为既包括认识论规则又包括决策理论规则的一套规则的属性，而不是孤立地认识论规则（如用于信念变化的概率规则）的属性。在同一篇论文中，描述了一套连贯的规则包，其中包括概率版本的溯因。（有关对范弗拉森批评概率版本溯因的不同回应，请参见 Kvanvig 1994，Harman 1997，Leplin 1997，Niiniluoto 1999 和 Okasha 2000。）

### 3.2 辩护

如今几乎没有人愿意认同一个将解释力量与真理之间存在必然联系的真理观——例如，因为它规定解释上的优越性对于真理是必要的。因此，溯因的先验辩护似乎是不可能的。事实上，迄今为止提出的所有辩护都是经验性质的，因为它们诉诸于据称支持这样一种主张的数据，即（以某种形式）溯因是一种可靠的推理规则。

最著名的这类论证是由理查德·博伊德在 1980 年代发展起来的（参见 Boyd 1981, 1984, 1985）。它首先强调科学方法论的理论依赖性，包括设计实验的方法、评估数据的方法、在竞争性假设之间进行选择的方法等。例如，在考虑实验设置必须防范的可能混杂因素时，科学家们在很大程度上依赖已经被接受的理论。接下来的论证引起了人们对这种方法论表面上的可靠性的关注，毕竟，这种方法论已经产生并继续产生令人印象深刻的准确理论。特别是，通过依赖这种方法论，科学家们目前已经能够找到越来越多仪器上充分的理论。博伊德随后认为，科学方法论的可靠性最好通过假设它所依赖的理论至少是大致正确来解释。基于这一点以及这些理论大多是通过溯因推理得出的事实，他得出结论认为，溯因必须是一种可靠的推理规则。

批评者指责这一论证是循环论证。具体来说，有人说这一论证建立在一个前提上——科学方法论受到大致正确的背景理论的指导，而这个前提又建立在对其可信度的最佳解释的推断上。而这种推断的可靠性正是问题所在。（参见，例如，Laudan 1981 和 Fine 1984。）

对此，Stathis Psillos（1999 年，第 4 章）做出了回应，引用了归功于 Richard Braithwaite 的区分，即前提循环和规则循环之间的区别。如果一个论证的结论在其前提中，那么这个论证就是前提循环的。相比之下，规则循环论证是一种其结论断言有关推理规则的论证，而这个推理规则正是在同一个论证中使用的。正如 Psillos 所强调的，Boyd 的论证是规则循环的，但不是前提循环的，而规则循环的论证，Psillos 认为_不一定_是恶性循环的（尽管前提循环的论证总是恶性循环的）。更准确地说，在他看来，一个关于某一给定规则_R_可靠性的论证，如果基本上依赖于_R_作为一个推理原则，那么并不是恶性的，只要使用_R_并不保证对_R_的可靠性得出积极的结论。Psillos 声称，在 Boyd 的论证中，这一限定得到满足。因为虽然 Boyd 得出结论说科学方法依赖的背景理论大致上是真实的，是基于一个溯因步骤，但溯因本身的使用并不能保证他的结论的真实性。毕竟，承认溯因的使用对确保科学方法成功的最佳解释是相关背景理论的大致真实性没有任何帮助。因此，Psillos 得出结论说，Boyd 的论证仍然站得住脚。

即使 Boyd 的论证中使用溯因可能导致结论溯因是_不_可靠的，人们仍然可能对论证是否规则循环存在疑虑。假设某个科学团体不依赖溯因，而是依赖一条我们可以称之为“推理至最糟解释”（IWE）的规则，这条规则允许推断出可用数据的_最糟_解释。我们可以有把握地假设，大多数情况下使用这条规则会导致采纳非常不成功的理论。然而，该团体可能会通过以下推理来证明其使用 IWE 的合理性：“科学理论往往非常不成功。这些理论是通过应用 IWE 得出的。认为 IWE 是一个可靠的推理规则——即，一个从真前提到真结论的推理规则——显然是解释我们的理论如此不成功的最糟解释。因此，通过应用 IWE，我们可以得出结论，IWE 是一个可靠的推理规则。”虽然这将是一个完全荒谬的结论，但导致这一结论的论证与 Boyd 为溯因的可靠性所做的论证一样，不能被指责为恶性循环（如果 Psillos 是正确的话）。因此，似乎规则循环必须存在其他问题。

这是公平的指出，对于 Psillos 而言，一个规则循环论证并不能保证对所讨论规则的积极结论，这并不足以使这样的论证成立。另一个必要条件是“人们不应该有理由怀疑规则的可靠性——当前没有任何可以让人怀疑这个规则的东西可用”（Psillos 1999, 85）。而对于溯因的可靠性有足够的理由值得怀疑；事实上，上述论证_假定_了它是不可靠的。然而，有两个问题出现了。首先，我们为什么要接受额外的条件？其次，我们真的_没有_理由怀疑溯因的可靠性吗？当然，我们做出的_一些_溯因推理导致我们接受_虚假_。在我们基于溯因接受多少虚假之前，我们可以正当地开始怀疑这个规则？对这些问题还没有给出明确的答案。

尽管规则循环性既不邪恶也不具有其他问题，人们仍然会想知道 Boyd 的论证如何说服溯因的批评者，因为它依赖于溯因。但是，Psillos 明确指出，哲学论证的目的并不总是，也不必总是说服对手接受自己的立场。有时，目的更为谦逊，是为了确保或使自己确信自己所支持的立场是正确的，或者自己很想支持这个立场。在这种情况下，我们不需要将 Boyd 的论证视为试图说服溯因的反对者其可靠性。相反，它可以被看作是从已经对溯因持同情态度的人的视角内部为这个规则辩护；参见 Psillos 1999 (89)。

有人尝试以更直接的方式来为 溯因 辩护，即通过枚举归纳。这些尝试的共同想法是，每一次新记录的 溯因 成功应用（如发现了海王星，其存在是基于解释性的理由假设的（见第 1.2 节））都进一步支持了这样一个假设： 溯因 是一条可靠的推理规则，就像每一次新观察到的黑乌鸦都在一定程度上支持了所有乌鸦都是黑色的假设。由于不涉及 溯因 推理，这种类型的论证更有可能吸引那些不相信 溯因 的人。请参阅 Harré 1986, 1988, Bird 1998 (160), Kitcher 2001 和 Douven 2002，了解沿着这些思路的建议。

## 4. 溯因 与贝叶斯证实理论

在过去的十年中，贝叶斯证实理论已经牢固地确立自己作为证实的主导观点；目前，讨论证实理论问题时很难不明确表明自己在这个问题上的立场是否偏离了标准的贝叶斯思维。无论是哪个版本的溯因，都将解释赋予了证实理论的角色：解释考虑有助于使一些假设更可信，而使另一些假设不那么可信。相比之下，贝叶斯证实理论根本不提及解释的概念。这是否意味着溯因与证实理论中的主流学说相冲突？一些作者最近提出论点，不仅溯因与贝叶斯主义是兼容的，而且它是一个迫切需要的补充。迄今为止，对这一观点的最充分的辩护是由利普顿（2004 年，第 7 章）提出的；正如他所说，贝叶斯主义者也应该是“解释主义者”（他对溯因的拥护者的称呼）。（有关其他辩护，请参见 Okasha 2000，McGrew 2003，Weisberg 2009 和 Poston 2014，第 7 章；有关讨论，请参见 Roche 和 Sober 2013，2014，以及 McCain 和 Poston 2014。）

这需要一些澄清。对于一个贝叶斯主义者来说，成为一个解释主义者意味着什么？为了应用贝叶斯规则并确定在学习_E_后_H_的概率，贝叶斯代理将不得不确定在_E_条件下_H_的概率。为此，他需要为_H_和_E_分配无条件概率以及给定_H_的_E_的概率；前两者大多被称为“先验概率”（或只是“先验”），后者是_H_在_E_上的“似然性”。（这是官方的贝叶斯故事。并非所有同情贝叶斯主义的人都遵循这个故事。例如，根据一些人的观点，认为条件概率是基本的，我们从中推导出无条件概率更为合理；请参见 Hájek 2003 及其中引用的文献。）贝叶斯如何确定这些值？众所周知，概率论在我们拥有一些概率后会给我们更多的概率；它不会从零开始给我们概率。当_H_蕴含_E_或_E_的否定时，或者当_H_是一个在_E_上赋予一定机会的统计假设时，那么似然性就会“分析地”得出。（这一说法假定了一些版本的 Lewis（1980）的主要原则，关于这个原则是否是分析的存在争议；因此有引号。）但情况并非总是如此，即使是这样，仍然存在如何确定先验的问题。这就是利普顿所说的溯因介入的地方。在他的建议中，贝叶斯主义者应该根据解释考虑来确定他们的先验概率和（如果适用）似然性。

确切地说，解释性考虑如何指导一个人选择先验概率？这个问题的答案并不像人们一开始想的那么简单。假设你正在考虑分配给一组竞争假设的先验概率，并且希望遵循利普顿的建议。你应该如何做呢？一个明显的答案可能看起来是这样的：无论你将分配什么样的确切先验概率，你应该将一个更高的先验概率分配给最能解释现有数据的假设，而不是分配给任何竞争对手（前提是有一个最佳解释）。然而，请注意，你的邻居是一个贝叶斯主义者，但他认为证实与解释无关，他可能会给最佳解释分配一个甚至比你分配给该假设的先验概率更高的先验概率。事实上，他对最佳解释的先验概率甚至可能一直比你的要高，不是因为在他看来解释与证实在某种程度上相关—他认为不相关—而是，嗯，就是因为。在这种情况下，“就是因为”是一个完全合理的理由，因为根据贝叶斯标准，任何确定先验概率的理由都被视为合法。根据主流贝叶斯认识论，先验概率（有时是似然）是可以随意确定的，这意味着只要两者都是连贯的（即它们遵守概率论的公理），那么一个先验概率的分配和另一个一样好。利普顿对贝叶斯主义者提出成为解释主义者的建议是完全一般性的。但是，如果你的邻居想要遵循这个建议，他应该做些什么不同呢？如果他想要给与你相同的最佳解释相同的先验概率，即，_降低_他对最佳解释的先验概率？还是应该给最佳解释比他已经给出的甚至更_高_的先验概率？

也许利普顿的建议并不是为了解决那些已经基于与解释无关的理由给出最高先验概率的人。这个想法可能是，只要一个人确实给出最高先验概率给那些假设，一切都好，或者至少比如果一个人不这样做要好，无论一个人分配这些先验的原因是什么。那么，解释性考虑如何指导一个人选择先验概率的问题的答案很可能是，如果这不是一个人已经在做的事情，那么一个人应该给最佳解释比其竞争对手更高的先验概率。如果已经这样做了，那么应该继续保持现状。

(另外，值得注意的是，根据标准的贝叶斯用法，“先验”一词并不一定指的是一个人在接收_任何_数据之前分配的信念程度。如果已经有数据了，那么很明显，可以给最能解释当时可获得的数据的假设分配更高的先验。然而，即使在任何数据被知晓之前，人们也可以明智地谈论“最佳解释”。例如，一个假设可能被认为比其竞争对手更好地解释了问题，因为前者需要更简单的数学，或者因为它仅以熟悉的概念陈述，而其他假设则不然。更一般地说，这种判断可能基于 Kosso（1992 年，30 页）所称的假设或理论的“内在特征”，即“无需观察世界即可评估的特征”。)

对于上述关于解释如何指导选择先验的问题，Jonathan Weisberg（2009 年）给出了一个更有趣的答案。我们说，主流贝叶斯派认为一组先验概率的分配与其他任何一组一样好。然而，所谓的客观贝叶斯派并不这样认为。这些贝叶斯派认为，为了被接受，先验必须遵守超出概率公理之外的原则。客观贝叶斯派在确定应遵守哪些进一步原则方面存在分歧，但至少有一段时间他们认为“无差别原则”是其中之一。粗略地说，这一原则建议，在没有相反理由的情况下，我们给予竞争假设相等的先验。然而众所周知，在其原始形式下，“无差别原则”可能导致概率的不一致分配，因此几乎不可能被宣传为一种理性原则。问题在于，通常有各种方式来划分逻辑空间，这些方式在给定手头问题时似乎都是合理的，并且并非所有方式都会导致相同的先验概率分配，即使假设“无差别原则”。Weisberg 的建议意味着解释考虑可能会支持某些划分而不是其他划分。也许我们不会总是得出一个唯一的划分来应用“无差别原则”，但如果最终只得出几个划分，这已经是进步了。因为我们可以通过两个步骤以有动机的方式得出我们的先验概率，即首先将“无差别原则”分别应用于各个划分，从而可能获得不同的先验分配，然后通过对所获得的先验进行加权平均，其中权重也应取决于解释考虑。结果将再次是一个概率函数——根据 Weisberg 的说法，这是唯一正确的先验概率函数。

提案在某种程度上令人感兴趣，但正如 Weisberg 所承认的那样，在其当前形式下，它并没有走得很远。首先，目前尚不清楚解释性考虑如何确定提案第二步所需的权重。另外，希望考虑解释性因素通常会使我们得到一组可管理的分区可能是徒劳的，即使这样做了，这也可能仅仅是因为我们忽视了在逻辑空间中划分的许多貌似合理的方式。（后一点当然呼应了坏一堆的论点。）

关于溯因和贝叶斯推理之间的联系的另一个建议——可以在 Okasha 2000 年，McGrew 2003 年，Lipton 2004 年（第 7 章）和 Dellsén 2018 年找到——即解释性考虑可能作为一种启发，用于确定在我们对某些情况一无所知且无法做得比猜测更好时的先验和似然，即使只是粗略地确定。这一建议对一个公认的事实敏感，即我们并不总是能够为每个感兴趣的假设分配一个先验，或者说在给定假设的情况下某个证据有多大可能性。考虑该假设的解释力量可能有助于我们弄清楚，即使只是在一定范围内，为其分配什么先验，或者根据给定证据为其分配什么似然。

Bayesians，特别是那些更谦逊的人，可能会反驳说，如果且仅当（a）可以确定先验和似然具有一定的精确性和客观性，或者（b）可以确定似然具有一定的精确性并且可以预期先验会随着越来越多的证据累积而“消失”，或者（c）可以预期先验和似然都会消失时，应该遵循贝叶斯程序。他们可能会说，在其余情况下，我们应该简单地避免应用贝叶斯推理。因此，在这些情况下，就无需在这些情况下需要溯因增强的贝叶斯主义。一些不容置疑的数学结果表明，在属于（a）、（b）或（c）的情况下，我们的概率最终会收敛于真相。因此，在这些情况下，也不需要上述作者建议的那种溯因启发式方法。（Weisberg 2009，第 3.2 节，提出了类似的担忧。）

Psillos（2000）提出了溯因可能如何补充贝叶斯证实理论的另一种方式，这种方式非常符合皮尔斯对溯因概念的理解。这个想法是，溯因可能帮助我们选择可供测试的合理候选人，而实际测试则要遵循贝叶斯的方法。然而，Psillos 承认（2004）这一提议赋予了溯因一个对于致力于解释的人来说太过有限的角色。

最后，文献中迄今尚未考虑的一种可能性是，溯因和贝叶斯主义并不像上述提议中那样协同工作，而是以不同的推理方式运作；贝叶斯主义者和解释主义者可以说是出现在不同剧本中的角色。人们普遍认为，有时我们以绝对的方式言说和思考自己的信念，而其他时候则以渐进的方式言说和思考。目前尚不清楚这些不同的信念言说和思考方式——信念的认识论和信念程度的认识论，用理查德·福利（1992）的术语来说——彼此之间如何相关。事实上，目前尚不清楚这两者之间是否有任何直接联系，甚至是否根本就不存在联系。不管怎样，鉴于这种区别是不可否认的，一个合理的建议是，正如有不同的谈论和思考信念方式一样，也有不同的谈论和思考信念_修正_方式。特别是，溯因很可能属于信念的认识论，并且每当我们以绝对方式推理我们的信念时就会被调用，同时贝叶斯规则可能属于信念程度的认识论。铁杆的贝叶斯主义者可能会坚持认为，无论以绝对方式进行的任何推理最终都必须以贝叶斯术语来证明，但这假定了连接信念的认识论与信念程度的认识论的桥梁原则的存在——正如前面提到的，目前尚不清楚这样的原则是否存在。

## Bibliography

* Achinstein, P., 2001. _The Book of Evidence_, Oxford: Oxford University Press.
* Adler, J., 1994. “Testimony, Trust, Knowing,” _Journal of Philosophy_, 91: 264–275.
* Bach, K. and Harnish, R., 1979. _Linguistic Communication and Speech Acts_, Cambridge MA: MIT Press.
* Bird, A., 1998. _Philosophy of Science_, London: UCL Press.
* Bigelow, J., 2010. “Quine, Mereology, and Inference to the Best Explanation,” _Logique et Analyse_, 212: 465–482.
* Bovens, L. and Hartmann, S., 2003. “Solving the Riddle of Coherence,” _Mind_, 112: 601–633.
* Boyd, R., 1981. “Scientific Realism and Naturalistic Epistemology,” in P. Asquith and R. Giere (eds.), _PSA 1980_, (vol. II), East Lansing MI: Philosophy of Science Association, pp. 613–662.
* –––, 1984. “The Current Status of Scientific Realism,” in J. Leplin (ed.), _Scientific Realism_, Berkeley CA: University of California Press, pp. 41–82.
* –––, 1985. “Lex Orandi est Lex Credendi,” in P. Churchland and C. Hooker (eds.), _Images of Science_, Chicago IL: University of Chicago Press, pp. 3–34.
* Brem, S. and Rips, L. J., 2000. “Explanation and Evidence in Informal Argument,” _Cognitive Science_, 24: 573–604.
* Callebaut, W. (ed.), 1993. _Taking the Naturalistic Turn_, Chicago IL: University of Chicago Press.
* Campos, D., 2011. “On the Distinction Between Peirce’s Abduction and Lipton’s Inference to the Best Explanation,” _Synthese_, 180: 419–442.
* Climenhaga, N., forthcoming. “Inference to the Best Explanation Made Incoherent,” _Journal of Philosophy_, [preprint available online](http://philsci-archive.pitt.edu/12756/1/IBE%20Made%20Incoherent.pdf).
* Dascal, M., 1979. “Conversational Relevance,” in A. Margalit (ed.), _Meaning and Use_, Dordrecht: Reidel, pp. 153–174.
* Dellsén, F., 2017. “Reactionary Responses to the Bad Lot Objection,” _Studies in History and Philosophy of Science_, 61: 32–40.
* –––, 2018. “The Heuristic Conception of Inference to the Best Explanation,” _Philosophical Studies_, 175: 1745–1766.
* Douven, I., 1999. “Inference to the Best Explanation Made Coherent,” _Philosophy_, _of Science_, 66: S424–S435.
* –––, 2002. “Testing Inference to the Best Explanation,” _Synthese_, 130: 355–377.
* –––, 2008. “Underdetermination,” in S. Psillos and M. Curd (eds.), _The Routledge Companion to the Philosophy of Science_, London: Routledge, pp. 292–301.
* –––, 2013. “Inference to the Best Explanation, Dutch Books, and Inaccuracy Minimisation,” _Philosophical Quarterly_, 63: 428–444.
* –––, 2016a. _The Epistemology of Indicative Conditionals_, Cambridge: Cambridge University Press.
* –––, 2016b. “Explanation, Updating, and Accuracy,” _Journal of Cognitive Psychology_, 28: 1004–1012.
* –––, 2017. “What Is Inference to the Best Explanation? And Why Should We Care?” in T. Poston and K. McCain (eds.), _Best Explanations: New Essays on Inference to the Best Explanation_, Oxford: Oxford University Press, pp. 4–22.
* –––, 2020. “The Ecological Rationality of Explanatory Reasoning,” _Studies in History and Philosophy of Science_, 79: 1–14.
* –––, forthcoming. _The Art of Abduction_, Cambridge MA: MIT Press.
* Douven, I. and Mirabile, P., 2018. “Best, Second-best, and Good-enough Explanations: How They Matter to Reasoning,” _Journal of Experimental Psychology: Language, Memory, and Cognition_, 44: 1792–1813.
* Douven, I. and Schupbach, J., 2015a. “The Role of Explanatory Considerations in Updating,” _Cognition_, 142: 299–311.
* –––, 2015b. “Probabilistic Alternatives to Bayesianism: The Case of Explanationism,” _Frontiers in Psychology_, 6: 459. doi:10.3389/fpsyg.2015.00459
* Douven, I. and Wenmackers, S., 2017. “Inference to the Best Explanation versus Bayes’s Rule in a Social Setting,” _British Journal for the Philosophy of Science_, 68: 535–570.
* Dragulinescu, S., 2016. “Inference to the Best Explanation and Mechanisms in Medicine,” _Theoretical Medicine and Bioethics_, 37(3): 211–232.
* Fann, K. T., 1970. _Peirce’s Theory of Abduction_, The Hague: Martinus Nijhoff.
* Fine, A., 1984. “The Natural Ontological Attitude,” in J. Leplin (ed.), _Scientific Realism_, Berkeley CA: University of California Press, pp. 83–107.
* Foley, R., 1992. “The Epistemology of Belief and the Epistemology of Degrees of Belief,” _American Philosophical Quarterly_, 29: 111–124.
* Folina, J., 2016. “Realism, Skepticism, and the Brain in a Vat,” in S. Goldberg (ed.), _The Brain in a Vat_, Cambridge: Cambridge University Press, pp. 155–173.
* Forster, M. and Sober, E., 1994. “How to Tell when Simpler, More Unified, or Less _Ad Hoc_, Theories will Provide More Accurate Predictions,” _British Journal for the Philosophy of Science_, 45: 1–36.
* Frankfurt, H., 1958. “Peirce’s Notion of Abduction,” _Journal of Philosophy_, 55: 593–596.
* Fricker, E., 1994. “Against Gullibility,” in B. K. Matilal and A. Chakrabarti (eds.), _Knowing from Words_, Dordrecht: Kluwer, pp. 125–161.
* Goldman, A., 1988. _Empirical Knowledge_, Berkeley CA: University of California Press.
* Hájek, A., 2003. “What Conditional Probability Could Not Be,” _Synthese_, 137: 273–323.
* Harman, G., 1965. “The Inference to the Best Explanation,” _Philosophical Review_, 74: 88–95.
* –––, 1973. _Thought_, Princeton NJ: Princeton University Press.
* –––, 1997. “Pragmatism and Reasons for Belief,” in C. Kulp (ed.), _Realism/Antirealism and Epistemology_, Totowa NJ: Rowman and Littlefield, pp. 123–147.
* Harré, R., 1986. _Varieties of Realism_, Oxford: Blackwell.
* –––, 1988. “Realism and Ontology,” _Philosophia Naturalis_, 25: 386–398.
* Hobbs, J. R., 2004. “Abduction in Natural Language Understanding,” in L. Horn and G. Ward (eds.), _The Handbook of Pragmatics_, Oxford: Blackwell, pp. 724–741.
* Janssen, M., 2002. “Reconsidering a Scientific Revolution: The Case of Einstein _versus_, Lorentz,” _Physics in Perspective_, 4: 421–446.
* Josephson, J. R. and Josephson, S. G. (eds.), 1994. _Abductive Inference_, Cambridge: Cambridge University Press.
* Kitcher, P., 2001. “Real Realism: The Galilean Strategy,” _Philosophical Review_, 110: 151–197.
* Koehler, D. J., 1991. “Explanation, Imagination, and Confidence in Judgment,” _Psychological Bulletin_, 110: 499–519.
* Koslowski, B., Marasia, J., Chelenza, M., and Dublin, R., 2008. “Information Becomes Evidence when an Explanation Can Incorporate it into a Causal Framework,” _Cognitive Development_, 23: 472–487.
* Kosso, P., 1992. _Reading the Book of Nature_, Cambridge: Cambridge University Press.
* Krzyżanowska, K, Wenmackers, S., and Douven, I., 2014. “Rethinking Gibbard’s Riverboat Argument,” _Studia Logica_, 102: 771–792.
* Kuipers, T., 1984. “Approaching the Truth with the Rule of Success,” _Philosophia_, _Naturalis_, 21: 244–253.
* –––, 1992. “Naive and Refined Truth Approximation,” _Synthese_, 93: 299–341.
* –––, 2000. _From Instrumentalism to Constructive Realism_, Dordrecht: Kluwer.
* Kvanvig, J., 1994. “A Critique of van Fraassen’s Voluntaristic Epistemology,” _Synthese_, 98: 325–348.
* Kyburg Jr., H., 1990. _Science and Reason_, Oxford: Oxford University Press.
* Laudan, L., 1981. “A Confutation of Convergent Realism,” _Philosophy of Science_, 48: 19–49.
* Lewis, D., 1980. “A Subjectivist’s Guide to Objective Chance,” in R. Jeffrey (ed.), _Studies in Inductive Logic and Probability_, Berkeley CA: University of California Press, pp. 263–293.
* Li, M. and Vitanyi, P., 1997. _An Introduction to Kolmogorov Complexity and its Applications_, New York: Springer.
* Lipton, P., 1991. _Inference to the Best Explanation_, London: Routledge.
* –––, 1993. “Is the Best Good Enough?” _Proceedings of the Aristotelian Society_, 93: 89–104.
* –––, 1998. “The Epistemology of Testimony,” _Studies in History and Philosophy of Science_, 29: 1–31.
* –––, 2004. _Inference to the Best Explanation_, (2nd ed.), London: Routledge.
* Lombrozo, T., 2007. “Simplicity and Probability in Causal Explanation,” _Cognitive Psychology_, 55: 232–257.
* –––, 2012. “Explanation and Abductive Inference,” in K. Holyoak and R. Morrison (eds.), _Oxford Handbook of Thinking and Reasoning_, Oxford: Oxford University Press, pp. 260–276.
* –––, 2016. “Explanatory Preferences Shape Learning and Inference,” _Trends in Cognitive Sciences_, 20: 748–759.
* Lombrozo, T. and Gwynne, N. Z., 2014. “Explanation and Inference: Mechanistic and Functional Explanations Guide Property Generalization,” _Frontiers in Human Neuroscience_, 8. doi:10.3389/fnhum.2014.00700
* Maher, P., 1992. “Diachronic Rationality,” _Philosophy of Science_, 59: 120–141.
* McAuliffe, W., 2015. “How Did Abduction Get Confused with Inference to the Best Explanation?” _Transactions of the Charles S. Peirce Society_, 51: 300–319.
* McCain, K. and Poston, T., 2014. “Why Explanatoriness is Evidentially Relevant,” _Thought_, 3: 145–153.
* McGrew, T., 2003. “Confirmation, Heuristics, and Explanatory Reasoning,” _British Journal for the Philosophy of Science_, 54: 553–567.
* McMullin, E., 1992. _The Inference that Makes Science_, Milwaukee WI: Marquette University Press.
* –––, 1996. “Epistemic Virtue and Theory Appraisal,” in I. Douven and L. Horsten (eds.), _Realism in the Sciences_, Leuven: Leuven University Press, pp. 13–34.
* Moore, G. E., 1962. “Proof of an External World,” in his _Philosophical Papers_, New York: Collier Books, pp. 126–149.
* Moser, P., 1989. _Knowledge and Evidence_, Cambridge: Cambridge University Press.
* Musgrave, A., 1988. “The Ultimate Argument for Scientific Realism,” in R. Nola (ed.), _Relativism and Realism in Science_, Dordrecht: Kluwer, pp. 229–252.
* Niiniluoto, I., 1998. “Verisimilitude: The Third Period,” _British Journal for the Philosophy of Science_, 49: 1–29.
* –––, 1999. “Defending Abduction,” _Philosophy of Science_, 66: S436–S451.
* Okasha, S., 2000. “Van Fraassen’s Critique of Inference to the Best Explanation,” _Studies in History and Philosophy of Science_, 31: 691–710.
* Olsson, E., 2005. _Against Coherence_, Oxford: Oxford University Press.
* Pargetter, R., 1984. “The Scientific Inference to Other Minds,” _Australasian Journal of Philosophy_, 62: 158–163.
* Peirce, C. S. \[**CP**]. _Collected Papers of Charles Sanders Peirce_, edited by C. Hartshorne, P. Weiss, and A. Burks, 1931–1958, Cambridge MA: Harvard University Press.
* Poston, T., 2014. _Reason and Explanation_, Basingstoke: Palgrave Macmillan.
* Psillos, S., 1999. _Scientific Realism: How Science Tracks Truth_, London: Routledge.
* –––, 2000. “Abduction: Between Conceptual Richness and Computational Complexity,” in A. K. Kakas and P. Flach (eds.), _Abduction and Induction: Essays on their Relation and Integration_, Dordrecht: Kluwer, pp. 59–74.
* –––, 2004. “Inference to the Best Explanation and Bayesianism,” in F. Stadler (ed.), _Induction and Deduction in the Sciences_, Dordrecht: Kluwer, pp. 83–91.
* Putnam, H., 1981. _Reason, Truth and History_, Cambridge: Cambridge University Press.
* Roche, W. and Sober, E., 2013. “Explanatoriness is Evidentially Irrelevant, or Inference to the Best Explanation Meets Bayesian Confirmation Theory,” _Analysis_, 73: 659–668
* –––, 2014. “Explanatoriness and Evidence: A Reply to McCain and Poston,” _Thought_, 3: 193–199.
* Russell, B., 1912. _The Problems of Philosophy_, Oxford: Oxford University Press.
* Schupbach, J., 2014. “Is the Bad Lot Objection Just Misguided?” _Erkenntnis_, 79: 55–64.
* Schupbach, J. and Sprenger, J., 2011. “The Logic of Explanatory Power,” _Philosophy of Science_, 78: 105–127.
* Schurz, G., 2008. “Patterns of Abduction,” _Synthese_, 164: 201–234.
* Shalkowski, S., 2010. “IBE, GMR, and Metaphysical Projects,” in B. Hale and A. Hoffmann (eds.), _Modality: Metaphysics, Logic, and Epistemology_, Oxford: Oxford University Press, pp. 169–187.
* Skyrms, B., 1993. “A Mistake in Dynamic Coherence Arguments?” _Philosophy of Science_, 60: 320–328.
* Sloman, S., 1994. “When Explanations Compete: The Role of Explanatory Coherence on Judgments of Likelihood,” _Cognition_, 52: 1–21.
* Sober, E., 2015. _Ockham’s Razor: A User’s Manual_, Cambridge: Cambridge University Press.
* Stanford, K., 2009. “Underdetermination of Scientific Theory,” in _Stanford Encyclopedia of Philosophy_ (Winter 2009 Edition), Edward N. Zalta (ed.), URL = <[Underdetermination of Scientific Theory (Stanford Encyclopedia of Philosophy/Winter 2009 Edition)](https://plato.stanford.edu/archives/win2009/entries/scientific-underdetermination/)>.
* Teller, P., 1973. “Conditionalization and Observation,” _Synthese_, 26: 218–258.
* Thagard, P., 1978. “The Best Explanation: Criteria for Theory Choice,” _Journal of Philosophy_, 75: 76–92.
* van Fraassen, B., 1980. _The Scientific Image_, Oxford: Oxford University Press.
* –––, 1983. “Glymour on Evidence and Explanation,” in J. Earman (ed.), _Testing Scientific Theories_, Minneapolis: University of Minnesota Press, pp. 165–176.
* –––, 1985. “Empiricism in the Philosophy of Science,” in P. Churchland and C. Hooker (eds.), _Images of Science_, Chicago IL: University of Chicago Press, pp. 245–308.
* –––, 1989. _Laws and Symmetry_, Oxford: Oxford University Press.
* Vogel, J., 1990. “Cartesian Skepticism and Inference to the Best Explanation,” _Journal of Philosophy_, 87: 658–666.
* –––, 2005. “The Refutation of Skepticism,” in M. Steup and E. Sosa (eds.), _Contemporary Debates in Epistemology_, Oxford: Blackwell Publishing, pp. 72–84.
* Weintraub, R., 2013. “Induction and Inference to the Best Explanation,” _Philosophical Studies_, 166: 203–216.
* Weisberg, J., 2009. “Locating IBE in the Bayesian Framework,” _Synthese_, 167: 125–143.
* Williams, J. and Lombrozo, T., 2010. “The Role of Explanation in Discovery and Generalization: Evidence from Category Learning,” _Cognitive Science_, 34: 776–806.
* Williamson, T., 2017. “Semantic Paradoxes and Abductive Methodology,” in B. Armour-Garb (ed.), _Reflections on the Liar_, Oxford: Oxford University Press, pp. 325–346.

## Academic Tools

| ![sep man icon](https://plato.stanford.edu/symbols/sepman-icon.jpg) | [How to cite this entry](https://plato.stanford.edu/cgi-bin/encyclopedia/archinfo.cgi?entry=abduction).                                                                      |
| ------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ![sep man icon](https://plato.stanford.edu/symbols/sepman-icon.jpg) | [Preview the PDF version of this entry](https://leibniz.stanford.edu/friends/preview/abduction/) at the [Friends of the SEP Society](https://leibniz.stanford.edu/friends/). |
| ![inpho icon](https://plato.stanford.edu/symbols/inpho.png)         | [Look up topics and thinkers related to this entry](https://www.inphoproject.org/entity?sep=abduction\&redirect=True) at the Internet Philosophy Ontology Project (InPhO).   |
| ![phil papers icon](https://plato.stanford.edu/symbols/pp.gif)      | [Enhanced bibliography for this entry](https://philpapers.org/sep/abduction/) at [PhilPapers](https://philpapers.org/), with links to its database.                          |

## Other Internet Resources

\[Please contact the author with suggestions.]

## Related Entries

[epistemology: Bayesian](https://plato.stanford.edu/entries/epistemology-bayesian/) | [induction: problem of](https://plato.stanford.edu/entries/induction-problem/) | [Peirce, Charles Sanders](https://plato.stanford.edu/entries/peirce/) | [scientific explanation](https://plato.stanford.edu/entries/scientific-explanation/) | [scientific realism](https://plato.stanford.edu/entries/scientific-realism/) | [simplicity](https://plato.stanford.edu/entries/simplicity/) | [skepticism](https://plato.stanford.edu/entries/skepticism/) | [underdetermination, of scientific theories](https://plato.stanford.edu/entries/scientific-underdetermination/)

[Copyright © 2021](https://plato.stanford.edu/info.html#c) by\
Igor Douven <[_igor.douven@paris-sorbonne.fr_](mailto:igor%2edouven%40paris-sorbonne%2efr)>
