# 阿里·法拉比的逻辑与语言哲学 philosophy of logic and language (Wilfrid Hodges and Therese-Anne Druart)

*首次发表于 2019 年 4 月 16 日星期二；实质性修订于 2023 年 5 月 9 日星期二*

阿里·法拉比（约公元 870 年至公元 950 年，伊拉克）致力于向伊斯兰帝国受教育的阿拉伯语人民介绍亚里士多德的作品。他的一些重要著作全部或部分已经失传。但他的许多解释亚里士多德《逻辑学篇》（亚里士多德关于逻辑和相关主题的著作集）的书籍幸存下来，西方翻译本的数量也在稳步增加。有关阿里·法拉比的一般信息，请参阅阿尔法拉比的条目。

法拉比研究语言在人类生活和社会中的各种作用。他强调语言用于传达信息、提出问题和解决分歧，以及描述区别和分类。他认为语言在某种意义上复制意义，并且需要避免语言与意义之间的不匹配。他将亚里士多德的逻辑呈现为一套方法，用于行使说服力、调节辩论、发现真理和获得确定性。他还探讨了逻辑在诗歌中的应用。在这一过程中，他对诸如形而上学问题的来源、事件的时间结构以及诗歌与音乐之间的关系等问题做出了许多深刻的观察。

* [1. 阿里·法拉比的著作及其背景](https://plato.stanford.edu/entries/al-farabi-logic/#AlFrbsWritTheiBack)
* [2. 语言的起源](https://plato.stanford.edu/entries/al-farabi-logic/#OrigLang)
* [3. 推理艺术的起源](https://plato.stanford.edu/entries/al-farabi-logic/#OrigSyllArts)
* [4. 请求与问题](https://plato.stanford.edu/entries/al-farabi-logic/#RequQues)

  * [4.1 辩论中的问题](https://plato.stanford.edu/entries/al-farabi-logic/#QuesDeba)
  * [4.2 寻求信息的问题](https://plato.stanford.edu/entries/al-farabi-logic/#QuesSeekInfo)
* [5. 逻辑的定义](https://plato.stanford.edu/entries/al-farabi-logic/#DefiLogi)
* [6. 推理工具](https://plato.stanford.edu/entries/al-farabi-logic/#ToolForInfe)

  * [ 6.1 分类逻辑](https://plato.stanford.edu/entries/al-farabi-logic/#CateLogi)
  * [ 6.2 假设逻辑](https://plato.stanford.edu/entries/al-farabi-logic/#HypoLogi)
  * [ 6.3 模态逻辑](https://plato.stanford.edu/entries/al-farabi-logic/#ModaLogi)
  * [ 6.4 演示逻辑](https://plato.stanford.edu/entries/al-farabi-logic/#DemoLogi)
* [ 7. 逻辑推论](https://plato.stanford.edu/entries/al-farabi-logic/#LogiCons)
* [8. 真理与虚假](https://plato.stanford.edu/entries/al-farabi-logic/#TrutFals)
* [9. 阿拉伯语的基础](https://plato.stanford.edu/entries/al-farabi-logic/#FounArab)
* [10. 诗歌与音乐](https://plato.stanford.edu/entries/al-farabi-logic/#PoetMusi)
* [ 参考文献](https://plato.stanford.edu/entries/al-farabi-logic/#Bib)

  * [ 阿里·法拉比著作](https://plato.stanford.edu/entries/al-farabi-logic/#WorkAlFrb)
  * [其他作者的作品](https://plato.stanford.edu/entries/al-farabi-logic/#WorkOtheAuth)
* [ 学术工具](https://plato.stanford.edu/entries/al-farabi-logic/#Aca)
* [其他互联网资源](https://plato.stanford.edu/entries/al-farabi-logic/#Oth)
* [ 相关条目](https://plato.stanford.edu/entries/al-farabi-logic/#Rel)

---

## 1. 阿里·法拉比的著作及其背景

阿里·法拉比的存世著作超过一千页，致力于解释亚里士多德的逻辑著作内容，面向阿拉伯语读者。其中大部分材料显示出阿里·法拉比对语言本质的浓厚兴趣。

阿里·法拉比的思想背景并不是亚里士多德本人，而是在埃及亚历山大兴盛的新柏拉图学派哲学课程中的亚里士多德部分（见 D’Ancona 2022）。阿里·法拉比告诉我们，最终“教学从亚历山大迁至安条克”（在叙利亚），直到学派的最后成员四散“带着他们的书”。他说他自己曾与其中一位最后的成员尤哈纳·本·海兰一起学习，他们一起阅读亚里士多德直到《后分析篇》的结尾。他补充说，亚历山大学派开始将亚里士多德《逻辑学篇》中自模态逻辑开始的部分描述为“不被阅读的部分”，因为在基督徒统治下，模态材料一直被隐藏起来。（阿里·法拉比的记述在历史学家伊本·阿比·乌赛比‘a 引用的一部失传作品中，见 Fakhry 2002: 159 的翻译。）

阿里·法拉比对历史进行了过于简化（见 Lameer 1997 和 Watt 2008）。但他正确地暗示了他对亚里士多德的理解是基于亚历山大的课程及其在中东的后续发展。例如，他在其具有影响力的《目录》中对逻辑的描述已被证明是对亚历山大教材中一部作品的编辑翻译。因此，要理解阿里·法拉比，了解他何时表达自己的深思熟虑观点，何时仅仅传递亚历山大课程的部分内容变得重要。目前尚无关于这一点的共识，也没有关于他写作作品的顺序的一致看法。

亚历山大的新柏拉图主义者将哲学视为通过对“一”之知识实现幸福的途径（见 Wildberg 2021）。为了增加对这种神秘努力的智力严谨性，他们在教学大纲的早期阶段包括了亚里士多德的逻辑著作。他们设计了一系列入门文本，以解释为什么亚里士多德的逻辑可以拯救我们免受错误思维和行为的影响，以及亚里士多德为什么以他所给出的形式设计了他的逻辑大纲——他的《逻辑学》。阿里·法拉比的演讲中的一些特点最容易通过亚历山大人来解释。例如，他们的教学大纲中的亚里士多德部分，在波菲里的《导论》之后，从亚里士多德的《范畴学》开始，因此阿里·法拉比恰当地寻求理由，解释为什么范畴是逻辑中的一个基本第一步。他们的《逻辑学》还包括亚里士多德的《修辞学》和《诗学》；因此，阿里·法拉比解释了修辞和诗歌艺术实际上是逻辑的一部分的原因。如果他们的《逻辑学》中包含有关音乐的书籍，阿里·法拉比将解释为什么音乐理论是逻辑的一个分支。（有关音乐，请参见下面的第 10 节。）

阿里·法拉比的逻辑著作采用各种形式。其中一些是对亚里士多德著作的长篇评论，详细解剖亚里士多德的文本；尽管我们有其他部分，但只有《解释学》的长篇评论完整保存下来。更友好的是摘要，阿里·法拉比在其中以自己的观点重新撰写《逻辑学》中的著作。还有一些文本作为《逻辑学》的导论，尽管它们不一定是基础性的。我们特别关注这最后一组中的三部作品。第一部作品《指示》教导逻辑是正确区分的艺术，因此是整个生活幸福的关键。它还解释了我们必须在学习逻辑之前学习语言。第二部作品《表达》是《指示》的续篇，介绍了逻辑的词汇。

第三部作品是极具原创性和洞察力的《信函》，它将语言、逻辑和形而上学巧妙地编织在一起。我们将以这部作品作为理解阿里·法拉比对语言和逻辑整体观点的最佳基础。下面的第 2 至第 5 节将回顾我们认为是这一观点的核心。剩下的第 6 至第 10 节将探讨特定主题。

除了亚历山大材料外，阿里·法拉比还能够利用高度专业的阿拉伯语《Organon》书籍的翻译。他还有一些来自罗马帝国的逻辑作家的翻译，比如亚历山大·阿弗罗迪西亚斯和塞米斯提乌斯。据悉，他并未利用早期与逻辑相关的阿拉伯文献，比如伊本·穆卡法的《逻辑》和阿尔·坎迪的著作。 （Zimmermann [1981: lxviii–xcviii] 和 Lameer [1994: 第 1 章] 讨论了阿里·法拉比可以获得的早期逻辑著作。另请参阅 Walzer [1962: 129–136]，了解阿里·法拉比关于亚里士多德诗学的来源。）

阿里·法拉比（Al-Fārābī）在逝世后的两百年间直接影响了几位阿拉伯作家的逻辑和语言研究。这些作家包括伊本·巴杰（拉丁文中称为阿韦姆帕斯），伊本·西那（阿维森纳），阿尔-加扎里（阿尔加泽利），以及伊本·鲁什德（阿维罗艾斯）。他还影响了一些中世纪犹太作家，包括梅蒙尼德斯。拉丁人称他为 Alfarabius、Alpharabius 或 Abunaser。他的影响可以在学术界对决定论、诗意论证以及科学分类的处理中看到。

## 2. 语言的起源

阿里·法拉比描述了语言在社区中的发展。在[信件]（114, 115）134.16–135.14 中，他描绘了一个社区，其成员具有概念（ma‘qūlāt，字面意思是“智力事物”，或者在学术术语中是“可理解的”），使他们能够在外部世界中单独或作为某种事物挑选出可识别的对象。他将从外部世界感知中获得的这些概念称为“初级概念”。这些人有信念，他们能够推理世界，思考他们的行动，并通过指点进行交流。但是到目前为止，这些人还没有语言。

语言发展的第一步是人们意识到，如果他们同意用声音标记一些对象和概念，他们可以更好地彼此沟通需求。这建立了简单（即未复合）初级概念和词语之间的约定性关联，最终这种关联被整个社区接受。据说这些词语“表示”与其相关的概念。阿里·法拉比设想一个立法者将调节这种关联并为社区的利益增加一些词语（[信件]（120）138.4–8）。因此，形成了一个国家词汇表；使用的声音形成了一个国家字母表。

一旦确立了词语表示意义的观念，社区将致力于发展语言，使词语模仿主要概念的“排序”或“制度”（intiẓām）（[信件]（122）139.2–4）。这种制度包括概念作为概念所具有的任何关系。例如，两个概念可能相似；因此，会努力寻找一对相似的词语来表达这些概念。阿里·法拉比指出，这种努力可能会失败，导致同音异义词的产生——一个词用于表示两个不同的概念（[信件]（124）140.8–10）。模仿相似性的压力也导致了隐喻的产生（[信件]（127）141.10）。

此外，有些概念是从其他概念派生出来的；派生概念的词语应该在语法上派生自另一个概念的词语。未经派生的概念的词语不应该在语法上派生。阿里·法拉比指出，在实践中，这种关联有时会失败。例如，他认为分词“活着的”（ḥayy）是派生的，但有时被用来表示未经派生的概念“动物”（[信件]（26）74.16，（36）81.18f）。同样，未经派生的概念“存在”有时被派生的被动分词 mawjūd 表示（[信件]（84）113.9）。

概念也可以结合成复合概念，这种可能性也会在语言中得到复制。 （[信件]（126）140.20–141.3。）从这个角度来看，阿里·法拉比做了许多包含组合性论题首次明确表述的评论。

阿里·法拉比明显认为词语和概念之间的不匹配是一件坏事，但他为什么这样认为，以及他认为应该采取什么措施，这就不那么清楚了。最初的匹配是由社区的一个不加思考的共识建立的，可能是因为它起到了某种社会目的。阿里·法拉比从未详细分析这个目的可能是什么，或者例如，使用派生词来表示未派生的概念会如何受到损害。他是否真的担心不匹配会导致沟通的崩溃，即使整个社区都采用了这种不匹配？格尔曼（2015/6：138f）提出，他的担忧更具体：他担心如果出现不匹配，教师——包括哲学教师——可能无法可靠地向他们的学生传递信息。

另一种可能性是，他害怕不匹配会导致逻辑推理中的错误，因为构成有效论证的概念之间的关系在相应的词语中不可见。在[表达]102.8-15 中，在告诉我们演绎和三段论发生在内部言语而非外部之后，他评论道：

> 大多数学生无法想象概念在头脑中的排序方式，因此人们会采用表示这些概念的表达，以便让学生的头脑能够从这些表达过渡到概念。

任何概念和词语之间的不匹配都可能干扰这一过程。

无论如何，阿里·法拉比并不急于改革语言。他满足于继续使用“存在”这个词。

阿里·法拉比 从未建议语言应该复制的主要概念体系的一部分。有些概念比其他概念更具包容性；例如，“动物”比“人类”更具包容性。根据亚里士多德的一种解读，阿里·法拉比认为存在十个最具包容性的简单主要概念（忽略了“事物”、“概念”、“一”和“存在”这些包含所有概念的概念）。他将这十个概念称为“范畴”（maqūlāt，不要与上面的 ma‘qūlāt 混淆）。但我们更倾向于称它们为其另一个名称“至上种属”，因为阿里·法拉比混淆地将“范畴”这个名称也应用于所有简单主要概念。语言确实区分了最小包容性的概念，即仅适用于一个事物的概念，通过为它们使用专有名称。但期望语言复制这种包容性体系的其余部分是不现实的，我们也不知道阿里·法拉比曾建议过这样做的地方。

上述提到的一些历史过程将导致人们思考主要概念及其体系。阿里·法拉比指出，结果人们现在将拥有对概念进行分类的概念，而不是对外部世界中可识别对象的概念。因此，这些是一种新型概念，阿里·法拉比将其称为“次级概念”（通常翻译为“第二智性”）。思考次级概念将使我们得到“第三级概念”，以此类推无穷。阿里·法拉比提到的次级概念的例子包括“种属”、“物种”、“更/少包容性”、“已知”、“概念”。（所有这些都在[信件]（7,8）64.9–65.8 中。）

尽管阿里·法拉比并未推荐任何特定的语言改革，但他确实为那些试图从其他语言翻译哲学并需要引入专业术语的哲学家提出了一些详细的建议。《信件》（155）157.19–158.21。如果我们能确定哪些用于翻译希腊作家的专业术语应归功于他，将比较阿里·法拉比的建议与他自己的实践将会很有趣。

他确实为自己开创的概念引入了一些新名称。三个值得注意的例子是：

1. 他将 yufīdu“提供”引入为一个艺术术语，用于表达在哲学辩论中提出问题的答案“提供”信息（参见下文第 4 节）。除了这种用法，这个词在他的著作中很少见，而在语言学作家中以相关意义使用这个词可能早于他的使用（参见 Giolfo & Hodges 2018）。
2. 他从语言学家那里借用 istithnā’（“例外”）这个术语，作为他假设逻辑推理规则的名称（参见下文第 6 节）。这个选择基于这些规则与被称为“例外”的句法结构性质之间的形式上的相似性。
3. 他选择“次要概念”这个名称是基于与波菲里的“第二次赋予”理论类似的比喻，即用于谈论词语的词语。

## 3. 推理艺术的起源

阿里·法拉比关于语言起源的故事延伸至关于语言如何使得以新方式处理概念成为可能的故事。对于阿里·法拉比来说，语言和概念之间的一个关键区别在于语言是公共的，允许人们之间进行交流。因此，许多概念的新用途与社会互动有关。在每种情况下，他都对处理概念的新技能的发展感兴趣。技能涉及处理这些工具的仪器和规则；因此，新技能的出现涉及接受处理其仪器的规则（qawānīn，单数 qānūn）。一组相关技能及其规则被称为“艺术”（ṣinā‘a）。

他首先提到的新艺术是修辞学（khiṭāba），即演说家的艺术，它利用概念来说服人们做或相信某些事情。这种技能的主要使用者是政治和宗教领袖，他们利用它来确立自己的权威。除了修辞学，阿里·法拉比还提到了诗歌艺术（shi‘r），它利用概念来使人们达到某种心境。他认为这种艺术对宗教领袖最有用。他还提到了一种他称之为诡辩艺术的艺术。这种艺术体现了从错误前提中产生错误论点的技能。很难理解是什么导致他得出这样一个结论，除了亚历山大的课程表中夹在辩证方法书和修辞方法书之间的一本亚里士多德的著作《诡辩论证》。亚里士多德的这部著作通常被理解为教授检测和驳斥错误论点的艺术，而不是创造它们的艺术！

语言的引入之一效果是人们能够了解其他人的观点。他们将不可避免地发现，有些人与他们持不同意见，尤其是在关于宇宙本质的推测方面。因此，辩证法或辩论（jadal）的艺术被引入，以解决两人之间意见的分歧。但是，对一个分歧的辩证性解决并不保证达成的结论是真实的；辩论者中的一方可能只是更擅长辩论而已。

意识到辩证法并不能导致确定的真理将激发一些个体寻求其他能够做到这一点的方法。在这一点上，社区发现了“证明”（burhān）的艺术，这种艺术确立了确定的真理。阿里·法拉比 认为，在希腊人中，这种艺术是由柏拉图实现的。

> 柏拉图是第一个意识到演绎方法并将其与辩证、诡辩、修辞和诗意方法区分开来的人。但从他的观点来看，它们在使用和内容上有所区别，并根据他在闲暇时的思考和卓越的天赋所得到的指导。他并没有为它们规定普遍规则。之后，亚里士多德在他的著作《后分析》中确立了这样的规则。《修辞学》（55.13–17）

在新柏拉图学派中，逻辑的高潮是《后分析》，其中涉及对知识的确定性处理。新柏拉图主义者还认为，亚里士多德已经安排了他的《逻辑学篇》（Organon），通过《范畴学》、《论释》和《前分析》这条路径汇集了逻辑工具，而从《后分析》开始的下坡路则允许学生依次在演绎方法、辩证方法、诡辩方法、修辞方法和诗意方法中应用这些工具。阿里·法拉比继承了亚里士多德的这种观点；参见《亚里士多德》第 82-93 页。

他还继承了新柏拉图学派对逻辑要素进行分类的热情。因此，在[信件]中，他根据引发它们产生的社会需求对逻辑艺术进行了分类，而在[亚里士多德]中，他根据亚里士多德专门用于它们的《论理学篇》对它们进行了分类。这两种分类原则导致了逻辑艺术之间基本相同的划分。但有时他使用的原则会在不同的地方划分出区别。例如，在[三段论]中，他通过它们使用的三段论（即两前提推理）来对逻辑种类进行分类；我们将在下面的第 6 节中介绍这种分类。在其他地方，他通过它们在三段论中接受的前提种类来对逻辑种类进行分类。

所有种类的逻辑，甚至是较少涉及三段论的种类，都必须有关于采用何种起始前提的规则。在修辞学的情况下，实用性意味着演讲者应选择听众会接受的前提；同样，在辩证学中，从具有一定可信度的前提开始是有意义的。阿里·法拉比认为，在实践中，社区将坚持增加客观性的限制：例如，专家同意前提，或者没有人对其表示明确反对。他提出了一些类似的条件，包括前提必须至少是“标准的”（masshūr）或“被接受的”（maqbūl）。 （有关这些条件及其适用范围的讨论，请参见 Aouad 1992 和 Black 1990：第 5A 章。Chatti 2017 回顾了阿里·法拉比如何对假设逻辑中的推理起点进行分类。Kleven 2023 研究了阿里·法拉比一些导论性论文中对推理起点的处理，并提供了一些有趣的手稿信息。）

修辞推理可能是最难理解的，特别是当【Letters】暗示修辞学家的工作在说服听众后就完成了。当然，这个目标可能通过根本不涉及逻辑的手段来实现吧？但最近已经开始认可，拉丁文随笔【Didascalia】发表在【修辞学】中，可能是阿里·法拉比失传的《修辞学长评》序言的翻译。事实上，这部作品远远超出了关于说服方法的模糊言论；参见 Woerther 2020。例如，阿里·法拉比指出，关于给这位现在的病人施以何种疗法的医学推理，以及关于如何增加这块特定田地产量的农业推理，都不能被证明，因为它们仅适用于这个特定个体（病人或田地）；因此，我们必须将它们视为修辞推理（Woerther（2020 年，第 346 页））。根据这种计算，我们最受尊敬的专家的许多推理必须算作修辞学。

在《前分析学》第二部分的第 23 和 24 节末尾，亚里士多德讨论归纳论证和类比论证，旨在表明它们都可以归结为三段论。由于【三段论】是《前分析学》的精华，阿里·法拉比在书的结尾部分提供了一些关于他自己将归纳和类比归结为三段论的章节。一些《三段论》的手稿，以及后来的作品《简短三段论》，包括一对我们将称之为尾声的文章；第一篇文章是关于“转移”逻辑设备，第二篇是关于将这一设备应用于将伊斯兰法学（fiqh）中的论证归结为三段论。在这两篇文章中的第一篇中，阿里·法拉比提到了一些命名的论证形式（例如“从观察到未观察的转移”），很明显他是在提及早期关于这些论证形式的辩论；但关于这些辩论是在罗马帝国的医学圈还是在伊斯兰神学中，目前尚无共识。

这一复杂体系中的一个特定分支是阿里·法拉比对两种类比的区分，他分别称之为 taḥlīl 和 tarkīb。一些作者将这些词解读为“分析”和“综合”，并试图将其解读为两种不同形式证明的描述。但这显然是错误的，因为阿里·法拉比本人所作的区分并不在于最终证明的形式，而在于在寻找中间项时成功的标准。阿里·法拉比本人在这一点上有些矛盾；出于对亚里士多德的忠诚，他想强调演绎推理，但他在第二篇论文中应用到法学的关键点并不在于最终三段论证明的形式。（有关 Tailpiece 的更多细节，请参见 Chatti 和 Hodges 2020，以及 Hodges 2020，关于这一时期对搜索算法日益增加的兴趣。）

进一步比较阿里·法拉比辩证法与伊斯兰法学的评论可在 Gyekye（1989）和 Young（2017：539-543）中找到。

## 4. 请求和问题

阿里·法拉比 阐述了语言起源于沟通请求的需求。各种类型的请求和问题是他思想的一个核心特征。在[辩论]43.2f 中，他区分了两种主要类型的请求：一种是要求承诺的请求，另一种是要求知识或信息的请求。这种区分对应于辩论和科学中问题的角色。我们从辩论开始。

### 4.1 辩论中的问题

阿里·法拉比 援引亚里士多德的话说，在辩论中的主要目的是驳斥提出的观点；达成一致是次要目的（[辩论] 14.7-9）。因此，辩论的规则规定，其中一位辩手，被称为“提问者”（sā’il），负责驳斥另一位辩手（“回答者”，mujīb）提出的观点。提问者的任务还包括引导回答者对陈述作出承诺。（承诺被称为“让步”或“提交”，taslīm。）提问者必须试图驳斥这些陈述，而回答者必须试图捍卫它们。提问者通过向回答者提出一对不相容的可能性来引导其作出承诺，而回答者必须选择要捍卫的可能性。这对不相容的可能性被称为“所寻求的”（maṭlūb，在拉丁文中为 quaesitum），或者“问题”（mas’ala）。

最简单的情况是，问者要求回答者对某个基本概念是否在世界中实例化做出承诺。例如，“真空存在还是不存在？”更复杂的情况是，结合了两个概念的问题，比如“天空是不是球形的？”更奇怪的是，“石头和人类，哪一个是动物？”（[辩论] 43.16–20）当回答者做出承诺时，问者必须设计一个推论，其结论与承诺相矛盾，然后试图让回答者承诺这个推论的前提和有效性。回答者可以对这些观点中的任何一个提出质疑，然后两位参与者交换角色。

阿里·法拉比评论说，辩论的规则除了解决意见分歧之外，还被发现对其他目的有用。例如，在证明科学中，辩论训练参与者成为灵活和系统性思考者；它清除了诡辩的胡言乱语，并提供了可以通过证明方法检验的初步结论。事实上，如果不参与辩论，人类不可能达到哲学真理。([辩论] 29.21–38.3) 问者和回答者之间的关系可以作为教师和学生之间关系的模板，只要这不仅仅是从教师传递信息给学生。它也可以作为合作研究的基础。([演示] 77.1–83.9)

### 4.2 寻求信息的问题

阿里·法拉比对寻求信息的问题的处理受到亚里士多德《逻辑学篇》中使用疑问词的两种背景的强烈影响。其中之一是在《范畴论》中，亚里士多德（至少阿里·法拉比的阅读）将基本概念分类为十大至上范畴。亚里士多德通过疑问词部分解释了他的分类：“多少”（阿拉伯语 kam），“如何”（kayfa），“何处”（ayna），“何时”（matā）。阿里·法拉比注意到了大部分这些疑问词，并增加了两个：“什么”（mā）和“哪一个”（ayyu）（[信件]（166）165.17，（183）181.16）。在[表达] 47.5-12 中，他指出对其中一些问题的回答可以通过疑问词或从中派生的词进行分类：“数量”（kamiyya），“质量”（kayfiyya），“本质”或“是什么”（māhiyya）。 （请参阅 Diebler 2005 中关于这些问题的讨论。）

第二个背景是《后分析学》的第二本书。在这本书的开头，亚里士多德列出了我们寻求的四个“事物”，即它是如此（annahu yūjadu），为什么是这样（li-mādhā），它是什么（mā）以及它是哪一个（ayyu）。在[信件]（210）200.16 中，阿里·法拉比增加了问题“是否如此”（hal），这将亚里士多德的“它是如此”转变为一个问题。

《表达》和《信件》的大部分内容都致力于解释我们如何通过提出这些类似问题来获取哲学信息。因此，对于每个主要概念，我们可以问“它是什么”？阿里·法拉比将概念的种属（jins）定义为对这个问题的回答。通过问“哪一个事物”（在该种属中），我们得到一个更具体的描述，提供了种（naw‘）在种属下方。阿里·法拉比解释了如何通过进一步提问来引出概念的区别和定义。一个概念的“本质”（dhāt）由包含在概念定义中的概念组成；这些本质中的概念也被描述为该概念的“构成”（muqawwim）。

阿里·法拉比努力说服我们，哲学家的问题是日常问题的自然延伸。（Diebler 2005 与普通语言哲学进行了合理的比较。）但这种努力几乎没有成功。在日常情境中，我们会问关于一棵棕榈树，“它是什么？”并接受答案“它是一个物体”吗？（[信件]（167）166.16）结果是，阿里·法拉比无意中提出了在问题哲学中仍然引起共鸣的问题。将“它是一个物体”与 Cross 和 Roelofsen 2018 年讨论的答案“[他]是一个身高超过三英寸的人”进行比较。

## 5. 逻辑的定义

在他的《论证》（64.5-7）中，阿里·法拉比告诉我们：

> 两种艺术或科学之间的区别在于拥有不同的主题（mawḍū‘）。如果它们的主题相同，那么这些艺术或科学也是相同的；如果它们的主题不同，那么这些艺术或科学也是不同的。它们的主题可以通过拥有不同的特征（aḥwāl，单数ḥāl）或本身而有所不同。

因此，一门艺术或科学——阿里·法拉比对两者之间的区别几乎不关心——研究一些被称为其对象的实体，并将一些特征归因于这些实体。像其他亚里士多德学派一样，阿里·法拉比在说算术的对象是数字和算术的主题是数字之间来回变换；这两种说法意思相同。

在[信件]第 11-18 节中，阿里·法拉比试图用这些术语确定几门艺术或科学，即“逻辑艺术和自然、政治、数学和形而上学科学”。第一步是确定它们的对象。在所有这些情况下，他说，“主要对象”是主要概念。例如，在数学中，我们从数量的角度研究主要概念，抽象出它们的其他特征。【见 Druart 2007 和 Menn 2008 关于形而上学如何融入这一图景的重要问题。】

阿里·法拉比告诉我们逻辑学研究的初级概念的特征是什么：

> 就初级概念而言，它们被表达所指，它们是普遍的，它们是谓词或主语，它们是相互定义的，它们被询问并在有关它们的问题中被考虑，它们是逻辑的。它们被用来研究它们的化合物，彼此之间的关系，以及附着在它们上面的前述事物，以及这些化合物在化合后的特征。([Letters] (12) 67.1–5)。

他没有举例，但这里有一个例子。在他的书《三段论》27.6f 中，他声称以下论证是一个有效的三段论：

> 没有石头是动物。一些身体是动物。
>
> 这意味着：某个身体不是一块石头。

我们可以解释他的说法：

(*)

如果第一句被视为普遍否定句，主语为“石头”，谓语为“动物”，第二句为存在肯定等等，第三句等等，则第三句由前两句推出。

这种释义说明了为什么“谓语”和“主语”被列为逻辑特征之一，也说明了为什么逻辑必须考虑基本概念的复合（因为这三句表达了命题，而命题是概念的复合）。阿里·法拉比提到被“问及并在问题中被提及”可能指的是辩证问题，或者是提取关于概念信息的问题，或者可能两者兼而有之。

将逻辑艺术的特征列为对其他概念进行分类的概念，因此它们是次要概念。这将逻辑与数学和自然科学区分开来。例如，自然科学找到原因，但这些原因“不在范畴之外”（[信件]（16）68.17）。

通过将简单的初级概念描述为逻辑的“第一”或“主要”主题，阿里·法拉比暗示逻辑可能有进一步的主题。这是有道理的；为什么逻辑规则不应该同样适用于次要概念和主要概念？事实上，阿里·法拉比设置事物的方式使他能够解释应该如何进行。关于次要概念的法则将需要三级概念。但没有必要使用三级概念重新陈述逻辑规则，因为“附着于初级概念的特征与附着于次要概念的特征完全相同”，一直延伸到最高层次（[信件]（9）65.16f）。

阿里·法拉比 的逻辑定义存在明显的批评，即他列出的特征过于模糊。例如，我们如何利用他的列表来确定诗歌艺术是否属于逻辑的一部分？真正的问题在于，对于拥有新柏拉图主义传统的阿里·法拉比 本人来说，逻辑包括亚里士多德在《逻辑学》中所说的一切，而阿里·法拉比 从未找到任何抽象的方式来描述这一系列材料。

阿里·法拉比 在他的《目录》中对逻辑主题的描述更加模糊（59.9-11）。

> 关于逻辑的主题，它给出规则的主题是概念，因为表达所指示的东西，以及表达所指示的概念。

这个定义可能是从新柏拉图传统的文本中复制过来的。

我们简要回顾一下为什么阿里·法拉比在逻辑课程中包括范畴主题的问题。亚历山大传统的评论通常以对将要讨论的材料目的的陈述开始。阿里·法拉比的梗概《范畴论》没有这样的陈述，但更长的《范畴论评论》的残片确实回答了这个问题。它们告诉我们（第 196 页），亚里士多德的《范畴论》旨在列举简单的普遍初级概念，但亚里士多德发现仅列举十个至高种类就足够了。有些人（第 202 页）认为《范畴论》并非专门用于逻辑；但“在我们提到的条件下，《范畴论》的内容事实上成为逻辑的一个特定部分”。但实际上，在残片中至少没有提到这样的条件。需要展示的是，在课程的第一本书中研究的范畴的特征（aḥwāl）将在后面的书中起到一定作用。但这个问题根本没有得到解决。阿里·法拉比在《修辞学》87.16–89.4 中提到了这一点，他说前提的术语可以属于十个至高种类中的任何一个，以任何组合形式。这只是证实了至高种类与逻辑推理无关的怀疑。

## 

在这一部分，我们快速浏览阿里·法拉比的逻辑形式系统。我们假设读者对亚里士多德的三段论有一定了解，就像在史密斯（2022）中所述。阿里·法拉比讨论了四种逻辑系统：6.1 范畴三段论，6.2 假设三段论，6.3 情态三段论，6.4 演绎三段论。

### 6.1 范畴逻辑

在[三段论]和[简短三段论]中，阿里·法拉比认可了范畴三段论的三种形式，遵循亚里士多德的观点。在这三种形式中，他分别认可了四种、四种和六种有效的形式，如史密斯（2022）所列，但顺序并不总是相同的。（例如，在[三段论]中，他将达里放在赛勒兰特之前）。再次遵循亚里士多德，他将第一种形式的三段论视为“完美的”，即不证自明，无需证明。对于第二和第三种形式的形式，他通过归结为第一种形式来给出理由，主要是遵循亚里士多德的观点。（他与亚里士多德在这些理由中的主要分歧在于，他从不使用归谬法作为证明，尽管他承认这样的证明是有效的。）亚里士多德对达拉普提的理由依赖于转换蕴涵：从“每个 B 都是 A”推断“一些 A 是 B”（参见[三段论] 30.3）。如果我们允许“每个 B 都是 A”在没有 B 的情况下为真，则这种蕴涵就不成立。因此，阿里·法拉比宣称，“每个 B 都是 A”除非至少有一个 B，否则为假（[范畴学] 124.14）。尽管他有时似乎忘记了自己说过这句话，但他是有记录的最早明确阐明“每个 B 都是 A”这一蕴涵的逻辑学家。

在范畴三段论中，他与亚里士多德的主要区别可能在于他对形式前提对的处理（即，用术语表示的）。亚里士多德说“没有三段论发生”是指没有三段论的结论是有效的。阿里·法拉比将这些前提对称为“无效的”（ghayr muntij）。亚里士多德利用有效推理规则保持真实性的事实来证明形式前提对的无效性；对于任何提出的结论，他找到了可以替代术语的名词，使得前提为真，而结论为假。在罗马帝国时期，逻辑学家倾向于跳过这些证明，而是转而依赖被认为能够给出形式前提对生产性的必要和充分条件的“三段论法则”（参见李，1984 年：119f）。这通常是对亚里士多德工作严谨性的一种放松，因为用来证明三段论法则的论据水平较低。尽管阿里·法拉比曾简要提到过亚里士多德的反例方法（[三段论] 20.2-5），但实际上他完全依赖于三段论法则。相比之下，波斯人保罗和阿拉伯作家伊本·穆卡法的早期逻辑文本，都是亚历山大观点的继承者，完全严格地复制了亚里士多德在范畴逻辑中的非生产性证明。

对于阿里·法拉比的范畴三段论的进一步讨论，请参见 Chatti 和 Hodges 2020 以及 Chatti 2019。

### 6.2 假设逻辑

一个假设句由两个较短的句子连接在一起组成，如连接形式“如果 p 则 q”或分离形式“要么 p 要么 q”。对于这些句子的逻辑，阿里·法拉比几乎完全将注意力集中在像假言三段论这样的简单推理上：

> 如果这个可见的东西是一个人类，那么它是一个动物；但它是一个人类。因此它是一个动物。

或者是具有排他性的析取推理：

> 这个数字要么是偶数要么是奇数；但它是偶数。因此它不是奇数。

他的论述与博伊修斯（Boethius）的《假言三段论》内容如此相似，以至于它们必定源自一种现已失传的共同来源。

阿里·法拉比指出，假设性句子可以被链接成“复合三段论”，例如在..

> 要么 p，要么 q。如果 p，则非 r。如果非 r，则 s。如果 s，则 t。非 t。因此 q。

这正式化了他在[短三段论] 90.6-9 中的一个论点。他还举例说明了连锁的范畴三段论，尽管这些已经可以在亚里士多德那里找到。

### 6.3 模态逻辑

在阿里·法拉比的幸存作品中几乎没有提到模态三段论。他必定在他的《先验分析评论》中讨论了模态三段论；但是这部作品的相关部分已经遗失，我们只能依靠伊本·西那、阿维森纳和迈蒙尼德的引用。这些引用告诉我们，阿里·法拉比只接受或拒绝了一个模态三段论的情态。[17]

根据《关于诠释的评论》193.3–19 和梅蒙尼德斯的引用，我们知道阿里·法拉比声称，与伽兰相反，逻辑与可能性陈述在需要预测实际结果的实用艺术中是有用的，比如医学、农业和航海（Schacht & Meyerhof 1937: 67）。乍一看，这似乎是可能性和概率之间的混淆。但更有可能的是，阿里·法拉比认为，模态逻辑的规律可以被调整，以便用“可能”代替模态运算符。事实上，阿里·法拉比在《论证》44.20–22 中确实说，在许多艺术中，“必然地每个 A 都是 B”和“大多数 A 都是 B”被视为等价的。（但这是否会将“可能”与“必然”而不是“可能”相关联呢？阿里·法拉比在这里无法回答这个问题。）

还有进一步的证据表明阿里·法拉比在模态逻辑中尝试其他句子形式。亚里士多德（Street 2001）和阿维森纳（伊本·西那）（Maqālāt 102.13f）都引用阿里·法拉比考虑包含“就某种程度而言”的三段论前提，例如，“每个 A 都可以是 B，就某种程度而言，它是 B”。

根据阿维森（伊本·鲁什德，Maqālāt 129.11–17），我们也知道阿里·法拉比观察到得出三段论的方法：

> 每个 C 都是可能的 B。每个 B 都是可能的 A。
>
> 因此，每个 C 都是一个可能的 A。

（亚里士多德接受为完美的形式）我们需要第二前提中的“每个 B”意味着“每个可能的 B”；否则，Cs 可能是可能的 Bs 但不是实际的 Bs，这样它们就会逃避第二前提的量词，从而违反普遍性原则。 阿里·法拉比 由此推断，在这种情态三段论中，亚里士多德打算将“每个 B”解读为“每个可能的 B”。

阿里·法拉比 究竟是什么意思，从阿维森的描述中很难理解。但阿里·法拉比 对此的运用似乎包含了一种在情态语境中给出巴巴拉[18]的集合论理论的尝试。阿维森（伊本·鲁什德，《论文》154.18f）的另一段引文显示，阿里·法拉比 使用“de omni”这一格言（再次可能是一个集合论论证）来证明一个陈述中的三段论，其中主要前提具有否定主语：

> 雕塑不是动物。不是动物的东西不是人类。
>
> 因此，雕塑不是人类。

这是 Celarent[19]，但中间项为否定。

### 6.4 演绎逻辑

我们在上文第 3 节中看到，阿里·法拉比认为亚里士多德在他的《后分析》中规定了演绎推理的普遍规则。但除了一些关于寻找定义的技巧的讨论外，阿里·法拉比在[演绎]中给我们提供的唯一规则是带有额外部分的巴巴拉式三段论。（这些内容在[演绎]33.1-39.4 节中；它们被引用于 Strobino 2019 年。）

这令人失望，但至少可以清楚地知道多余的部分是从哪里来的。指示性推理提供确定的知识，在《确定性》中，阿里·法拉比写下了一些确定性的必要和充分条件。（这些“确定性条件”由洛佩斯-法赫杰特（López-Farjeat）在 2020 年列出，并由布莱克（Black）在 2006 年进行讨论。）这里相关的条件是最后一个：

> 如果前面的条件成立，那么它们成立并非偶然。

([确定性] 100.18。) 换句话说，他们坚持根据本质。此外，在[实现] 51.15–52.17 的讨论中，阿里·法拉比告诉我们，演绎必须从一些基本概念出发，而所讨论的概念是回答“什么”，“通过什么”，“如何”，“从什么”和“为什么”这些问题的概念。这些问题并不完全是上面 4.2 节中列出的问题，但显然属于那个领域。再次表明，演绎应当从讨论对象的构成性质，即它们的本质出发。

假设我们有 A 和 B 的构成性质，它们确保每个 A 都是 B，同样也有确保每个 B 都是 C 的构成性质。然后，通过巴巴拉，我们知道每个 A 都是 C；但是为了演绎的确定性，我们希望“每个 A 都是 C”由 A 和 C 的构成性质确保。阿里·法拉比的一些三段论证实了这一点，考虑了不同类型的构成性质。在其他一些情况下，构成性质未能产生所需的句子，阿里·法拉比评论说，三段论提供了事实，但没有原因。

Galston (1981)提出了一些理由，认为阿里·法拉比可能认为演绎证明是一个遥不可及的理想，只有在辩证层面上进行了大量工作之后才能达到。

## 7. 逻辑推论

阿里·法拉比 最复杂的三段论定义在[Expressions] 100.3–5：三段论是

> 在头脑中以一种顺序（tartīb）排列的事物，使得当这些事物按照这种顺序排列后，头脑不可避免地发现自己在看着之前不知道的另一件事物，从而现在知道了它，因此头脑有能力顺从它所看到的事物，就好像它已经知道那件事物一样。

对“秩序”的提及必须归功于亚里士多德在《话题》156a22-26 的言论，即在辩论中，提问者有时可以通过以错误的顺序提问来愚弄回答者。但是，通过将秩序的概念引入三段论的定义中，阿里·法拉比在逻辑学中开辟了一个新的篇章（关于此事，请参见霍奇斯 2018 年的著作以及阿维森纳对这一主题的发展）。

要进一步阐明阿里·法拉比对逻辑推论的理解，我们需要探讨他对逻辑规则的看法。在[目录] 53.5-9 中，他告诉我们：

> 逻辑艺术通常提供的规则的本质是构成智力并引导一个人沿着正确和真实的道路前进，在一切可能存在概念错误的地方，以及保护和防范他免受概念中的错误、失误和错误的规则，以及通过这些规则在概念中测试事物的规则，在这些地方人们不能相信不会犯错误。

但这些是新柏拉图主义的共同之处。在其他地方，阿里·法拉比对逻辑规则说了更深刻的话。

回顾上文第 3 节，根据阿里·法拉比的理解，逻辑的正确方法在柏拉图时代已经被认识到，他们是“根据物质”拥有的，而亚里士多德将它们重新表述为“普遍规则”。当他说亚里士多德在《先分析篇》中考虑陈述句时，“从它们的构成（ta’līf）而不是从它们的物质的角度”时，他很可能心中有同样的东西（[《关于诠释的评论》] 53.3f）。如果我们回到上文第 5 节中(*)处详细说明的例子三段论，他在这里所表达的观点就变得清晰了。这实际上是阿里·法拉比对这种三段论形式的第二次尝试。在他的第一次尝试中（[《三段论》] 26.11f），他遵循了亚里士多德首选的风格，用字母 A、B、C 代替“物质”石头、动物和身体。因此，相应的释义将会是

(**)

对于所有的基本概念 A、B 和 C，如果第一个句子被视为主观 A 和谓语 B 的普遍否定句，等等等等。

这里(**)是一个普遍规则，即对于所有的 A、B 和 C；前提和结论不包含实质，是纯粹的组合。

Mallet (1994: 329–335) 指出了阿里·法拉比在《分析》95.5–8 中的一段话，他说他将解释“我们如何定义三段论”以及它的使用方式。

> 首先，通过对主题的了解，这些主题是普遍前提，其特定（juz’iyyāt）被用作三段论中的主要前提，以及在每个单独的艺术中。

如果“特定”在这种情况下意味着通常的含义，即存在量化的陈述，那么这是错误的：存在量化的陈述几乎从不是三段论中的主要前提。但正如马莱特指出的那样，如果“普遍前提”是像(**)这样的陈述，它们的“特定”是形式为(*)的相应陈述，那么这段话就是有意义的。阿里·法拉比（Al-Fārābī）随后表示，自然语言的三段论通过成为陈述某种形式的所有论证都是有效的普遍规则的实例而得到验证。如果这是正确的阅读，那么阿里·法拉比在强烈意义上是一位形式逻辑学家。事实上，一些作家确实写过关于阿里·法拉比“形式主义”的文章（Zimmermann 1981: xl）。

然而，需要谨慎的理由是存在的。正如马莱特（Mallet）的论文所示，阿里·法拉比在他对主题的进一步讨论中，无论是在【分析】还是在【辩论】中，都朝着完全不同的方向前进（这里过于复杂，无法在此展开，但请参阅 Hasnawi 2009 和 Karimullah 2014）。此外，阿里·法拉比有一种习惯，即通过参考关于术语（即它们的内容）的真实世界信息来证明推理，而这些信息并不包含在前提中。他在巴罗科（Baroco）的三段论形式中做到了这一点（【三段论】27.8–12，【简短三段论】79.5–12），在【对《解释篇》的注释】136.10 中还有一个单前提的例子。在其他地方，他对论证的“形式”玩起了快速和松散的游戏，例如将归纳的多前提版本简化为一个两前提的范畴三段论，而没有提供任何关于如何将一系列前提简化为单一前提的迹象（【三段论】35.14–18）。

阿里·法拉比经常提到“普遍规则”。但它们不一定是可以毫无例外地成立的法则。例如，以他的论文【规范】为例，该论文在结尾（272.16）提出了一个关于展示“诗歌的普遍规则”的声明。该作品主要由对不同类型诗歌的分类、对分析诗歌中使用的术语的定义以及与装饰艺术的广泛比较组成。这些“规则”最多只是一个渴望从事诗歌理论研究的人应该了解的事物。

在任何情况下，阿里·法拉比提供的逻辑规则的唯一种类的理由是它们在使用它们的社区中被发现对某些目的有用（参见上文第 3 节）。总的来说，阿里·法拉比将逻辑的形式规则视为启发而不是科学理论的法则。可以认为阿里·法拉比是现在被称为“非正式逻辑”的创始人之一，就像希区考克（2017 年）和沃尔顿（1989 年）的文本中所述。他赋予对话的关键作用将他置于沃尔顿书中相同的世界中。

## 8. 真与伪

阿里·法拉比定义概念为“真实”（ṣādiq），如果它在外部世界与在灵魂中是相同的（[信件]（88）116.3f）。为了使这个定义有意义，概念不需要是命题的。例如（[信件] 118.5f），概念“真空”不是真实的，因为它只存在于头脑中而不在外部世界中（Abed（1991：111-115）讨论了阿里·法拉比关于存在和真理之间的关系）。

阿里·法拉比经常使用派生的使动名词 taṣdīq，它的意义范围在“验证”（即，确信某事是真实的）和“同意”（即，将某事视为真实）之间。阿里·法拉比在[演示] 20.4–21.12 中阐述了这种区别。他还指出了 taṣdīq 的意义范围在[达成] 90.6f 中：它可以“通过确定的演示或通过说服”发生。很可能确定的演示带来验证，而说服带来同意。

在《表达》86.17–87.4 中，阿里·法拉比告诉我们教学有三个步骤：“概念化”（taṣawwur），在这一步中，我们理解概念是什么，以及我们的老师告诉我们有关它的内容；taṣdīq；以及“记忆”（hifẓ）[24]。在另一处关于教学的段落中，《论证》80.22 补充说：“我们可以寻求对简单事物或复合事物的 taṣdīq”。对于阿里·法拉比来说，命题总是复合的，因此他告诉我们非命题性概念可以被认同。

认为命题和非命题性概念都可以是真实的观点在阿里·法拉比的思想中根深蒂固，尽管他意识到并非每个人都同意这一点（《对《解释论》的评论》52.13f）。例如，这一观点使他能够将非命题性概念的定义和命题的论证看作是重叠的程序；可以有在部分顺序上除外的定义，这些定义与论证相同（《论证》47.11）。一百年后，阿维森纳发现有必要区分非命题性和命题性概念。他挑衅性地使用阿里·法拉比自己的术语 taṣawwur 和 taṣdīq 来确定区别；对于阿维森纳，任何概念都可以被概念化，但只有命题可以继续被验证。

一个句子可以通过添加否定粒子在真和假之间切换，例如，“不是这样的”（laysa）。但重要的是否定粒子放在哪里；阿里·法拉比讨论了许多情况，特别是没有明确的整体模式。他详细讨论的一个情况是否定附加到名词上，就像我们从名词“公正”形成名词“不仅仅”。（形容词被视为名词。）他遵循了忒奥弗拉斯托斯（Fortenbaugh 等人，1992 年：148-153）的观点，将这种否定形式称为“转位”（‘udūlī）；他认为由此产生的转位句是一种肯定，而不是否定。没有东西既公正又不公正，但有些东西既不是，例如婴儿。因此，婴儿不仅仅，但也不是不公正。出于类似的原因，婴儿不是不公正。此外，婴儿是公正是错误的，不公正也是错误的。同样，每一个热量都是曲线的是错误的（而不是无意义的）（《范畴学》125.8）。 （Thom 2008 通过这些概念开辟了一条清晰的道路。）

在阿里·法拉比之前，转位似乎在推理规则中几乎没有起到作用。因此，阿里·法拉比中具有转位主语术语的示例值得注意：

> 如果每个人类都是动物，那么每个非动物就是非人类。

这出现在他的（[分析] 114.11f）中，他将其引入为一个话题。（Fallahi 2019 讨论了这个例子。）

阿里·法拉比 认为“来这里”既不真实也不虚假，但“你必须来这里”，可以代替它（yaqūmu maqāma(hu)），这是真实的或虚假的（《短论解释》47.3–48.1）。原因在于第一句与第二句不同，形式不正确，不能真实或虚假。他可能是在说每个命令都与某个陈述句具有相同的含义，但真实与虚假取决于所使用的词语形式，而不仅仅是含义。但更仔细的检查显示，他使用短语 yaqūmu maqāma 来表示一些相当弱的等价关系，在这些情况下，两个句子明显意思不同。例如，当辩论中的提问者用一个转位的肯定句代替否定句提问时，他使用这个短语（《解释论注释》136.6）。他对不同种类等价关系的措辞（例如，bi-manzila，“在相同的角色中”，这对于熟悉古典阿拉伯语言学的读者来说是熟悉的）值得更仔细研究。他写过《表达》和《信件》，他当然应该让他的介词短语受到认真对待。

在《解释论注释》89.12–100.25 中，阿里·法拉比提出了一种独特的关于未来陈述的观点（参见亚当森 2006 年，克努蒂拉 2020 年）。关于未来一场战斗，“这场战斗要么会发生，要么不会发生”是一个必然命题，因此是真实的。但由于未来是不确定的，句子“这场战斗将发生”和“这场战斗不会发生”都不是真实的；此外，真值的缺失是世界内在的事物，不仅仅是我们的无知。假设如果真主知道扎伊德明天会离家出走，那么现在就是真实的扎伊德明天会离家出走，这个蕴涵是一个必然真理。但这个蕴涵的必然真理并不意味着其结论的必然真理，因此扎伊德仍然有选择的自由。阿里·法拉比并没有明确表示他是否怀疑模态推理。

> 必然地，如果 p，那么 q。必然地 p。因此必然地 q。

或者他是否允许有关于真主必然知晓的事情存在限制。（Hasnawi [1985: 28f] 将这段话与关于真主预知的穆斯林辩论联系起来。）

## 9. 阿里·法拉比的阿拉伯语基础

在几部作品中，阿里·法拉比分析了阿拉伯语作为一种语言的特点。其中一些作品包含了阿拉伯语与其他语言的比较：希腊语、叙利亚语、波斯语和粟特语在《信函》中的比较，希腊语和波斯语在《对《解释学》的评注》中的比较，以及希腊语在《表达》中的比较。阿里·法拉比对希腊语的评论包括一些错误翻译和可疑的语法评论，明显表明他并没有读过希腊语（关于阿里·法拉比对语言的了解，请参见 Zimmermann 1981: xlviif）。此外，阿里·法拉比有强烈的倾向将语言分类为（1）阿拉伯语和（2）“其他语言”。因此，这些比较远非对比较语言学的严肃贡献，人们不禁要问他为什么要包括这些比较。

在阅读比较时，一个模式显现出来。阿里·法拉比 从一个概念体系的理论出发（参见上文第 2 节），他的主要关注点是阿拉伯语如何很好地反映这个体系。"其他语言" 的作用始终是为了说明其他语言如何反映概念体系，而阿拉伯语未能做到。正如门恩所说，“实际上，他重建的希腊语充当了一个理想的逻辑语言，即一个语言，其中语法形式总是跟踪逻辑形式”（2008: 68）。

阿里·法拉比 的出发点是范畴三段论中出现的句子所需的概念

> Zayd 是人类。每匹马都是动物。一些马不是石头。

这里的“Zayd”、“马”、“动物”、“石头”表示简单的基本概念；专有名词“Zayd”表示一个特定的（juz’），其他三个词表示普遍概念（kullī）。“每个”和“一些”是量词（sūr），而“是”是一个连词（rābiṭ），表示两个基本概念之间的联系，从而形成肯定或否定（[《论释义》评论] 102.17）。阿里·法拉比将所有这些词分为两类：名词（ism）表示基本概念，而剩下的是助词（ḥarf）（形容词算作名词）。他明确指出这些助词是一个不同类别的群体，需要进一步分类（[《表达》] 42.8–12）。

阿里·法拉比引入了一个概念，区分了永久适用于事物的概念和仅临时适用的概念（称为意外，'araḍ）。因此，概念具有时间制度，语言应该反映这一点。在这里，阿里·法拉比最具先验性，声称概念的时间制度应该由三时态系统表示：过去、现在和未来。我们不知道他是否仅仅没有注意到阿拉伯语法中表示时间的其他方式（例如，连续时态和完成时结构），或者出于某种原因，他将这些特征视为边缘。

广泛遵循亚里士多德，阿里·法拉比在名词和介词之外增加了第三类，即动词（kalima）。动词同时表达三个意义：一个简单的主要概念，一个连词连接，以及一个时态（因此是一个时间）。因此，在“扎伊德走了”的例子中，动词“走了”表达了（i）主要概念“走路”，（ii）将这个概念与扎伊德的概念联系起来，以及（iii）过去时态。

尽管阿里·法拉比在时态方面存在盲点，但他关于阿拉伯语动词的评论包含了他一些最好的语言洞察。例如，他指出，由于时态仅用于谈论事件，人们不应期望找到一个以“人”（insān）这样的永久概念为主要概念的动词。他通过从“人”构建一个动词来测试这一点，即 ta’annasa。按照阿拉伯语的规则，人们会理解它表示具有时间特征的事物，即“成为人”（《关于《解释篇》的评论》34.4–9）。阿里·法拉比还使用了一个非常微妙的语言论证，以表明阿拉伯语有一个可以表达纯粹时态的词，而无需系动词或主要概念。这个词就是 kāna，比如“扎伊德在走路”，扎伊德 kāna 雅姆希。我们可以看到，在 kāna 中没有系动词，他告诉我们，因为“扎伊德在走路”，扎伊德雅姆希，已经在动词雅姆希中包含了一个系动词（《关于《解释篇》的评论》42.6–18）。甚至在阿里·法拉比讨论现在时如何表示时间点或时间间隔时，我们也可以看到他对事件结构的分析开端（《关于《解释篇》的评论》40.3–41.18）。

## 10. 诗歌与音乐

对于阿里·法拉比来说，音乐理论与修辞学和诗歌一样，处于演绎艺术的边缘：

> ……很明显，[音乐的理论艺术] 与每种语言的语言学者的学术有很多共同之处，并且与修辞学和诗歌的艺术有很多共同之处，后两者在许多方面都是逻辑艺术的一部分。 ([音乐] 173)

诗歌与音乐的联系是明显的，他告诉我们，因为诗歌通过配乐而得以完善。除此之外，还有形式上的相似之处：诗歌由有限字母表中的字母串联而成，音乐则由有限调色板中的声音串联而成。阿里·法拉比 承认，在音乐的情况下，调色板是由自然规定的，而一个国家的字母表中的字母是由该国家的习俗所采纳的 ([音乐] 120f)。但他指出，在一些文化中，伴随诗歌的音乐被视为诗歌的一部分，这可以通过事实来证明，即有时诗歌的韵律是由音乐而不是文字来维持的 ([诗歌] 91.15–17)。

阿里·法拉比 从未将音乐单独视为一个三段论的艺术。这其中有一个明显的原因，即音乐不是由词语构成的，而三段论（按亚里士多德的定义，阿里·法拉比 在 [三段论] 19.8 中引用）是一种言语表达（qawl）的形式。但这仅要求前提是言语的；根据亚里士多德的定义和阿里·法拉比 在上文第 7 节中引用的定义，结论只是一个“事物”。在亚里士多德那里，实践三段论的结论是一个行动，而不是一个命题。 （因此亚里士多德，《动物运动论》7，701a11：“这两个命题导致一个行动的结论”。）

在什么意义上诗歌包含着迫使人们对之前不知道的事情表示同意的词语？阿里·法拉比 在三篇论文中讨论了这个问题：《规范》、《诗歌》和《比例》。在所有这些论文中，他的部分答案是诗歌激发了我们的想象力，从而说服我们采取某些态度。

这将“三段论”理解得非常宽泛。但在阿里·法拉比的这三部作品中，他也指出诗歌包含着“潜在地”是三段论的文本。他唯一的例子，在《比例》505.22f 中，产生了无效的第二种形式的范畴三段论，如下所示（参见 Aouad & Schoeler 2002）：

1. 这个人很美丽。太阳很美丽。因此这个人就是太阳。[28]
2. 火行动迅速。剑行动迅速，即用来杀戮。因此剑就是火。

他的观点可以通过几种不同的方式来揭示；读者可能会有比我们更好的建议。但至少在这里有一种阿里·法拉比文本的阅读。考虑以下阿布·塔玛姆的一行诗，写于 838 年以庆祝一次军事胜利：

> 知识就在两支军队之间闪耀的明亮长矛中。 (翻译自 Stetkevych 2002: 156)

阅读这段文字时，我们意识到诗人告诉我们，武器通过决定性的胜利给予了确定性。知识是给予确定性的东西。因此，我们可以通过它所引起的联想来扩展这段文字：

> 长矛给予确定性。知识给予确定性。因此长矛就是知识。

这不是演绎推理。这是对诗歌引发我们思维的思考的表达。明确指出，我们被引导去欣赏这场战斗解决了局面的事实。这段文字就像阿里·法拉比上面的两个例子一样，是一个无效的三段论，但它处于第二种情况的事实实际上并不重要。

请注意，在这种情况下，诗歌提供了结论，而不是前提。因此，它符合布莱克（1990 年：226）的描述：“最终成品中出现的不是一个诗意的三段论...而仅仅是它的结论”。听众是否应该通过心灵感应来重建诗人的前提？布莱克将这个问题留给我们自己解决，但事实上，这是我们之前遇到过的问题。在[Syllogism] 19.10f 中，阿里·法拉比告诉我们，

> 一个三段论只是为了达到之前定义的问题而构成的；问题首先被假定，然后通过三段论来验证它。

这适用于各种情境，例如，在辩论中，提问者的任务是找到一个三段论来驳斥回答者的主张；“探讨总是关于一个尚未找到三段论的问题”（[辩论] 45.6）。在诗歌情况下，听众的任务是找到一个产生诗意三段论的中项；在我们的例子中，中项是“给予确定性”。而且，前提通过想象的建议而不是推理得出结论。

阿里·法拉比 的诗学三段论描述仍存在一些问题。首先，他从未说过只有结论出现在诗中。Black (1990: 235–8) 暗示实践三段论可能是理解其他情况的关键。

其次，为什么阿里·法拉比 在 [规范] 268.15 中说“诗歌言论在所有情况下都是绝对错误的”？这是一个严重夸张，这引发了一个问题，即诗人是否真的期望观众对一连串谎言表示同意。看来我们可以解决这个问题。阿里·法拉比 在 [规范] 中的陈述是典型的新柏拉图主义者口号，见证了亚历山大的伊利亚斯（在《范畴论序言》117.2–5）。

> 无论是所有前提（三段论）都是真实的，使其成为演绎的，还是它们都是虚假的，使其成为诗意和神话，或者一些是真实的，一些是虚假的。

阿里·法拉比自己更加成熟的观点，在[比例] 506.3f 中是

> 诗歌的目的既不是说谎也不是不说谎；它的目的和目标是唤起想象力和灵魂的激情。

即使有些明显的谎言发生，我们肯定会承认（尽管阿里·法拉比本人在我们手头的文本中从未明确说过）虚假陈述仍可能包含诗意真理。

## Bibliography

### Works of Al-Fārābī

* \[_Analysis_] _Kitāb al-taḥlīl_ (_Analysis_), in \[_Logic_] 2: 95–129.
* \[_Aristotle_] “The philosophy of Aristotle, the parts of his philosophy, the ranks of order of its parts, the position from which he started and the one he reached.” Translated by Mohsin Mahdi from an Arabic manuscript in Istanbul. Mahdi’s translation is Part III of his book Mahdi 2001 (below).
* \[_Attainment_] _Kitāb taḥṣīl al-saʻāda_, Jaʻfar Āl Yāsīn (ed.), Beirut: Dār al-Andalus, 1981; translated as “The Attainment of Happiness” in Mahdi 2001: 13–50.
* \[_Canons_] “Fārābī’s Canons of Poetry”, A. J. Arberry (ed./trans.), 1938, _Rivista degli Studi Orientali_, 17(2): 266–278. (Arabic and translation.)
* \[_Catalogue_] _Iḥṣā al-‘ulūm_ (_Catalogue of the sciences_), ‘Uthmān Amīn (ed.), Paris: Dar Bibliyun, 2005. Latin translations (1) by Gundisalvi in Jakob Schneider, _De Scientiis: secundum versionem Dominici Gundisalvi_, Herder, Freiburg 2006 (includes German translation); (2) by Gerard of Cremona in Alain Galonnier, _Le De Scientiis Alfarabii de Gérard de Crémone: Contribution aux problèmes de l’acculturation au XIIe siècle (édition et traduction du texte)_, Turnhout: Brepols, 2017 (includes English translation).
* \[_Categories_] _Kitāb al-qāṭāghūrīyās ayy al-maqūlāt_, in \[_Logic_] 1: 89–131; translated in D. M. Dunlop, 1958/1959, in “Al-Fārābī’s paraphrase of the _Categories_ of Aristotle”, _Islamic Quarterly_, 4: 168–197 and 5(1): 21–54.
* \[_Certainty_] _Sharā’iṭ al-yaqīn_ (_Conditions of certainty_), in _Kitāb al-burhān wa-kitāb al-sharā’iṭ al-yaqīn_ (_Book of demonstration and book of conditions of certainty_), Majid Fakhry (ed.), Beirut: Dar el-Machreq, 1986, pp. 97–104.
* \[_Commentary on Categories_] “Al-Fārābī’s _Long Commentary_ on Aristotle’s _Categoriae_ in Hebrew and Arabic: A critical edition and English translation of the newly found extant fragments”, Mauro Zonta (ed./trans.), _Studies in Arabic and Islamic Culture II_, Binyamin Abrahamov (ed.), Ramat-Gan: Bar-Ilan University Press, 2006, pp. 185–253.
* \[_Commentary on De Interpretatione_] _Sharḥ ‘ibāra_, W. Kutsch and S. Marrow (eds.), Beirut: Dar el-Machreq, 1986; translated in Zimmermann 1981: 1–219. We cite the page and line numbers in the Kutsch and Marrow edition; Zimmermann’s translation gives these too.
* \[_Commentary on Prior Analytics_] _Kitāb sharḥ al-qiyās_, surviving part in _Almanṭiqīyāt lil-Fārābī_ (_The logical works of al-Fārābī_), M. T. Daneshpazhuh (ed.), Qum: Maktabat Ayat Allah, 1988, vol. 2, pp. 263–553.
* \[_Debate_] _Kitāb al-jadal_ (_Debate_), in \[_Logic_] 3: 13–107. A fuller text is translated in David M. DiPasquale, _Alfarabi’s Book of Dialectic (Kitab al-Jadal): On the Starting Point of Islamic Philosophy_, Cambridge: CUP, 2019. (DiPasquale’s references to the Arabic text are to an edition not generally available.)
* \[_Demonstration_] _Kitāb al-burhān_ (_Demonstration_), in _Kitāb al-burhān wa-kitāb al-sharā’iṭ al-yaqīn_ (_Book of demonstration and book of conditions of certainty_), Majid Fakhry (ed.), Beirut: Dar el-Machreq, 1986, pp. 19–96.
* \[_Eisagoge_] _Kitāb al-īsāḡūjī_, in \[_Logic_] 1: 75–87; translated in D. M. Dunlop, 1956, “AlFārābī’s _Eisagoge_”, _Islamic Quarterly_, 3(2): 117–138.
* \[_Expressions_] _Kitāb al-alfāẓ al-musta‘mala fī al-manṭiq_ (_Expressions used in logic_), Muhsin Mahdi (ed.), Beirut: Dar el-Machreq, 1968.
* \[_Harmony_] Al-Fārābī(?), _L’armonia delle opinioni dei due sapienti: il divino Platone e Aristotele_ (_Al-jam‘ bayna ra’yay al-ḥakīmayn aflaṭūn al-ilāhī wa-arisṭūṭālīs_), Cecilia Martini Bonadeo (ed.), Pisa: Plus, 2009; English translation in Charles E. Butterworth, _Alfārābī, The Political Writings, ‘Selected Aphorisms’ and other texts_, Ithaca, NY: Cornell University Press, 2001, pp. 117–167.
* \[_Indication_] _Risālat al-tanbīh ‘alā sabīl al-sa‘āda_ (_Indication of the way to happiness_), S. Khālīfāt (ed.), Amman: Jami‘at al-urdunīya, 1987; translated in Jon McGinnis and David C. Reisman, _Classical Arabic Philosophy: An Anthology of Sources_, Indianapolis: Hackett, 2007, pp. 104–120.
* \[_Interpretation_] _Kitāb pārī armīniyās ayy al-‘ibāra_, in \[_Logic_] 1: 133–163; translated in Zimmermann 1981: 220–247.
* \[_Introductory_] _Al-tawṭi’a aw al-risāla ṣuddira bihā al-manṭiq_ (_Preparation, or essay introducing logic_), in \[_Logic_] 1: 55–62; translated in D. M. Dunlop, 1957, “Al-Fārābī’s Introductory _Risālah_ on logic”, _Islamic Quarterly_, 3: 224–235.
* \[_Letters_] _Kitāb al-ḥurūf_ (_Book of letters_), Muhsin Mahdi (ed.), Beirut: Dar al-Machreq, 1990. Paragraphs (108) to (157) are translated in Khalidi 2005, pp. 1–26. A new edition with full translation by Charles E. Butterworth is near publication. We cite this work in the style (_m_) _n_._p_, where _m_ is the paragraph number, _n_ the page number and _p_ the line number, all as in Mahdi’s edition; Khalidi gives the paragraph numbers.
* \[_Logic_] _Al-manṭiq ‘inda al-Fārābī_ (_The Logic of al-Fārābī_), Rafiq al-‘Ajam (ed.), Beirut: Dar al-Machreq, vol. 1 1985, vols. 2 and 3 1986.
* \[_Music_] _Kitāb al-Mūsīqā al-kabīr_ (_Great Book of Music_), Ghattas ‘Abd-al-Malik Khashaba (ed.), Cairo: Dār al-Kātib al-‘arabī li-al-ṭibā‘a wa-al-nashr, 1967; French translation in Rudolphe D’Erlanger, 1930, _La Musique Arabe_, vols 1, 2 (of six), Paris: Librairie Orientaliste Paul Geuthner.
* \[_Poetry_] “Kitāb al-shi‘r”, M. Mahdi (ed.), _Shi‘r_, 1959: 90–95; English translation in Geert Jan van Gelder and Marlé Hammond, 2008, _Takhyīl: The Imaginary in Classical Arabic Poetics_, Warminster: Gibb Memorial Trust: 15–18; French translation in Benmakhlouf et al. 2007: 112–118.
* \[_Proportion_] _Qawl al-Fārābī fī al-tanāsub wa-al-ta’līf_ (_About proportion and composition_), in M. T. Daneshpazhuh (ed.), _Al-manṭiqīyāt lil-Fārābī_ (_The logical works of alFārābī_), Qum: Maktabat Ayat Allah, Qum, 1988, vol. 1, pp. 504.1–506.6; French translation in Benmakhlouf et al. 2007: 107–111.
* \[_Rhetoric_] _Kitāb al-Ḫaṭāba_, in J. Langhade and M. Grignaschi (eds.), _Al-Fārābī, deux ouvrages inédits sur la rhétorique_, Beirut: Dar el-Machreq, 1971, pp. 30–121.
* \[_Sections_] _Al-fuṣūl al-khamsa_ (_The five sections_), in \[_Logic_] 1: 63–73; translated in D. M. Dunlop, 1955, “Al-Fārābī’s Introductory Sections on Logic”, _Islamic Quarterly_, 2: 264–282.
* \[_Short Syllogism_] _Kitāb al-qiyās al-ṣaḡīr_ (Also known as _Logic of the Theologians_) in _Al-manṭiq ‘inda al-Fārābī_ (_The Logic of al-Fārābī_), R. al-‘Ajam (ed.), Beirut: Dar al-Mashreq, 1986, vol 2, pp. 65–93; translated in Nicholas Rescher, “Al-Fārābī’s short commentary on Aristotle’s _Prior Analytics_”, Pittsburgh, PA: University of Pittsburgh Press, 1963.
* \[_Sophistry_] _Kitāb al-amkinati al-muḡliṭa_ (_Situations of sophistry_), in \[_Logic_] 2: 131–164.
* \[_Syllogism_] _Kitāb al-qiyās_ (_Syllogism_), in \[_Logic_] 2: 11–64; translated in Saloua Chatti and Wilfrid Hodges, 2020, _Al-Fārābī, Syllogism_, (Ancient Commentators on Aristotle), London: Bloomsbury Academic.

### Works of Other Authors

* Abed, Shukri B., 1991, _Aristotelian Logic and the Arabic Language in Alfārābī_, Albany: State University of New York Press.
* Adamson, Peter, 2006, “The Arabic Sea Battle: Al-Fārābī on the Problem of Future Contingents”, _Archiv Für Geschichte Der Philosophie_, 88(2): 163–188. doi:10.1515/AGPH.2006.007
* Alon, Ilai and Shukri Abed, 2007, _Al-Fārābī’s Philosophical Lexicon_, two volumes, Cambridge: E. J. W. Gibb Memorial Trust.
* Aouad, Maroun, 1992, “Les Fondements de la _Rhétorique_ d’Aristote Reconsidérés Par Fārābi, Ou Le Concept de Point de Vue Immédiat et Commun”, _Arabic Sciences and Philosophy_, 2(1): 133–180. doi:10.1017/S0957423900001582
* Aouad, Maroun and Gregor Schoeler, 2002, “The Poetic Syllogism According to Al-Farabi: An Incorrect Syllogism of the Second Figure”, _Arabic Sciences and Philosophy_, 12(2): 185–196. doi:10.1017/S0957423902002096
* Benmakhlouf, Ali, Stéphane Diebler, and Pauline Koetscher (eds.), 2007, _Al-Fārābī, Philosopher à Bagdad au Xe siècle_, Paris: Seuil.
* Black, Deborah L., 1989, “The ‘Imaginative Syllogism’ in Arabic Philosophy: A Medieval Contribution to the Philosophical Study of Metaphor”, _Mediaeval Studies_, 51: 242–267. doi:10.1484/J.MS.2.306851
* –––, 1990, _Logic and Aristotle’s Rhetoric and Poetics in Medieval Arabic Philosophy_, (Islamic philosophy and theology, 7), Leiden: Brill.
* –––, 2006, “Knowledge (_‘Ilm_) and Certitude (_yaqin_) in al-Farabi’s Epistemology”, _Arabic Sciences and Philosophy_, 16(1): 11–45. doi:10.1017/S0957423906000221
* Boethius \[c. 477–524 CE], 1969, _De Hypotheticis Syllogismis_, Luca Obertello (ed.), Brescia: Paideia.
* Chase, Michael, 2007, “Did Porphyry Write a Commentary on Aristotle’s _Posterior Analytics_? Albertus Magnus, al-Fārābī, and Porphyry on _per se_ predication”, in _Classical Arabic Philosophy: Sources and Reception_, Peter Adamson (ed.), London: Warburg Institute, pp. 21–38.
* Chatti, Saloua, 2017, “The semantics and pragmatics of the conditional in al-Fārābī’s and Avicenna’s theories”, _Studia Humana_, 6(1): 5–17.
* –––, 2019, _Arabic Logic from Al-Fārābī to Averroes: A Study of the Early Arabic Categorical, Modal, and Hypothetical Syllogistics_, Cham, Switzerland: Birkhäuser.
* Cross, Charles and Floris Roelofsen, 2018, “Questions”, _The Stanford Encyclopedia of Philosophy_ (Spring 2018 Edition), Edward N. Zalta (ed.), URL = [https://plato.stanford.edu/archives/spr2018/entries/questions/](https://plato.stanford.edu/archives/spr2018/entries/questions/).
* D’Ancona, Cristina, 2022, “Greek Sources in Arabic and Islamic Philosophy”, _The Stanford Encyclopedia of Philosophy_ (Spring 2022 Edition), Edward N. Zalta (ed.), URL = [https://plato.stanford.edu/archives/spr2022/entries/arabic-islamic-greek/](https://plato.stanford.edu/archives/spr2022/entries/arabic-islamic-greek/).
* D’Arcy, Guillaume de Vaulx, 2010, “La _Naqla_, Étude du Concept de Transfert dans L’œuvre d’al-Fārābī”, _Arabic Sciences and Philosophy_, 20(1): 125–176. doi:10.1017/S0957423909990129
* Diebler, Stéphane, 2005, “Catégories, conversation et philosophie chez al-Fārābī”, in _Les Catégories et leur Histoire_, Ole Bruun and Lorenzo Corti (eds.), Paris: Vrin, pp. 275–305.
* Druart, Thérèse-Anne, 2007, “Al-Fārābī, the categories, metaphysics, and the Book of Letters”, _Medioevo, Rivista di storia della filosofia medievale_, 32: 15–37.
* –––, 2011, “Al-Fārābī: an Arabic account of the Origin of Language and of Philosophical Vocabulary”, _Proceedings of the American Catholic Philosophical Association_, 84: 1–17. doi:10.5840/acpaproc2010841
* –––, 2015/6, “Why music matters for language and interpretation: al-Fārābī”, _Mélanges de l’Université Saint-Joseph_, 66: 167–179.
* –––, 2021, “Al-Fārābī”, _The Stanford Encyclopedia of Philosophy_ (Winter 2021 Edition), Edward N. Zalta (ed.), URL = [https://plato.stanford.edu/archives/win2021/entries/al-farabi/](https://plato.stanford.edu/archives/win2021/entries/al-farabi/)
* –––, 2022, “Al-Fārābī: The happy marriage between logic and music”, _Ishraq_, 10: 72–88.
* Elamrani-Jamal, Abdelali, 1983, _Logique Aristotélicienne et Grammaire Arabe_, (Études musulmanes, 26 ), Paris: Vrin.
* Elias \[attr., 6th century CE], 1890, _In Categorias Prooemium_, in Adolph Busse (ed.), _Eliae in Porphyrii Isagogen et Aristotelis Categorias Commentaria_, (Commentaria in Aristotelem Graeca 18.1), Berlin: Reimer, pp. 107–134. \[[_In Categorias Prooemium_ available online](https://archive.org/details/inporphyriiisago00elia/page/106)] See also entry on [Elias](https://plato.stanford.edu/entries/elias/).
* Eskenasy, Pauline E., 1988, “Al-Fārābī’s Classification of the Parts of Speech”, _Jerusalem Studies in Arabic and Islam_, 11: 55–82.
* Fakhry, Majid, 2002, _Al-Fārābī: Founder of Islamic Neoplatonism, His Life, Works and Influence_, London: Oneworld.
* Fallahi, Asadollah, 2019, “Fārābī and Avicenna on contraposition”, _History and Philosophy of Logic_, 40(1): 22–41.
* Farmer, Henry George, 1957, “The Music of Islam”, in Egon Wellesz (ed.), _Ancient and Oriental Music_, London: Oxford University Press, pp. 421–477.
* Fortenbaugh, William W., Pamela M. Huby, Robert W. Sharples and Dimitri Gutas (eds.), 1992, _Theophrastus of Eresus: Sources for his Life, Writings, Thought and Influence_, Leiden: Brill.
* Galen \[c. 129–c. 216 CE], 1896, _Institutio Logica_, Carolus Kalbfleisch (ed.), Leipzig: Teubner.
* Galston, Miriam S., 1981, “Al-Fārābī on Aristotle’s Theory of Demonstration”, in Parviz Morewedge (ed.), _Islamic Philosophy and Mysticism_, Delmar, NY: Caravan Books, pp. 23–34.
* –––, 1988, “Al-Fārābī et la logique Aristotélicienne dans la philosophie islamique”, in M. A. Sinaceur (ed.), _Aristote Aujourd’hui_, Paris: Érès, Unesco, pp. 192–217.
* García Cuadrado, José Angel, 2003, “La distinción nombre-verbo en los comentarios al _Perihermeneias_ de Al-Fārābī y Averroes”, _Revista Española de Filosofía Medieval_, 10: 157–169.
* Germann, Nadja, 2015, “Logic as the Path to Happiness: Al-Fārābī and the Divisions of the Sciences”, _Quaestio_, 15(January): 15–30. doi:10.1484/J.QUAESTIO.5.108587
* –––, 2015/6, “Imitation—ambiguity—discourse: some remarks on al-Fārābī’s philosophy of language”, _Mélanges de l’Université Saint-Joseph_, 66: 135–166.
* –––, 2022, “The power of words: al-Fārābī’s philosophy of language”, _Ishraq_, 10: 193–211.
* Giolfo, Manuela E. B. and Wilfrid Hodges, 2018, “Syntax, Semantics, and Pragmatics in al-Sīrãfī and Ibn Sīnã”, in Georgine Ayoub and Kees Versteegh (eds.), _Foundations of Arabic Linguistics III_, Leiden: Brill. doi:10.1163/9789004365216\*007
* Gottschalk, Hans B., 1990, “The Earliest Aristotelian Commentators”, in Richard Sorabji (ed.), _Aristotle Transformed: The Ancient Commentators and their Influence_, London: Duckworth, pp. 55–81.
* Grinaschi, Mario, 1972, “Les traductions latines des ouvrages de la logique arabe et l’abrégé d’Al-Fārābī”, _Archives d’Histoire doctrinale et littéraire du Moyen Age_, 47: 41–107.
* Günther, Sebastian, 2010, “The principles of instruction are the grounds of our knowledge; Al-Fārābī’s philosophical and al-Ghazālī’s spiritual approaches to learning”, in Osama Abi-Mershed (ed.), _Trajectories of Education in the Arab World: Legacies and Challenges_, London: Routledge, pp. 15–35.
* Gutas, Dimitri, 1983, “Paul the Persian on the classification of the parts of Aristotle’s philosophy: a milestone between Alexandria and Baġdâd”, _Der Islam: Zeitschrift für Geschichte und Kultur des Islamischen Orients_, 60(2): 231–267. Reprinted in Dimitri Gutas, _Greek Philosophers in the Arabic Tradition_, Aldershot: Ashgate 2000. doi:10.1515/islm.1983.60.2.231
* Gyekye, Kwame, 1972, “The Term _Istithnā’_ in Arabic Logic”, _Journal of the American Oriental Society_, 92(1): 88–92. doi:10.2307/599652
* –––, 1989, “Al-Fārābī on the Logic of the Arguments of the Muslim Philosophical Theologians”, _Journal of the History of Philosophy_, 27(1): 135–143. doi:10.1353/hph.1989.0001
* Haddad, Fuad S., 1969, “Alfārābī’s Views on Logic and Its Relation to Grammar”, _Islamic Quarterly_, 13: 192–207.
* –––, 1989, _Alfarabi’s Theory of Communication_, Beirut, Lebanon: American University of Beirut.
* Hasnawi, Ahmad, 1985, “Fārābī et la pratique de l’exégèse philosophique (remarques sur son Commentaire au De Interpretatione d’Aristote)”, _Revue de Synthèse (Paris)_, 106 (117): 27–59.
* –––, 1992, “Fārābī al-”, in _Encyclopédie Philosophique Universelle_, vol. 3, Les oeuvres philosophiques: dictionnaire dir. par Jean-François Nattei sous la direction de André Jacob, Paris: Presses Universitaires de France.
* –––, 2009, “Topique et syllogistique: la tradition arabe (Al-Fārābī et Averroès)”, in _Les lieux de l’argumentation: Histoire du syllogisme topique d’Aristote à Leibniz_ (Studia Aristarum, 22), Joël Biard and Fosca Mariani Zini (eds.), Turnhout: Brepols Publishers, pp. 191–226. doi:10.1484/M.SA-EB.4.00075
* –––, 2012, “L’objet du _De Interpretatione_ d’Aristote Selon Al-Fārābī”, in _Ad Notitiam Ignoti: L’ “Organon” Dans La “Translatio Studiorum” à l’époque d’Albert Le Grand_ (Studia Artistarum, 37), Julie Brumberg-Chaumont (ed.), Turnhout: Brepols Publishers, 259–283. doi:10.1484/M.SA-EB.5.101361
* Hasnawi, Ahmad and Wilfrid Hodges, 2016, “Arabic Logic up to Avicenna”, in _The Cambridge Companion to Medieval Logic_, Catarina Dutilh Novaes and Stephen Read (eds.), Cambridge: Cambridge University Press, 45–66. doi:10.1017/CBO9781107449862.003
* Hitchcock, David, 2017, _On Reasoning and Argument: Essays in Informal Logic and on Critical Thinking_ (Argumentation Library, 30), Cham: Springer International Publishing. doi:10.1007/978-3-319-53562-3
* Hodges, Wilfrid, 2012, “Formalizing the Relationship Between Meaning and Syntax”, in Markus Werning, Wolfram Hinzen, and Edouard Machery (eds.), _The Oxford Handbook of Compositionality_, Oxford: Oxford University Press, pp. 245–261.
* –––, 2018, “Proofs as Cognitive or Computational: Ibn Sı̄nā’s Innovations”, _Philosophy & Technology_, 31(1): 131–153. doi:10.1007/s13347-016-0242-2
* –––, 2019, “Remarks on al-Fārābī’s missing modal logic and its effect on Ibn Sı̄nā”, _Eshare: An Iranian Journal of Philosophy_, 1(3): 39–73.
* –––, 2020, “Medieval Arabic notions of algorithm: some further raw evidence”, in _Fields of Logic and Computation III, Essays Dedicated to Yuri Gurevich on the Occasion of his 80th Birthday_, ed. Andreas Blass, Patrick Cegielski, Nachum Dershowitz, Manfred Droste and Bernd Finkbeiner, Lecture Notes in Computer Science 12180: Springer, pp. 133–146.
* Hodges, Wilfrid and Manuela E. B. Giolfo, 2022, “Al-Fārābī against the grammarians??”, in Manuel Sartori and Francesco Binaghi (eds)., _The Foundations of Arab Linguistics V: Kitab Sibawayhi, The Critical Theory_, Leiden: Brill, pp. 157–178.
* Ibn Abī Uṣaybi‘a, Aḥmad ibn al-Qāsim \[d. 1270 CE], 1965, _Uyūn al-anbāʼ fī ṭabaqāt al-aṭibbāʼ_, Nizār Riḍā (ed.), Beirut: Dar Maktabat al-Hayah.
* Ibn al-Muqaffa‘ \[8th century CE], 1978, _Al-manṭiq_ (_Logic_), M. T. Dāneshpazhūh (ed.), Tehran: Iranian Institute of Philosophy.
* Ibn Miskawayh, Aḥmad bin Muḥammad \[932–1030 CE], 1987, _Tartīb al-sa‘ādah_ (_Ranking of Happiness_), Hungarian translation by Miklós Maróth as _A boldogságról: a boldogság könyve_, Budapest: Európa Könyvkiadó.
* Ibn Rushd \[= Averroes, 1126–1198 CE], 1983, _Maqālāt fī al-manṭiq wa-al-‘ilm al-ṭabī‘ī_ (_Essays on Logic and Natural Science_), Jamāl al-Dīn al-‘Alawī (ed.), Casablanca.
* Ibn Sīnā \[= Avicenna, ca. 980–1037 CE], 1951, _Al-madkhal_ (_Introduction_), M. El-Khodeiri, G. C. Anawati and F. El-Ahwani (eds.), Cairo: Nashr Wizāra al-Ma‘ārif al-‘Umũmiyya.
* –––, 1964, _Al-qiyās_ (_Syllogism_), S. Zayed (ed.), Cairo.
* –––, 2013, _Al-ta‘līqāt_ (_Annotations_), Seyyed Hossein Mousavian (ed.), Tehran: Iranian Institute of Philosophy.
* Karimullah, Kamran, 2014, “Alfarabi on Conditionals”, _Arabic Sciences and Philosophy_, 24(2): 211–267. doi:10.1017/S0957423914000022
* –––, 2017, “Influence of Late-Antique (ca. 200–800 A.D.) Prolegomena to Aristotle’s _Categories_ on Arabic Doctrines of the Subject Matter of Logic: Alfārābī (d. ca. 950 A.D.), Baghdad Peripatetics, Avicenna (d. 1037 A.D.)”, _Archiv Für Geschichte Der Philosophie_, 99(3): 237–299. doi:10.1515/agph-2017-0013
* Kemal, Salim, 2003, _The Philosophical Poetics of Alfarabi, Avicenna and Averroës: The Aristotelian Reception_, Abingdon: Routledge.
* Khalidi, Muhammad Ali (ed.), 2005, _Medieval Islamic Philosophical Writings_, Cambridge: Cambridge University Press. doi:10.1017/CBO9780511811050
* Kleven, Terence J., 2013, “Alfārābī’s Commentary on Porphyry’s Isagoge (_Kitāb īsāgūjī_)”, _Schede Medievali_, 51: 41–52.
* –––, 2013/4, “Al-Fārābī’s introduction to the five rational arts with reference to _The Five Aphorisms_ (_al-Fusūl al-Khamsa)_ and Ibn Bājja’s _Comments_ (_Ta‘ālīq_)”, _Mélanges de l’Université Saint-Joseph_, 65: 165–194.
* –––, 2023, “Al-Farabi on what is known prior to the syllogistic arts in his Introductory _Letter_, the _Five Aphorisms_, and the _Book of Dialectic_”, in Katja Krause et al. (eds.), _Contextualising Premodern Philosophy: Explorations of the Greek, Hebrew, Arabic, and Latin traditions_, New York/London: Routledge, pp. 276–292.
* Knuuttila, Simo, 2020, “Medieval Theories of Future Contingents”, _The Stanford Encyclopedia of Philosophy_ (Summer 2020 Edition), Edward N. Zalta (ed.), URL = [https://plato.stanford.edu/archives/sum2020/entries/medieval-futcont/](https://plato.stanford.edu/archives/sum2020/entries/medieval-futcont/)
* Kolman, Oren, 2004, “Transfer principles for generalized interval systems”, _Perspectives of New Music_, 42(1): 150–190.
* Lameer, Joep, 1994, _Al-Fārābī and Aristotelian Syllogistics_, Leiden: Brill.
* –––, 1997, “From Alexandria to Baghdad: reflections on the genesis of a problematical tradition”, in _The Ancient Tradition in Christian and Islamic Hellenism. Studies on the transmission of Greek philosophy and sciences dedicated to HJ Drossaart Lulofs on his ninetieth birthday_, Leiden: CNWS Publications 50, pp. 181–191.
* –––, 2006, _Conception and Belief in Sadr al-Din Shirazi (ca. 1571–1635)_, Tehran: Iranian Institute of Philosophy.
* Langhade, Jacques, 1994, _Du Coran à la philosophie: La langue arabe et la formation du vocabulaire philosophique de Fārābī_, Damascus: Institut Français de Damas.
* Lee, Tae-Soo, 1984, _Die Griechische Tradition der Aristotelischen Syllogistik in der Spätantike_, Göttingen: Vandenhoeck and Ruprecht.
* López-Farjeat, Luis Xavier, 2020, “Al-Farabi’s Psychology and Epistemology”, _The Stanford Encyclopedia of Philosophy_ (Summer 2020 Edition), Edward N. Zalta (ed.), URL = [https://plato.stanford.edu/archives/sum2020/entries/al-farabi-psych/](https://plato.stanford.edu/archives/sum2020/entries/al-farabi-psych/)
* MacFarlane, John, 2017, “Logical Constants”, _The Stanford Encyclopedia of Philosophy_ (Winter 2017 Edition), Edward N. Zalta (ed.), URL = [https://plato.stanford.edu/archives/win2017/entries/logical-constants/](https://plato.stanford.edu/archives/win2017/entries/logical-constants/)
* Mahdi, Muhsin, 2001, _Alfarabi, Philosophy of Plato and Aristotle_, Ithaca, NY: Cornell University Press.
* Mallet, Dominique, 1994, “Le _kitāb al-taḥlīl_ d’Alfarabi”, _Arabic Sciences and Philosophy_, 4(2): 317–335. doi:10.1017/S0957423900001260
* –––, 1996, “_Kalām_ et dialectique dans le commentaire des _Topiques_ d’Alfārābī”, _Bulletin d’études orientales_, 48: 165–182.
* Menn, Stephen, 2008, “Al-Fārābī’s _Kitāb Al-Ḥurūf_ and His Analysis of the Senses of Being”, _Arabic Sciences and Philosophy_, 18(1): 59–97. doi:10.1017/s0957423908000477
* Netton, Ian Richard, 1992, _Al-Fārābī and his School_, Richmond: Curzon Press.
* Paul the Persian \[6th century CE], 1875, _Logica_, in Jan Pieter Nicolaas Land (ed.), _Anecdota Syriaca_ Vol. 4, Brill, Lugdunum Batavorum (Katwijk), pp. 1–30.
* Philoponus, John \[6th century CE], 1905, _In Aristotelis Analytica Priora Commentaria_, Maximilianus Wallies (ed.), Berlin: Reimer.
* Porphyry \[c. 234–c. 305 CE], 1887, _Porphyrii Isagoge et In Aristotelis Categorias Commentarium_, Adolfus Busse (ed.), Berlin: Reimer.
* Pourjavady, Reza and Sabine Schmidtke, 2015, “An Eastern Renaissance? Greek Philosophy under the Safavids (16th–18th Centuries AD)”, _Intellectual History of the Islamicate World_, 3(1–2): 248–290. doi:10.1163/2212943X-00301010
* Rashed, Marwan, 2009, “On the Authorship of the Treatise on the Harmonization of the Opinions of the Two Sages Attributed to Al-Fārābī”, _Arabic Sciences and Philosophy_, 19(1): 43–82. doi:10.1017/S0957423909000587
* –––, 2020, “Abū Hāšim al-Ğubbā’ī, algèbre et inférence”, _Arabic Sciences and Philosophy_, 30(2): 191–228.
* Rauf, Muhammad, Mushtaq Ahmad, and Zafar Iqbal, 2013, “Al-Fārābī’s Philosophy of Education”, _Educational Research International_, 1(2): 85–94. \[[Rauf, Ahmad, and Iqbal 2013 available online](http://www.erint.savap.org.pk/PDF/Vol.1\(2\)/ERInt.2013\(1.2%E2%80%9309\).pdf)]
* Rescher, Nicholas, 1966, _Galen and the Syllogism_, Pittsburgh, PA: University of Pittsburgh Press.
* –––, 1968, _Studies in Arabic Philosophy_, Pittsburgh PA: University of Pittsburgh Press.
* Schacht, Joseph and Max Meyerhof, 1937, “Maimonides against Galen, on Philosophy and Cosmogony”, _Bulletin of the Faculty of Arts of the University of Egypt_ (_Majallat Kulliyat alĀdāb bi-al-Jāmi‘a al-Miṣriyya_), 5: 53–88.
* Schoeler, Gregor, 1983, “Der poetische Syllogismus: Ein Beitrag zum Verständnis der ‘logischen’ Poetik der Araber”, _Zeitschrift der Deutschen Morgenländischen Gesellschaft_, 133(1): 43–92.
* Smith, Robin, 2022, “Aristotle’s Logic”, _The Stanford Encyclopedia of Philosophy_ (Winter 2022 Edition), Edward N. Zalta and Uri Nodelman (eds.), URL = <[https://plato.stanford.edu/archives/win20228/entries/aristotle-logic/](https://plato.stanford.edu/archives/win2022/entries/aristotle-logic/)>
* Stetkevych, Suzanne Pinckney, 2002, _The Poetics of Islamic Legitimacy: Myth, Gender, and Ceremony in the Classical Arabic Ode_, Bloomington, IN: Indiana University Press.
* Street, Tony, 2001, “‘The Eminent Later Scholar’ in Avicenna’s Book of the Syllogism”, _Arabic Sciences and Philosophy_, 11(2): 205–218. doi:10.1017/S0957423901001096
* Street, Tony and Nadja Germann, 2021, “Arabic and Islamic Philosophy of Language and Logic”, _The Stanford Encyclopedia of Philosophy_ (Spring 2021 Edition), Edward N. Zalta (ed.), URL = [https://plato.stanford.edu/archives/spr2021/entries/arabic-islamic-language/](https://plato.stanford.edu/archives/spr2021/entries/arabic-islamic-language/)
* Strobino, Riccardo, 2019, “Varieties of demonstration in Alfarabi”, _History and Philosophy of Logic_, 40(1): 42–62.
* –––, 2021, _Avicenna’s Theory of Science: Logic, Metaphysics, Epistemology_, Oakland, California: University of California Press.
* Szabó, Zoltán Gendler, 2022, “Compositionality”,_The Stanford Encyclopedia of Philosophy_ (Fall 2022 Edition), Edward N. Zalta and Uri Nodelman (eds.), URL = [https://plato.stanford.edu/archives/fall2022/entries/compositionality/](https://plato.stanford.edu/archives/fall2022/entries/compositionality/)
* Thom, Paul, 2008, “Al-Fārābī on Indefinite and Privative Names”, _Arabic Sciences and Philosophy_, 18(2): 193–209. doi:10.1017/S0957423908000544
* Türker, Sadık, 2007, “The Arabico-Islamic Background of Al-Fārābī’s Logic”, _History and Philosophy of Logic_, 28(3): 183–255. doi:10.1080/01445340701223423
* Vallat, Philippe, 2004, _Fārābī et l’École d’Alexandrie: Des prémisses de la connaissance à la philosophie politique_, Paris: Vrin.
* Versteegh, Kees, 1997, _Landmarks in Linguistic Thought III: The Arabic Linguistic Tradition_, London: Routledge.
* –––, 2000, “Grammar and Logic in the Arabic Grammatical Tradition”, in Sylvain Auroux, Konrad Koerner, Hans-Josef Niederehe, and Kees Versteegh (eds.), _Handbuch für die Geschichte der Sprach-und Kommunikationswissenschaft I_, Berlin and New York: Mouton de Gruyter, pp. 300–306.
* Walton, Douglas N., 1989, _Informal Logic: a Handbook for Critical Argument_, New York: Cambridge University Press.
* Walzer, Richard, 1962, _Greek into Arabic: Essays on Islamic Philosophy_, London: Cassirer.
* Watt, John W., 2008, “Al-Fārābī and the History of the Syriac Organon”, in _Malphono w-Rabo d-Malphone: Studies in Honor of Sebastian P. Brock_, George A. Kiraz (ed.), Piscataway, NJ: Gorgias Press, pp. 703–731.
* Wildberg, Christian, 2021, “Neoplatonism”, _The Stanford Encyclopedia of Philosophy_ (Winter 2021 Edition), Edward N. Zalta (ed.), URL = [https://plato.stanford.edu/archives/win2021/entries/neoplatonism/](https://plato.stanford.edu/archives/win2021/entries/neoplatonism/)
* Woerther, Frédérique, 2022, “Définitions et usages de la rhétorique d’après les Didascalia in Rethoricam Aristotilis ex Glosa Alpharabii”, _Ishraq_, 10: 327–346.
* Wolfson, Harry Austryn, 1973, “The Terms _taṣawwur_ and _taṣdīq_ in Arabic philosophy and their Greek, Latin and Hebrew Equivalents”, in Isidore Twersky and George H. Williams (eds.), _Studies in the History and Philosophy of Religion_, Cambridge, MA: Harvard University Press, vol. 1, pp. 478–492.
* Young, Walter Edward, 2017, _The Dialectical Forge: Juridical Disputation and the Evolution of Islamic Law_ (Logic, Argumentation & Reasoning, 9), Cham: Springer International Publishing. doi:10.1007/978-3-319-25522-4
* Zimmermann, F. W., 1972, “Some Observations on Al-Fārābī and Logical Tradition”, in _Islamic Philosophy and the Classical Tradition, Essays presented by his friends and pupils to Richard Walzer on his seventieth birthday_, S. M. Stern, Albert Hourani, and Vivian Brown (eds.), Oxford: Cassirer, pp. 517–546.
* –––, 1981, _Al-Fārābī’s Commentary and Short Treatise on Aristotle’s De Interpretatione_, Oxford: British Academy and Oxford University Press.
* Zonta, Mauro, 1998, “Al-Fārābī’s Commentaries on Aristotelian Logic: New Discoveries”, in _Philosophy and Arts in the Islamic World: Proceedings of the Eighteenth Congress of the Union Européenne des Arabisants et Islamisants_ (Orientalia Lovaniensia Analecta, 87), U. Vermeulen and D. De Smet (eds.), Leuven: Peeters, pp. 219–232.
* –––, 2011, “About Todros Todrosi’s Medieval Hebrew Translation of al-Fārābī’s Lost Long Commentary/Gloss Commentary on Aristotle’s _Topics_, Book VIII”, _History and Philosophy of Logic_, 32(1): 37–45. doi:10.1080/01445340.2010.506093

## Academic Tools

| ![sep man icon](https://plato.stanford.edu/symbols/sepman-icon.jpg) | [How to cite this entry](https://plato.stanford.edu/cgi-bin/encyclopedia/archinfo.cgi?entry=al-farabi-logic).                                                                      |
| ------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ![sep man icon](https://plato.stanford.edu/symbols/sepman-icon.jpg) | [Preview the PDF version of this entry](https://leibniz.stanford.edu/friends/preview/al-farabi-logic/) at the [Friends of the SEP Society](https://leibniz.stanford.edu/friends/). |
| ![inpho icon](https://plato.stanford.edu/symbols/inpho.png)         | [Look up topics and thinkers related to this entry](https://www.inphoproject.org/entity?sep=al-farabi-logic\&redirect=True) at the Internet Philosophy Ontology Project (InPhO).   |
| ![phil papers icon](https://plato.stanford.edu/symbols/pp.gif)      | [Enhanced bibliography for this entry](https://philpapers.org/sep/al-farabi-logic/) at [PhilPapers](https://philpapers.org/), with links to its database.                          |

## Other Internet Resources

* [Al-Fārābī at Islamic Philosophy Online, last update 2 September 2007.](http://www.muslimphilosophy.com/farabi)
* [“Fārābī ii. Logic” by Deborah L. Black, in _Encyclopaedia Iranica_, last update January 24 2012.](http://www.iranicaonline.org/articles/farabi-ii)
* [Mahadurot: Modular Hebrew Digitally Rendered Texts, editor-in-chief Yehuda Halper: retrieved 27 April 2023.](http://www.mahadurot.com/) The Hebrew texts online are critical editions of medieval texts. They include a translation of \[_Debate_], and a translation of \[_Sophistry_] is promised.

## Related Entries

[al-Farabi](https://plato.stanford.edu/entries/al-farabi/) | [al-Farabi: philosophy of society and religion](https://plato.stanford.edu/entries/al-farabi-soc-rel/) | [al-Farabi: psychology and epistemology](https://plato.stanford.edu/entries/al-farabi-psych/) | [Arabic and Islamic Philosophy, disciplines in: philosophy of language and logic](https://plato.stanford.edu/entries/arabic-islamic-language/) | [Arabic and Islamic Philosophy, historical and methodological topics in: Greek sources](https://plato.stanford.edu/entries/arabic-islamic-greek/) | [Aristotle, General Topics: logic](https://plato.stanford.edu/entries/aristotle-logic/) | [compositionality](https://plato.stanford.edu/entries/compositionality/) | [future contingents: medieval theories of](https://plato.stanford.edu/entries/medieval-futcont/) | [Ibn Rushd \[Averroes\]](https://plato.stanford.edu/entries/ibn-rushd/) | [Ibn Sina \[Avicenna\]: logic](https://plato.stanford.edu/entries/ibn-sina-logic/) | [logical constants](https://plato.stanford.edu/entries/logical-constants/) | [Neoplatonism](https://plato.stanford.edu/entries/neoplatonism/) | [questions](https://plato.stanford.edu/entries/questions/)

### Acknowledgments

The first author takes responsibility for the text of this entry but couldn’t possibly have written it without the unfailing encouragement, advice and wisdom of the second author.

[Copyright © 2023](https://plato.stanford.edu/info.html#c) by\
[Wilfrid Hodges](http://wilfridhodges.co.uk/) <[_wilfrid.hodges@btinternet.com_](mailto:wilfrid%2ehodges%40btinternet%2ecom)>\
\[Therese-Anne Druart]\(http://philosophy.cua.edu
