# 亚里士多德的评论者 Aristotle, commentators on (Andrea Falcon)

_首次发表于 2005 年 8 月 11 日；实质性修订于 2021 年 9 月 24 日_

希腊后期至晚期古代的哲学表达方式之一是哲学评论。在这个时期，柏拉图和亚里士多德被视为哲学权威，他们的作品受到了深入研究。本文简要介绍了希腊后期对亚里士多德哲学兴趣的复兴如何最终发展成一种新的文学形式：哲学评论。它还追溯了评论传统在晚期古代的发展，以解释这一传统的活力和丰富性。特别强调了对《范畴学》的古代研究，因为这篇简短但难以理解的论文在后希腊时期的亚里士多德复兴中起到了核心作用。读者应该记住，以评论形式研究亚里士多德并不意味着停止真正的哲学思考。相反，哲学家通常使用评论格式不仅来阐述亚里士多德的作品，还作为原创哲学理论的载体。这种方法的一个结果是，至少在开始阶段，回归亚里士多德并不涉及接受任何明确的教义。更直接地说（也更大胆地说）：公元前 1 世纪的亚里士多德学派哲学家并不被期望接受任何明确的教义。当时亚里士多德被视为权威，不是因为他不容置疑，而是因为他值得仔细研究。

* [1. 引言](https://plato.stanford.edu/entries/aristotle-commentators/#Int)
* [2. 评论传统的前史：公元前 1 世纪《范畴学》的命运](https://plato.stanford.edu/entries/aristotle-commentators/#PreHisComTraForCat1stCenBCE)
* [3. 亚历山大·阿弗罗迪西亚斯和公元 1 至 2 世纪的亚里士多德传统](https://plato.stanford.edu/entries/aristotle-commentators/#AleAphAriTra1st2ndCenCE)
* [4. 晚期古代的评论传统：《范畴论》在阅读课程中的地位](https://plato.stanford.edu/entries/aristotle-commentators/#ComTraLatAntPlaCatReaCur)
* [5. 对《心灵论》的解释性工作](https://plato.stanford.edu/entries/aristotle-commentators/#ExeLabDeAni)
* [6. 博伊修斯和拉丁传统](https://plato.stanford.edu/entries/aristotle-commentators/#BoeLatTra)
* [7. 结论](https://plato.stanford.edu/entries/aristotle-commentators/#Con)
* [参考文献](https://plato.stanford.edu/entries/aristotle-commentators/#Bib)
* [A. 翻译的一手资料](https://plato.stanford.edu/entries/aristotle-commentators/#PriSouTra)
* [B. 二手资料](https://plato.stanford.edu/entries/aristotle-commentators/#SecSou)
* [学术工具](https://plato.stanford.edu/entries/aristotle-commentators/#Aca)
* [其他互联网资源](https://plato.stanford.edu/entries/aristotle-commentators/#Oth)
* [相关条目](https://plato.stanford.edu/entries/aristotle-commentators/#Rel)

***

## 1. 介绍

评论的主要目的是解释一篇文本。通常，文本被分成引文（lemma 的复数形式）。引文是从文本中引用的内容，以便解释和解读。有时文本会被完整引用，有时只会引用开头部分。引文后面总是跟着对文本的分析。评论并不仅仅是针对哲学作品而写的。任何被视为权威的诗歌或科学作品都可以成为评论的对象。例如，对希波克拉底文集写评论的传统早在很早之前就开始了。尽管这个传统的大部分已经失传，但迦伦对希波克拉底文集写的评论却流传至今。迦伦是一位生活在公元 2 世纪下半叶的医生和作家。有趣的是，他的解释活动并不仅限于希波克拉底。迦伦还对亚里士多德的几部作品写了评论，特别是《解释学》、《前分析》、《后分析》和《范畴学》（迦伦，《论我自己的书》，XIX 47）。很可能，这些评论并不是为了出版，而只是为了供一小群朋友和学生使用。以下是迦伦关于他对《范畴学》的评论的说法：

> 对于亚里士多德的《十个范畴》这部作品，我以前没有写过任何评论，无论是为了自己还是为了他人；后来有人向我要求对这部作品提出的问题写一些评论，<我写了一篇评论>，并且明确要求他只向已经通过老师阅读过这本书的学生展示，或者至少已经开始阅读了其他一些评论，比如阿德拉斯托斯和阿斯帕修斯的评论（迦伦，《论我自己的书》，XIX 42，库恩，译者：辛格）。

迦伦明确表示，他将评论者视为一位教师和一位解释者，他的受众对象是学生。此外，正如迦伦所述，评论是一种教学工具，面向具有特定知识水平的学生。预期阅读迦伦关于《范畴学》的评论的学生并不是绝对的初学者。他们是那些之前已经通过老师学习过这篇论文或者通过一些更基础的评论了解了《范畴学》的学生。迦伦提到了阿德拉斯托斯和阿斯帕修斯所写的评论。通过这样做，他给我们展示了关于《范畴学》所写评论的丰富性。这些评论作为已经形成的解释实践的一部分，一个接一个地被写出来。大部分这些评论已经失传。我们已经没有了阿德拉斯托斯和阿斯帕修斯对《范畴学》所写的评论，也没有了迦伦对同一篇论文所写的评论。这个令人沮丧的（至少对我们来说）事实最终归因于这种文学作品的本质。每一代评论者都根据自己的理论关注点和倾向来阅读和解释亚里士多德的著作，然后被下一代评论者所取代。一个显著的例外是亚历山大·阿弗洛狄修斯，他是迦伦的年轻同时代人。他的评论之所以保存下来，是因为后来的评论者将其作为典范。但并非所有评论都流传至今。例如，亚历山大对《范畴学》写了一篇评论（辛普利修斯，《范畴学注释》，1.16）。这篇评论遭遇了与阿德拉斯托斯、阿斯帕修斯、迦伦和其他评论一样的命运。如今，它只以片段的形式保存下来，被纳入后来一代解释者对《范畴学》所写的评论中。

## 2. 评论传统的前史：公元前 1 世纪《范畴论》的命运

对亚里士多德的批判性研究始于希腊后期，并延续至随后的几个世纪，主要针对的是《范畴论》等在之前几个世纪中被大多数人忽视的作品。这种研究假设亚里士多德的著作以适应当时的兴趣和需求而存在。尽管我们所掌握的信息有限，但它强烈暗示公元前 1 世纪进行了一场激烈的编辑活动。提奥斯的阿佩利孔（Apellicon of Teos）是一位书籍爱好者而非哲学家，似乎在 90 年代制作了亚里士多德的著作副本（斯特拉波，《地理学》第 13 卷第 1 章第 54 节）；阿米苏斯的泰拉尼翁（Tyrannion of Amisus）是一位语法学家和亚里士多德的狂热爱好者（_philaristotelês_），他有机会接触到阿佩利孔的书籍，可能在 60 年代也做了同样的事情（但我们的来源并没有说他编辑了亚里士多德的著作；他们只说他“整理了亚里士多德的书籍”；斯特拉波，《地理学》第 13 卷第 1 章第 54 节；普鲁塔克，《苏拉》第 26 章第 468 B-C 节）。根据传统，这些编辑工作被罗得的安德洛尼科斯（Andronicus of Rhodes）超越，他被认为是第一本可靠的亚里士多德著作的编辑者；这个版本对后希腊时期回归亚里士多德产生了巨大影响。关于安德洛尼科斯及其所谓的亚里士多德版本的更多信息，请参阅

> [罗得的安德洛尼科斯补充说明。](https://plato.stanford.edu/entries/aristotle-commentators/supplement.html)

实际上，安德洛尼库斯不能完全对亚里士多德哲学的复兴负责。这种复兴至少在一定程度上是独立于他的。此外，对于《范畴论》在这一背景下的受欢迎程度的深入研究表明，对亚里士多德的回归采取了不同的形式，并涉及许多不一定相互关联的解释立场。最后，回归亚里士多德不一定意味着接受亚里士多德所陈述的观点，或者将其编纂成评论的形式。简而言之，在这个早期阶段，亚里士多德传统中没有正统观念（见 Moraux 1973 年，第 xii-xx 页，尤其是第 xvi-xvii 页）。

我们知道五位古代《范畴论》解释者（_palaioi exêgêtai_）的名字，他们的活动发生在公元前 1 世纪：安德洛尼库斯、博伊特斯、阿泰诺多罗斯、亚里斯顿和尤多罗斯（Simplicius，《范畴论注释》159.32-33）。很可能，在这五位哲学家中，只有西顿的博伊特斯写了一篇关于《范畴论》的评论。博伊特斯的活动可以追溯到公元前 1 世纪下半叶。Simplicius 将他的“逐字解释”与安德洛尼库斯的解释进行了对比，后者据说“对《范畴论》进行了释义”（Simplicius，《范畴论注释》29.28-30.5）。我们不能排除 Simplicius 将自己的文学约定投射到安德洛尼库斯和博伊特斯身上的可能性，也不能排除安德洛尼库斯和博伊特斯在两种不同的解释练习中从事。一方面，有一些明确的证据表明，安德洛尼库斯确实重新表述和澄清了亚里士多德的文本，以揭示亚里士多德的意图。另一方面，没有理由认为安德洛尼库斯的写作风格后来会被编纂成释义的形式。忒米斯提乌斯是古代这种特定解释形式的倡导者。忒米斯提乌斯没有提及前辈的名字。他的沉默可以有不同的解释：要么忒米斯提乌斯不知道安德洛尼库斯在《范畴论》上的工作，要么这个工作并不是以释义的形式写成的。幸运的是，我们无需确定安德洛尼库斯是否是亚里士多德的第一位释义者。重要的是，他的解释风格被认为与博伊特斯不同。与安德洛尼库斯不同，博伊特斯通过逐字评论的形式对整本《范畴论》进行了深入研究。博伊特斯在古代备受推崇。Simplicius 称他为“了不起的博伊特斯”（《范畴论注释》1.18），“高贵的博伊特斯”（《范畴论注释》379.32），并赞赏他“敏锐的思维”（《范畴论注释》1.23 和 434.18）。除了一些证言外，他的评论并没有保存下来。然而，从这些少数证言中，我们可以清楚地看出，博伊特斯关注斯多葛派对亚里士多德《范畴论》提出的困难。斯多葛派对《范畴论》的批评是基于这样的假设，即这篇论文是关于语言和语言表达的。他们认为，《范畴论》作为一篇关于语言和语言表达的论文是不完整的。博伊特斯反对了对《范畴论》的这种解读，并预见了一种解释路线，这种解释路线后来被包括波菲里在内的其他人所采用。

博伊特斯对《范畴论》的辩护证明了这篇论文受到了广泛的阅读和研究，不仅仅是在友好于亚里士多德的哲学家中。斯多葛派哲学家阿泰诺多罗斯被认为写了一本（Simplicius，《范畴论注释》86.22）或多本（波菲里，《范畴论注释》62.25-26）名为《反对亚里士多德的范畴论》的书。我们对阿泰诺多罗斯了解甚少。如果他是塔尔苏斯的阿泰诺多罗斯，他是奥古斯都皇帝的朋友和顾问，并被他任命为塔乌罗斯的总督，那么阿泰诺多罗斯活跃在公元前 1 世纪下半叶。没有令人信服的理由认为他对《范畴论》的批评是以评论的形式写成的。阿泰诺多罗斯可能只是提出了一系列的异议和困难，就像晚于一百多年的柏拉图主义者卢修斯和尼科斯特拉图斯所做的一样（Simplicius，《范畴论注释》1.18-20：“其他人选择简单地提出从文本中产生的一系列困难（_aporiai_），这就是卢修斯所做的，之后是尼科斯特拉图斯，他接手了卢修斯的工作”）。

对于亚历山大的尤多鲁斯，我们也不能将所有古代《范畴论》的解释者都归功于他的评论。一般认为尤多鲁斯在公元前 1 世纪中期繁荣起来。西姆普利修斯不仅仅将尤多鲁斯列为古代《范畴论》的解释者之一。他还保留了九个证词，其中尤多鲁斯与亚里士多德存在分歧。但是，我们无法推断出他的文学作品形式。唯一可以确定的是，《范畴论》的研究并不局限于亚里士多德学派。事实上，尤多鲁斯并不是一个亚里士多德学派的哲学家。西姆普利修斯将他称为“学院派的尤多鲁斯”（《范畴论》187.10）。

《范畴论》古代解释者名单上的最后一个名字是亚历山大的亚里斯托。根据西塞罗（《卢库卢斯》§§ 11-12）的记载，亚里斯托是在公元前 87 年亚历山大举行的会谈中出席的学者之一，会谈的参与者包括前克利托马库斯和拉里萨的赫拉克利图斯以及菲洛和拉里萨的安提阿科斯。西塞罗将亚里斯托描绘为安提阿科斯的学生。菲洛德莫斯在公元前 1 世纪写的《学院索引》也证实了亚里斯托是安提阿科斯的学生。在那里，我们被告知安提阿科斯“接管了<学院>”（XXXIV 34 Dorandi）。还列出了他的学生名单，其中包括安提阿科斯的兄弟阿里斯托、亚历山大的亚里斯托和迪奥，以及佩加莫的克拉提普斯（XXXV 1-6 Dorandi）。接下来的内容非常有趣：“亚历山大的亚里斯托和佩加莫的克拉提普斯背叛了学院，转向了亚里士多德学派”（XXXV 10-16 Dorandi）。我们并没有得知他们转向亚里士多德哲学的原因。对于亚里斯托的情况，很难看出这种背叛实际上涉及了什么。根据我们所掌握的信息，我们只能说他写过关于《范畴论》的内容。但没有证据表明亚里斯托以评论的形式写作。他的情况与安德洛尼库斯和博伊特斯显著不同。安德洛尼库斯和博伊特斯试图组织、澄清并捍卫亚里士多德的哲学。而这似乎并不适用于亚里斯托。他的名字从未与安德洛尼库斯和博伊特斯联系在一起。此外，没有证据表明亚里斯托知道安德洛尼库斯和博伊特斯的存在，或者他受到他们的作品或解释亚里士多德的活动的影响。安德洛尼库斯的版本在亚里斯托转向亚里士多德哲学的过程中起到了作用，这只是一种推测。

在随后的评论传统中，关于《范畴论》在公元前 1 世纪早期的接受情况有大量的信息。即使对于专业读者来说，如何最好地组织这些信息仍然令人困惑。阅读本条目的读者可以在迈克尔·格里芬最近出版的专著中找到关于现存证据的有用介绍（格里芬 2015 年，与 Falcon 2018 年和 Menn 2018 年提供的批评意见一起阅读；格里芬在 2020 年做出了回应）。根据格里芬的说法，罗得的安德洛尼库斯在后希腊时期对亚里士多德的《范畴论》产生了重要影响。安德洛尼库斯将《范畴论》放在他对亚里士多德著作的目录（_pinakes_）的开头。他认为这篇论文是亚里士多德关于断言论的理论的有用介绍，面向亚里士多德逻辑的（绝对）初学者。最终，安德洛尼库斯对《范畴论》的看法并不重要；重要的是他在他的_pinakes_中给这部作品分配的位置。他在_pinakes_中给予这部作品的重要性有助于引起人们对这个曾经默默无闻的作品的关注。

我们从亚里士多德《范畴学》早期接受的丰富而复杂的故事中学到了什么？我们了解到，回归亚里士多德吸引了具有不同哲学背景和不同哲学议程的思想家。我们还了解到，他们对亚里士多德的批判性参与导致了对《范畴学》的几种创造性阅读。在我们可以重建这些阅读的程度上，它们并没有使我们更接近亚里士多德的原始意图。更直接、更大胆地说：亚里士多德的《范畴学》与他的所有解释者之间存在着差距，这个差距早在希腊化时代就出现了。因此，他所有后来的（即后希腊化时代的）解释者都在努力弥合这个差距。安德洛尼库斯和西顿的博伊特斯等《范畴学》的早期解释者并不例外。他们的努力，就像我们的努力一样，最多只能取得有限的成功。最终，亚里士多德的《范畴学》当时如今仍然是一部难以捉摸的作品。

## 3. 亚历山大·阿弗罗迪西亚斯与公元 1 至 2 世纪的亚里士多德传统

对亚里士多德哲学的兴趣复兴超越了公元前 1 世纪。对亚里士多德著作的解释活动在公元 1 至 2 世纪继续蓬勃发展。在这两个世纪中，新的解释层面被添加进来。它们对形成解释传统做出了重要贡献，并在亚历山大·阿弗罗迪西亚斯的评论中达到了顶峰。

亚历山大·埃加伊斯（Alexander of Aegae）写过《范畴论》（Simplicius，《范畴论注释》10.20；13.16）和《天体论》（Simplicius，《天体论注释》430.29–32）。据说他是尼禄皇帝的老师，他的解释活动可以追溯到公元 1 世纪上半叶。关于他的文学作品形式无从得知。这也是另外两位《范畴论》解释者的情况：Sotion 和 Achaius（Simplicius，《范畴论注释》159.23）。虽然他们的解释活动更难确定日期，但有一种论证认为它发生在公元 1 世纪。我们掌握的信息非常有限，但很难逃脱这样的结论：《范畴论》继续成为亚里士多德哲学解释活动的核心。在公元 2 世纪上半叶，情况没有改变。根据伽利略的说法，阿弗洛狄修斯的阿德拉斯托斯（Adrastus of Aphrodisias）和阿斯帕修斯（Aspasius）写了关于《范畴论》的初级注释。从伽利略提到这些注释的方式来看，它们一定很容易获取，并且常常被用来向学生介绍《范畴论》。阿德拉斯托斯和阿斯帕修斯成为备受尊敬的亚里士多德解释者。我们知道，他们的注释在大约一百年后仍然被伟大的普罗提诺斯（Plotinus）使用（Porphyry，《普罗提诺斯传》14.12–14）。阿斯帕修斯的解释活动不仅限于《范畴论》。他还写了关于《解释学》、《物理学》和《形而上学》的注释。这些注释被后来的评论者使用和引用。阿斯帕修斯在古代享有的声誉解释了为什么他对亚里士多德的《伦理学》的注释得以保存。这个注释是对亚里士多德文本的最早保存的注释。

对于公元 2 世纪末期哲学实践发展的一个重要影响是帝国权力对教育事务的干预。这种干预似乎可以追溯到安东宁皮乌斯皇帝（Emperor Antoninus Pius），据说他在各省设立了公共的修辞学和哲学讲座。但是，马库斯·奥勒留斯（Marcus Aurelius）是第一个在公元 176 年在雅典设立帝国哲学讲座的人。根据我们的古代资料，这些讲座包括柏拉图派、斯多亚派、亚里士多德派和伊壁鸠鲁派哲学。很可能，亚里士多德哲学讲座的第一任讲席持有人是大马士革的亚历山大（Alexander of Damascus），他是伽利略的老同学，伽利略形容他为“对柏拉图的教义有所了解，但更倾向于亚里士多德的教义”（Galen，《预感论》XIV 627–628 Kühn）。哲学讲座的持有人是一位公共教师，每年领取薪水。他的教学内容包括对学派创始人的文本进行讲解。亚历山大·阿弗洛狄修斯（Alexander of Aphrodisias）是这些公开任命的亚里士多德哲学讲座教师之一。亚历山大自称为一位教师，即“教师”（_didaskalos_）（《命运论》164.15）。作为一位亚里士多德哲学的教师，亚历山大不仅关注阐释这一哲学，还致力于在哲学学派之间的辩论中为其辩护。他对亚里士多德的态度最好地表达在他的《论灵魂》（_De anima_）的开头。这篇论文不是对亚里士多德的《论灵魂》的注释，而是基于亚里士多德《论灵魂》中确立的原则对灵魂进行的研究。亚历山大在这里设定给自己的任务是澄清和推广亚里士多德的观点：

> 在所有哲学问题中，本文作者对亚里士多德的权威怀有特殊的尊重，确信他在这些问题上的教导比其他哲学家更具真理性。因此，本篇论文的目的将得到满意的实现，如果我在这里阐述的有关灵魂的学说能够尽可能清晰地表达亚里士多德在这个主题上所说的；而我自己的贡献将限于对亚里士多德自己已经非常出色地阐述的具体观点的解释。（亚历山大，《论灵魂》2. 5–9，翻译参考 Fotinis）

亚历山大写了大部分（但不是全部）亚里士多德著作的评论。他的评论包括《先验篇》（第一卷）、《论题》、《气象学》和《论感觉和感觉对象》。他对《后验篇》和《物理学》的评论已经失传，但有关这些评论的片段已经被收集和编辑（分别在 Moraux 1979 和 Rashed 2011 中）。他对《生成与毁灭》的失传评论的长篇摘录，仅在阿拉伯传统中存在，现已有英文翻译（Gannagé 2005）。传世的亚历山大所著的《形而上学》评论，只有前五卷（阿尔法到德尔塔）是真实的。自从普雷彻尔（Praetcher 1906）以来，对其他卷（伊普西龙到纽）的评论通常被归属于以弗所的迈克尔。这种归属现在得到了证实（在 Luna 2001 中），除了卷《伊普西龙》应该归属于亚历山大的斯蒂法努斯（Golitsis 2016b）。传统上被归属于亚历山大的《诡辩篇》评论是伪作，应该归属于以弗所的迈克尔（Ebbesen 1981，I：268-285）。

尽管亚历山大以他对亚里士多德的评论而闻名，但他的解释活动还包括对亚里士多德哲学中特定解释点的简短讨论。这些讨论被收集在《问题与解决方案》的四本书中，以及另一本传统上被称为《Mantissa》（字面意思是“附加物”）的书中。这些收集物生动地展示了在阐述亚里士多德哲学中所采用的各种解释方法的非凡多样性。在某种程度上，采用这种多样化的解释方法可以通过这些著作的教学功能来解释，这些著作被用作教学工具，用于教授对亚里士多德哲学有不同知识水平的学生。

亚历山大的解释活动最终是一种系统化的努力，旨在提取他认为是亚里士多德真正思想的努力。正是这种对大师哲学的忠诚，加上他解释的精妙，使亚历山大获得了“亚里士多德最真实的解释者”的声誉（Simplicius，《物理学注释》80.16）。他的评论设定了解释标准，在古代及以后的时期基本上没有被超越。但重要的是要认识到，他的非凡成就是始于公元前 1 世纪的解释传统的顶峰。更重要的是：亚历山大不是第一个，而是最后一个真正的亚里士多德解释者。尽管后来的评论家受到亚历山大的深刻影响，但他们的解释理想却截然不同。他们的主要目标不再是为了自身而恢复和保护亚里士多德的思想，而是为了找到亚里士多德和柏拉图之间的一致，并将这两位哲学家呈现为同一哲学观点的一部分。 （有关亚历山大作为评论家和哲学家的更多信息，请参阅[亚历山大·阿弗罗迪西亚斯](https://plato.stanford.edu/entries/alexander-aphrodisias/)条目的第 2 和第 3 节。）

## 4. 晚期古代的评论传统：《范畴论》在阅读课程中的地位

通过对亚里士多德著作的密切阅读，对他的研究在晚期古代继续进行，甚至有所增加。所有主要著作的评论也在这个时期供应不断。与亚历山大的阿弗洛狄修斯不同，这些评论者中的大多数认为自己是柏拉图的追随者（即柏拉图主义者）。在他们看来，柏拉图的哲学优于后来出现的所有思想体系。此外，他们认为所有这些后来的体系最初都是对柏拉图哲学的更或多或少忠实的发展。亚里士多德并不例外。相反，这些评论者将亚里士多德视为柏拉图的真正后裔。这就解释了为什么他们回归柏拉图，并不意味着拒绝亚里士多德的哲学。相反，他们相信这种哲学可以融入柏拉图的框架中。晚期古代的柏拉图主义作为一个思想体系是如此全面，以至于可以容纳亚里士多德的哲学作为其中的一个要素。在这种思维方式下，一个重要的解释关注点是在柏拉图和亚里士多德之间找到实质性的一致。对于这些评论者中的大多数人来说，亚里士多德和柏拉图在具体问题上的分歧并不妨碍两位哲学家在更深层次上的和谐。

实际上，这些评论者试图通过将亚里士多德的著作纳入学习柏拉图哲学的先决条件来调和柏拉图和亚里士多德。在这些著作中，《范畴论》继续享有特殊地位。遵循可能可以追溯到罗得的安德洛尼库斯的传统（参见关于《范畴论》早期命运的部分），这篇论文被认为是整个哲学的初级入门，并被用来教授对哲学知之甚少或根本不了解的初学者（波菲里，《范畴论注》56. 28–29）。因此，对这篇简短但难以理解的论文的解释活动在晚期古代从未停止，事实上还有所增加。在这一时期，接受《范畴论》的关键人物无疑是波菲里（约 234-305 年）。他写了一本七卷本的评论，面向一位名叫格达留斯的学生。在这个评论中，波菲里对《范畴论》进行了完整的解释，包括解决了所有先前的疑难问题（辛普利修斯，《范畴论注》2. 6–9）。他还处理了对《范畴论》的传统批评，包括对先前的解释者（如斯多亚派的阿特诺多罗斯和科努图斯，柏拉图派的卢修斯和尼科斯特拉图斯）提出的异议的全面讨论。他对《范畴论》的辩护依赖于亚里士多德学派的传统（如西顿的博伊特斯，阿弗洛狄修斯的亚历山大）。最近在所谓的阿基米德手稿中发现了这篇评论的一部分（参见 R. Chiaradonna, M. Rashed, and D. Sedley 2013）。波菲里还写了另一篇关于《范畴论》的评论。这本第二本评论已经传世。在更全面的评论中取得的成果在这里被缩写、简化，并以师生对话的形式呈现。以不同格式写成的两篇关于同一篇论文的评论的存在必须从这样一个事实的角度来理解，即这些评论是教学工具，用于教授具有不同技能和对《范畴论》有不同熟悉程度的学生。最后，波菲里是《导论》（字面意思是“引言”）的作者。这本简短的书是对亚里士多德的断言理论的引言。它面向哲学的绝对初学者，他们的阅读课程随后继续以亚里士多德的逻辑开始，从《范畴论》开始。

评论者对格达留斯的评论立即取得了成功，并成为模仿的典范。伊安布利科斯（约公元 242-325 年）据说写了一篇关于《范畴论》的评论，紧随其后，缩写了论证并使整体解释更加清晰。他还据说借助了毕达哥拉斯学派阿基塔斯关于范畴学说的著作，扩充了波菲里的解释（西姆普利修斯，《范畴论注释》2.9-14）。所讨论的文献是一篇晚期伪造的作品，旨在将亚里士多德的十个范畴学说归于毕达哥拉斯学派。伊安布利科斯认为这篇伪作是对亚里士多德《范畴论》的真实预期，也是他对哲学的愿景的辩护，即哲学是一种智慧的单一传统，从毕达哥拉斯开始，继续到柏拉图和亚里士多德。据报道，伊安布利科斯在试图证明亚里士多德与毕达哥拉斯学说的整体一致性时，利用了阿基塔斯的观点。

伊安布利科斯的评论已经失传，但我们可以通过德西普斯了解到这篇评论对于关于《范畴论》的古代辩论的贡献。除了他是伊安布利科斯的学生之外，关于德西普斯几乎一无所知。他的评论提供了对针对亚里士多德《范畴论》的疑难问题提出的解决方案的简明清晰的介绍。德西普斯选择了问答的形式，这更适合他的目的，即提供对他的前辈们（尤其是波菲里和伊安布利科斯）在解释方面取得的简要而相对简单的总结。德西普斯并不是一个原创的思想家。根据西姆普利修斯的说法，他在波菲里和伊安布利科斯的考虑中几乎没有增加任何东西（西姆普利修斯，《范畴论注释》2.29-30）。

在掌握了《范畴论》之后，哲学学生应该在转向柏拉图的对话之前阅读亚里士多德的其他著作。普罗克鲁斯（约公元 410-485 年）的老师叙利亚努斯同时向他的学生介绍了柏拉图和亚里士多德，从亚里士多德开始。普罗克鲁斯约于公元 430 年抵达雅典，当时学院的前任校长普鲁塔克已是一位年迈的人，叙利亚努斯已经接任。作为一位年轻而有才华的学生，普罗克鲁斯与普鲁塔克一起阅读了亚里士多德的《灵魂论》和柏拉图的《费多》。但是当普鲁塔克去世后，叙利亚努斯负责他的哲学教育。在叙利亚努斯的指导下，普罗克鲁斯在不到两年的时间里掌握了整个亚里士多德著作。他学习了逻辑、伦理、政治、物理学的作品，并以神学结束（马里努斯，《普罗克鲁斯的生平》，第 12 和 13 章）。逻辑-伦理-政治-物理学-神学的顺序不仅反映了教学的某种组织，也反映了哲学的特定观念。首先，亚里士多德逻辑著作的首要地位并不对于逻辑的本质持中立态度。到了这个时候，逻辑被视为哲学的工具。这种对逻辑及其与哲学其他部分的关系的看法体现在我们现在仍然用来指代亚里士多德逻辑著作的集体标题《Organon》中。此外，非神学著作是学习《形而上学》之前的准备工作。这部著作是亚里士多德的最后一部被研究的作品。叙利亚努斯将上帝的概念设计为他在亚里士多德哲学课程中的顶点，上帝被看作是一个智慧和活生生的存在，享有完美实在状态。普罗克鲁斯掌握了《形而上学》并了解了亚里士多德关于神的概念后，叙利亚努斯引导他去研究柏拉图，柏拉图的对话以一定的顺序阅读，以《蒂迈欧篇》和《巴门尼德篇》为高潮。

这种对亚里士多德和柏拉图哲学的态度从叙利亚努斯传给了普罗克鲁斯，再从普罗克鲁斯传给了阿蒙尼乌斯（约公元 440-520 年）。阿蒙尼乌斯对《范畴学》、《解释学》、《先验分析学》和波菲里的《导引篇》的评论流传至今。最后一篇是对《导引篇》的现存最早的评论。到那时，《导引篇》已经成为哲学阅读课程的一部分，接下来是《范畴学》、《解释学》和《先验分析学》。然而，请注意，阿蒙尼乌斯并没有写他对《范畴学》、《先验分析学》和《导引篇》的评论，它们是他的学生在他的亚里士多德讲座上产生的。（有关他的评论的更多信息和讨论，请参见[阿蒙尼乌斯](https://plato.stanford.edu/entries/ammonius/)条目的 1.2 节。）其他评论起源于讲堂，是教师口头讲解的抄录。基于奥林匹奥多罗斯（约公元 495-565 年）及其弟子埃利亚斯（大卫）的讲座所写的《范畴学》评论传世至今。菲洛波努斯（约公元 490-570 年）编辑了他自己关于《范畴学》的讲座。然而，这些后期评论中最有影响力的是西姆普利修斯于公元 529 年后写的评论。西姆普利修斯是一个博学多才、综合能力非凡的人。他也非常清楚自己处于一个悠久而庄严的解释传统的末尾。他有意试图吸收和融合这一传统的各个方面。通过这样做，他给我们留下了对古代《范畴学》接受情况最全面、最有信息量的调查。乍一看，西姆普利修斯可能看起来像一个现代学者。他的评论显示了一个深思熟虑、专注解释者的高度关注和精确性，通过对文本的仔细阅读来阐释《范畴学》。然而，有一个重要的区别不能被忽视。西姆普利修斯是一个柏拉图主义者，他的解释活动旨在表明柏拉图和亚里士多德在实质上是一致的。西姆普利修斯对自己解释的理想非常明确：

> 我相信，<值得尊敬的解释者>不应该仅仅通过看亚里士多德反对柏拉图的言辞来指责哲学家的不一致，而是应该朝向精神，追寻他们在大多数问题上的和谐（西姆普利修斯，《范畴学》7. 29–32，Michael Case 译）

在西姆普利修斯中达到顶峰的评论传统是为了更好地理解《范畴学》而做出的真诚努力。通过阅读西姆普利修斯的评论，人们仍然能够感受到这一传统在解释这篇短而难以捉摸的论文时所采用的非凡机智。但同时也会给人一种印象，即这一传统对这篇论文没有特权性的了解。相反，从亚里士多德哲学复兴开始一直到晚期古代，人们对《范畴学》仍然感到困惑。不仅是它的标题，而且它的统一性、结构和在亚里士多德著作中的位置都受到了激烈的讨论。例如，评论传统中出现了各种标题：《论主题的导引》、《存在的种类》、《十种种类》、《十个范畴》、《范畴学》（西姆普利修斯，《范畴学》15. 26–30）。每个标题都反映了对这篇论文内容的某种解释。安德洛尼库斯是第一个被知道更喜欢使用《范畴学》这个标题的解释者。他拒绝了《论主题的导引》这个标题，并认为《范畴学》的最后几章，即所谓的《后述范畴学》是后来添加的，与本书的目的相悖。他认为添加这些章节的人也在书上题写了《论主题的导引》这个标题（西姆普利修斯，《范畴学》279. 8–10）。相反，阿弗洛狄修斯的阿德拉斯托斯在一本名为《关于亚里士多德著作的顺序》的著作中捍卫了《范畴学》与《论主题》之间的关系（西姆普利修斯，《范畴学》16. 1–4）。这场争论不仅限于亚里士多德著作的组织，而且还涉及到这篇论文的哲学意义。最终，《范畴学》这个标题占了上风，并且随着这个标题，一种对这篇论文的解释方式也得以确立。

## 5. 《论灵魂》的解释性工作

从很早开始，解释者们努力理解亚里士多德的印象在《论灵魂》的评论传统中得到了证实。例如，考虑《论灵魂》3.5。在那里，亚里士多德著名地论证了存在一种与外界分离、不受影响和不混合的智力（《论灵魂》430 a 17–18）。在古代，评论者们传统上将这种智力称为主动（或生产）智力，即“诺斯·波伊提科斯”。关于如何准确理解这种智力的讨论早在很早之前就开始了。有证据表明，忒奥弗拉斯托斯（Theophrastus）对此感到困惑（忒米斯提乌斯（Themistius），《论灵魂》110.18–28）。除此之外，主动智力应该是什么样的东西并不明显。更直接地说，它不清楚是人类智力还是神圣智力。亚里士多德在《论灵魂》之外的著作中所说的同样令人困惑。在他的著作《动物的产生》中，亚里士多德谈到了一种“从外部进入”的智力（736 b 27）。但是，亚里士多德在这个背景下所做的评论到底如何理解并不清楚。这个神秘智力的地位到底是什么？它如何与《论灵魂》中提供的讨论相吻合？

亚历山大·阿弗罗迪西亚斯（Alexander of Aphrodisias）发展了一种解释线索，将主动智力视为非人类智力，并将其与上帝等同起来。他对《论灵魂》的评论已经失传。取而代之的是他根据亚里士多德在自己的《论灵魂》中建立的原则所写的《论灵魂》。在那里，亚历山大将主动智力与“第一原因，即对其他一切事物的存在的原因和原则”等同起来（亚历山大，《论灵魂》89. 9–10）。这种理解的智力不仅是人类思维的原因，也是宇宙中一切存在的原因。这种智力也可以成为思维的对象。当这种情况发生时，这种智力从外部进入我们：“这是从外部进入我们并且不朽的智力”（亚历山大，《论灵魂》90.19–20）。亚历山大明确指出，这种智力是唯一不朽的，因此也是神圣的。亚历山大对亚里士多德关于主动智力的概括性评论的类似但不完全相同的尝试也可以在《论智力》中找到，这是一篇短文，被归属于亚历山大·阿弗罗迪西亚斯，并保存在我们所知的简短解释性著作集合《Mantissa》中。

对于亚里士多德智力处理的接受，还有一篇重要的文章，那就是塞米修斯在公元 350 年左右写的《论灵魂》的释义。在那里，塞米修斯认为，积极智力是人类形态的最准确规范（《论灵魂》100.35-36）。换句话说，我们作为人类的本质就是积极智力（《论灵魂》100.36-101.1）。尽管塞米修斯没有点名道姓，但他显然是在反对亚历山大·阿弗罗迪西亚斯提出的观点。对于塞米修斯来说，积极智力不是上帝或一切存在依赖的至高原则。对他来说，积极智力是人类灵魂的一个组成部分：“积极智力在灵魂中，它就像是人类灵魂中最尊贵的部分”（《论灵魂》103.4-5）。尽管这样理解的积极智力是人类的智力，但它明确不被看作是个人的智力。塞米修斯坚决指出，只有一个独立、纯粹、不受影响的积极智力。同样，只有一个独立、纯粹、不受影响的潜在智力，即“努斯·杜纳梅伊”。根据塞米修斯的说法，积极智力和潜在智力都不受生成和消亡的影响。可消亡的只有普通或被动智力，“科伊农”或“帕塞蒂科斯·努斯”。这第三种智力与身体混合在一起，它的命运是随着身体一起消亡（《论灵魂》105.28-29；106.14-15）。

## 6. 博伊修斯和拉丁传统

从公元四世纪开始，亚里士多德的《逻辑学》在拉丁传统中成为阅读课程的一部分。与希腊传统一样，拉丁传统在教授亚里士多德时采用了各种各样的解释工具：释义、初级和高级评论。此外，拉丁传统还面临一个特定的问题，即为学生提供相关文本的适当翻译。

传统认为，公元四世纪的马里乌斯·维克托里努斯（Marius Victorinus）翻译了《范畴论》（Categories）、《解释学》（De Interpretatione）和波菲里的《导论》（Isagoge）（卡西奥多罗斯，《学院》II，3，18）。同样的传统认为，他还在《范畴论》上写了一本八卷本的评论。几年后，维提乌斯·阿戈里乌斯·普雷泰克斯塔图斯（Vettius Agorius Praetextatus）被认为翻译了忒米斯提乌斯（Themistius）所著的《先分析》（Prior Analytics）和《后分析》（Posterior Analytics）的希腊释义（博伊修斯，《论智慧》2 3.6–4.3）。除了维克托里努斯翻译的《导论》的摘录外，这些作品都没有流传下来。一本匿名的《范畴论》释义被错误地归属于奥古斯丁，并且通常被称为《十个范畴》（Categoriae Decem），之所以保存下来，只是因为它在早期中世纪被广泛阅读和使用。最后，马尔提努斯·卡佩拉（Martianus Capella）的《辩证学》（On Dialectic）中保留了《范畴论》的不完整释义（他的《哲学与墨丘利之婚姻》的第四卷，是一本影响深远的关于七门自由艺术的教科书，可以追溯到公元五世纪）。即使这些简短的评论足以证明，人们为了满足当时学生的教学需求，进行了持续的努力，提供了一本反映当时教学需求的拉丁语《逻辑学》（Organon）。

拉丁世界对亚里士多德的接受的关键人物是博伊修斯（Boethius）。博伊修斯（约公元 475 年-526 年）以他的《哲学慰藉》（Consolation of Philosophy）而闻名，他是将亚里士多德逻辑传统传播到早期中世纪的传播者。他对亚里士多德和柏拉图的态度与晚期古代的其他评论者并无不同。他将柏拉图和亚里士多德视为哲学权威，并坚信最好的哲学方法是阅读和评论他们的著作。与晚期古代的柏拉图主义者一样，他相信柏拉图和亚里士多德在基本观点上是一致的，并且认为亚里士多德的思想是柏拉图思想的真正发展。在这种思维框架下，博伊修斯计划翻译他能找到的亚里士多德的所有著作以及柏拉图的所有对话，并对它们进行评论，以显示柏拉图和亚里士多德在最重要的哲学观点上的一致性（博伊修斯《论智慧》2 79. 9–80.9）。

博伊修斯只能部分地实现这个计划。他成功地翻译了《范畴论》、《解释学》、《先验分析》、《论题学》和《诡辩论证》。此外，他还翻译了波菲里的《导论》。博伊修斯对《导论》写了两本评论。除了这些评论之外，他还写了两本关于《解释学》的评论。

写双重评论的实践应该从激发整个评论传统的教育关怀的角度来理解。以下是博伊修斯解释为什么他在《论释义》上写了两个评论（或者说是同一评论的两个版本）的原因：

> 我的计划是在一个组织成两个版本的评论中揭示亚里士多德最微妙的教义；因为第一个版本所包含的内容在某种程度上为那些进入这些更深奥和微妙的问题的人铺平了一条更容易的道路。但是因为第二个版本是在与解说者更微妙的教义相关的情况下发展的，所以它被呈现给那些在这个探究和研究中已经有所进展的人阅读和学习（博伊修斯，《论释义》2 186. 2–9，Kretzmann 之后）。

博伊修斯还写了一篇关于《范畴论》的评论。这个评论旨在作为该论文的初级阐述。博伊修斯计划写一篇第二篇评论，针对更高级的学生（博伊修斯，《范畴论》160 A-B）。只是不清楚他是否能够完成这篇更高级的评论。（有关博伊修斯作为评论者的更多信息，请参见《阿尼修斯·曼利乌斯·塞韦里努斯·博伊修斯》条目的第 2 节。）

在博伊修斯之后，对《导论》、《范畴学》和《论解》的注释工作仍在继续。这项工作通常采用注释的形式，即在《导论》、《范畴学》和《论解》的副本边缘上写下的注解。对早期拉丁中世纪传统中达成的注释结果的调查超出了本条目的范围。我建议读者参考参考文献以了解更多关于早期中世纪注释传统的内容。

## 7. 结论

评论者们没有以一套明确的教义来定义哲学，这套教义是所有古代对亚里士多德的评论者所共享的。古代评论者所共享的是阅读和评论亚里士多德文本的实践，他们的关键假设是亚里士多德是一个哲学权威，他的著作值得被认真研究。

由于相关文献几乎完全丧失，我们对亚里士多德第一代诠释者了解甚少。从我们所了解到的少量资料中，并没有形成一个统一的画面。我们所拥有的信息并不支持所有这些诠释者都写了评论。评论最终成为解释学的标准形式。但即使在评论传统中，也存在多种解释立场的空间。不同的评论者根据不同的（常常是相互竞争的）关注点，发展出不同的解释线索，推动他们的解释学。在亚历山大·阿弗罗迪西亚斯的解释学传统中，主要（但不仅仅）是为了在古代哲学派别之间的辩论背景下捍卫亚里士多德的哲学。亚历山大·阿弗罗迪西亚斯将亚里士多德视为自己的导师，并致力于解释和阐述亚里士多德独特的哲学立场。虽然晚期古代的柏拉图主义者将自己置于这一传统的延续中，但他们的解释主要是为了发展一种哲学，坚持柏拉图和亚里士多德之间的连续性。他们在评论中的假设是亚里士多德和柏拉图在很大程度上是一致的。

## Bibliography

The surviving ancient Greek commentaries on Aristotle are published in the series _Commentaria in Aristotelem Graeca_, H. Diels (ed.), Berlin: Reimer 1882–1909. These commentaries are being translated in the Ancient Commentators on Aristotle Project. The General Editor is Richard Sorabji. Co-editor: M. Griffin. Associate Editors: Pieter Adamson, Richard McKirahan, John Sellars.

Richard Sorabji has also produced a sourcebook in three volumes:

* Sorabji, R., 2005, _The Philosophy of the Commentators 200–600_ _AD_. _A Sourcebook_. (Volume 1: Psychology; Volume 2: Physics; Volume 3: Logic and Metaphysics), London, Ithaca, NY: Duckworth, Cornell University Press.

### A. Primary Sources

* Alessandro di Afrodisia. _Commento al De caelo di Aristotele. Frammenti del primo libro_, A. Rescigno (ed.), Amsterdam: Hakkert, 2004.
* –––. _Commento al De caelo di Aristotele. Frammenti del secondo, terzo, e quarto libro_, A. Rescigno (ed.), Amsterdam: Hakkert, 2008.
* Alexander of Aphrodisias, _On Aristotle Prior Analytics 1.1–7_, J. Barnes, S. Bobzien, K. Flannery, K. Ierodiakonou (trans.), London: Duckworth and Ithaca: Cornell University Press, 1991.
* –––, _On Aristotle Prior Analytics 1.8–13_, J. B. Gould, and I. Müller (trans.), London, Duckworth and Ithaca: Cornell University Press, 1999.
* –––, _On Aristotle Prior Analytics 1.14–22_, J. B. Gould, and I. Müller (trans.), Ithaca: Cornell University Press, 1999.
* –––, _On Aristotle Prior Analytics 1.23–31_, I. Müller (trans.), London: Duckworth and Ithaca: Cornell University Press, 2006.
* –––, _On Aristotle Prior Analytics 1.32–46_, I. Müller (trans.), London: Duckworth and Ithaca: Cornell University Press, 2006.
* –––, _On Aristotle Topics 1_, J. van Ophuijsen (trans.), London: Duckworth and Ithaca: Cornell University Press, 2001.
* –––, _On Aristotle Topics 2_, L. Castelli (trans.), London: Bloomsbury Publishing, 2020.
* –––, _On Aristotle On Coming-to-Be and Perishing 2.2–5_, E. Gannagé (trans.), Ithaca: Cornell University Press, 2005.
* –––, _On Aristotle Meteorology 4_, E. Lewis (trans.), London: Duckworth and Ithaca: Cornell University Press, 1996.
* –––, _On Aristotle Metaphysics 1_, W. E. Dooley (trans.), London: Duckworth and Ithaca: Cornell University Press, 1989.
* –––, _On Aristotle Metaphysics 2 and 3_, W. E. Dooley, and A. Madigan (trans.), Ithaca: Cornell University Press, 1992.
* –––, _On Aristotle Metaphysics 4_, A. Madigan (trans.), London: Duckworth and Ithaca: Cornell University Press, 1993.
* –––, _On Aristotle Metaphysics 5_, W. E. Dooley (trans.), London: Duckworth and Ithaca: Cornell University Press, 1993.
* –––, _On Aristotle On Sense Perception_, J. A. Towey (trans.), London: Duckworth and Ithaca: Cornell University Press, 2000.
* –––, _On Fate_, R. W. Sharples (ed.), London: Duckworth, 1983.
* –––, _On the Soul_ (Part 1), V. Caston (trans.), Bristol: Bristol Classical Press, 2012.
* –––, _The De anima of Alexander of Aphrodisias_, A. P. Fotinis (trans.), Washington, D.C.: University Press of America, 1979.
* –––, _Quaestiones 1.1–2.15_, R. W. Sharples (trans.), London: Duckworth and Ithaca: Cornell University Press, 1992.
* –––, _Quaestiones 1.16–3.15_, R. W. Sharples (trans.), London: Duckworth and Ithaca: Cornell University Press, 1994.
* –––, _Ethical Problems_, R. W. Sharples (trans.), London: Duckworth and Ithaca: Cornell University Press, 1990.
* –––, _Supplement to “On the Soul”_, R. W. Sharples (trans.), London: Duckworth and Ithaca: Cornell University Press, 2004.
* –––, _Le commentaire aux “Second Analytiques”_, P. Moraux (ed.), Berlin, New York: Walter de Gruyter, 1979.
* –––, _Commentaire perdu à la “Physique” d’Aristote (Livres IV–VIII)_, M. Rashed (ed.), Berlin, New York: Walter de Gruyter, 2011.
* \[Alexander of Aphrodisias], _On Aristotle Metaphysics 12_, F. Miller (trans.), London: Bloomsbury Publishing, 2021.
* Ammonius, _On Aristotle Categories_, S. M. Cohen, and G. B. Matthews (trans.), Ithaca: Cornell University Press, 1991.
* –––, _On Aristotle On Interpretation 1.1–8_, D. Blank (trans.), London: Duckworth and Ithaca: Cornell University Press, 1996.
* –––, _On Aristotle on Interpretation 9_, D. Blank (trans), with Boethius, _On Aristotle On Interpretation 9_, N. Kretzmann (trans.), London: Duckworth and Ithaca: Cornell University Press, 1998.
* Aspasius, _On Aristotle Nicomachean Ethics 1–4, 7–8_ D. Konstan (trans.), London: Duckworth and Ithaca: Cornell University Press, 2006.
* Aspasius, Michael of Ephesus, Anonymous, _On Aristotle Nicomachean Ethics 8–9_ D. Konstan (trans.), London, Duckworth and Ithaca: Cornell University Press, 2001.
* Boethius, _On Aristotle On Interpretation, 1–3_, A. Smith (trans.), Bristol: Bristol Classical Press, 2010.
* –––, _On Aristotle On Interpretation, 4–6_, A. Smith (trans.), Bristol: Bristol Classical Press, 2011.
* Cicero, _Lucullus_, in _Cicero: On Academic Scepticism_, C. Brittan (trans.), Indianapolis: Hackett Publishing Company, 2006.
* Dexippus, _On Aristotle’s Categories_, J. Dillon (trans.), London: Duckworth and Ithaca: Cornell University Press, NY, 1990.
* Galen, _On My Own Books_, in C. Kühn (ed.), _Galeni opera omnia_ (20 vols. In 22), Leipzig: C. Knolbloch, 1819–1833.
* Marinus, _Life of Proclus_, in _Proclus ou sur le bonheur_, H. D. Saffrey and A. P. Segonds (eds.), Paris: Les Belles Lettres, 2002.
* Philodemus, _History of the Academy_, in Filodemo, _Storia dei filosofia. Platone e l’Accademia_. T. Dorandi (ed.), Napoli: Bibliopolis, 1991.
* Philoponus, _On Aristotle‘s Categories 1–5_, R. Sirkel, T. Tweedale, J. Harris (trans.), Bristol: Bristol Classical Press, 2015.
* –––, _On Aristotle‘s Categories 6–15_, M. Share (trans.), London: Bloomsbury Publishing, 2019.
* –––, _On Aristotle Posterior Analytics 1.9–18_, R. McKirahan (trans.), Bristol: Bristol Classical Press, 2012.
* –––, _On Aristotle Posterior Analytics 1.19–34_, O. Goldin, M. Martijn (trans.), Bristol: Bristol Classical Press, 2012.
* –––, _On Aristotle On Coming-to-Be and Perishing 1.1–5_, C. J. F. Williams, Ithaca: Cornell University Press, 1999.
* –––, _On Aristotle On Coming-to-Be and Perishing 1.6–2.4_, C. J. F. Williams, Ithaca: Cornell University Press, 2000.
* –––, _On Aristotle On Coming-to-Be and Perishing 2.5–11_, I. Kupreeva (trans.), Ithaca: Cornell University Press, 2005.
* –––, _On Aristotle Meteorology 1.1–3_, I. Kupreeva (trans.), Bristol: Bristol Classical Press, 2011.
* –––, _On Aristotle Meteorology 1.4–9, 12_, I. Kupreeva (trans.), Bristol: Bristol Classical Press, 2012.
* –––, _On Aristotle Physics 1.1–3_, C. Osborne (trans.), London: Duckworth and Ithaca: Cornell University Press, 2006.
* –––, _On Aristotle Physics 1.4–9_, C. Osborne (trans.), London: Duckworth and Ithaca: Cornell University Press, 2006.
* –––, _On Aristotle Physics 2_, A. R. Lacey (trans.), London: Duckworth and Ithaca: Cornell University Press, 1993.
* –––, _On Aristotle Physics 3_, M. Edwards (trans.), London: Duckworth and Ithaca: Cornell University Press, 1994.
* –––, _On Aristotle Physics 4.1–5_, K. Algra, J. van Ophuijsen (trans.), Bristol: Bristol Classical Press, 2013.
* –––, _On Aristotle Physics 4.6–9_, P. Huby (trans.), Bristol: Bristol Classical Press, 2012.
* –––, _On Aristotle Physics 4.10–14_, S. Broadie (trans.), Bristol: Pristol Classical Press, 2011.
* –––, _On Aristotle Physics 5–8_, P. Lettinck (trans.) with Simplicius, _On Aristotle on the Void_, J. O. Urmson (trans.), London: Duckworth and Ithaca: Cornell University Press, 1994.
* –––, _Corollaries on Place and Void_, D. Furley (trans.), with Simplicius, _Against Philoponus On the Eternity of the World_, Ch. Wildberg (trans.), London: Duckworth and Ithaca: Cornell University Press, 1991.
* –––, _On Aristotle On the Intellect_, W. Charlton, and F. Bossier (trans.), London: Duckworth and Ithaca: Cornell University Press, 1991.
* –––, _On Aristotle On the Soul 1.1–2_, Ph. van der Eijk (trans.), London: Duckworth and Ithaca: Cornell University Press, 2005.
* –––, _On Aristotle On the Soul 1.3–5_, Ph. van der Eijk (trans.), London: Duckworth and Ithaca: Cornell University Press, 2006.
* –––, _On Aristotle On the Soul 2.1–6_, W. Charlton (trans.), London: Duckworth and Ithaca: Cornell University Press, 2005.
* –––, _On Aristotle On the Soul 2.7–12_, W. Charlton (trans.), London: Duckworth and Ithaca: Cornell University Press, 2005.
* –––, _On Aristotle On the soul 3.1–8_, W. Charlton (trans.), London: Duckworth and Ithaca: Cornell University Press, 2000.
* –––, _On Aristotle On the Soul 3.9–13_, W. Charlton (trans.), London: Duckworth and Ithaca: Cornell University Press, 2000.
* Plutarch, _Sulla_, in Plutarch, _Vitae parallelae_, K. Ziegler (ed.), Leipzig: Teubner, 1973.
* Porphyre, _Commentaire aux Catégories d’ Aristote_, R. Bodéüs (ed.), Paris: Vrin, 2008. \[This is a new edition of the text with French translation and notes.]
* –––, _On Aristotle’s Categories_, S. K. Strange (trans.), London: Duckworth and Ithaca: Cornell University Press, 1992.
* –––, _Introduction_, J. Barnes (trans.), Oxford: Oxford University Press, 2003.
* Porphyry, _Life of Plotinus_, in _Plotini opera_, P. Henry and H.-R. Schwyzer (eds.), Oxford: Oxford University Press, 1964–1982.
* Porphyry, _Ad Gedalium_, see Chiaradonna, Rashed, & Sedley 2013.
* Ptolémée “al–Gharîb”, _Épître à Gallus: sur la vie, le testament et les écrits d’Aristote_, M. Rashed (ed.). Paris: Les Belles Lettres, 2021.
* Simplicius, _On Aristotle’s Categories 1–4_, M. Chase (trans.), London: Duckworth and Ithaca: Cornell University Press, 2003.
* –––, _On Aristotle’s Categories 5–6_, Frans A. J. de Haas, and B. Fleet (trans.), London: Duckworth and Ithaca: Cornell University Press, 2001.
* –––, _On Aristotle’s Categories 7–8_, B. Fleet (trans.), London: Duckworth and Ithaca: Cornell University Press 2002.
* –––, _On Aristotle’s Categories 9–15_, R. Gaskin (trans.), London: Duckworth and Ithaca: Cornell University Press, 2000.
* –––, _Commentaire sur les Catégories,_ I: _Introduction, Première partie_. Traduction de Ph. Hoffmann. Commentaire et notes par I. Hadot, Leiden, New York/Cologne, Copenhagen: Brill, 1990.
* –––, _Commentaire sur les Catégories,_ III: _Préambule aux_ _Catégories_. Commentaire au premier chapitre des _Catégories._ Traduction de Ph. Hoffmann. Commentaire et notes à la traduction de C. Luna, Leiden, New York, Cologne, Copenhague: Brill, 1990.
* –––, _Commentaire sur les Catégories, Chapitres 2–4._ Traduction de Ph. Hoffmann. Commentaire et notes à la traduction de C. Luna, Paris: Les Belles Lettres, 2001.
* –––, _On Aristotle Physics 1.3–4._ P. Huby and C. C. W. Taylor (trans.), Bristol: Bristol Classical Press, 2011.
* –––, _On Aristotle Physics 1.5–9._ H. Baltussen, M. Atkinson, M. Share, I. Mueller (trans.), Bristol: Bristol Classical Press, 2012.
* –––, _On Aristotle Physics 2_, B. Fleet (trans.), London: Duckworth and Ithaca: Cornell University Press, 1997.
* –––, _On Aristotle Physics 3_, J. O. Urmson and P. Lautner (trans.), London: Duckworth and Ithaca: Cornell University Press, 2001.
* –––, _On Aristotle Physics 4.1–5 and 10–14_, J. O Urmson (trans.), Ithaca: Cornell University Press, 1992.
* –––, _On Aristotle Physics 5_, J. O. Urmson (trans.), London: Duckworth and Ithaca: Cornell University Press, 1997.
* –––, _On Aristotle Physics 6_, D. Konstan (trans.), London: Duckworth and Ithaca: Cornell University Press, 1989.
* –––, _On Aristotle Physics 7_, C. Hagen (trans.), London: Duckworth and Ithaca: Cornell University Press, 1994.
* –––, _On Aristotle Physics 8.1–5_, M. Share, I. Bodnár, M. Chase (trans.), Bristol: Bristol Classical Press, 2013.
* –––, _On Aristotle Physics 8.6–10_, R. McKirahan (trans.), London: Duckworth and Ithaca: Cornell University Press, 2001.
* –––, _Corollaries on Place and Time_, J. O. Urmson and L. Siorvanes (trans.), Ithaca: Cornell University Press, 1992.
* –––, _On Aristotle On the Heavens 1.1–4_, R. J. Hankinson (trans.), London: Duckworth and Ithaca: Cornell University Press, 2002.
* –––, _On Aristotle On the Heavens 1.2–3_, I. Müller (trans.), Bristol: Bristol Classical Press, 2010.
* –––, _On Aristotle On the Heavens 1.3–4_, I. Müller (trans.), Bristol: Bristol Classical Press, 2011.
* –––, _On Aristotle On the Heavens 1.5–9_, R. J. Hankinson (trans.), London: Duckworth and Ithaca: Cornell University Press, 2004.
* –––, _On Aristotle On the Heavens 1.10–12_, R. J. Hankinson (trans.), London: Duckworth and Ithaca: Cornell University Press, 2006.
* –––, _On Aristotle On the Heavens 2.1–9_, I. Müller (trans.), London: Duckworth and Ithaca: Cornell University Press, 2004.
* –––, _On Aristotle On the Heavens 2.10–14_, I. Müller (trans.), London: Duckworth and Ithaca: Cornell University Press, 2005.
* –––, _On Aristotle On the Heavens 3.1–7_, I. Müller (trans.), Bristol: Bristol Classical Press, 2009.
* –––, _On Aristotle On the Heavens 3.7–4.6_, I. Müller (trans.), Bristol: Bristol Classical Press, 2009.
* \[Simplicius], _On Aristotle’s On the Soul 1.1–2.4_ , J. O. Urmson, P. Lautner (trans.), Ithaca: Cornell University Press, 1996.
* –––, _On Aristotle’s On the Soul 2.5–12_ , C. Steel, C. J. O. Urmson (trans.), London: Duckworth and Ithaca: Cornell University Press, 1997.
* –––, _On Aristotle’s On the Soul 3.1–5_, H. J. Blumenthal, (trans.), London: Duckworth and Ithaca: Cornell University Press, 1995.
* –––, _On Aristotle’s On the Soul 3.6–13_, C. Steel (trans.), Bristol: Bristol Classical Press, 2013.
* Strabo, _Geographika_, S. Radt (ed.) (10 volumes), Göttingen: Vandenhoeck & Ruprecht, 2002–2010.
* Syrianus, _On Aristotle Metaphysics 3–4_, D. O’Meara and J. Dillon (trans.), London: Duckworth and Ithaca: Cornell University Press, 2008.
* –––, _On Aristotle Metaphysics 13–14_, D. O’Meara and J. Dillon (trans.), London: Duckworth and Ithaca; Cornell University Press, 2006.
* Themistius, _On Aristotle’s On the Soul_, R. B. Todd (trans.), London: Duckworth and Ithaca: Cornell University Press, 1996.
* –––, _On Aristotle Physics 1–3_, R. B. Todd (trans.), Bristol: Bristol Classical Press, 2011.
* –––, _On Aristotle Physics 4_, R. B. Todd (trans.), London: Duckworth and Ithaca: Cornell University Press, 2003.
* –––, _On Aristotle Physics 5–8_, R. B. Todd (trans.), London: Duckworth and Ithaca: Cornell University Press, 2008.
* –––, _On Aristotle Metaphysics 12_, Y. Mayrav (trans.), London: Bloomsbury Publishing, 2020.

### B. Secondary Sources

* Achard, M., Renaud, F. (eds), 2008, _Le commentaire philosophique (I)-(II)_, _Laval théologique et philosophique_, 63: 1 and 3.
* Adamson, P., Baltussen, H., Stone, M. (eds.), 2004, _Philosophy, Science and Exegesis in Greek, Arabic and Latin Commentaries_, _Bulletin of the Institute of Classical Studies_ (Supplementary Volume): 83(1–2).
* Alberti, A., Sharples, R. W. (eds.), 1999, _Aspasius: the Earliest Extant Commentary on Aristotle’s Ethics_, Berlin and New York: De Gruyter 1999.
* Asztalos, M., 1993, “Boethius as a Transmitter of Greek Logic to the Latin West: The _Categories_”, in _Harvard Studies in Classical Philology_, 95: 367–407.
* Baltussen, H., 2007, “From Polemic to Exegesis: the Ancient Philosophical commentary”, in _Poetics Today_, 28: 247–289.
* –––, 2008, _Philosophy and Exegesis in Simplicius. The Method of a Commentator_, London: Duckworth.
* –––, 2016, _The Peripatetics: Aristotle’ Heirs 322 BCE–200 CE_, London and New York: Routledge.
* Barnes, J., 1997, “Roman Aristotle”, in J. Barnes and M. Griffin (eds.), _Philosophia Togata II_, Oxford: Oxford University Press, pp. 1–67.
* –––, 2005, “Les catégories et les _Catégories_”, in O. Bruun and L. Corti (eds.), _Les Catégories et leur histoire_, Paris: Vrin, pp. 11–80.
* Blumenthal, H. J., 1987, “Alexander of Aphrodisias in the Later Greek Commentaries on Aristotle’s _De anima_”, in J. Wiesner (ed.), _Aristoteles._ _Werk und Wirkung_, volume 2, Berlin and New York: Walter de Gruyter, pp. 90–106.
* –––, 1996, _Aristotle and Neoplatonism in Late Antiquity Interpretations of the De anima_, London: Duckworth.
* Brunn, O., Corti, L. (eds.), 2005, _Les Catégories et leur histoire_, Paris: Vrin.
* Celluprica, V., D’Ancona, C. (eds.), 2004, _Aristotele e i suoi esegeti neoplatonici_, Naples: Bibliopolis.
* Chiaradonna, R., 2011, “Interpretazione filosofia e ricezione del _corpus_. Il caso di Aristotele (100 a.C.–250 d.C)”, _Quaestio_ 11, L. Del Corso and P. Pecere (eds.), _Il libro filosofico. Dall’antichità al XXI secolo – Philosophy and the Books. From Antiquity to the XXI Century_, pp. 83–114.
* –––, 2012, “Commento filosofico”, in P. D’Angelo (ed.), _Forme letterarie della filosofia_, Roma: Carocci, pp. 71–103.
* Chiaradonna, R., Rashed, M., 2010, “Before and After the Commentators: an Exercise in Periodization”, _Oxford Studies in Ancient Philosophy_, 38: pp. 250–297.
* –––, 2020, _Boéthos de Sidon: exégète et philosophe_. Berlin and New York: Walter de Gruyter.
* Chiaradonna, R., Rashed, M., Sedley, D., 2012, “A Rediscovered ‘Categories’ Commentary”, _Oxford Studies in Ancient Philosophy_, 44: 129–194.
* D’Ancona, C., (ed.), 2007, _The Libraries of the Neoplatonists_, Leiden, Boston, Köln: Brill.
* Dietze–Mager, G., 2015, “Die _Pinakes_ des Andronikos im Licht der Vorrede in der Aristoteles-Schrift des Ptolomeios”, in _Aevum_, 89(1): pp. 93–123.
* –––, 2015, “Aristoteles–Viten und –Schriftenkatalogen. Die Aristoteles Schrift des Ptolemaios im Light der Überlieferung”, _Studi classici e orientali_, 61: 97–166.
* Donini, P.L., 1987, “Testi e commenti, manuali e commento: la forma sistematica e i metodi della filosofia in età post-ellenistica”, in H. Temporini and W. Haase (eds.), _Aufstieg und Niedergang der römischen Welt_ (Volume 36, No. 7), Berlin and New York: Walter de Gruyter, pp. 5027–5094.
* –––, 2010, _Commentary and Tradition_, Berlin and New York: Walter de Gruyter.
* Düring, I., 1957, _Aristotle in the Ancient Biographical Tradition_, Göteborg: Almquist & Wiksell.
* Ebbesen, S., 1987, “Boethius as an Aristotelian Commentator”, in J. Wiesner (ed.), _Aristoteles: Werk und Wirkung_ (Volume 2), Berlin and New York: Walter de Gruyter, pp. 266–311.
* Evangeliu, C., 1988, _Aristotle’s Categories and Porphyry_, Leiden, New York, Köln: Brill.
* Falcon, A. (ed.), 2011, _Aristotelianism in the First Century BCE. Xenarchus of Seleucia_, Cambridge: Cambridge University Press.
* –––, 2016, _Brill’s Companion to the Reception of Aristotle in Antiquity_, Leiden and Boston: Brill.
* –––, 2017, _Aristotelismo_, Torino: Einaudi.
* –––, 2018, “The Early Reception of Aristotle’s _Categories_. Comments on M. Griffin, _Aristotle’s Categories in the Early Roman Empire_”, in _Documenti e studi sulla tradizione filosofica medievale_, 29: 1–12.
* Fazzo, S., 2004, “Aristotelianism as a Commentary Tradition”, in P. Adamson, H. Baltussen, M. Stone (eds.), _Philosophy, Science and Exegesis in Greek, Arabic and Latin Commentaries_, _Bulletin of the Institute of Classical Studies_ (Supplementary Volume), 83(1): 1–19.
* Geerlings, W., Schulze, Ch. (eds.), 2002, _Der Kommentar in Antike und Mittelalter_, Leiden, Boston, Köln 2002: Brill.
* Gibson, R.K., Shuttleworth Kraus, Ch. (eds.), 2002, _The Classical Commentary: Histories, Practices, Theory_, Leiden, Boston, Köln: Brill.
* Golitsis, P., 2008, _Les commentaires de Philopon et de Simplicius à la Physique d’ Aristote_, Berlin and New York: Walter de Gruyter.
* –––, 2016a, “John Philoponus’s Commentary on the Third Book of Aristotle’s _De anima_”, in R. Sorabji, _Aristotle Re–interpreted: New Findings on Seven Hundred Years of the Ancient Commentators_, London: Bloomsbury Publishing, pp. 393–412.
* –––, 2016b, “Who were the real authors of the _Metaphysics_ commentary ascribed to Alexander and Pseudo–Alexander?”, in R. Sorabji, _Aristotle Re–interpreted: New Findings on Seven Hundred Years of the Ancient Commentators_, London: Bloomsbury Publishing, pp. 565–587.
* Golitisis, P., Ierodiakonou, K. (eds.), 2019, _Aristotle and his Commentators_, Berlin and New York: Walter de Gruyter.
* Gottschalk, H. B., 1987, “Aristotelian Philosophy in the Roman World from the Time of Cicero to the End of the Second Century AD”, in H. Temporini and W. Haase (eds.), _Aufstieg und Niedergang der römischen Welt_ (Volume 36, Number 2), Berlin and New York: Walter de Gruyter 1987, pp. 1079–1174.
* –––, 1997, “Change and Continuity in Aristotelianism”, in R. Sorabji (ed.) _Aristotle and After_, _Bulletin of the Institute of Classical Studies_ (Supplementary Volume), 68: pp. 109–115.
* Goulet-Cazé, M.O. (ed.), 2000, _Le commentaire entre tradition et innovation_, Paris: Vrin, 2000.
* Griffin, M., 2013, “Which Athenodorus commented on Aristotle’s _Categories_?”, _Classical Quarterly_, 62(1): 199–208.
* –––, 2015, _Aristotle’s Categories in the Early Roman Empire_, Oxford: Oxford University Press.
* –––, 2020, “Articulating Preconceptions: A Reconsideration of _Aristotle’s Categories in the Early Roman Empire_”, in _Documenti e studi sulla tradizione medievale_, 31: 1–37.
* Hadot, I., 1991, “The Role of the Commentaries on Aristotle in the Teaching of Philosophy according to the Prefaces of the Neoplatonic Commentaries to the _Categories_”, in H.J. Blumenthal and H. Robinson (eds.), _Aristotle and the later tradition_, _Oxford Studies in Ancient Philosophy_ (Supplementary Volume), Oxford: Clarendon Press, 1991, pp. 175–189.
* –––, 2002, “Simplicius or Priscianus? On the author of the commentary on Aristotle’s _De anima_: a methodological study”, _Mnemosyne_, 55: 159–199.
* –––, 2002, “Der fortlaufende philosophische Kommentar”, in W. Geerlings, Ch. Schulze (eds.), _Der Kommentar in Antike und Mittelalter_, Leiden, Boston, Köln 2002: Brill, pp. 183–199.
* –––, 2014, _Le néoplatonicien Simplicius à la lumière des recherches contemporaines. Un bilan critique_, Sankt Augustin: Akademia Verlag.
* –––, 2015, _The Harmonization of Aristotle and Plato at Alexandria and Athens_, Leiden and Boston: Brill.
* Hadot, I. (ed.), 1997, _Simplicius, sa_ _vie, son œuvre, sa survie_, Berlin and New York: Walter de Gruyter.
* Hadot, P., 1971, _Marius Victorinus. Recherches sur sa vie et ses œuvres_, Paris: études augustinienne.
* Hatzimichali, M., 2013, “The Texts of Plato and Aristotle in the First Century BC,” in M. Schofield (ed.), _Plato, Aristotle, Pythagoras in the First Century BC_, Cambridge: Cambridge University Press, pp. 1–27.
* –––, 2016, “Andronicus of Rhodes and the Construction of the Corpus Aristotelicum”, in A. Falcon (ed.), _Brill’s Companion to the Reception of Aristotle in Antiquity_, Leiden and Boston: Brill, pp. 81–100.
* Hein, Ch.,1985, _Definition und Einteilung der Philosophie: von der spätantiken Einleitungsliteratur zur arabischen Enzyklopädie_, Frankfurt am Main: Peter Lang.
* Hoffmann, Ph., 1998, “La fonction des prologues exégétiques dans la pensée pédagogique néoplatonicienne”, in B. Roussel and J.-D. Dubois (eds.), _Entrer en matière_, Paris: CERF, pp. 209–245.
* –––, 2006, “What was Commentary in Late Antiquity? The Example of the Late Neoplatonic Commentaries”, in M. L. Gill and P. Pellegrin (eds.), _A Companion to Ancient Philosophy_, London: Blackwell Publishing, pp. 597–622.
* Karamanolis, G., 2004, “Porphyry: The First Platonist Commentator on Aristotle”, in P. Adamson, H. Baltussen, M. Stone (eds.), _Philosophy, Science and Exegesis in Greek, Arabic and Latin Commentaries_, _Bulletin of the Institute of Classical Studies_ (Supplementary Volume) 83(1): pp. 97–120.
* –––, 2006, “Plato and Aristotle in Agreement? Platonists on Aristotle from Antiochus to Porphyry”, Oxford: Oxford University Press.
* Longo, A. (ed.), 2009, _Syrianus et la métaphysique de l’antiquité tardive_, Naples: Bibliopolis.
* Lynch, J.P., 1972, _Aristotle School_, Berkeley, Los Angeles: University of California Press.
* Luna, C., 2001, _Trois études sur la tradition des commentaires anciens à la Métaphysique_, Leiden, Boston, Köln: Brill.
* Mansfeld, J., 1994, _Prolegomena: Questions to be Settled before the Study of an Author, or a Text_, Leiden, Boston, Köln: Brill.
* Marenbon, J., 1993, “Medieval Latin Commentaries and Glosses on Aristotelian Logical Texts, Before _c._ 1150 AD”, in C. Burnett (ed.), _Glosses and Commentaries on Aristotelian Logical Texts: The Syriac, Arabic, and Medieval Latin Traditions_, London: The Warburg Institute, pp. 77–127; reprinted as chapter 2 in J. Marenbon, 2000, _Aristotelian Logic, Platonism and the Context of Early Medieval Philosophy in the West_, Aldershot: Ashgate Publishing.
* –––, 1997, “Glosses and Commentaries on the _Categories_ and _De Interpretatione_ before Abelard”, in J. Fried (ed.) _Dialektik und Rhetorik im früheren und hohen Mittelalter_, Münich, pp. 21–49; reprinted as chapter 9 in J. Marenbon, 2000, _Aristotelian Logic, Platonism, and the Context of Early Medieval Philosophy in the West_, Aldershot: Ashgate Publishing.
* Menn, S., 2018, “Andronicus and Boethus: Reflections on M. Griffin, _Aristotle’s Categories in the Early Roman Empire_”, in _Documenti e studi sulla tradizione filosofica medievale_, 29: 13–43.
* Merlan, P., 1967, “The Peripatos”, in A.H. Armstrong (ed.), _The Cambridge History of Later Greek and Early Medieval Philosophy_, Cambridge: Cambridge University Press, pp. 899–949.
* Minio-Paluello, L., 1945, “The Text of the _Categories_: the Latin Tradition”, _Classical Quarterly_ 39: 63–74; reprinted in L. Minio-Paluello, 1972, _Opuscula: The Latin Aristotle_, Amsterdam, pp. 28–39.
* Most, G.W. (ed.), 1999, _Commentaries-Kommentare_, Göttingen: Vandenhoeck-Ruprecht.
* Moraux, P., 1942, _Alexandre d’Aphrodise: exégète de la noetique d’Aristote_ Liège and Paris: Faculté des Lettres and E. Droz.
* –––, 1973, _Der Aristotelismus bei den Griechen_, volume 1: _Die Renaissance des Aristotelismus im I. Jh. v. Chr_, Berlin and New York: Walter de Gruyter.
* –––, 1984, _Der Aristotelismus bei den Griechen_, volume 2: _Der Aristotelismus im I und II Jh. n. Chr._, Berlin and New York: Walter de Gruyter.
* –––, 1986, “Porphyre, commentateur de la ”Physique“ d’Aristote”, in Ch. Rutten and A, Motte (eds.), _Aristotelica: Mélanges offert à Marcel Corte_, Bruxelles: Ousia, pp. 327–344.
* –––, 1986, “Les début de la philologie aristotélicienne”, in G. Cambiano (ed.), _Storiografia e dossografia nella filosofia antica_, Turin: Tirrenia Stampatori, pp. 127–147.
* –––, 2001, _Der Aristotelismus bei den Griechen_, volume 3: _Alexander von Aphrodisias_, J. Wiesner (ed.), Berlin and New York: Walter de Gruyter.
* Perkams, M., 2019, “The Date and Place of Andronicus’Edition of Aristotle’’s Works According to a Neglected Arabic Source”, in _Archiv für Geschichte der Philosophie_, 101: 445–468.
* Praetcher, K., 1906, “Commentaria in Aristotelem Graeca XXII 2”, in _Göttingische Gelehrte Anzeigen_, 11: 861–907.
* –––, 1909, “Die griechischen Aristoteleskommentare, Commentaria in Aristotelem Graeca”, in _Byzantinische Zeitschrift_ 18: 516–538; reprinted in English translation in R. Sorabji (ed.), _Aristotle Transformed: The Ancient Commentators and their Influence_, London: Duckworth 1990, pp. 31–54.
* Primavesi, O., 2007, “Ein Blick in den Stollen von Skepsis: vier Kapitel zur frühen Überlieferung der Corpus Aristotelicum”, _Philologus_ 151: 51–77.
* –––, 2021, “Werk und Überlieferung”, in Ch. Rapp and K. Corcilius (eds.), _Aristoteles Handbuch_, Stuttgart: J. B. Metzler, second, augmented edition, pp. 67–74.
* Rashed, M., 2007, _Essentialisme: Alexandre d’Aphrodise entre logique, physique et cosmologie_, Berlin and New York: Walter de Gruyter.
* –––, 2014, _L’héritage aristotélicien. Textes inédits de l’antiquité_, Paris: Les Belles Lettres. Second, augmented, edition.
* Reinhardt, T., 2007, “Andronicus of Rhodes and Boethus of Sidon on Aristotle’s _Categories_”, in R. Sorabji and R. W. Sharples (eds.), _Greek and Roman Philosophy 100 BC-200 AD_, London: Bulletin of the Institute of Classical Studies (Supplementary Volume 94), pp. 513–529.
* Richard, M., 1950, “_Apo phonês_”, _Byzantion_, 20: 191–222.
* Schofield, M., 2012, _Plato, Aristotle, and Pythagoras in the First Century B.C._, Cambridge: Cambridge University Press.
* Sellars, J., 2004, “The Aristotelian Commentators: A Biographical Guide”, in P. Adamson, H. Baltussen, M. Stone (eds.), _Philosophy, Science and Exegesis in Greek, Arabic and Latin Commentaries_, _Bulletin of the Institute of Classical Studies_ (Supplementary Volume), 83(2): 239–268.
* Sharples, R.W., 1987, “Alexander of Aphrodisias: Scholasticism and Innovation”, in H. Temporini and W. Haase (eds.), _Aufstieg und Niedergang der römischen Welt_ (Volume 36, Number 2), Berlin, New York: Walter de Gruyter, pp. 1176–1243.
* –––, 1997, “The Peripatetic School”, in D. J. Furley (ed.), _Routledge History of Philosophy_, volume 2: _From Aristotle to Augustine_, London: Routledge, pp. 147–187.
* –––, 1998, “Alexander and pseudo-Alexander of Aphrodisias, _scripta minima_. _Questions_ and _problems_, _makeweights_ and prospects”, in W. Kullmann, J. Althoff, and M. Asper (eds.), _Gattungen wissenschaftlicher Literatur in der Antike_, Tübingen: Günter Narr Verla, pp. 383–403.
* –––, 2007, “Aristotle’s Exoteric and Esoteric Works: Summaries and Commentaries”, in R. Sorabji and R. W. Sharples (eds.), _Greek and Roman Philosophy 100 BC-200 AD_, London: University of London, Institute of Classical Studies, supplementary volume 94, pp. 505–512.
* –––, 2008, “Habent sua fata libelli: Aristotle’s _Categories_ in the 1st Century BC”, _Acta antiqua Hungarica_, 48: 273–287.
* –––, 2010, _Peripatetic Philosophy, 200 B.C. to A.D. 200. An Introduction and Collection of Sources in Translation_, Cambridge: Cambridge University Press.
* Shiel, J., 1990, “Boethius’ commentaries on Aristotle” in R. Sorabji (ed.), _Aristotle Transformed_, London: Duckworth and Ithaca: Cornell University Press, pp. 349–372.
* Schroeder, F. M., Todd, R. B. (eds.), 1990, _Two Greek Aristotelian commentators on the Intellect_, Toronto: Pontifical Institute of Medieval Studies.
* Solmsen, F., 1944, “Boethius and the History of the _Organon_”, in _American Journal of Philology_, 65: 69–74.
* Sorabji, R. (ed.), 1990, _Aristotle Transformed: The Ancient Commentators and their Influence_, London: Duckworth.
* –––, 2016, _Aristotle Re–interpreted: New Findings on Seven Hundred Years of the Ancient Commentators_, London: Bloomsbury Publishing.
* Tarán, L., 1981, “Aristotelianism in the 1st century BC”, _Gnomon_ 57: 721–750; reprinted in L. Taràn, 2001, _Collected Papers_, Leiden, Boston, Köln: Brill, pp. 479–524.
* Tuominen, M., 2009, _The Ancient Commentators on Plato and Aristotle_, Berkeley, Los Angeles: University of California Press.
* Watts, E., 2006, _City and School in Late Antiquity: Athens and Alexandria_, Berkeley, Los Angeles: University of California Press.

## Academic Tools

| ![sep man icon](https://plato.stanford.edu/symbols/sepman-icon.jpg) | [How to cite this entry](https://plato.stanford.edu/cgi-bin/encyclopedia/archinfo.cgi?entry=aristotle-commentators).                                                                      |
| ------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ![sep man icon](https://plato.stanford.edu/symbols/sepman-icon.jpg) | [Preview the PDF version of this entry](https://leibniz.stanford.edu/friends/preview/aristotle-commentators/) at the [Friends of the SEP Society](https://leibniz.stanford.edu/friends/). |
| ![inpho icon](https://plato.stanford.edu/symbols/inpho.png)         | [Look up topics and thinkers related to this entry](https://www.inphoproject.org/entity?sep=aristotle-commentators\&redirect=True) at the Internet Philosophy Ontology Project (InPhO).   |
| ![phil papers icon](https://plato.stanford.edu/symbols/pp.gif)      | [Enhanced bibliography for this entry](http://philpapers.org/sep/aristotle-commentators/) at [PhilPapers](http://philpapers.org/), with links to its database.                            |

## Other Internet Resources

* [List of the ancient commentators on Aristotle](https://de.wikisource.org/wiki/Commentaria\*in\*Aristotelem\*Graeca), with links to digitalized versions of the _Commentaria in Aristotelem Graeca_ \[CAG].
* [Ancient Commentators on Aristotle Project](http://www.ancientcommentators.org.uk/).
* [Concordances with the _Commentaria in Aristotelem Graeca_](http://www.archimedespalimpsest.org/) \[CAG].
* [Aristoteles Latinus](http://www.ancientcommentators.org.uk/concordance-with-cag.html), hosted by KU Leuven.
* [The Archimedes Palimpsest Project](http://www.archimedespalimpsest.org/), William Noel, _et al_.
* [Richard Sorabi on the Ancient Commentators](http://www.historyofphilosophy.net/commentators-sorabji), a short interview with Peter Adamson (Philosophy, LMU Munich).
* [History of Philosophy _without any gaps_](https://historyofphilosophy.net/), Peter Adamson (Philosophy, LMU Munich).

## Related Entries

[Alexander of Aphrodisias](https://plato.stanford.edu/entries/alexander-aphrodisias/) | [Ammonius](https://plato.stanford.edu/entries/ammonius/) | [Aristotelianism: in the Renaissance](https://plato.stanford.edu/entries/aristotelianism-renaissance/) | [Aristotle](https://plato.stanford.edu/entries/aristotle/) | [Aristotle, General Topics: categories](https://plato.stanford.edu/entries/aristotle-categories/) | [Boethius, Anicius Manlius Severinus](https://plato.stanford.edu/entries/boethius/) | [David](https://plato.stanford.edu/entries/david/) | [Elias](https://plato.stanford.edu/entries/elias/) | [Neoplatonism](https://plato.stanford.edu/entries/neoplatonism/) | [Olympiodorus](https://plato.stanford.edu/entries/olympiodorus/) | [Philoponus](https://plato.stanford.edu/entries/philoponus/) | [Plato](https://plato.stanford.edu/entries/plato/) | [Plotinus](https://plato.stanford.edu/entries/plotinus/) | [Porphyry](https://plato.stanford.edu/entries/porphyry/) | [Simplicius](https://plato.stanford.edu/entries/simplicius/)

### Acknowledgments

Thanks to Christopher Shields for commenting on a draft of this entry.

[Copyright © 2021](https://plato.stanford.edu/info.html#c) by\
[Andrea Falcon](http://www.andreafalcon.net/) <[_Andrea.Falcon@concordia.ca_](mailto:Andrea%2eFalcon%40concordia%2eca)>
