# 阿毘达磨 Abhidharma (Noa Ronkin)

_首次发布于 2010 年 8 月 16 日星期一；实质性修订于 2022 年 6 月 1 日星期三_

在释迦牟尼佛涅槃后的最初几个世纪，随着佛教在印度次大陆的传播，出现了多种思想学派和教师传承。这些新形式的学术僧团有着独特的理论和实践兴趣，在组织、解释和重新审视佛陀零散的教导时，它们发展出一种特定的思想体系和阐述方法，称为阿毘达磨（巴利语为阿毗达摩）。梵文术语_abhidharma_似乎源自于“关于（_abhi_）教导（梵文_dharma_，巴利文_dhamma_）”的表达。然而，对于佛教的注释传统而言，这个术语大致意味着“更高”或“更进一步”的教导，它既指代新学术运动的教义探讨，也指代由其对佛教思想进行系统阐述而产生的文本体系。这一文献体系包括佛教经典的“三藏”之一，即_阿毗达磨藏_（巴利语为_阿毗达摩藏_），以及其评论和后来的注释性文本。

阿毘达磨作为一种独立的文学体裁和思想探讨分支，与经论（佛陀经文体系）相对应，后者是佛陀的讲演系统（梵文，_sūtras_，巴利文，_suttas_）。与早期口语化的佛教经文不同，阿毘达磨方法以技术术语呈现佛陀的教导，这些术语经过精心定义，以确保分析的准确性。在内容上，阿毘达磨在努力提供理论对应于佛教禅修实践，更广泛地说，提供有关众生体验的系统性阐述方面具有独特性。它通过将意识体验——在这个意义上是一个人的“世界”——分析为其组成的心理和物理事件（梵文，_dharmā_，巴利文，_dhammā_，以下分别称为_dharmas/dhammas_）。将_dharmas_分析为多个类别，并通过它们的多方面因果关系的统一结构综合起来的总体探究被称为“_法_理论”。对_dharmas_的性质和相互作用进行的详尽调查延伸到形而上学、认识论和本体论领域，并在不同佛教学派之间引发教义争议。阿毘达磨对这些争议的分析和论证方法为中观派和瑜伽行派的大乘学派提供了参考框架，并定义了议程。因此，作为一个独特的教义运动，阿毘达磨对后来的佛教思想产生了显著影响，并催生了佛教系统哲学和解经学。

* [1. 阿毘达磨：其起源和文本](https://plato.stanford.edu/entries/abhidharma/#AbhOriTex)
* [1.1 文体和流派](https://plato.stanford.edu/entries/abhidharma/#LitStyGen)
* [1.2 阿毘达磨解经：从](https://plato.stanford.edu/entries/abhidharma/#AbhExeDhaDha)​\*[Dharma](https://plato.stanford.edu/entries/abhidharma/#AbhExeDhaDha)_​_[_到_](https://plato.stanford.edu/entries/abhidharma/#AbhExeDhaDha)_​_[dharmas](https://plato.stanford.edu/entries/abhidharma/#AbhExeDhaDha)\*
* [2. 阿毘达磨分类学：经验的形而上学](https://plato.stanford.edu/entries/abhidharma/#DhaTaxMetExp)
* [3. 时间：从无常到瞬时](https://plato.stanford.edu/entries/abhidharma/#TimImpMom)
* [4. 内在本质：分类与本体论之间](https://plato.stanford.edu/entries/abhidharma/#IntNatBetCatOnt)
* [5. 因果：存在即功能](https://plato.stanford.edu/entries/abhidharma/#CauExiFun)
* [6. 认识论：感知和意识过程理论](https://plato.stanford.edu/entries/abhidharma/#EpiPerTheConPro)
* [参考文献](https://plato.stanford.edu/entries/abhidharma/#Bib)
* [主要来源](https://plato.stanford.edu/entries/abhidharma/#PriSou)
* [次要来源](https://plato.stanford.edu/entries/abhidharma/#SecSou)
* [学术工具](https://plato.stanford.edu/entries/abhidharma/#Aca)
* [其他互联网资源](https://plato.stanford.edu/entries/abhidharma/#Oth)
* [相关条目](https://plato.stanford.edu/entries/abhidharma/#Rel)

***

## 1. 阿毘达磨：起源和文本

印度佛教的早期历史知之甚少，试图建立起一套连贯的历史年表仍然吸引着当代学者们的注意。 一个普遍被接受的传统说法是，大约在公元前三世纪初，原始佛教团体分裂为两派或两个兄弟会：声闻部（巴利语，Theriyas）和大众部，从那时起，它们各自拥有自己的受戒传统。 在随后的大约两个世纪中，这两个派别之间出现了教义争议，导致了各种思想学派（_vāda; ācariyavāda_）和师承传承（_ācariyakula_）的形成（_Vin_ 51–54；_Mhv_ V 12–13。参见 Cousins 1991，27–28；Frauwallner 1956，5ff 和 130ff；Lamotte 1988，271ff；Wynne 2019，269–283）。

根据传统佛教记载，在大乘佛教教义兴起时，大约在公元前一世纪，有十八个部派或声闻部的学派，这些学派是南传佛教的传统，即上座部（“长老教义的拥护者”）。然而，十八这个数字在佛教史学中成为惯例，出于象征和记忆的原因（Obeyesekere 1991），实际上，不同的佛教文献保存了不同的学派列表，总数超过十八个。很可能是佛教团体的早期形成时期产生了多个智力分支，这些分支由于佛教团体在整个印度次大陆的地理扩展以及每个僧团所面临的特定问题而自发发展。每个僧团倾向于专攻特定的学科，有自己的实践习俗和与在俗圈子的关系，并受到特定领土、经济、语言和方言在其环境中盛行的影响。事实上，“十八部派”的名称表明它们起源于特定的教义、地理位置或特定创始人的遗产：例如，般若部（“主张一切事物存在的教义”）、经部（“依赖经典的人”）/例部（“运用例证的人”）和人见部（“肯定个人存在的人”）；喜马拉雅部（“雪山之人”）；或跋提部（“与跋提有关的人”）等。正如 Gethin（1998 年，52 页）所指出的，与基督教中的教派或宗派不同，“后来佛教传统提到的一些学派很可能是类似于‘笛卡尔派’、‘英国经验论者’或‘康德派’的思想学派，属于现代哲学史的范畴。”斯坦福大学/条目/阿毘达磨/注释。html#note-2)]

据惯例认为，多个古代佛教学派传承了自己版本的阿毘达磨藏，但只有两个完整的经藏得以保存，代表了两个学派：阿育王时代的撒娜斯提婆学派，他们在公元前二世纪或前一世纪从释迦部派中独立出来，成为印度北部，尤其是西北部的主导学派，并传播到中亚；以及锡兰的南传部，是释迦部的一个分支，传播到印度南部和东南亚部分地区。这两个现存的经藏构成了佛教经典的“三藏”之一。撒娜斯提婆学派和南传部的注释传统认为，他们各自的经藏阿毘达磨由一套七部经典组成，尽管每个学派规定了不同的经典。撒娜斯提婆学派的阿毘达磨藏包括《众会分说》（_Discourse on the Collective Recitation_）、《法藏》（_Compendium of_ _Dharmas_）、《识量论》（_Manual of Concepts_）、《识藏》（_Compendium of Consciousness_）、《界藏》（_Compendium of Elements_）、《论事品》（_Literary_ _Exposition_）和《智行门》（_The Foundation of Knowledge_）。这七部经文仅在其古代汉译本中完整保存。南传部的阿毘达磨藏包括《法集》（_Enumeration of_ _Dhammas_）、《分别》（_Analysis_）、《界说》（_Discourse on Elements_）、《人品记》（_Designation of Persons_）、《论题》（_Points of Discussion_）、《对》（_Pairs_）和《因缘》（_Causal Conditions_）。这七部经文以巴利文保存，除了《对》之外，其他都已被翻译成英文。

后代对阿毗达磨经进行了评论，并引入了各种阐释手册，阐明了经典体系的基本原理。这些后经文本是单一作者的作品，展示了各自学派完全发展的辩论立场和教派世界观。大部分南传部阿毗达磨体系包含在佛教学者佛陀耶舍（Buddhaghosa）编写的全面著作《净土之道》（_Visuddhimagga_，公元五世纪）中。更直接的入门阿毗达磨手册包括佛达多（Buddhadatta）的《阿毗达磨入门》（_Abhidhammāvatāra_，公元五世纪）和阿努鲁达（Anuruddha）的《阿毗达磨要略》（_Abhidhammatthasaṅgaha_，公元十二世纪）。根据汉译，舍利弗部传承保留了三种不同版本的权威阿毗达磨注释或《毗婆沙》（_vibhāṣā_），日期可追溯到公元一至二世纪，其中最后一个也是最著名的是《大毗婆沙》（_Mahāvibhāṣā_）。《毗婆沙》汇编记录了几个世纪的学术活动，代表了多个舍利弗部门派，尤其是被称为舍利弗部-毗婆沙学派的喀什米尔舍利弗部门派。然而，对后世中国和藏传佛教最有影响的舍利弗部手册是世亲（Vasubandhu）的《阿毗达磨宝藏》（_Abhidharmakośa_，公元五世纪）。《阿毗达磨宝藏》的自注包含了对正统舍利弗部立场的重要批评，后来的舍利弗部-毗婆沙大师们试图驳斥这些批评。在这一类别中尤为著名的是世亲同时代的僧伽跋陀罗（Saṅghabhadra）的《依正理论》（_Nyāyānusāra_）。这部全面的论著重新确立了正统舍利弗部观点，被认为是幸存下来的最后一部舍利弗部作品之一。

总的来说，阿毘达磨/阿毗达磨文本基本上是与早期佛教学派历史形成时期同时期的著作，为一组人提供了定义自身并捍卫其立场免受其他派别不同解释和批评的手段。尽管阿毗达磨思维的许多内容以及其方法在某种程度上借鉴于《阿含经》/《尼柯耶经》，即《经》的集合（巴利语，经文），但其文学主体包含了对佛陀言教的解释，这些解释针对每个思想学派具体而言，并对重点强调的教义问题进行了哲学阐释。这些内容继续由随后几代的僧侣完善，他们为两个幸存的上座部和舍利弗部学派的巩固做出了贡献。

### 1.1 文学风格和流派

学术界对“阿毗达磨”一词的解释一直存在两种不同观点，这两种观点都取决于前缀“阿毗”的含义。首先，将“阿毗”理解为“关于”，则“阿毗达磨”被理解为一个学科，其主题是佛陀的教导“法”。其次，将“阿毗”解释为优势和区别，也有人认为“阿毗达磨”是一种独特的、更高层次的教导；是佛陀教导的本质，或者超越了佛陀言论所包含的内容，在某种程度上类似于“形而上学”（例如，Dhs-a 2-3；Horner 1941；von Hinüber 1994）。佛教传统本身区分了经法和阿毗达磨两种教导方式，通过对比经法中需要进一步澄清的部分性、比喻性表达方式，与阿毗达磨中全面阐述教义的表达方式和问答方式进行对比（A IV 449-456；Dhs-a 154）。这与传统所作的其他区分相吻合，例如区分具有隐含意义（梵文 neyārtha，巴利文 neyyattha）和具有明显意义（梵文 nītārtha，巴利文 nītattha）的文本（A I 60；Ps I 18），以及用传统术语表达的文本（梵文 saṃvṛti，巴利文 sammuti）与用终极术语表达的文本（梵文 paramārtha，巴利文 paramattha）之间的区别（Vibh 100-101；Mp I 94-97）。从阿毗达磨的角度来看，经文是用传统术语传达的，其终极含义需要进一步解释。

阿毘达磨的经典文本是经过数十年，甚至数个世纪的演变而形成的作品，这些作品源自经藏中已有的经律部分的材料。这在这一体裁的两个特征中得到了证实，这两个特征可以追溯到更早期的佛教文献。第一个特征是文本的分析风格，试图精确总结佛法的重要观点，并提供构成有情体验的心理和物理因素的全面分类。这种分析工作包括将材料的主要部分围绕着详细的因素列表和它们的类别集合进行安排，形成了教义主题的矩阵（梵文，_mātṛkā_，巴利文，_mātikā_）。在佛陀的经文集中，已经有一些文本是根据分类列表进行安排的，提供了关于教义项目的公式化处理，这些项目在其他地方有所阐述。列表显然是强大的记忆辅助工具，在早期佛教文献中的普遍存在在一定程度上可以解释为口头传承的结果，而这种传承已经持续了数个世纪。例如，四部主要的阿含/部，即“结集”（_saṃyukta/saṃyutta_）的集合，根据特定主题对佛陀的教导进行分组，包括四圣谛，四念处，五蕴，六入，七觉支，八正道，十二因缘等。类似的分类列表构成了南传的《分别说》和《人品品》的目录，以及舍宗部的《集会品》和《法藏品》，这些都被构建为对这些列表的评论。

阿毘达磨文献的第二个特征是其倾向于通过问答式阐释进行辩论性诠释。这些文本似乎是早期佛教团体内关于教义讨论的产物。同样，这种讨论已经出现在《阿含/部》（例如，_M_ I 292–305, III 202–257）的收集中：它们经常以待澄清的教义观点开始，然后采用问题和答案的教学方法阐述相关主题。这些文本还记录了更正式的辩论方法和对竞争理论的驳斥，这些方法阐明了阿毘达磨作为对日益辩论环境需求的回应的演变过程。佛教思想在当时经历的制度化过程以及佛教团体在印度次大陆的传播，与口头传统向书面传播方法的过渡以及各种佛教学派之间关于教义的僧侣辩论的兴起相吻合。佛教僧侣团体与同时代的梵文语法学家、耆那教徒和婆罗门学派之间也存在知识吸收和教义争议，他们不断发展的学术和分析运动也必然促成了阿毘达磨的辩论性诠释和辩论风格。辩证法格式和对教义解释差异的意识展示是《论辩论》和《心部》的特征。后来，后经典阿毘达磨文本成为复杂的哲学论著，采用复杂的辩论方法和独立调查，导致教义结论与它们的经典前辈相去甚远。

阿毘达磨文献起源于早期佛教团体内讨论佛法的两种方法：第一种意在总结和分析佛陀教导的重要观点，第二种通过僧侣辩论来阐释和解释教义（Bronkhorst 2016, 29–46; Cousins 1983, 10; Dessein 2016, 4–7; Gethin 1992b, 165; Gethin 2022, 227–242）。

### 1.2 阿毘达磨解经：从_Dharma_到_dharmas_

佛陀在《阿毘达磨》/《尼柯耶》中收集的经文从不同的角度分析了众生的体验：从名色（_nāma-rūpa_）、五蕴（梵文，_skandha_，巴利文，_khandha_）、十二入（_āyatana_）或十八界（_dhātu_）的角度。所有这些分析方式都描述了众生体验的过程，这些过程是一系列根据各种原因和条件产生和终止的身心过程。《经藏》和《阿毘达磨》世界观之间一个显著的区别是，《阿毘达磨》将这些过程的时间尺度缩短，因此现在它们被看作是从一刻到另一刻运作的。换句话说，《阿毘达磨》重新解释了《经》所描绘的顺序过程的术语，将其应用于离散的、瞬时的事件（Cousins 1983, 7; Ronkin 2005, 66–78）。

这些事件被称为_法_（巴利语_dhammas_），与表示佛陀教义的单数_dharma/dhamma_不同。《阿含经/部》使用_法_这个形式来传达所遇到现象的多元表现，即我们通过六个根（五个普通的生理感官加上心\[_manas_]）体验到的所有感官现象。然而，经典的阿毘达磨论著在心理范畴内进行微妙的区分，并边缘化多种心理能力之间的差异。在这个背景下，_法_被视为特定心理能力称为心理认知意识（梵语_manovijñāṇa_，巴利语_manoviññāṇa_）的对象，后者被认为是感官知觉过程中的中心认知操作。心理认知意识是一种特殊类型的意识，它区分刺激对感官根的影响，并在必要条件齐备时出现。_法_不仅仅是像思想、概念或记忆这样的心理对象。作为心理认知意识的对象，_法_可以被视为感知：快速的意识类型（_citta_），在顺序流中出现和消失，每个都有自己的对象，并与五个外向的感官模式（视觉、听觉等）的认知意识互动。经典的阿毘达磨文本将_法_描绘为具有多种能力的心理-生理事件，通过这些能力，心灵将一个特定的感知，尤其是新呈现的感知，与已经拥有的一组或一大批思想结合和吸收，从而理解和概念化它。\[[6](https://plato.stanford.edu/entries/abhidharma/notes.html#note-6)]

最终，_dharmas_ 就是一切：所有经验事件都被理解为从 _dharmas_ 的相互作用中产生。虽然原子的类比在这里可能是有用的，但 _dharmas_ 显著地涵盖了物理和心理现象，并且通常被理解为瞬息的事件、发生或动态属性，而不是持久的实体。\[7] 阿毘达磨的阐释试图对每种可能类型的经验提供详尽的说明——即可能出现在一个人意识中的每种发生类型——以其构成 _dharmas_。这一事业涉及将普通知觉的对象分解为其构成的离散 _dharmas_，并澄清它们的因果关系。将 _dharmas_ 的分析分为多个类别并通过它们的多方面因果关系的综合将其合成为一个统一结构的总体探究被称为“_dharma_ 理论”。

## 2. _dharma_ 分类学：一种经验形而上学

阿毘达磨试图个体化和确定每个法的独特身份，产生复杂交叉的法的分类法，由多个标准或一组特质组织的法。不同学派的阿毘达磨文本提出了不同的法分类法，列举了更多或更少有限数量的法类别。然而，重要的是要记住，术语“法”既表示代表一种事件类型的任何类别，也表示其特定令牌或实例中的任何一个。上座部尼共提出了八十二种法类别的系统，这意味着在经验世界中有八十二种可能的事件类型，而不是八十二个事件。这些被组织成四种分类。前三个类别包括意识的裸现象（citta），它包含一个单一的法类型，其基本特征是认知对象；相关心理（cetasika）包含五十二种法；以及物质或物理现象（rūpa）包括二十八种构成所有物理事件的法（阿毗达磨 1）。这三个广泛类别中的八十一个法类型都是有条件的（saṅkhata）。有条件的法随着众多原因和条件的影响而产生和终止，并构成轮回中所有领域的有情体验（saṃsāra）。第四类别包括的第八十二法是无条件的（asaṅkhata）：它既不通过因果互动而产生，也不会终止。这第四类别中的单一事件是涅槃（巴利语，nibbāna）。

阿毘達磨採用了一種將七十五種基本_法_組織成五類的系統。前四類包括所有有條件的(_saṃskṛta_)_法_，包括意識(_citta_，一種單一_法_)；相關心理(_caitta_，包括四十六_法_)；和物質現象(_rūpa_，十一_法_)；還有與思想脫离的因素(_cittaviprayuktasaṃskāra_，十四_法_)。最後一類在經典中沒有提及，也不在南傳上座部的列表中，但在所有時期的北印度阿毘達磨文本中佔主導地位。其中包含的具體_法_各不相同，但它們都被理解為解釋一系列經驗事件，它們本身與物質形式和思想都脫節。阿毘達磨的第五類別，即無為(_asaṃskṛta_)，包括三種_法_，即空間和兩種滅(_nirodha_)，後者是一個表示佛教道路達到頂峰的術語(Cox 1995; 2004A, 553–554)。

阿毘達磨通過對這些類別進行詳細分析，從而創建了關係模式，使得每個確認的經驗、現象或事件都可以通過特定的定義和功能來確定和識別。特別重要的是對意識或_citta_的分析，阿毘達磨教義思想的許多基本原則都建立在此之上。考慮南傳上座部對意識的分析，其基本原則與其他阿毘達磨系統共享。

阿毘达磨中意识运作的典范是在感官知觉过程中体验到的_citta_，在阿毘达磨（以及佛教一般）中，被视为有情体验的范式。_Citta_永远不能被体验为赤裸的意识在其自身起源的时刻，因为意识总是有意向的，指向特定的客体，这些客体是通过某些心理因素认知的。因此，_Citta_总是与其适当的_cetasika_或心理因素一起出现，这些心理因素执行不同的功能，并与之一起出现和消失，具有相同的客体（无论是感官的还是心理的）并根植于相同的感官。任何给定的意识时刻——也被称为_citta_这个术语——因此是_citta_及其相关心理因素的独特集合，比如感觉、概念化、意志或注意力等，这些因素是任何思维过程中所需的几种因素之一。每个集合只意识到一个客体，出现一瞬间然后消失，随后是另一个_citta_组合，通过其特定的相关心理因素选择不同的客体。

阿毗达磨经典方案，从《阿毗达磨藏》第一册《法集要》中概括而来，并由评注传统组织，描述了八十九种基本意识瞬间，即由心和心所组成的集合（《阿毗达磨藏》第一册；《修心论》第十四章 81-110；《阿毗达磨论》1-15；《阿毗达磨释》1-5）。它根据它们发生的位置最广泛地对这些基本心类型进行分类，从包括四十五种心类型的色界开始，其中最突出的是涉及感官对象知觉机制的心；接下来是十八种形界意识，涉及达到禅定吸收的心；然后是八种无色界意识，构成了达到进一步无色境界的心；最后，有十八种超越世间的意识，构成了觉醒时刻的心：它们的对象是涅槃。在这四个广泛类别中，还有许多其他分类。例如，一些法是善的，另一些是恶的；有些是果报的，另一些则不是；有些是有动机的，另一些则没有动机。考克斯（2004 年 A，552）写道，这些属性矩阵形成了“由实际发生的法所展示的所有可能条件和特征的抽象网络。然后，可以根据每种分类可能性来具体说明任何特定法的个体特征，从而完整评估该法可能发生的范围。”

各种学者认为，这个系统反映了对_法_动态概念的理解：阿毘达磨将_法_理解为构成一个人世界的属性、活动或相互关联的模式，而不是静态的实体（例如，Cox 2004A，549 页以下；Gethin 1992A，149-150 页；Karunadasa 2010，第 4 章；Nyanaponika 1998，第 2 和 4 章；Ronkin 2005，第 4 章；Waldron 2002，2-16 页）。阿毘达磨的_法_列表“明确开放”，反映了“某种不情愿和犹豫地断言这样或那样是_法_的明确列表”的态度（Gethin 2017，252 页），为关于什么是_法_和什么不是_法_的持续辩论留下了空间。对于阿毘达磨和佛教整体而言，一个人世界的界限由一个人的生活经验的界限设定，而生活经验的因果基础是一个人认知装置的运作。根据佛教之道，基于一个人认知装置的生活经验的本质应通过禅修实践来反思一个人心灵的本质。从这个角度来看，阿毘达磨代表了禅修实践的理论对应物。在这种佛教实践的背景下，_法_是意识时刻的独特（但相互关联的）功能、能量或因果相关的方面，在这个意义上是意识时刻的“组成部分”。

阿毘达磨的范畴分析因此是一种辨识阿毘达磨的默观修行：它并非旨在对所有现存的阿毘达磨进行封闭清单式的归类，而是“具有涉及两个同时过程的双重解脱目的”（Cox 2004A, 551）。首先，作为“评估性”分析，阿毘达磨分类学描绘了心灵的构成和运作方式，并解释了构成普通善意意识与觉醒心灵的区别。例如，在达到禅定吸收状态的心灵中产生的意识类型变得越来越精致，可能永远不会涉及某些在普通（甚至善意）意识中可能发生的倾向或玷污。根据 Gethin（2004, 536）的说法，观察阿毘达磨如同阿毘达磨，“涉及观察它们如何产生和消失，如何放弃某人想要放弃的特定品质，以及如何发展某人想要发展的特定品质。以这种方式观察阿毘达磨，人开始理解\[...]关于这些阿毘达磨的四个确切真理——它们与苦的关系，苦的产生，苦的停止以及停止苦的方法。通过看到这四个真理，人意识到关于世界的终极真理——关于阿毘达磨的真理。”\[9] (https://plato.stanford.edu/entries/abhidharma/notes.html#note-9)

第二个“描述性”的解脱过程涉及对_dharmas_进行分类，揭示了有情体验的流动性质，并验证了基本的佛教教义——无我（梵文，_anātman_，巴利文，_anatta_）。对_dharmas_越来越详细的列举表明，在任何现象或其组成部分中都找不到任何本质或独立的自我，因为体验的所有方面都是无常的，随着众多原因和条件的产生和消逝。甚至被归类为无条件的少数_dharmas_（即没有因果关系）也被证明是无我的。对_dharmas_的辨析实践因此破坏了我们在情感和智力上把握的表面坚固世界，这个世界充满了欲望和执著的对象。“试图_把握_《法集》，或《因明》的世界，”盖丁指出（1992B，165 页），“它会从手指间流失。”

然而，_阿毘达磨_对于诸行无常的概念，作为有情体验的基本构成单元或最终分析单元的多样性的概念，标志着佛教对_阿毘达磨_理解的相当大的转变。阿毘达磨思想逐渐倾向于将_阿毘达磨_解释为现象世界的基本构成要素，并将_阿毘达磨_越来越多地与主要存在者联系起来。_阿毘达磨_分类中的无条件类别也肯定了持久或永久_阿毘达磨_的可能性，与通过因果互动产生和终止的所有其他_阿毘达磨_形成对比。因此，_阿毘达磨_的注释在佛教圈中引发了可以称之为本体论争议的教义争议，涉及诸如_阿毘达磨_的本质是什么；在_阿毘达磨_的内部构成中，是什么使其成为独特的本质；_阿毘达磨_的存在方式；它们因果互动的动态；以及它们构成的现实的本质。佛教各派构建的独特原则及其随后的本体论解释在很大程度上受到将无常解释为瞬时性的激进解释的影响。

## 3. 时间：从无常到瞬时

无论是诸部部洲阿毗达磨（Sarvāstivāda）还是大部后藏部阿毗达磨（post-canonical Theravāda）都建立了一种激进的瞬时论（梵文_kṣāṇavāda_，巴利文_khāṇavāda_），通过将现象在时间上原子化，将它们解剖成一系列离散的瞬时事件，这些事件一旦产生就消失了。尽管在佛陀的讲演中并非一个独立的主题，瞬时论似乎是与无常原则（梵文_anitya_，巴利文_anicca_）相结合而产生的。这个观念是佛陀关于众生体验本质的经验导向教导的基础：所有身心现象都在一个不断的有条件构建过程中，并且彼此相互联系，是依赖性产生的（例如_A_ I 286；_M_ I 230，336，500；_S_ II 26，III 24–5，96–9，IV 214）。关于这三个相互关联观念的经文阐述导致了一个公式（_A_ I 152），其中陈述了有条件现象（梵文_saṃskārā_，巴利文_saṅkhārā_）具有产生（_uppāda_）、“持续物的变化”（_ṭhitassa aññathatta_）和溶解或终止（_vaya_）的本质。这个公式被称为“有条件之三性”（_tisaṅkhatalakkhaṇa_）。部洲部-毗婆沙（Sarvāstivāda-Vaibhāṣika）引入了有条件现象的四个特征：产生、持续、衰败和消亡。这些被归类为“离思维因素”的_法_类别。

佛教学派使用有条件现象的特征作为解释工具，重新解释无常，将其称为瞬间性。阿毗达磨-毗婆舍那派提出了一个完整的瞬间性理论，根据这一理论，所有的身心现象都是瞬间的。阿毗达磨派使用术语“刹那”（_kṣaṇa_）具有高度技术性，作为最小的、确定的不可分割的时间单位，其长度被等同于心理事件的持续时间，作为最短的可想象的实体。关于一个瞬间的长度，阿毗达磨派并没有达成共识，但文献中提到的数字在现代术语中介于 0.13 到 13 毫秒之间（见 Gethin 1998, 221; von Rospatt 1995, 94–110）。这种用法假定了时间的原子论观念，因为时间并非无限可分。事实上，术语_kṣaṇa_经常与物质原子和音节的概念并列讨论，这些概念同样被理解为不可分割的。

在阿毘达磨的框架内，物质现实（_rūpa-dharma_）被简化为离散的瞬时原子，并且很多关注点集中在本体论和认识论问题上，比如感官对象在任何时候是否真实，或者原子是单独还是集体地对知觉的产生做出贡献。原子现实被理解为不断变化的：对我们而言，一个由持久物质和变化特质构成的世界实际上是一系列快速出现和消逝的时刻。这个过程并非随机，而是根据每个原子的特定能力和功能运作。对物质现实的这种原子分析精神同样适用于心理现实：意识被理解为一系列快速出现和停止的离散意识时刻。\[[10](https://plato.stanford.edu/entries/abhidharma/notes.html#note-10)] 因此，在任何给定时刻，物质和心理现象之间的变化比率是一比一：它们完全同步发生（Kim 1999, 54）。在这一点上，舍宗与根本说一致。

阿毘达磨（“一切有部”）在他们的立场上独树一帜，认为有条件现象的特征在每一刻都作为独立的实体存在。因此，他们主张，所有有条件的_法_——无论过去、现在还是未来——都作为实体（_dravyatas_）存在于任何给定时刻的范围内。这引发了一系列问题，其中之一是，阿毘达磨对时刻的定义很难与其作为时间最短单位的概念相一致（von Rospatt 1995, 44–46 & 97–98）。阿毘达磨对这一批评的回应是，有条件现象的四个特征的活动（_kāritra_）是连续的：任何事件的诞生和消亡之间的界限被称为一个时刻。然而，这个解决方案意味着一个单一事件在给定时刻内经历四个阶段，这必然侵犯了它的瞬时性（Cox 1995, 151; von Rospatt 1995, 52ff）。

Theravādins 创造了他们自己独特的瞬间性学说版本。他们似乎并不像 Sarvāstivādins 那样关心物质和心理实相的本体论和认识论。相反，他们更关注统辖感官数据认知过程的心理装置，因此更关注物质和心理现象之间变化比例。《阿毗达磨》经典的《疑经》可能是“瞬间”（_khaṇa_）一词首次以“非常短暂的时间段，分为起源和终止瞬间”之义的文本出现（Kim 1999, 60–61）。依赖于有条件现象的三个特征，巴利评论后来提出了一个方案，其中每个现象的每个瞬间被细分为起源（_uppādakkhaṇa_）、持续（_ṭhitikkhaṇa_）和终止（_bhaṅga\*\*kkhaṇa_）三个不同瞬间（_Spk_ II 266；_Mp_ II 252）。这些是一个单一瞬间现象的三个阶段，被定义为一个单一的_dhamma_或意识瞬间。一个_dhamma_发生在第一个子瞬间，持续到第二个，然后在第三个终止（Karunadasa 2010, 234ff）。评论传统通过将现象解剖为一系列离散的瞬间事件来对现象进行时间分析，这些事件一旦在意识中产生就消失了。当一个事件耗尽时，它会引发一个同类的新事件，紧随其后。结果是一个不间断的、流动的因果相连的瞬间事件连续体（_santāna_）。这些事件相继发生得如此之快，以至于我们将它们构成的现象视为在时间上延伸。

Theravādins 将术语_khaṇa_用作瞬间的表达，其维度并非固定，而是可以由上下文确定。例如，_citta\*\*khaṇa_指的是一个心理事件所需的瞬间。在基本意义上，表示非常短暂的时间段的术语“瞬间”并不意味着时间的原子概念是明确且最终的最小时间单位，而是留下了时间是无限可分的可能性（von Rospatt 1995, 59–60 & 94–95）。在这里，起源、持续和终止的三个瞬间并不对应于三个不同的实体。相反，它们代表一个瞬时现象的三个阶段，并被定义为一个单一的意识瞬间：一个_dhamma_在第一个子瞬间发生，在第二个子瞬间持续，在第三个子瞬间消亡。通过这种方式，Theravādins 避免了 Sarvāstivāda-Vaibhāṣikas 所面临的一些困难，即如何将有条件的特征压缩为一个不可分割的瞬间，以及如何解释它们的本体论地位。Theravādins 还声称，只有心理现象是瞬时的，而物质现象（例如，常识对象）会持续一段时间。Theravādin 的注释传统随后对这一命题进行了详细阐述，并提出了一种独特的物质和心理现象之间的比例观点，声称物质现象持续十六或十七个意识瞬间（_Kv_ 620；_Vibh-a_ 25–28；_Vism_ XX 24–26；Kim 1999, 79–80 & §3.1）。

尽管早期佛教学派对瞬时性概念有不同解释，但它们都是从对诸行无常的分析中推导出这一概念，即从_法_作为身心事件的动态来看。将瞬间与这些瞬息不断事件的持续时间等同起来，即使是最短暂的事件，也导致直接确定瞬间的这些事件。然而，瞬时性教义为佛教学派带来了一系列问题，特别是关于持续瞬间的地位以及对_法_之间连续性和因果互动的解释（见下面的第 5 节）。如果_法_经历持续阶段或在任何给定瞬间内存在为实体，它们如何能是瞬间的呢？如果经验是一系列严格瞬时的_法_，那么连续性和因果关系如何可能呢？

有人可能会认为，从“无常”到“持久”的概念转变是学究式的文字主义的结果，并证明了阿毘达磨倾向于对法的再实体化和实在化（Gombrich 1996, 36–37, 96–97 & 106–107）。然而，瞬时性教义的对象并不是时间中的存在或时间的流逝本身，而是在认识论术语和某种伯格森意义上，时间经验的构建。时间不是被外在强加在自然事件上的超验秩序矩阵，而是被视为法运作的固有特征。瞬时性教义分析法随时间的推移：作为在意识中产生和终止的心理-生理事件，并通过它们的兴衰动态构建时间。因此，三时间序列是次要的，是由受条件制约的法的过程中生成的。实际上，从无常原则到瞬时性理论的概念转变是时间尺度的转变。虽然《经量部》世界观将三时间解释为过去、现在和未来生命，但阿毘达磨将它们视为任何受条件制约的法每时每刻经历的阶段。无常标志着法经过一段时间，但也包含在每一个意识时刻中（_Vibh-a_ 7–8; Sv 991; _Vism_ XIV 191; Collins 1992, 227）。

## 4. 内在本质：在分类学和本体论之间

为了保持无常的原则并解释普通经验中的连续性和因果关系，佛教学派引入了对_dharmas_本质的新颖解释。这些解释的核心是内在本质（梵文，_svabhāva_，巴利文，_sabhāva_）的概念，在阿毘达磨思想的系统化中起着重要作用，与_dharma_理论的巩固密切相关，并被视为推动阿毘达磨对本体论日益关注的动力。

术语_svabhāva/sabhāva_在经典中并不常见，而在其他一些撒威瓦和南传部的经典文献中，对_dharma_的定义并未提及其固有本质，以证实其真实存在\[[11](https://plato.stanford.edu/entries/abhidharma/notes.html#note-11)]。这种情况在后经典文献中发生了显著变化，其中_svabhāva_成为一个标准概念，在_dharma_解释中被广泛使用。从早期的_vibhāṣā_汇编开始，阐释性阿毗达磨文献中的一个经常出现的观念是，_dharmas_的定义是基于它们的_svabhāva_。例如，在_Abhidharmakośabhāṣya_传承的一个定义是：“_dharma_意味着‘支持’，即支持固有本质(_svabhāva_)”，而_Mahāvibhāṣā_则指出“固有本质能够维持其自身的身份，不会失去它\[...]，就像无条件的_dharmas_能够维持其自身的身份一样”(Cox 2004A, 558–559)。同样，在南传阿毗达磨评论中普遍存在的一个定义是：“_dhammas_之所以被称为如此，是因为它们承载其固有本质，或者因为它们被因果条件所承载”(例如，_Dhs-a_ 39-40；_Paṭis-a_ I 18；_Vism-mhṭ_ I 347)。评论经常将_dhammas_与其固有本质等同起来，可以互换使用术语_dhamma_和_sabhāva_。例如，_Visuddhimagga_宣称“_dhamma_只意味着固有本质”(_Vism_ VIII 246)，而_Dhammasaṅgaṇi_的子评论指出“除了由其承载的固有本质之外，没有其他被称为_dhamma_的东西”，并且“术语_sabhāva_表示仅仅是作为_dhamma_的事实”(_Dhs-mṭ_ 28 & 94；另见 Karunadasa 2010: Ch. 1)。

这些对_法_作为携带其固有本性的评注性定义不应被本体论地解释为_法_是具有固有存在的实体。巴利的评注，格辛（2004 年，533 页）警告说，“人们经常过分看待这些评注，将其视为关于_法_的确切本体论地位以及关于‘自性’概念的中观批判的后来争议的一部分。” 实际上，将_法_定义为携带其固有本性传达了这样一种观念，即在它们背后没有持久的代理者。补充说_法_是由因果条件所承载的，反驳了由与它们自身不同的基础实体所承载的固有本性的观念。正如_法_是在适当条件和品质的依赖下发生的心理物理事件一样，它们的固有本性也是依赖于其他条件和品质而产生的，而不是依赖于比它们更真实的基础（同上；卡鲁纳达萨 1996 年，13-16；尼亚纳波尼卡 1998 年，40-41）。

我们还必须注意，_法_在其固有本质方面被呈现的背景是分类，多种标准和特质被应用以创建一个全面的分类系统，区分任何给定_法_的特定特征。Cox（2004A：559–561）已经表明，在北印度阿毗达磨文本的早期时期，如_舍利弗阿毗达磨论_和_大毗婆沙_的部分，固有本质的概念是在包含方法（_saṃgraha_）的背景下发展的，即应用_法_在特定类别中的包含的过程。_法_是由定义它们的固有本质所决定（_pariniṣpanna_），因此不应被认为具有单独存在的固有本质。“‘决定’意味着_法_的两个进一步特征\[…]首先，正如在一个结构良好的分类模式中的类别是明确且不受波动影响的，_法_作为‘决定’，也是清晰且不可更改地被区分：它们是独特个体化的，因此不会与其他_法_混淆。\[其次，]由固有本质决定的决定不会发生变化或修改，因此，_法_，实际上是固有本质的类型或类别，被确立为稳定和不变的”（_同上_，562）。因此，在早期的舍卫派释经文本中，_自性_被用作确定_法_是什么的一个非时间性、不变的标准，而不一定是_法_存在的标准。这里的关注重点主要是什么使_法_的类别类型独特，而不是_法_的本体论地位。

然而，从前述的范畴论理论出发，成熟的阿毘达磨关于_dharma_的现实性得出了本体论的结论。在对_dharma_概念的转变中，与术语_svabhāva_中固有的歧义相吻合，该术语在逻辑和词源上根植于_bhāva_一词，后者开始表示“存在方式”（_ibid_，565-568）。在_vibhāṣā_总集和同时期的文本中，“对分类本身的明确强调在重要性上逐渐减弱，重点转向澄清个别_dharma_的特性，最终是本体论地位。因此，术语_svabhāva_获得了“固有本性”的主导意义，指定个别_dharma_的存在方式\[...] 通过独特的固有本性确定个别_dharma_也意味着肯定它们的存在，这既是术语_svabhāva_的词源意义，也是_dharma_作为经验的基本组成部分的作用的自然功能。这进而导致了一个表达这种本体论焦点的新术语的突出：即_dravya_（_ibid_，569）。_Dravya_意味着“真实存在”，在诸部洲论的框架内，由固有本性决定的_dharma_作为真实实体（_dravyatas_）存在，与普通经验的复合对象以及相对时间和地点的相对概念或偶然性相对立。固有本性的存在表明，一个_dharma_是一个主要的存在者，不论其时间状态如何，即它是过去、现在还是未来的_dharma_，因此诸部洲论宣称“一切事物都存在”。

Theravāda 拒绝了 Sarvāstivāda 的本体论模型，声称_dhammas_只存在于当下。但 Theravāda 的阿毗达磨与 Sarvāstivāda 分享了作为范畴理论的_dhamma_分析的相同原则，这些原则使有情体验个体化。在这里，_sabhāva_的分类功能也引发了_dhammas_存在的本体学内涵。作为阿毗达磨分析的最终单位，_dhammas_被认为是体验的最终构成要素。“没有其他东西，无论是一个存在，或一个实体，或一个人或一个人”，一段著名的巴利评论摘录宣称（_Dhs-a_155）。\[12]虽然这种说法旨在驳斥对人的现实性的对立 Pudgalavāda 立场，坚称除了_dhammas_之外没有存在的存在或人，但出现了这样一个观念，即现象世界本质上是一个_dhammas_的世界：在有情体验的范围内，除了_dhammas_之外没有其他实际性，构成任何给定_dhamma_的离散、个体化特质的是其固有本质。Theravāda 在对比瞬时性理论的基础上详细阐述了_sabhāva_的概念，并且获得了作为_dhamma_持续瞬间的基础以及作为起源和终止时刻的参照点的意义。在_dhamma_发生之前，它尚未获得固有本质，当它终止时，它就被剥夺了这种固有本质。然而，作为当前事件，虽然具有其固有本质，它存在为终极现实，其固有本质证明了其作为这种存在的实际存在（_Dhs-a_45；_Vism_VIII 234，XV 15）。一段评论性段落甚至将这一瞬间称为“自我的获得”（_Vism-mhṭ_I 343）。

## 5. 因果：存在即功能

阿毘达磨的本体论调查引发了一系列教义问题，成为佛教各派之间持续辩论的主题。一个主要争议集中在无常原则上：如果一切现象都是无常的，那么诸声闻派挑战了舍宗部和上座部，那么_法_必须不断变化，既不能存在于过去和未来，也不能在现在持续任何短暂的时间。另一方面，根据瞬间_法_对经验进行系统分析要求阿毘达磨提供对统治心理和身体连续性的过程的严格说明。推动这些过程的是因果互动，但因果关系的概念据称受到瞬间性理论的影响。如果原因、条件及其结果都是瞬间事件，那么一个已经结束的事件如何有结果？一个在短暂时刻经历起源、持续和终止阶段的事件如何具有因果效力？尽管在教义上存在差异，舍宗部和上座部阿毗达磨都不得不面对这些挑战，并通过制定复杂的直接接触理论来授予因果效力。

阿毘达磨发展了一种因果关系的分析，涉及四种条件（_pratyaya_）和六种因素（_hetu_）之间错综复杂的相互关系。根据包括_Vijñānakāya_、_Prakaraṇapāda_和_Jñānaprasthāna_在内的经典文献，如《阿毗达磨倶舍论》（AKB 2.49）所记录的，这四种条件分别是：1）根本原因（字面上是“作为条件的原因”，_hetupratyaya_），被认为是激发结果和起源过程的最重要因素；2）直接前因，介于意识瞬间和其直接前一瞬间之间的联系；3）对象支持，适用于所有_法_，因为它们是意识的对象；以及 4）优势，促进感官辨别意识，例如视觉认知意识对视觉认知意识的优势。这六种原因是：1）工具性（_kāraṇahetu_），被视为产生结果的主要因素；2）同时性或共存，连接同时出现的现象；3）同质性，解释引发现象表面连续性的_法_的同质流动；4）联结，仅在心理_法_之间起作用，并解释为什么意识元素总是以心理因素的组合形式出现；5）支配，形成个体的习惯性认知和行为倾向；以及 6）结果，指的是积极善恶_法_的结果。这四种条件和六种原因相互作用，解释了现象体验：例如，每个意识瞬间既充当同质原因，也是引发意识及其随后瞬间伴随物上升的直接前因条件。

在因果条件的分析之下，存在的概念被视为有效行动，或者业力。业力是佛教思想中的一个基本原则，自其创立以来，它推动着在轮回中的重复经历。在阿毗达磨的解释中，法的有效行动或独特功能主要被理解为因果功能。对于正统的诸部毗婆舍那，法作为实体（dravyatas）的存在是由其固有性质和特定的因果功能所决定的。然而，固有性质是真实存在的一个非时间性决定因素。决定一个法的时空存在的是其独特的因果功能：过去和未来的法具有功能的能力（sāmarthya），而现在的法也发挥着独特的活动（kāritra）。现在的活动是一种内在的因果效力，有助于在一个法自身的意识系列中产生效果。正是这种活动决定了一个法的现在存在，并定义了其现在时刻的跨度限制。相比之下，能力是一种外部指向另一个意识系列的调节效力：它构成了一个条件，有助于另一个法产生其自身的效果。一个法的现在活动会出现并消失，但过去和未来的法都有因果功能的潜力，并由于其固有性质而作为实体存在。对于诸部毗婆舍那来说，这个模型——坚持在现在时刻的变化之内的恒定变化，并暗示了法在三个时间段内的存在——既保留了无常的原则，又解释了连续性和因果效力。

阿毘达磨-毗婆沙学派对一法的活动和能力之间的区分意味着每一个法或意识瞬间影响其系列中的下一个瞬间，但它也可以作为产生不同效果的促成条件。活动产生了法系列中的下一个瞬间，而能力则产生了不同的效果，并解释了过去法的因果效力。威廉姆斯（1981 年，246-247 页）有益地指出，我们可以将这种因果关系解释为“水平”和“垂直”，分别在意识系列内部和超越其之外。例如，视觉意识的瞬间水平地产生了下一个视觉意识的瞬间，并且可能或可能不会，取决于其他因素如光线等，垂直地产生对物体的视觉。“由此可见，存在即具有水平因果关系，可能或可能不包括垂直因果关系——这一事实提醒我们，我们在这里处理的是经常根据其所做的事情在系统中定位的初级存在”（同上）。因此，活动或水平因果关系—一法促成其自身意识系列的下一个瞬间—将该法作为其种类的特定事件进行个体化。一法的能力或垂直因果关系，通过其促进意识系列之外其他法的发生，将其定位在将其与其他法的不断兴衰连接起来的相互关系网络中，并通过展现其独特的质量和运作强度进一步将其个体化为那个非常特定的法。

Saturāntika 和 Theravāda 发展了与他们拒绝 Sarvāstivāda 本体论模型并声称_dharmas_仅存在于现在相结合的替代因果条件理论。Sautrāntika 通过参考“种子”(_bīja_)或随后_dharma_系列中的修改来解释过去和未来_dharmas_之间的因果关系。Sautrāntika 关于种子的理论是后来大乘佛教思想中两个极其重要概念的先驱，即瑜伽行派的“存储意识”(_ālayavijñāna_)和佛性(_tathāgatagarbha_)(Cox 1995, 94–95; Gethin 1998, 222)。Theravāda 关于因果条件的理论，如_Paṭṭhāna_中所述，提出了一组二十四个条件关系(_paccaya_)，解释了现象在影响另一个现象的产生方面可能发挥作用的所有可能方式。这二十四个条件关系是：1)根本原因(_hetupaccaya_)；2)对象支持；3)优势；4)接近；5)接触；6)同时性；7)相互关系；8)支持；9)决定性支持；10)先前存在；11)后续存在；12)习惯培养；13)业力；14)果报；15)营养；16)控制能力；17)_jhāna_ - 一种特定于禅定成就的关系；18)道路 - 一种特定于佛教道路阶段的关系；19)联结；20)分离；21)存在；22)不存在；23)消失；24)不消失。\[[15](https://plato.stanford.edu/entries/abhidharma/notes.html#note-15)]Theravāda 二十四个条件中的大多数条件在 Sarvāstivāda 理论中都有对应物，两个系统都显示出各种其他平行兴趣和相似之处。因此，很可能这两个系统在这两所学派分开之前就起源，并在分开后继续发展(Conze 1962, 152–153; Kalupahana 1961, 173)。

尽管它们有所不同，但无论是舍利弗部派还是上座部关于因果条件的理论都基于_法_是心理-生理事件，具有特定功能，并且要定义一个_法_需要确定它的功能(Gethin 1992A, 150)。因此，每个_法_在因果关系网络中的相对位置首先是其个体化的手段。仅在辅助意义上，这种网络才是因果效力分析。在这里重新出现的是_法_分析的范畴维度，作为关于条件关系相同性的心理事件的形而上理论。类似于空间-时间坐标系，使人能够识别和描述物质对象，条件关系网络可以被视为一个坐标系，其中任何给定的_法_都有一个位置，这意味着作为一个_法_就是一个在关系网中占据位置的事件的想法，这与唐纳德·戴维森(Donald Davidson)关于因果关系的相同性原则和事件同一性的条件的思想相似(2001 119–120 & 154–161)。相同类型的两个_法_实例将以完全相同的方式适应因果条件网络，但然后将根据它们独特的因果效力程度和方式在个体实例上加以区分。

## 6. 认识论：感知和意识过程理论

在试图解释什么影响解脱洞见以及构成觉悟心灵的因素时，阿毘达磨的探讨延伸至认识论领域。我们已经看到，阿毗达磨对有情体验的分析揭示了我们所感知的作为一系列时间延伸、不间断的现象流实际上是一个快速发生的因果相连的意识瞬间序列或_cittas_（即，由_citta_和_caitta/cetasika_组成的集合体），每个瞬间都有其特定对象。成熟的阿毗达磨因此将现象在时间中的分析与由意识构成的高度复杂意识过程相结合，将有序意识瞬间之间的因果关系溶解为感知活动。正如在第 2 节中先前指出的，对于阿毗达磨和佛教认识论一般而言，感官知觉是感性有情体验的范例。与每一个意识实例一样，感官知觉是有意向性的，包含在感官、它们相应类型的辨别意识以及它们适当的感官对象之间的互动中。然而，不同的佛教学派对感知体验的独特性质以及感官的特定作用和感官对象的地位持有不同立场。然而，南传阿毗达摩和舍斯特毗舍迦-毗婆沙众学派都主张一种观点，即提出感知意识与其感官对象之间存在直接接触，后者被理解为_sensibilia_，因为我们所感知的不是常识对象，而是它们的可感知品质。我们可以将这种观点描述为现象主义实在论（Dreyfus 1997, 331 & 336）。

阿毗达磨在其评论和手册中阐明了其意识过程（_citta-vīthi_）的理论，主要是在佛教论述者 Buddhaghosa、Buddhadatta（公元 5 世纪）和 Anuruddha（公元 10 或 11 世纪）的著作中，这些著作基于_Dhammasaṅgaṇi_和_Paṭṭhāna_中早期的描述（_Vism_ XIV 111–124，XVII 126–145；_Dhs-a_ 82–106 和 267–287；_Vibh-a_ 155–160；_Abhidh-s_ 17–21）。这一理论与_dhamma_分类学和对_citta_的分析并不分离，正如在第 2 节中先前概述的那样。相反，与存在的概念（无论是范畴的还是本体的）作为功能性的概念一致，它分析感官知觉是由先前分类中揭示的八十九种_citta_类型执行的特定功能所导致的。根据这一分析，意识流中的特定功能发生在该连续体的特定瞬间，因为正常的意识流涉及心智通过一系列相关心理因素的连续集来接收和放下感官对象。其结果是对心理和物质现象的相当静态的描述，这些现象在一系列意识瞬间中在意识中出现。

限制账户到普通众生的意识过程，描述了两种过程：五门过程（_pañcadvāra_）和心门过程（_manodvāra_）。这些过程可能连续发生，或者心门过程可能独立发生。五门过程解释了感官知觉，因为信息直接从五个身体感官领域接收。心门过程内化了通过感官接收的信息，并表征专注于思想或记忆的心灵。在“门”处的对象，在佛教思想中被视为第六感官，可能是过去、现在或未来，纯粹是概念性的，甚至是超越性的。然而，通常情况下，心门处的对象将是过去的记忆或一个概念。如果没有感知活动，如在深度、无梦的睡眠中，心灵处于一种被称为无为模式（_bhavaṅga_）的休息状态。在一个人的一生中，同一类型的_citta_执行这种无为心灵的功能，这是心灵恢复的自然模式。当一个概念或记忆发生时，心灵从其无为模式切换到一个简单的心门过程，而没有注意力集中在其他五个感官领域。最简单的心门过程是以下功能的连续：1）转向思想的对象：持续一个瞬间的功能，并内化为对象支持；2）推动：持续最多七个瞬间，执行心灵积极回应对象的善恶业力功能；3）保持：保持意识过程的对象一两个瞬间。

心灵从其不活动模式切换到任何五感门过程中的一个，当一个对象出现在适当感官的“门”时。这种感官知觉过程涉及更多的功能：1）扰乱的不活动心灵：由于感官对象的刺激而产生的功能。它持续两个瞬间，在这期间感官接触发生，即感官对象对适当感官的物质的物理冲击；2）转向：持续一个瞬间，在这期间心灵转向适当感官“门”上的对象；3）感知：持续一个瞬间，是对感官对象的纯粹感知，几乎没有解释；4）接收：持续一个瞬间，起到中介作用，使适当的辨别意识（视觉、听觉等）之间能够过渡；5）调查：持续一个瞬间，起到确定感官对象的性质和确定刚刚被识别的对象引起心灵反应的作用；6）推动：与心门过程中相同；7）保持：与心门过程中相同。例如，视知觉不仅涉及看见本身，还涉及一系列将视觉对象固定在心灵中的瞬间、识别其一般特征和确定其性质的瞬间。在心门和五感门过程中，感官及其感官对象条件了相应的理解意识的当前瞬间的产生，也就是说，这里的感知是建立在同时条件的模式上。在心门和五感门过程中，当保持功能停止时，心灵重新进入其不活动模式。

阿毘达磨中执行构成心门和五门过程大部分功能的意识类型属于结果性_cittas_，即过去积极善或恶意识的结果。这意味着对于呈现在一个人心中的感官数据的体验是由先前的行为决定的，超出了一个人的直接控制范围。每当一个人回忆或概念化，看到，听到，闻到，尝到或触摸到令人愉悦或令人满意的东西时，他体验到先前善意识的结果。而对于不愿或不令人满意的对象以及先前的恶意识分别如此。只有在意识过程的最后阶段，当心灵选择以某种方式积极回应其对象时，积极存在的善意或恶意意识起作用并构成将产生未来结果的业力。因此，阿毘达磨“提供了对因果关系过程的精确小规模分析”（Gethin 1998, 216）。

阿毘达磨-毗婆沙派提出了类似的感官知觉观，但认为感官对象存在为真实实体。然而，所说部派关于知觉的理论却有所不同。它建立在所说部派瞬时性的激进观点之上，根据这一观点，不存在真实的持续时间，只有一系列无限小的瞬间，并且建立在其因果观之上，根据这一观点，当因果效应出现时，原因就停止存在。将这些原则应用于感官知觉，使得难以解释知觉如何直接理解感官对象，因为这意味着当知觉意识产生时，对象已经消失。所说部派的回应是，意识并没有直接接触其感官对象。与现象实在主义相比，所说部派对感知意识的观点可以被描述为再现主义：它认为知觉通过代表其对象的方面（_ākāra_）的中介间接地理解其对象（Dreyfus 1997, 335 & 380–381）。

三大阿毘达磨传统——南传部、诃部和经部——的共同之处在于它们展现了一种类似的范式转变，将现象性、因果条件世界化简为认知和意识的活动。这种转变是印度哲学中更广泛运动的一部分，其中印度教、耆那教和佛教思想家转向了传统形而上学问题，转而专注于认识论、逻辑和语言的研究。他们的目的是系统地阐述有效认知的性质和手段。在佛教圈内，这种认识论转变见证了像阿僧祇（公元 400–480 年）和世覴部（公元 500 年左右）这样的思想家的兴起，他们是瑜伽行派的奠基人，尤其是迪那伽和达摩笈多，他们发展了复杂的逻辑和哲学体系（_见上文_，15–19）。阿毘达磨为这种认识论转变奠定了基础。这种新的重点从《毗婆沙》汇编时期开始占主导地位，并体现在阿毗达磨所用术语描述_法_的性质上。这种术语转变可通过“特有固有特征”（梵文，_svalakṣaṇa_，巴利文，_salakkhaṇa_）和“一般特征”（梵文，_sāmānyalakṣaṇa_，巴利文，_sāmānyalakkhaṇa_）这些术语来表示。

术语_lakṣaṇa/lakkhaṇa_意味着一个标记，或者一个特定的特征，用于区分一个指示的对象与其他对象。逻辑学家将这个术语用于“定义”一个概念或逻辑范畴。阿毘达磨将其应用于辨别_dharmas_的实践，区分一个_dharma_与其他_dharmas_共享的多个通用特征，以及（至少）一个定义_dharma_为那个独特个体事件的特定固有特征。后期阿毘达磨因此将特定固有特征的概念与固有本性相融合。“Dhammas”，南传部的注释文献陈述，“之所以被称为如此，是因为它们具有各自的特定固有特征”（_Vibh-a_ 45；_Vibh-mṭ_ 35；_Paṭis-a_ I 79；_Vism_ XV 3），而一个特定特征“是那种不被其他 dhammas 共有的固有本性”（_Vism-mhṭ_ II 137）。与固有本性一起使用或可互换，特定固有特征构成了一个 dhamma 的独特定义（_Vism_ VI 19, 35）。它是一个认识论和语言学上的决定因素，将一个 dhamma 作为一个可知的实例，由一个独特的文本描述来定义。

_阿毘达磨_中的阿毗达磨-毗婆沙也类似区分了_法_的特定固有特征和通用特征，并将前者与固有本性相对应，从而区分了“在澄清将术语_自性_应用于个别_法_和范畴群时遇到的模糊之处的认知或辨识_法_的层次”（考克斯 2004A，575）。考克斯指出，根据其固有本性或特征对_法_进行分析描述的区别在于“固有本性在解释性分类的背景下获得其特殊意义，而特征的起点在于视角认知。本体论是这两种系统的关注焦点，但从固有本性到特征的术语转变反映了从基于范畴的抽象本体论到经验性或认知确定的认识论本体论的同时转变。”这种新的认识论强调通过成熟的阿毗达磨解释提出的存在定义的修改而出现，后者将所有存在的因果效应视为认知。代表阿毗达磨思想发展史上的这一进展的是桑伽跋陀罗（公元五世纪），他在他的_正理随_中说：“成为产生认知（_buddhi_）的对象领域是存在的真正特征”（同上）。这意味着_法_作为我们经验世界的组成部分可以通过认知客观地识别。

总的来说，阿毘达磨项目，正如_法_理论及其支持性学说所显示的那样，从根本上说是以认识论为导向的。然而，该项目还意在确定经验世界的每个组成部分是可知的和可命名的，并且围绕着对这些组成部分的辨识而发展的话语中使用的词语和概念独特地定义了它们相应的指称物。_法_分析因此为概念现实主义铺平了道路：这是一种世界观，其基础是真理在于我们的概念和陈述与独立、确定的现实的特征之间的对应。概念现实主义并不一定对这种现实的本体论地位产生影响。但支持这种立场意味着与最早的佛教教义背道而驰，后者将佛陀对语言的看法呈现为传统的。

## Bibliography

### Primary Sources

The texts are ordered according to the Pali/Sanskrit alphabet. References to Pali texts are to the editions of the Pali Text Society unless noted otherwise.

* \[A] _Aṅguttara-nikāya_
* \[Dhs-a] _Atthasālinī_ (_Dhammasaṅgaṇi_ commentary)
* \[Abhidh-av] _Abhidhammāvatāra_
* \[Abhidh-s] _Abhidhammatthasaṅgaha_
* \[Akb] _Abhidharmakośabhāṣya_, P. Pradhan, (ed.), Patna: Jayaswal Research Institute, 1975
* \[Kv] _Kathāvatthu_
* \[Dhs] _Dhammasaṅgaṇi_
* \[Dhs-mṭ] _Dhammasaṅgaṇi-mūlaṭīkā_ (_Dhammasaṅgaṇi_ sub-commentary)
* \[Nett] _Nettippakaraṇa_
* \[Paṭis] _Paṭisambhidāmagga_
* \[Ps] _Papañcasūdanī_ (_Majjhima-nikāya_ commentary)
* \[Peṭ] _Peṭakopadesa_
* \[Bv] _Buddhavaṃsa_
* \[M] _Majjhima-nikāya_
* \[Mp] _Manorathapūraṇī_ (_Aṅguttara-nikāya_ commentary)
* \[Mhv] _Mahāvaṃsa_
* \[Mil] _Milindapañha_
* \[Vin] _Vinaya-piṭaka_
* \[Vibh] _Vibhaṅga_
* \[Vibh-mṭ] _Vibhaṅga-mūlaṭīkā_ (_Vibhaṅga_ sub-commentary)
* \[Vism] _Visuddhimagga_, H.C. Warren (ed.), Harvard Oriental Series, Cambridge: Harvard University Press, 1950
* \[Vism-mhṭ] _Visuddhimagga-mahāṭīkā_ (_Visuddhimagga_ commentary)
* \[S] _Saṃyutta-nikāya_
* \[Paṭis-a] _Saddhammappakāsinī_ (_Paṭisambhidāmagga_ commentary)
* \[Vibh-a] _Sammohavinodanī_ (_Vibhaṅga_ commentary)
* \[Spk] _Sāratthappakāsinī_ (_Saṃyutta-nikāya_ commentary)
* \[Sv] _Sumaṅgalavilāsinī_ (_Dīgha-nikāya_ commentary)

### Secondary Sources

* Bronkhorst, J., 2016, “Abhidharma and Indian Thinking,” in _Text, History, and Philosophy: Abhidharma Across Buddhist Scholastic Traditions_, B. Dessein & W. Teng (eds.), Leiden: Brill, pp. 29–46.
* Collins, S., 1992, “_Nirvāṇa_, Time, and Narrative,” _History of Religions_, 31: 215–46.
* Conze, E., 1962, _Buddhist Thought in India: Three Phases of Buddhist Philosophy_, London: George Allen & Unwin.
* Cousins, L., 1981, “The _Paṭṭhāna_ and the Development of the Theravādin Abhidhamma,” _Journal of the Pali Text Society_, 9: 22–46 .
* –––, 1983, “Pali Oral Literature,” in _Buddhist Studies: Ancient and Modern_, P. Denwood and A. Piatigorsky (eds.), London: Curzon, pp. 1–11.
* –––, 1991, “The ‘Five Points’ and the Origins of the Buddhist Schools,” in _The Buddhist Forum II_, T. Skorupski (ed.), London: School of Oriental and African Studies, University of London, pp. 27–60.
* –––, 2001, “On the Vibhajjavādins: the Mahiṃsāsaka, Dhammaguttaka, Kassapiya and Tambapaṇṇiya branches of the ancient Theriyas,” _Buddhist Studies Review_, 18: 131–182.
* Cox, C., 1995, _Disputed Dharmas: Early Buddhist Theories on Existence—an annotated translation of the section on factors dissociated from thought from Saṅghabhadra’s Nyāyānusāra_, Tokyo: Studia Philologica Buddhica XI, The International Institute for Buddhist Studies.
* –––, 2004A, “From Category to Ontology: The Changing Role of _Dharma_ in Sarvāstivāda Abhidharma,” _Journal of Indian Philosophy_, 32: 543–597.
* –––, 2004B, “Mainstream Buddhist Schools,” in _Encyclopedia of Buddhism_, R. Buswell (ed.), NY: McMillan, pp. 501–507.
* –––, 2009, “What’s in a Name? School Affiliation in an Early Buddhist Gāndhārī Manuscript,” _Bulletin of the Asia Institute,_ 23: 53–63.
* Davidson, D., 2001, _Essays on Actions and Events_, Oxford: Clarendon Press.
* Dessein, B., 2016, “Abhidharma as Rational Inquiry,” in _Text, History, and Philosophy: Abhidharma Across Buddhist Scholastic Traditions_, B. Dessein & W. Teng (eds.), Leiden: Brill, pp. 1–26.
* Dreyfus, G., 1997, _Recognizing Reality: Dharmakīrti’s Philosophy and its Tibetan Interpretations_, Albany: SUNY Press.
* Frauwallner, E., 1956, _The Earliest Vinaya and the Beginnings of Buddhist Literature_, trans. L. Petech, Rome: Is. M.E.
* Gethin, R., 1992A, _The Buddhist Path to Awakening: A Study of bodhi-pakkhiyā dhammā_, Leiden: E.J. Brill.
* –––, 1992B, “The _mātikās:_ Memorization, Mindfulness, and the List,” in _In the Mirror of Memory: Reflections on Mindfulness and Remembrance in Indian and Tibetan Buddhism_, J. Gyatso (ed.), New York: SUNY Press, pp. 149–172.
* –––, 1994, “_Bhavaṅga_ and Rebirth According to the Abhidhamma,” in _The Buddhist Forum III_, T. Skorupski and U. Pagel (eds.), London: School of Oriental and African Studies, University of London, pp. 11–35.
* –––, 1997, “Cosmology and Meditation: from the _Agañña-sutta_ to the Mahāyāna,” _History of Religions_, 36: 183–217.
* –––, 1998: _The Foundations of Buddhism_, Oxford and New York: Oxford University Press.
* –––, 2004, “He Who Sees Dhamma Sees Dhammas: Dhamma in Early Buddhism,” _Journal of Indian Philosophy_, 32: 513–542.
* –––, 2017, “Body, Mind and Sleepiness: On the Abhidharma Understanding of Styāna and Middha,” _Journal of the International College for Postgraduate Buddhist Studies_, 21: 254–216.
* –––, 2022, “Abhidhamma: Theravāda thought in relation to Sarvāstivāda Thought,” in _Routledge Handbook of Theravāda Buddhism,_ S. Berkwitz & A. Thompson, (eds.), Oxon and New York: Routledge, pp. 227–242.
* Gombrich, R., 1996, _How Buddhism Began: The Conditioned Genesis of the Early Teachings_, London and New Jersey: Athlone Press.
* –––, 2009, _What the Buddha Thought_, London: Equinox.
* Hinüber, Oskar von, 1978, “On the Tradition of Pāli Texts in India, Ceylon and Burma,” in _Buddhism in Ceylon and Studies on Religious Syncretism in Buddhist Countries (Symposien zur Buddhismusforschung, I_), H. Bechert (ed.), Göttingen: Vandenhoeck and Ruprecht, pp. 48–57.
* –––, 1996, _A Handbook of Pāli Literature_, Berlin: Walter de Gruyter.
* Horner, I.B., 1941, “Abhidhamma abhivinaya,” _The Indian Historical Quarterly_, 17: 291–310.
* Jayatilleke, K., 1963, _Early Buddhist Theory of Knowledge_, Delhi: Motilal Banarsidass.
* Kalupahana, D., 1961, “A Prolegomena to the Philosophy of Relations in Buddhism,” _University of Ceylon Review_, 19: 167–194.
* Karunadasa, Y., 1996, _The Dhamma Theory: Philosophical Cornerstone of the Abhidhamma_, The Wheel Publications No. 412/413, Kandy: Buddhist Publication Society. \[[Preprint available online](http://www.zeh-verlag.de/download/dhammatheory.pdf)]
* –––, 2010, _Theravāda Abhidhamma: Its Inquiry into the Nature of Conditioned Reality_, Hong Kong: Centre for Buddhist Studies, University of Hong Kong, HKU: CBS publication series.
* Kim, W.D., 1999, _The Theravādin Doctrine of Momentariness: A Survey of its Origins and Development_, unpublished D.Phil. thesis, University of Oxford.
* Kragh, Ulrich T., 2002, “The Extant Abhidharma Literature,” _Indian International Journal of Buddhist Studies_, 3: 123–167.
* Lamotte, E., 1988, _History of Indian Buddhism: From the Origins to the Śaka Era_, trans. Sara Webb-Boin, Paris: Louvain-la-Neuve.
* Norman, K. R., 1983, _Pāli Literature: Including the Canonical Literature in Prakrit and Sanskrit of All the Hīnayāna Schools of Buddhism_, Wiesbaden: Otto Harrassowitz.
* Nyanaponika Thera, 1998, _Abhidhamma Studies: Buddhist Explorations of Consciousness and Time_, Boston: Wisdom Publications.
* Obeyesekere, G., 1991, “Myth, History and Numerology in the Buddhist Chronicles,” in _The Dating of the Historical Buddha Part 1_, H. Bechert (ed.), Göttingen: Vandenhoeck and Ruprecht, pp. 152–82.
* Ronkin, N., 2005, _Early Buddhist Metaphysics: The Making of a Philosophical Tradition_, (Oxford Centre for Buddhist Studies Monograph Series), London and New York: Routledge-Curzon.
* Rospatt, A. von, 1995, _The Buddhist Doctrine of Momentariness: A Survey of the Origins and Early Phase of this Doctrine up to Vasubandhu_, Stuttgart: Franz Steiner Verlag.
* Waldron, W., 2002, “Buddhist Steps to an Ecology of Mind: Thinking about ‘Thoughts without a Thinker’,” _The Eastern Buddhist_, 34: 1–52.
* –––, 2003, _The Buddhist Unconscious: the ālaya-vijñāna in the Context of Indian Buddhist Thought_, London and New York: Routledge-Curzon.
* Willemen, C., B. Dessein, and C. Cox, 1998, _Sarvāstivāda Buddhist Scholasticism_, Leiden: Brill.
* Williams, P., 1981, “On the Abhidharma Ontology,” _Journal of Indian Philosophy_, 9: 227–257.
* Wynne, A., 2019, “Theriya Networks and the Circulation of the Pali Canon in South Asia: The Vibhajjavādins Reconsidered,” in _Buddhist Path, Buddhist Teachings: Studies in Memory of L.S. Cousins,_ N. Appleton & P. Harvey (eds.), Sheffield: Equinox Publishing, pp. 269–283.

## Academic Tools

| ![sep man icon](https://plato.stanford.edu/symbols/sepman-icon.jpg) | [How to cite this entry](https://plato.stanford.edu/cgi-bin/encyclopedia/archinfo.cgi?entry=abhidharma).                                                                      |
| ------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ![sep man icon](https://plato.stanford.edu/symbols/sepman-icon.jpg) | [Preview the PDF version of this entry](https://leibniz.stanford.edu/friends/preview/abhidharma/) at the [Friends of the SEP Society](https://leibniz.stanford.edu/friends/). |
| ![inpho icon](https://plato.stanford.edu/symbols/inpho.png)         | [Look up topics and thinkers related to this entry](https://www.inphoproject.org/entity?sep=abhidharma\&redirect=True) at the Internet Philosophy Ontology Project (InPhO).   |
| ![phil papers icon](https://plato.stanford.edu/symbols/pp.gif)      | [Enhanced bibliography for this entry](https://philpapers.org/sep/abhidharma/) at [PhilPapers](https://philpapers.org/), with links to its database.                          |

## Other Internet Resources

Abhidharma translations, information, and lectures:

* [Access to Insight](http://www.accesstoinsight.org/) \[Theravāda only]
* [BuddhaNet](http://www.buddhanet.net/)

Abhidhamma/Abhidharma canonical and exegetical texts in digital Pali and Sanskrit Buddhist canons:

* [Digital Pali Canon of the Vipassana Research Institute](http://www.tipitaka.org/index.shtml)
* [Digital Sanskrit Buddhist Canon](http://www.dsbcproject.org/)

## Related Entries

[atomism: 17th to 20th century](https://plato.stanford.edu/entries/atomism-modern/) | [atomism: ancient](https://plato.stanford.edu/entries/atomism-ancient/) | [consciousness: and intentionality](https://plato.stanford.edu/entries/consciousness-intentionality/) | [contradiction](https://plato.stanford.edu/entries/contradiction/) | [Dharmakīrti](https://plato.stanford.edu/entries/dharmakiirti/) | [epistemology](https://plato.stanford.edu/entries/epistemology/) | [Japanese Philosophy: Kyoto School](https://plato.stanford.edu/entries/kyoto-school/) | [Madhyamaka](https://plato.stanford.edu/entries/madhyamaka/) | [mind: in Indian Buddhist Philosophy](https://plato.stanford.edu/entries/mind-indian-buddhism/) | [Nāgārjuna](https://plato.stanford.edu/entries/nagarjuna/) | [Śāntarakṣita](https://plato.stanford.edu/entries/saantarak-sita/) | [Vasubandhu](https://plato.stanford.edu/entries/vasubandhu/) | yogaacaara

[Copyright © 2022](https://plato.stanford.edu/info.html#c) by\
Noa Ronkin <[_noa.ronkin@wolfson.oxon.org_](mailto:noa%2eronkin%40wolfson%2eoxon%2eorg)>
