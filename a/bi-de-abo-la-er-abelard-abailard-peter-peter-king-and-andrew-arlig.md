# 彼得·阿伯拉尔 Abelard \[Abailard], Peter (Peter King and Andrew Arlig)

_最初发表于 2004 年 8 月 3 日星期二；实质性修订于 2022 年 8 月 12 日星期五_

彼得·阿伯拉尔（1079 年-1142 年 4 月 21 日）\[‘阿巴拉德’或‘阿巴埃拉德’或‘哈巴拉兹’等等]是十二世纪卓越的哲学家和神学家。作为他那个时代的教师，他也以诗人和音乐家而闻名。在亚里士多德复兴之前，他将本土拉丁哲学传统推至巅峰。他的天才在他所做的一切中都表现出来。可以说，他是中世纪最伟大的逻辑学家，同样作为第一位伟大的名义主义哲学家而闻名。他捍卫理性在信仰事务中的运用（他是第一个以现代意义使用‘神学’的人），他对宗教教义的系统处理既因其哲学深刻和微妙而引人注目，也因其大胆而著称。阿伯拉尔在他的同时代人眼中似乎是一个超凡脱俗的存在：他机智敏捷、口齿伶俐、记忆力超群、自负过度，在辩论中无人能敌——支持者和反对者都说他从未输过一场辩论——他的个性魅力深深地影响着他接触过的所有人。他与赫洛伊斯的不幸恋情使他成为一个浪漫悲剧人物，他与克莱尔沃的伯纳德关于理性和宗教的冲突使他成为启蒙运动的英雄。尽管他的生活多姿多彩，但他的哲学成就是他名声的基石。

* [1. 生平与作品](https://plato.stanford.edu/entries/abelard/#LifWor)
* [1.1 生活](https://plato.stanford.edu/entries/abelard/#Lif)
* [1.2 作品](https://plato.stanford.edu/entries/abelard/#Wor)
* [2. 形而上学](https://plato.stanford.edu/entries/abelard/#Met)
* [3. 逻辑学](https://plato.stanford.edu/entries/abelard/#Log)
* [4. 语言哲学](https://plato.stanford.edu/entries/abelard/#Lan)
* [5. 心灵哲学](https://plato.stanford.edu/entries/abelard/#Min)
* [6. 伦理学](https://plato.stanford.edu/entries/abelard/#Eth)
* [7. 神学](https://plato.stanford.edu/entries/abelard/#The)
* [参考文献](https://plato.stanford.edu/entries/abelard/#Bib)
* [拉丁语原始文本](https://plato.stanford.edu/entries/abelard/#PriTexLat)
* [英文翻译的原始文本](https://plato.stanford.edu/entries/abelard/#PriTexEngTra)
* [英文中的选定次要文献](https://plato.stanford.edu/entries/abelard/#SelSecLitEng)
* [学术工具](https://plato.stanford.edu/entries/abelard/#Aca)
* [其他互联网资源](https://plato.stanford.edu/entries/abelard/#Oth)
* [相关条目](https://plato.stanford.edu/entries/abelard/#Rel)

***

## 1. 生平与著作

### 1.1 生平

彼得·阿伯拉尔的生平相对较为人所知。除了公开记录中记载的事件外，他的内心世界还体现在他的自传体书信《我的苦难故事》和与赫洛伊丝的著名通信中。

彼得·阿伯拉尔出生于 1079 年左右的勒帕莱小镇，这是布列塔尼靠近南特的一个小镇。他早年接受了文学训练，并热衷于学习；他后来的著作显示出对西塞罗、荷马、朱文纳尔、奥维德、卢坎、塞内加和维吉尔的熟悉。阿伯拉尔最终放弃了自己的继承权，包括随之而来的骑士头衔，转而追求哲学。他这样做是通过前往学习名师，其中最著名的是罗斯克林和尚普奥的威廉。

在十二世纪初的几年里，阿伯拉尔足够自信，开始在梅伦和科尔贝伊担任讲师，主要与尚普奥的威廉（巴黎）竞争学生和声誉。这种压力太大了——阿伯拉尔的健康状况恶化，他回到布列塔尼待了几年。

阿伯拉尔在 1108 年至 1113 年间恢复健康并保持雄心壮志后回到巴黎。他参加了尚波的威廉的讲座，并与威廉就普遍问题展开辩论。据阿伯拉尔称，他在辩论中战胜了他的老师，并因此声名鹊起，成为一位著名的辩士，在几所学校任教。大约在 1113 年，阿伯拉尔决定学习神学；他寻找当时最杰出的神学教师，劳昂的安瑟尔（不要与坎特伯雷的安瑟尔混淆），并成为他的学生。这并不是一个明智的选择：安瑟尔的传统教学方法并没有吸引阿伯拉尔，经过一番来回后，阿伯拉尔回到巴黎继续独自学习。这将是他最后一次跟随他人学习。

回到巴黎后，阿伯拉尔成为巴黎圣母院的常驻学者，这个职位他一直保持到与赫洛伊丝的浪漫纠葛导致他被阉割为止，此后他进入了圣丹尼修道院，而赫洛伊丝则进入了阿让特伊修道院。康复后，阿伯拉尔恢复在附近的修道院教书，主要讲授神学，特别是关于三位一体的问题。他的哲学分析方法被视为对更传统方法的直接挑战，一次在瓦桑召开的教会会议审查了阿伯拉尔的著作，谴责了他们，并要求阿伯拉尔公开信仰，这是一次令他感到羞辱的经历；不久之后，他被允许定居在一片荒凉无人居住的土地上，专心于沉思。

彼得·阿伯拉尔说，贫困迫使他恢复教学。他和成群结队涌向他的学生们建造了一个名为圣灵的祷告室，在那里他继续写作、教学和研究。这种田园诗般的生活在 1126 年左右结束，当时阿伯拉尔接受了成为布列塔尼圣吉尔达德吕伊修道院院长的邀请；不久之后，他将圣灵祷告室移交给了埃洛伊丝和其他修女，她们的修道院已被征用。阿伯拉尔发现圣吉尔达修道院的修士们难以相处，阻碍重重，甚至危险，他声称自己在那里居住期间遭到了数次谋杀未遂。在这段时期，他写了《灾难的历史》，并与埃洛伊丝通信。

到了 1130 年代中期，阿伯拉尔获准返回巴黎（保留修道院院长的头衔）并在圣吉纳维夫山的学校教书。就在这段时间，他的神学著作引起了克莱蒙修道院院长伯纳德的注意，后者对阿伯拉尔的一些结论以及他对信仰问题的处理方式提出了异议。在一些无果的解决分歧尝试之后，阿伯拉尔请求塞恩斯大主教安排于 1140 年 6 月 3 日在自己和伯纳德之间进行公开辩论，以解决他们之间的分歧。伯纳德最初以不应该辩论信仰问题为由拒绝了邀请，但后来接受了，并且未经阿伯拉尔知情，安排召集另一个调查委员会审查阿伯拉尔的作品，怀疑其中有异端之说。当阿伯拉尔发现这不是辩论而是一场假定的法庭时，他拒绝参与，并宣布打算直接向教皇上诉。他退出了审判程序，并开始前往罗马。教会议会谴责了它声称在他的作品中发现的十九个命题，并休会。伯纳德在阿伯拉尔离开法国之前成功地发起了一场请愿活动，请求教皇法庭介入；一封教皇的信件在阿伯拉尔在克卢尼时就已经收到，确认了索瓦松大会的决定；阿伯拉尔被责令保持沉默。据所有记载，阿伯拉尔立即遵守了这一要求，甚至与伯纳德和平和解。克卢尼修道院院长彼得尊者就这些事情写信给教皇，教皇解除了对阿伯拉尔的判决。阿伯拉尔在健康逐渐恶化的过程中一直受到彼得尊者的保护，先是在克卢尼，然后是在圣马塞尔。阿伯拉尔于 1142 年 4 月 21 日去世。他的遗体安葬在圣灵祷告室，今天（与埃洛伊丝一起）安息在巴黎的拉雪兹公墓。

### 1.2 作品

阿伯拉尔的学生们活跃于国王、哲学家、诗人、政治家、神学家和僧侣等领域；其中包括三位教皇和数位国家元首。中世纪晚期对阿伯拉尔思想的明确引用很少，这可能是因为苏瓦松议会的裁决所带来的阴影，但很明显，他对 12 世纪哲学产生了重要影响，也许对后来 14 世纪的思辨也有影响。

彼得·阿伯拉尔的著作日期甚至数量仍然在学者中大多数是模糊的并且存在争议的问题。其中一个原因是阿伯拉尔经常修订和重写，因此某一作品可能有几个不同版本在流通；另一个原因是他的一些著作可能代表着在课程和研讨会中不断发展的“教学笔记”。因此，当应用于我们现在拥有的阿伯拉尔作品集时，“创作日期”是否是一个明确定义的概念并不清楚。除了阿伯拉尔的通信可以相对精确地确定日期外，阿伯拉尔现存的作品可分为三类。

第一类包括阿伯拉尔关于_辩证法_的著作——涉及逻辑学、语言哲学、形而上学和心灵哲学的著作。他的两部杰作是：

* _Logica_ ‘_ingredientibus_’, “逻辑”（以“对于那些开始的人…”开头）。
* _Dialectica_, “辩证法”。

这两部作品都遵循自古代传承下来的“古代逻辑”_logica vetus_模式：波菲里的亚里士多德导论_Isagoge_；亚里士多德的_范畴论_和_论释义_；博伊西的_范畴三段论导论_、_范畴三段论_、_假设三段论_、_论题的差异_和_论分科_。 阿伯拉尔的作品涵盖了古代逻辑中呈现的材料，尽管它们以不同的方式呈现。 他的_Logica_‘_ingredientibus_’是对古代逻辑的一部密切的文本评论，尽管只有部分内容幸存，即对_Isagoge_、_范畴论_、_论释义_和_论题的差异_的评论；他的_Dialectica_是一部独立的辩证论著，按主题处理相同的材料，尽管开头部分（涵盖_Isagoge_和_范畴论_的开始部分）和结尾部分（关于分科和定义）均未被保存。此外，还有四部较小的辩证论作品：

* _Introductiones parvulorum_，“初级逻辑”。
* _Logica_ ‘_nostrorum petitioni sociorum_’, “逻辑”（以“在我们朋友的要求下…”开头）。
* _Tractatus de intellectibus_, “论理解”。
* _根据彼得·阿伯拉尔大师的看法_，“彼得·阿伯拉尔的观点。”

其中第一部分是关于古代逻辑的初级评论系列（尽管并未完全保存）；它们的简单水平使一些学者认为它们可能来自阿伯拉尔职业生涯的早期阶段，另一些学者则否认它们是阿伯拉尔的作品。第二部分，_Logica_ ‘_nostrorum petitioni sociorum_’ 是一部正在进行中的作品：它假定读者已了解阿伯拉尔早期的_Logica_ ‘_ingredientibus_’，并讨论了那里未涉及的高级问题，但在很长一段时间里，它也是对波菲里的_Isagoge_的直接解释或评论；它在某些文本上与阿伯拉尔的其他作品有相似之处，并显示出对神学的一些了解。第三部作品涉及概念，或者从逻辑的角度（大致提供术语的含义）和从心灵哲学的角度（作为心智内容的载体）来看。最后一部作品可能仅仅是对阿伯拉尔一些讲座的记录，涉及关于整体和部分的逻辑和形而上学难题。

第二类别包括 彼得·阿伯拉尔 关于伦理学的著作：

* _Ethica seu Scito teipsum_，即“伦理学，或，认识你自己。”
* _Collationes_, “对话”又名_哲学家、犹太人和基督徒之间的对话_。

_伦理学_提供了对道德价值以及应该归因于行动者及其行为的赞扬或责备程度的分析；它在第二卷开始时中断。_对话_是一对辩论（在阿伯拉尔梦中出现的人物之间）关于幸福的本质和至善：哲学家声称只遵循自然理性，首先与遵循旧法的犹太人辩论；然后哲学家与捍卫基督教伦理学的基督徒进行辩论，后者从哲学角度进行辩护。阿伯拉尔还为他的儿子写了一部轻松的实用建议作品：

* _Carmen ad Astralabium_, “星盘诗。”

在这一系列的对联中可以找到道德建议和教化情感。

第三类别包括 阿伯拉尔的 哲学神学作品。他的三部主要作品致力于对三位一体的哲学分析，这几个版本代表了他思想的不同阶段和对正统的尝试（每个版本都经过多次重写）：

* _神学_ ‘_至善_’。 “神学”（以“至善…”开头）。
* _基督教神学_, “基督教神学.”
* _学者神学_, “神学”（以“在学校中…”开头的神学）.

第一版《神学》似乎是在苏瓦松会议上被谴责的作品，最后一版是在桑斯会议上被谴责的作品。除了这三部作品外，阿伯拉尔还写了几篇评论：

* _Expositio orationis dominicae_，“主祷文分析”。
* _使徒信经解释_，"使徒信经的分析。"
* _阿坦阿修信经解释_，"阿坦阿修信经的信仰分析。"
* _六日创世记_, “对《创世记》1-2:25 的评论。”
* _Commentaria in Epistolam Pauli ad Romanos_, “对保罗致罗马人书的评论。”

前三篇评论很简短，但彼得·阿伯拉尔对《创世记》的前几节和保罗书信的讨论则是广泛而详细的（后者也与彼得·阿伯拉尔的伦理理论相关）。彼得·阿伯拉尔还在一部名为《独语》的短篇作品中探讨了关于信仰和理性的问题：

* _独语_，即“独语”。

这段简短的内心对话，以奥古斯丁的《独语录》为模型，让“彼得”与“阿伯拉尔”讨论问题。在一系列问题中，赫洛伊丝提出了更多实际性质的神学问题，这些问题是她代表自己和帕拉克利特修道院的修女们提出的：

* _赫洛伊丝的问题列表（阿伯拉尔的解决方案）_ ，即《Héloïse’s Problem-List (with Abelard’s Solutions)》。

实际问题也在 彼得·阿伯拉尔 的布道、赞美诗和挽歌（_planctus_）中得到解决。最后， 彼得·阿伯拉尔 创作了一部极具影响力的神学著作，其中完全没有理论推测：

* _Sic et non_，"赞成与反对"。

彼得·阿伯拉尔汇编了一系列 158 个问题，每个问题都附有教父的引文，这些引文暗示对问题的肯定答案，以及其他暗示否定答案的教父引文。阿伯拉尔并没有试图协调这些表面上矛盾的言论，但在他的序言中，他为正确的诠释调查制定了规则：寻找模棱两可之处，检查周围的背景，划分相关的区别等。

阿伯拉尔的学生和门徒们也记录了他的许多观点，尽管这些材料尚未被仔细探讨。阿伯拉尔的现存作品中提到了我们没有的其他作品：《文法学》，《修辞学》，一部在神学研究初期撰写的《以西结书》评论等。这些作品中可能有一些尚未被发现。

## 2. 形而上学

彼得·阿伯拉尔的形而上学是西方传统中名义主义的第一个伟大例证。虽然他认为普遍观念只是简单的词（_nomina_）这一观点证明了这一标签，名义主义，或者更好地说是非实在主义，是阿伯拉尔整个形而上学的特征。他不仅对普遍观念持非实在主义立场，对命题、事件、除了现在之外的时间、自然种类、关系、整体、绝对空间、形质复合体等也是如此。相反，阿伯拉尔认为，充满丰富多样性的具体个体已经足够构成这个世界。阿伯拉尔更倾向于简化、原子论和物质解释，他致力于尽可能地浇灭他的前辈和同时代人形而上学上的过度之处。

Abelard 辩护他的论点，即普遍概念只不过是词语，他认为关于普遍概念的本体论实在论是不连贯的。更确切地说，他认为世界上不能有任何符合博伊修斯对普遍概念的标准的真实对象，即作为整体同时存在于许多对象中，从而构成它们的实质（即使包含它的个体成为它所是的）。因此，Abelard 得出结论，普遍性不是世界的本体特征，而是语言的语义特征。

假设普遍概念是世界上的事物，以至于同一事物完全同时存在于苏格拉底和驴中，使每个都完全成为动物。Abelard 指出，那么同一事物“动物” 将同时具有理性（因为它在构成“人类”这一物种中的作用）和非理性（因为它在构成“驴”这一物种中的作用）。但是，那么对立面将同时存在于同一事物中，这是不可能的。

对于理性和非理性实际上并不存在于同一事物的反驳，彼得·阿伯拉尔提出了一个双重回应。首先，他拒绝了它们仅仅潜在存在的说法。每个物种实际上都受到相反物的影响，而属实际上存在于每个物种中作为一个整体；因此，在一个物种中由一个相反物影响，在另一个物种中由另一个相反物影响；由于在每个物种中完全相同，因此实际上受到相反物的影响，矛盾就产生了。其次，阿伯拉尔试图证明相反物不仅存在于属中，甚至存在于同一个个体中。苏格拉底是（一只）动物，布鲁内卢斯是驴；但通过传递性——因为每个都完全是_动物_——苏格拉底就是布鲁内卢斯，因此既理性又非理性。换句话说，每个本质上都是动物，而且本质上是理性的和本质上是非理性的。

如果我们对最后这段推理提出异议，理由是个体因其非本质特征而独特，阿伯拉尔回答说这个观点“使偶然性优先于实质性”。也就是说，这个异议声称个体之所以独特是因为偶然表征它们，这混淆了事物与它们的特征。

前景对于现实主义来说并不乐观，如果普遍性不是与单一事物等同，而是与一组事物等同。 阿伯拉尔指出，集合是其部分的后设，而且，此外，该集合与其部分之间并不像普遍性被认为是共同的那样共享。 试图以某种方式将普遍性与个体等同也没有帮助，例如声称苏格拉底作为人类被视为普遍的人类； 阿伯拉尔认为，如果普遍性确实是个体，那么我们将陷入这样的后果，即像苏格拉底这样的个体是共同的，或者存在与个体数量相同的普遍性，这两种情况都是荒谬的。

阿伯拉尔得出结论，普遍性仅仅是语言上的，而不是世界的特征。 更确切地说，阿伯拉尔认为，普通名词（如“动物”），动词和否定名词（如“非苏格拉底”）可以正确地适用于许多情况，因此被视为普遍性。 这些术语在语义上是一般的，因为它们的意义适用于不止一件事物，但它们并不因此命名某种一般的事物； 相反，它们分别指称每个适用该术语的个体。 例如，“动物”这个术语具有“有生物质”的意义，这是固有的一般性，并且它指称每个个体动物，因为每个个体都是有生物质的——正如阿伯拉尔所说，因为每个个体都具有生物质的地位。 但这是离开形而上学领域进入语义学领域； 请参阅[第 4 节](https://plato.stanford.edu/entries/abelard/#Lan)中对阿伯拉尔语言哲学的讨论。

阿伯拉尔认为，世界上除了上帝和天使之外的一切都是形式、物质，或者是形式和物质的组合体。某物的物质是它所制成的东西，无论是它在成品中持续存在（如房子中的砖块）还是被吸收进去（如面包中的面粉）。最终，所有物质对象都由地、空气、火和水这四种元素组成，但它们在大多数组合中并不保留它们的元素形式。一般来说，物质对象的形式就是其物质部分的配置：“我们严格地称形式是由部分的组合而来的。”例如，雕像的形式就是它的形状，这不过是其物质的安排——鼻子的弧度、眼睛的大小等等。因此，形式是对物质的_监理_，并且没有独立于物质的本体地位。这并不是否认形式的存在，而是提供了一个关于形式在某一主体中存在的特定解释，即该主体的物质被以某种方式配置。例如，雕像中形状的内含就是其青铜的排列方式。因此，物质事物与其所制成的东西相同——只有一个例外：人类，其形式是其非物质的（和不朽的）灵魂。严格来说，由于人类的灵魂能够独立于身体存在，它们毕竟不是形式，尽管只要它们与身体结合，它们就起着实质形式的作用。

形式和物质的物质复合体，除了人类之外，都是由其以某种方式配置的离散物质部分组成的整体。阿伯拉尔认可许多类型的整体：集合，无论成员如何选择；结构化的复合体，无论是自然统一的（如苏格拉底及其肢体）还是人为统一的（如房屋的墙壁、地板和屋顶）；连续的均匀物质“物质”，即物质，如水或黄金；几何对象，如线，由其部分的相对位置定义；时间整体，如一天及其组成的小时。这些整体大多在本体上仅仅是它们的物质部分。结构化的复合体是否具有任何独立的本体地位取决于它们组织形式的地位。

阿伯拉尔的实体整体理论并非现代意义上的纯粹部分论，因为他认为存在特权划分：正如一个属应该被划分为不仅仅是任何物种，而是其近类物种一样，一个整体的划分也必须是其主要部分。直觉上，有些整体具有自然划分，优先于其他划分；例如，一个句子被划分为单词、音节和字母，就是按照这个顺序。根据阿伯拉尔的观点，一个整体的主要部分是那些立即组合成完整整体的部分。他的意图似乎是，定义整体的构成（如果有的话）也说明了它的主要部分。一个房子由地板、墙壁和屋顶以正确的方式组合而成。每个主要部分（比如墙壁）是否需要所有的子部分（每块砖）存在是一个悬而未决的问题；例如，一个集合的主要部分就是集合的每个成员，无论任何成员的子部分是什么；一个聚合体的主要部分是彼此相邻的成员。

个体有各自的本性，根据其本性，它们属于确定的自然种类。但是，个体的本性并不是与其他个体真正共享或普遍的东西；阿伯拉尔对现实主义的驳斥表明这是不可能的。相反，阿伯拉尔认为自然种类是一组明确定义的具有相同特征的事物，广义上讲，这些特征使它们成为它们自己。为什么某个事物具有某些特征而不是其他特征，是由它是如何变成现在这个样子的——创造它的自然过程导致它具有它现在的特征，即成为它所属种类的事物；类似的过程导致类似的结果。根据这种解释，很明显自然种类并没有特殊地位；它们只不过是离散的整体，其成员原则是相似性，仅仅反映了世界被划分为离散的相似性类别的事实。此外，这种真实的相似关系本身并不超越那些相似的事物。自然种类的划分，大概是关于世界的一个“表面事实”：如果上帝以不同方式规定了它们，事情本可以有所不同；火可能是冷的，重物体可能向上掉落，青蛙可能会推理。如果这些因果力不同，那么自然种类也可能不同，或者可能没有像现在这样明显地区分开来。鉴于事实的现状，自然种类将世界划分为其关节，但它们是上帝选择的关节。

## 3. 逻辑

彼得·阿伯拉尔是自古以来最伟大的逻辑学家：他设计了一种纯粹的真值功能命题逻辑，认识到我们与弗雷格相关联的_力量_和_内容_之间的区别，并拟定了一个完整的蕴涵理论，即我们现在认为是逻辑推论的理论。他的逻辑系统在处理主题推理方面存在缺陷，但这不应妨碍我们对阿伯拉尔成就的认可。

彼得·阿伯拉尔观察到，相同的命题内容可以在不同的语境中以不同的方式表达：_苏格拉底在房子里_这一内容在陈述中表达为‘苏格拉底在房子里’；在疑问中表达为‘苏格拉底在房子里吗？’；在愿望中表达为‘要是苏格拉底在房子里就好了！’等等。因此，阿伯拉尔可以特别区分一句话的肯定力量与其命题内容，这种区分使他能够指出条件语句中的组成句子并未被肯定，尽管它们在被肯定时具有相同的内容——‘如果苏格拉底在厨房里，那么苏格拉底就在房子里’并未断言苏格拉底在厨房里或他在房子里，前提句和结论句也是如此，尽管在条件语句的范围之外可以使用相同的措辞来做出这样的断言。同样，这种区分使阿伯拉尔能够纯粹基于内容以真值功能方式定义否定和其他命题连接词，因此，否定被处理如下：非-_p_当且仅当_p_为真/假时为假/真。

对于阿伯拉尔来说，论证理论的关键在于_inferentia_，最好翻译为‘蕴涵’，因为阿伯拉尔要求所涉及的命题之间的连接既是必要的又是相关的。也就是说，结论——更确切地说，最终陈述的意义——是由前述陈述的意义所要求的，因此不能有其他可能。阿伯拉尔经常谈到最终陈述的意义被“包含”在前述陈述的意义中，就像我们谈论结论被包含在前提中一样。当所涉及的命题的逻辑形式(_complexio_)成立时，蕴涵是完全(_perfecta_)的。阿伯拉尔告诉我们，这意味着蕴涵在其术语的任何统一替换下成立，这一标准现在与波尔查诺相关联。传统的范畴三段论的四种形式和模式源自亚里士多德，以及源自博伊修斯的假言三段论的教义，都是完全蕴涵的实例，或者我们应该说，有效的推理。

有另一种结论可以是必要的并且与其前提相关，但_不_是形式上有效的（不构成完全蕴涵）。命题之间的必要联系，以及它们意义之间的联系，可能是由于在所有可能的世界中都成立的非形式化形而上学真理的作用。例如，人类是一种动物，因此结论“如果苏格拉底是人类，那么苏格拉底是动物”是必然成立的，并且前提的意义促使了结论的意义，但在统一替换下它并不形式上有效。阿伯拉尔认为这种不完全蕴涵是根据话题理论（被称为话题推理形式）成立的。上面的示例推理是通过“从种类”这一话题验证的，其中包含一组形而上关系，其中之一可以用规则“无论种类被陈述于何物，属类也是如此”来表达，这规则为蕴涵的推理力量奠定了基础。与博伊修斯相反，阿伯拉尔认为话题规则只适用于不完全蕴涵，并且特别是不需要用来验证前一段提到的范畴和假言三段论的经典形式。

阿伯拉尔花费了大量精力来探讨话题推理理论的复杂性，特别是绘制条件句、论证以及他所称的“论证”（大致上是从已承认的前提得出的结论）之间的精确关系。他研究的一个令人惊讶的结果之一是，他否认演绎定理的一个相关性，坚持认为有效的论证不一定对应于可接受的条件句，反之亦然，因为论证和条件句的要求是不同的。

最后，似乎彼得·阿伯拉尔的主题推理原则并不起作用，这一事实在涉及“对立”主题时变得明显：阿伯拉尔的原则导致了不一致的结果，这一结果被巴黎的阿尔贝里克所注意。这导致了十二世纪推理理论的危机，因为阿伯拉尔未能成功地回避这一困难。这些辩论似乎发生在 1130 年代后期，当时阿伯拉尔即将卷入与克莱蒙修道院的伯纳德的纠纷，他的注意力在其他地方。

## 4. 语言哲学

许多 彼得·阿伯拉尔 的语言哲学致力于分析特定表达或表达类别在逻辑上的功能：哪些词是量词，哪些暗示否定等，以便应用上述逻辑。为此，他依赖于传统的划分，源自亚里士多德，将主要的语言类别视为_名词_、_动词_，以及它们组合成_句子_。

阿伯拉尔认为名词是传统上具有意义的简单词，通常没有时态。在这种理解下，有各种各样的名词：专有名词和普通名词；形容词和副词；代词，无论是人称代词、所有格代词、反身代词还是关系代词；传统的感叹词，比如“天哪！”；以及，可以说，连词和介词（尽管缺乏明确的意义），还有分词和动名词（具有时态）。阿伯拉尔通常，尽管并非总是如此，将复合名词如“扫街者”进行还原处理。即便如此，他的列表仍不够一般化，无法列举所有的指称表达。事实上， 彼得·阿伯拉尔 对名词语义讨论的很大一部分实际上是围绕着代表其余部分的一个特殊情况展开的：普通名词。这些名词是普遍存在问题的核心，并且它们对语义学提出了特殊困难。

当 彼得·阿伯拉尔 提出他的主张，即普遍性仅是一种语言现象，因此普遍存在仅是“不过是词语”时，他提出了一个反对意见，即除非普通名称是普通项目的名称，否则它们将毫无意义，因此他的观点并不比他的老师罗斯林（认为普遍存在仅是嘴巴发出的声音）更好。对此，阿伯拉尔明确区分了名称具有的两种语义属性：指称（_nominatio_），即该术语适用于什么的问题；和意义（_significatio_），即听到该术语会使人想到什么，或更确切地说，该词意味着引发的概念的信息内容（_doctrina_），这是一个因果概念。对每个问题都需要做一些评论。

名称，无论是专有的还是普通的，都是指代个别或多个事物。名称与其所指的事物或事物种类联系在一起，就好像有人设计了名称来标记某个事物或某类事物，这个过程被称为“赋名”（仿效《创世记》2:19 中亚当给动物命名的过程），有点像洗礼。这种对指称的理性重建并不要求施加名称的人，即“赋名者”，除了有一个不明确的意图来挑选事物或事物种类之外，不需要有任何更多的东西，无论其性质如何：

> \[名称的发明者] 打算根据事物的某些本质或独特属性来加以施加，即使他自己不知道如何正确地思考某物的本质或独特属性。

一个名称“在其施加的本质中有一个定义，即使我们不知道它是什么。” 用现代术语来说，阿伯拉尔持有一种_直接指称_的理论，其中一个术语的外延并不取决于其意义。 我们经常完全不了解应该与成功施加的术语相关联的适当概念内容。

一个适当的名称——一个主体的名称——表示一个具体的个体（_hoc aliquid_），将其承载者与其他所有事物区别开来。因此，适当的名称是语义上的单数指称表达式，与索引词、指示词和单数描述（或描述性术语）密切相关。相比之下，普通名称在语义上与阿贝拉尔所称的“复数意义”有关。一方面，普通名称类似于复数名词；普通名称‘人’在语法上是单数的，但类似于复数词‘人们’——每个都指代每个人，尽管复数词表示个体作为集合的一部分，而普通名称分别指代每个个体。另一方面，普通名称类似于‘三人组’或‘一对’这样的术语，因为它们只在使用时确定地指代多个个体，而它们的外延是可变的。

因此，普通名称分别指代具体的个体，尽管不是指代它们作为个体。相反，它们分别指出那些具有特定本质的个体：‘人类’指的是苏格拉底和柏拉图，因为他们每个人都是人类。这不是任何共享特征；苏格拉底就是他所是的，即人类，同样柏拉图也是他所是的，即人类。阿贝拉尔在他的《逻辑学_ingredientibus_》中清楚地陈述了他的贬值立场。

> 现在看来，我们应该远离根据非任何事物的协议来接受事物之间的一致性——就好像我们要在现存的事物中联合不存在的东西一样！——也就是说，当我们说这个\[人类]和那个在人类身份上达成一致时，也就是说：在他们是人类这一点上。但我们的意思确切是说他们是人类，并且在这方面没有区别——让我重复一遍：\[他们在是人类这一点上没有区别]，尽管我们在这个解释中并没有诉诸于任何_事物_。

苏格拉底和柏拉图是真实的；他们的一致性也是真实的，但这并不能通过诉诸于任何事物来解释——他们的一致性就是他们各自作为人类的存在。从形而上学的角度来看，他们与人类的地位相同；这并不涉及任何形而上学上共同的共享成分，实际上也不涉及任何成分。这就是为共同名称的使用而施加的“共同理由”的意义。

对于所有表义而言，它都是在指称之后。名称也具有表义。阿伯拉尔认为，一个术语的表义是与听到它时与该术语相关联的概念的信息内容，在正常情况下。由于名称仅具有约定性的重要性，给定名称与哪个概念相关联在某种程度上取决于语言使用者的心理条件，因此阿伯拉尔可以将表义视为既是因果关系又是规范性概念：‘兔子’这个词应该导致英语的母语者在听到它时产生兔子的概念。阿伯拉尔小心地坚持认为，表义是概念中携带的信息内容的问题——仅仅是心理联想，甚至是与特定概念特征相关的心理形象，都不是词语的含义的一部分。理想情况下，概念将对应于贴近事物本质的真实定义，就像‘理性的有限动物’被认为是‘人类’的真实定义一样，不考虑其他相关特征（甚至是必要特征，比如可笑性）或偶然的形象（因为任何关于人的心理形象都会是具有明确特征的某个人）。当然，在我们的概念中实现这种清晰度是一项艰巨的任务，需要理解理解本身是如何运作的（请参阅[第 5 节](https://plato.stanford.edu/entries/abelard/#Min)中对阿伯拉尔心灵哲学的讨论）。然而，从这个例子中应该清楚的一点。一些名称的表义，比如与自然种类术语相对应的名称，是抽象的，因为它们仅包括指称术语所指事物的某些特征。它们并不积极排除所有其他特征，而且能够进一步确定规范：‘理性的有限动物’作为‘人类’概念的内容，表示所有人类，无论他们的其他特征是什么——高或矮，胖或瘦，男或女等等。

对于名称语义适用于大多数动词。与时态或语法人称相比，使动词与名称有所不同的特征是动词具有连接力（_vis copulativa_）。这是动词的一个原始且不可简化的特征，只有当它们以句法适当的方式与名称结合时才能发挥作用，类似于弗雷格概念中的‘未饱和性’。句子由名称和动词组成，整个句子的含义是其部分含义的函数。也就是说，阿伯拉尔语义学基本上是构成性的。构成的工作细节是复杂的。阿伯拉尔直接使用一种自然语言（拉丁语），尽管它具有人为性，但仍然是一种母语的第二语言。因此，阿伯拉尔被迫分析许多语言现象，这在更正式的框架中将被简单地禁止。

例如，阿伯拉尔指出，大多数动词可以以两种方式出现为谓语，即作为有限动词形式或作为与助动词连用的名词形式，因此我们可以说‘苏格拉底跑’或‘苏格拉底正在跑’；同样适用于及物谓词，例如‘苏格拉底打了柏拉图’和‘苏格拉底正在打柏拉图’。阿伯拉尔认为，一般来说，谓词的纯动词版本是基本形式，可以解释和澄清扩展版本；只有在简单动词形式缺乏时才严格需要后者。（实质动词‘是’需要特殊处理。）因此，对于阿伯拉尔来说，谓语陈述的基本分析认识到两个基本不同的语言范畴被结合在一起：名词 _n_ 和简单动词功能 _V_( )，结合在良构的句子 _V_(_n_) 中。

阿伯拉尔认为，句子（_propositiones_）必须表示的不仅仅是构成名词和动词的理解。首先，诸如‘苏格拉底跑’这样的句子涉及苏格拉底和跑步，而不仅仅是任何人的理解。我们谈论的是世界，而不仅仅是某人对世界的理解。其次，像‘如果某物是人类，它就是动物’这样的句子如果被视为关于理解，就是错误的，因为某人可以理解概念 _人类_ 而不理解概念 _动物_，因此前提可以成立而结论不成立。第三，理解是瞬息的个别事物，只是概念的心理标记。但至少有些结果性句子是必然的，必然性不能建立在转瞬即逝的事物上，因此也不能建立在理解上。因此，句子必须除了表示理解之外还表示其他东西，这些东西可以做纯粹的理解无法做到的事情。阿伯拉尔将这种表示句子所说的东西描述为其 _dictum_（复数 _dicta_）。

对于现代哲学的耳朵来说， 彼得·阿伯拉尔的_dicta_听起来可能像是命题，抽象实体，是真理和虚假的永恒载体。但是阿伯拉尔与任何此类实体都无关。他一再而强调地宣称，尽管_dicta_超越并且不同于表达它们的句子，但它们在本体论上根本没有立足之地。在短短的一段文字中，他说它们“根本不是真实的事物”，并两次称它们为“绝对的虚无”。它们支持句子，但它们并不是真实的事物。因为虽然一个句子表达了某事，但并没有某个东西被它所表达。句子的语义工作是_表达_某事，这与命名或指称某事不同。这实际上是一种提出事物如何的问题，只要不被赋予现实主义的解读。同样，真句子的真实并不是存在于某个永恒实体中的性质，而仅仅是句子所说的内容的断言—也就是说，阿伯拉尔采纳了真理的贬值论解释。如果事物与句子所说的一致，那么句子就是真实的，事物使句子真实或虚假，取决于它们的方式（以及句子所说的内容），不需要进一步的解释。句子“苏格拉底跑步”是真实的，因为苏格拉底在跑步，这就是所有可以说的，也是需要说的。

## 5. 心灵哲学

亚里士多德心灵哲学提供了两种关于意向性的分析：符合性理论认为我们通过在头脑中拥有对象的形式来思考一个对象，相似性理论则认为我们通过在头脑中拥有一种自然类似于对象的心像来思考。 阿伯拉尔拒绝了这些理论，并提出了一种关于思维的副词理论，表明心像和心智内容都不需要被视为本体上独立于头脑。 他对意向性进行了语境解释，依赖于心理表征的语言学解释，采用了一个用于理解的组合原则。

第一个亚里士多德分析认为，理解是头脑获得被理解对象的形式，而不是其物质。 要使一个理解关于某物——比如一只猫——就需要猫的形式在头脑或理解的灵魂中。 形式在物质中的内在性使得物质成为某种特定类型的东西，因此形式在物质中的内在性产生了一只实际的猫，而形式“猫”在头脑中的（非物质的）内在性则将头脑转化为对猫的理解：头脑变成了（形式上）与其对象相同。 由于理解的“关于性”被分析为理解和被理解事物之间形式的共同性或一致性，我们可以将这种方法称为“符合性理论”。 这一理论捕捉了理解在某种程度上继承或包含被理解对象的属性的直觉，通过将理解的意向性归约为头脑中的形式与世界中的形式的客观一致性。

第二个亚里士多德分析认为，理解是指心智拥有一个概念，这个概念是与所理解的自然相似的，或者自然类似于所理解的对象。对于一个理解来说，比如一只猫，就是指在心智中存在一个当前概念，这个概念是猫的自然相似物。称这种相似为“自然”的动机在于确保理解与被理解之间的相似是客观的，并且所有人都可以接触到相同的概念库。（符合性理论通过假设事物中形式的客观存在，并通过所有人同化或获取形式的相同过程来实现这一点。）我们可以称这种方法为理解的“相似理论”：根据心智行为与所理解事物之间相似程度和种类的不同对其进行分类。

相似理论在阐明相似或类似内容方面面临着众所周知的问题。例如，一个概念显然是非物质的，因此与任何物质对象都存在根本的区别。此外，似乎没有心智行为的形式特征，使得可以非平凡地说它类似于其他任何事物。为了解决这些困难，中世纪哲学家，就像几个世纪后的英国经验论者一样，诉诸于一种特定的相似，即图像相似。苏格拉底的肖像由于在视觉上以正确的方式类似于苏格拉底而与苏格拉底有关。正如有关其主题的图像存在一样，心智图像也是关于事物的。这些心智图像，无论是概念还是包含在概念中，解释了概念“关于”对象的方式。一个理解若要关于一只猫，就需要是或包含一只猫的心智图像。心智“关于性”的现象通过更为熟悉的图像关于性案例来解释，而这本身又归结为一种真实的相似关系。

尽管它们有共同的亚里士多德传统，但符合理论和类似理论并不等同。通过形式的内在性而使心灵转化的过程未必与心灵拥有概念相同。同样，自然的类似或相似无需被理解为形式的一致；形式上的一致并不意味着真正的相似，因为形式所具体化的主体不同。

调和符合理论和类似理论的标准方法是将心灵对概念的拥有解释为通过形式的内在性来转化自身的能力，将形式上的一致解释为自然的类似，即在心灵中拥有一个与所理解对象的形式相同的形式，就是拥有该对象的心理形象。

Abelard 反对一致性的论点如下。考虑一座塔，这是一个具有一定长度、深度和高度的物体；假设这些特征构成了它的形式，就像雕像的形状构成了它的形式一样。根据亚里士多德的形而上学，形式在主体中的内在性使主体成为具有该形式特征的东西，就像苏格拉底身上的白色使他成为白色的东西一样。塔的形式同样使它们所在之处变得高大、宽阔、厚重——所有物理属性。如果这些形式存在于心灵中，那么它们应该使心灵变得高大、宽阔和厚重，这是一个荒谬的结论：心灵“无法在长度或宽度上延伸自己”。然而，一致性理论的一个基本命题是，心灵具有与外部对象——塔相同的形式，尽管（比如）长度的形式本质上是物理的。因此，Abelard 得出结论，一致性是不连贯的。

阿伯拉尔对相似性理论的主要反对意见是，心理形象作为形象，就像任何符号一样，是惰性的：它们需要解释。符号只是一个对象。它可以被视为具有意义的角色，尽管不一定如此。Abelard 指出，这种区别同样适用于非心理符号：我们可以将雕像视为一块青铜或一幅肖像。心理形象同样是惰性的。因此，要使符号具有意义功能，除了它的存在或存在之外，还需要更多的东西。但是相似性理论并没有意识到需要将心理形象解释为形象，并因此错误地将理解与心灵中心理形象的存在等同起来。Abelard 得出结论，心理形象在思维中只起到工具性的作用，将它们描述为“事物的中介符号”（_intersigna rerum_）。意向性实际上源自于对心理形象的注意行为（_attentio_）。证据在于我们可以通过简单地关注心理形象的不同特征来“改变理解”：同一形象——比如，一棵无花果树——可以用来思考这棵无花果树本身，或者一般的树木，或者植物生命，或者我失去的爱人，与我一起坐在树下，或者任何其他事物。心理形象没有任何固有特征使其关于任何特定事物；如果有的话，Abelard 指出，我们可以通过检查确定一个符号是关于什么的——但我们不能。因此，心理形象无法解释理解的意向性，因为它们的作用仅仅是工具性的。我们用它们来思考，无法避免它们；但它们并不能解释意向性。

彼得·阿伯拉尔得出结论，即意向性是心灵的一个原始且不可归约的特征，是我们关注事物的行为。不同的关注行为在本质上彼此不同；它们之所以关注某事物，是因为它们是某种类型的关注。因此，阿伯拉尔采用了现今所称的“副词”思维理论。

鉴于意向性是原始的，阿伯拉尔采用了一种与心智内容相关的情境方法：他将这些不可归约的关注行为嵌入一个结构中，这个结构的表达有助于定义其构成要素的特征。阿伯拉尔提供的结构是语言的，一种心智行为的逻辑：正如词语可以表达思想一样，我们也可以使用语言的表达逻辑来给出对理解的理论。简言之，阿伯拉尔提供了一种非常类似于语言的心智表征或意向性的解释。为此，他采纳了一个组合性原则，认为理解的对象是其组成理解对象的功能。一个复杂对象的理解的统一性取决于其逻辑的简单性，这由阿伯拉尔所称的“一个主导连接词”（最广泛范围的逻辑运算符）所特征化。因此，一个复杂对象的理解可以被视为一系列不同理解对象的复合体，聚合在同一个思想中，其（逻辑）结构由“主导连接词”支配其组成理解对象的其他逻辑运算而产生。阿伯拉尔的关注行为展示了它们所表达的理解的逻辑结构，从而给出了书面或口头语言的语义。阿伯拉尔在逻辑和辩证法方面的许多著作都致力于拟定细节，作为阐明心智内容的一种方案。

## 6. 伦理学

阿伯拉尔认为传统基督教道德的理性核心是根据以下原则进行_意向主义_，即行动的道德价值仅取决于行动者的意图。他反对后果对道德的相关性的主要论点在于当代哲学家经常提到的道德运气。假设两个人都有钱和意图为穷人建立庇护所，但一个人在行动之前被抢劫，而第二个人能够实现他的意图。根据阿伯拉尔的观点，认为他们之间存在道德差异是认为“越富有的人就能变得越好…这是疯狂的极致！”以行为为中心的道德失去了对可能发生的情况的任何影响。同样，它也不能为考虑行动者的认识状态提供任何依据，尽管大多数人会承认无知在道德上可以使行动者免罪。阿伯拉尔用以下例子来说明这一点：想象一下同卵双胞胎的情况，兄弟和姐妹，在出生时被分开，每个人对对方的存在甚至一无所知；成年后他们相遇，坠入爱河，合法结婚并发生性关系。从技术上讲这是乱伦，但阿伯拉尔认为两者都没有错。

阿伯拉尔得出结论，行为本身在道德上是中立的。道德评价的适当对象是行动者，通过他或她的意图。可能会有人提出异议，即行为的执行或不执行可能会影响行动者的感受，进而可能影响他或她的意图，因此行为因此具有道德相关性（至少间接）。阿伯拉尔否认了：

> 例如，如果有人强迫一个僧侣被捆绑在两个女人之间的床上，由于床的柔软和身边女人的触摸使他感到愉悦（但没有同意），谁能认定这种自然所必然产生的愉悦是一种过错呢？

我们的构造使得在某些情况下感受到快乐是不可避免的：性交、享用美食等等。如果婚姻中的性快乐不是罪恶的，那么快乐本身，无论是在婚姻内还是外面，都不是罪恶的；如果它是罪恶的，那么婚姻就不能使其神圣化——如果得出这样的结论，即这些行为应该完全没有快乐地进行，那么阿伯拉尔声明它们根本无法完成，而且让它们以一种无法完成的方式进行是不合理的（上帝）。

在积极的一面，阿伯拉尔认为，除非意图是评估道德价值的关键因素，否则很难理解为什么强迫行为，即被迫做一些违背其意愿的事情，应该使行为者免罪；对于无知也是如此——尽管阿伯拉尔指出，重要的道德观念不仅仅是无知，而严格来说是疏忽。阿伯拉尔举一个极端案例来阐明他的观点。他认为，钉死基督的人并不邪恶。 （这个例子以及类似的例子使阿伯拉尔惹上了麻烦，而且不难理解为什么。）他们对基督神圣本质的无知本身并没有使他们邪恶；他们根据他们（错误和错误的）信念行事，钉死基督也不会使他们邪恶。他们的非疏忽性无知使他们的行为免于责备。事实上，阿伯拉尔认为，如果他们认为钉死基督是必要的，并且没有钉死基督，他们就会犯罪：无论案件的事实如何，道德行为中不遵循自己的良心会使行为者应受责备。

有两个明显的反对意见针对 彼得·阿伯拉尔 的意向论。首先，如何可能自愿地犯罪恶行？其次，由于意图只有代理人可以访问，那么阿伯拉尔的观点是否意味着不可能做出伦理判断？

关于第一个反对意见，阿伯拉尔有一个双重回答。首先，很明显我们经常想要去做某件事，同时又不想遭受惩罚。一个人想要与女人发生性关系，但又不想犯奸淫罪；他更希望她是未婚的。其次，很明显我们有时会“想要我们根本不想要想要的事情”：我们的身体会独立于我们的意志产生快乐和欲望的反应。如果我们根据这些欲望行动，那么我们的行为是“出于”意志，正如阿伯拉尔所称，尽管不是自愿的。欲望本身并没有恶意：只有在追随欲望时才有恶意，这与拥有相反欲望是相容的。

关于第二个反对意见， 阿伯拉尔 承认其他人无法了解行动者的意图 —— 当然，上帝可以接触内在的心理状态，因此可以有最终的审判。然而， 阿伯拉尔 认为伦理判断并不构成问题。上帝是唯一有权作出判断的人。然而，这个事实并不妨碍我们执行人类正义的规范，因为， 阿伯拉尔 认为，人类正义主要具有示范和威慑功能。事实上， 阿伯拉尔 认为，惩罚一个我们坚信没有恶意的行动者甚至可能是公正的。他举了两个例子。首先，一个女人在试图在夜晚保暖婴儿时意外使其窒息，并深感悲痛。 阿伯拉尔 主张我们应该因为她的惩罚可能对其他人有益的示例而惩罚她：这可能会让其他贫困母亲更加小心，不要在试图保暖婴儿时意外使其窒息。其次，一名法官可能有优秀的（但在法律上不允许的）证据表明一名证人在作伪证；由于他无法证明证人在撒谎，法官被迫根据证人的证词作出裁决，他相信被告是无辜的，但却被判有罪。人类正义可能适当地忽略意图问题。由于有神圣的正义，伦理观念并不是无用的 —— 即使在 阿伯拉尔 对人类正义的理解上也不应如此，因为它们是我们确定在惩罚人们作为示例或为了威慑他人时要促进或阻止哪些意图的手段。

因此，唯一可证实的罪恶是违背良心行事，除非一个人在道德上疏忽。然而，如果我们无法看待行为的内在价值或其后果，我们如何确定哪些行为是允许的或是义务的呢？除非良心有一个可靠的指引， 阿伯拉尔 的立场似乎会为善意的主观主义打开闸门。

Abelard 通过将顺从上帝意愿——道德正确行为的标志，本身就是自然法的一个实例——视为代理人意图符合纯粹形式标准的问题来解决问题，即黄金法则（“己所不欲，勿施于人”）。这个标准可以仅通过理性发现，不需要任何特殊启示或宗教信仰，并足以确保代理人意图的正确性。但是，解决这个问题立即导致另一个问题。即使我们承认阿贝拉尔的自然伦理学，代理人为什么要在意他或她的意图是否符合黄金法则呢？简而言之，即使阿贝拉尔在道德方面是正确的，为什么要做道德呢？

阿贝拉尔的答案是，我们的幸福——没有人会对此漠不关心——与美德有关，即与习惯性的道德正确行为有关。事实上，阿贝拉尔在《对话录》中的项目是要证明理性可以证明仅仅是自然伦理是不够的，并且代理人的幸福必然与接受传统基督教信仰原则紧密相连，包括对上帝和来世的信仰。特别是，他认为来世是我们应该向往的状态，它甚至比这个世界的美德生活更有道德提升，并且认识到这一点构成了想要做上帝想要的事情的本质，也就是按照黄金法则生活，这保证了我们的长期来世幸福，就像任何事情一样（在神圣恩典的帮助下）。

The Philosopher 首先与犹太人辩论，后者奉行“严格遵守”道德理论，即服从摩西律法。犹太人提出的论点之一是奴隶的赌注（显然是帕斯卡赌注的最早版本）。想象一个奴隶一个早晨被一个他不认识的人告知，他那位强大易怒的主人，当天不在家，留下了关于他不在时该怎么做的指示。奴隶可以遵循这些指示，也可以不遵循。他推理道，如果主人确实留下了指示，那么遵循指示他将得到奖赏，不遵循则会受到严厉惩罚；而如果主人没有留下指示，那么遵循指示他不会受到惩罚，尽管不遵循可能会受到轻微惩罚。（这符合帕斯卡赌注的标准收益矩阵。）这就是犹太人所处的位置：上帝显然要求对摩西律法，即留下的指示，无条件服从。哲学家认为犹太人可能有其他行动选择，并且无论如何，有理由认为道德不是按照法律行动的问题，而是取决于行动者的意图，正如我们前面所看到的那样。

然后，哲学家与基督徒辩论。他最初认为美德意味着幸福，因此不需要来世，因为一个有美德的人无论生死都保持相同状态。然而，基督徒推理称来世更好，因为除了通过生活美德所带来的好处外，行动者的意愿不再受到环境的阻碍。在来世，我们不再受身体控制，例如，不再受食物、住所、衣物等物质需求的限制。因此，当没有外部环境影响行动者的行动时，行动者可以像遵循美德的生活所允许的那样纯粹地幸福。哲学家承认，如此理解的来世甚至比这个世界中的美德生活都要明显改善，并与基督徒共同努力定义美德和至善的本质。美德本身就是奖赏，在来世中没有任何事物阻止我们尽可能地用美德奖赏自己。

## 7. 神学

阿伯拉尔认为，推理在信仰问题中的作用是有限的。他赋予推理任何作用都会引起与那些我们现在可能称之为“反辩者”（包括他的同僚克莱尔沃修道院院长伯纳德）的冲突。他赋予推理的作用是有限的，这一观点使他与那些他称之为“伪辩者”（包括他的前任老师罗斯林）发生冲突。

伯纳德·克莱尔沃和其他反辩证法者似乎认为，信仰命题的意义，只要能够理解，就是明显的；除了这种明显的意义，我们根本无法理解任何东西，在这种情况下，理性显然无济于事。也就是说，反辩证法者对宗教句子的明显含义是_语义现实主义者_。因此，他们对阿伯拉尔的不耐烦，因为阿伯拉尔似乎不仅试图模糊信仰命题的明显含义，这已经够糟糕了，而且还通过推理来这样做，而推理在理解明显含义（因为明显含义的明显性在于立即被理解，而无需推理）或者达到更深刻理解方面都没有任何作用。

阿伯拉尔对支撑复杂反辩证立场的语义现实主义毫无耐心。他并没有直接反对它，而是试图削弱它。从他对经文和教义的评论到他的思辨神学著作，阿伯拉尔首要关心的是展示宗教主张如何被理解，特别是辩证方法的应用如何能够澄清和阐明信仰命题。此外，他拒绝了有明显含义可被理解的说法。在他的《是与非》序言中概述了他的方法，阿伯拉尔描述了他如何最初提出一个问题，例如神职人员是否需要独身，然后从经书和教父的权威中摘录引文，这些引文至少似乎直接回答了问题，并将它们分为正面和负面的回应。（阿伯拉尔在序言中提供了解决权威之间明显矛盾的建议，使用各种技巧：查看双方是否使用相同意义的词语；提出相关区别以解决问题；查看引文的背景；确保作者是在发表自己的观点，而不仅仅是报告或转述他人的立场；等等。）现在，阿伯拉尔引用的每个权威似乎都清晰明了地支持或反对某个问题的正面回答或负面回答。如果有明显含义的情况，阿伯拉尔似乎已经在权威中找到了它们，在有争议的问题的对立面。他在序言中的建议是说，那些似乎是明显含义的句子实际上必须仔细审查，以了解它们的含义。然而，这只是说它们根本没有明显含义；我们必须使用理性来揭示它们的含义。因此，反辩证法者没有立场。

有一个更严重的威胁在宗教中正确使用理性，阿伯拉尔认为（《基督教神学》3.20）：

> 那些自称是辩证法师的人往往更容易被引向异端，因为他们认为自己装备充足，理由充分，因此更加自信，他们会更自由地攻击或捍卫任何立场。他们的傲慢是如此之大，以至于他们认为没有什么是他们那些微不足道的推理无法理解和解释的。他们蔑视一切权威，自吹自擂，只相信自己——因为那些只接受他们的理性所说服的人，无疑只对自己负责，就好像他们有一双不熟悉黑暗的眼睛。

这类伪辩士认为理性是一切主张的最终仲裁者，包括关于信仰事项的主张。更确切地说，阿伯拉尔指责他们认为(a)一切都可以用人类理性来解释；(b)我们只应接受理性说服我们的东西；(c)对权威的诉诸没有理性的说服力。他认为，真正的辩士拒绝(a) - (c)，承认人类理性是有限的，并且一些重要的真理可能超出这些限制但并非超出信仰；关于信仰事项的主张我们应接受取决于它们的来源（权威）的认识可靠性以及它们与理性的一致性在可以调查的范围内。

阿伯拉尔拒绝(a) - (c)的论据是复杂而微妙的。对于理性可以有效应用于特定信仰条款的主张，阿伯拉尔在自己的著作中提供了一个具体案例研究。阿伯拉尔在神学上的大部分工作致力于他对三位一体的辩证研究。他阐述了一个关于身份的原创理论来解决围绕三位一体的问题，这个理论在形而上学中具有更广泛的适用性。他研究的结果是，三位一体的信仰是理性上可证明的，因为就理性所能达到的范围而言，我们发现这个教义是有意义的——至少，在辩证学的工具被正确运用之后。

传统的同一性观，源自博伊修斯，认为事物可以在种类上、具体上或数量上是相同或不同的。 阿伯拉尔接受了这一观点，但发现这并不足以处理三位一体的问题。 他在《基督神学》中提出的同一性理论的核心包括四种额外的同一性模式：（1）本质上的同一性和差异；（2）数值上的同一性和差异，阿贝拉尔将其与本质上的同一性和差异紧密联系在一起，比博伊修斯允许的区分更加精细；（3）定义上的同一性和差异；（4）属性上的同一性和差异（_in proprietate_）。 大致而言，阿贝拉尔对本质和数值上的同一性的阐述旨在改进传统观点中给出的事物的同一性条件；他对定义上的同一性的阐述旨在为事物的特征提供同一性条件；他对属性上的同一性的阐述则打开了一个可能性，即一个单一事物具有几个不同特征的情况下可能存在不同的同一性条件。

阿贝拉尔认为，当两件事在本质上是数值上相同的具体事物（_essentia_）时，它们在本质上是相同的，否则在本质上是不同的。 例如，晨星在本质上与傍晚星是相同的，因为每颗都是金星。 再次，构成具体事物的形式元素在本质上彼此相同，并且在本质上与它们是形式组成部分的具体事物相同：苏格拉底就是他的本质（苏格拉底就是作为苏格拉底的本质）。 然而，相应的一般命题并不适用于部分。 阿贝拉尔坚持认为，部分在本质上与其作为部分的整体本质上是不同的，他推理认为，给定的部分完全包含在整体中，连同其他部分，因此小于整体的数量。

数值差异并不精确映射到本质差异。数值相同失败可能是由于两种原因之一。首先，当一个对象具有另一个对象没有的部分时，它们就不是数值上相同的，此时这些对象在本质上也是不同的。其次，当两者都没有属于另一个对象的部分时，它们在数值上是不同的。因此，数值差异意味着数值相同的失败，但反之不成立：一个部分在数值上并不等同于整体，但它在数值上也不与整体不同。因此，一件事与另一件事本质上不同，当它们只有一个部分是共同的时，此时它们在数值上并不相同；或者它们没有任何部分是共同的，此时它们在数值上是不同的，也不是数值上相同的。由于事物既可能不是数值上相同也不是数值上不同，所以问题“有多少事物存在？”本身就是不完整的，必须更加明确，这一事实在彼得·阿伯拉尔讨论三位一体时得到了利用。

本质上和数值上的相同与差异直接适用于世界上的事物；它们是身份的外延形式。相比之下，在定义上的相同与差异大致类似于现代关于属性身份的理论。阿伯拉尔认为，当一个事物的本质要求它是另一个事物时，它们在定义上是_相同的_，反之亦然；否则它们在定义上是不同的。

最后，当它们指定相互表征的特征时，事物在_属性上相同_。 阿伯拉尔提供了一个例子来阐明这个概念。 一个大理石立方体既体现了白色又体现了硬度；白色的东西本质上与硬的东西相同，因为它们在数量上是同一件具体的东西，即大理石立方体；然而大理石立方体中的白色和硬度在定义上显然是不同的——但即便如此，白色的特征是硬度（白色的东西是硬的），反之亦然，硬的特征是白色（硬的东西是白色的）。 白色和硬度的属性是“混合的”，因为尽管它们在定义上有所不同，但每个属性都适用于同一具体事物（即大理石立方体）本身，同时也适用于它被另一个属性表征的情况。

有趣的情况是，某物具有“保持完全不混合”的属性，这些属性使它们表征的事物在_属性上不同_。 考虑一个形式-物质复合体与其物质的关系。 一个形式-物质复合体所用的物质本质上与复合体相同，因为每个都是整个物质复合体本身。 尽管它们在本质上相同，但它们并不相同；物质不是复合体，反之亦然。 物质不是复合体，因为复合体是由物质构成的，但物质并不是由自身构成的。 复合体也不是物质，因为“没有任何东西在任何方面是其自身的组成部分或自然先于自身”。 相反，物质在复合体之前，因为它具有与复合体相关的_优先权_属性，而复合体在其物质之后，因为它具有与其物质相关的_后继性_属性。 现在尽管在本质上相同，但物质并不以后继性为特征，不像复合体，而复合体也不以优先性为特征，不像物质。 因此，物质和复合体在属性上是不同的；属性_优先权_和_后继性_是不混合的——它们在属性上不同。

现在是回报的时候了。彼得·阿伯拉尔运用他的同一性理论来阐明三位一体，具体如下。这三位在本质上彼此相同，因为它们都是同一件具体的事物（即上帝）。它们在定义上彼此不同，因为作为父亲的本质与作为儿子或圣灵的本质并不相同。这三位在数量上彼此不同，否则它们就不会是三位，但它们在数量上与上帝并不不同：如果它们不同，就会有三位上帝，而不是一个。此外，每位有其独特适用的属性——_非生_适用于父亲，_生_适用于儿子，_出于_适用于圣灵，以及属于它的与众不同的属性，比如父亲的_权能_，儿子的_智慧_，圣灵的_善良_。在阿伯拉尔的技术意义上，这些独特属性是纯粹的，因为这些位在它们的独特属性上彼此不同，而这些属性并不适用于上帝；而这些与众不同的属性是混合的，因为上帝被每个属性所表征（全能的上帝是智慧的上帝是善良的上帝）。阿伯拉尔认为，人类理性无法超越这一点；但理性验证了这一分析（严格来说只是一个“类似”或类比）尽其所能。

## Bibliography

### Primary texts in Latin

* _Carmen ad Astralabium_. Edited by J. M. A. Rubingh-Bosscher in _Peter Abelard: Carmen ad Astralabium, a Critical Edition_. Groningen: phil. diss. Rijksuniversiteit 1987.
* _Collationes_ a.k.a. _Dialogus inter Philosophum, Iudaeum, et Christianum_. Edited by Giovanni Orlandi, with introduction, translation, and notes by John Marenbon, in _Peter Abelard: Collationes_, Oxford University Press 2001.
* _Commentaria in Epistolam Pauli ad Romanos_. Edited by Eligius M. Buytaert in _Petri Abaelardi opera theologica_. Corpus christianorum (continuatio mediaevalis) Vol. 11. Brepols: Turnholt 1969, 389–340.
* _Dialectica_. Edited by L. M. De Rijk in _Petrus Abaelardus: Dialectica_, Assen: Van Gorcum 1970 (second edition).
* _Epistolae_: Ep. 1 edited by Monfrin (see the entry below for the _Historia calamitatum_); Epp. 2–5 edited by J. T. Muckle, _Mediaeval Studies_ 15 (1953) 68–94; Epp. 6–7, edited by J. T. Muckle, _Mediaeval Studies_ 17 (1955) 241–281; Ep. 8, edited by T. P. McLaughlin, _Mediaeval Studies_ 18 (1956) 242–297; Epp. 9–14 edited by E. R. Smits in _Peter Abelard: Letters IX–XIV_, Groningen: Rijksuniversiteit 1983; Ep. 15 edited by Josef Reiners, BGPTM 8 (1910) 63–80; Ep. 16, edited by Victor Cousin and Charles Jourdain, _Petri Abaelardi opera_ Vol. 1 (Paris 1849) 703–707, corrected against Van Den Eynde, _Antonianum_ 38 (1963) 219; Ep. 17, edited by Charles Burnett, _Mittellateinisch Jahrbuch_ 21 (1986), 152–155; _Apologia contra Bernardum (Ne iuxta Boethianum)_, edited by Eligius M. Buytaert in CCCM 12 359–368; _Epistola contra Bernardum_ edited by Raymond Klibansky, _Medieval and Renaissance Studies_ 5 (1961), 1–27; _Confessio fidei “Uniuersis”_ edited by Charles Burnett, _Mediaeval Studies_ 48 (1986), 182–189.
* _Ethica seu Scito teipsum_. Edited by R. M. Ilgner in _Petri Abaelardi opera theologica_. Corpus christianorum (continuatio mediaevalis) Vol. 190. Brepols: Turnholt 2001.
* _Expositio orationis dominicae_. Edited by Charles Burnett, “Expositio orationis dominicae ‘Multorum legimus orationes’” in _Révue Benedictine_ 95 (1985) 60–72.
* _Expositio symboli Apostolorum_. Edited by Victor Cousin and Charles Jourdain, _Petri Abaelardi opera_ Vol. 1 (Paris 1849) 603–615. \[[Available online thanks to the Bibliothèque nationale de France](https://gallica.bnf.fr/ark:/12148/bpt6k69212x?rk=21459;2)].
* _Expositio fidei in symbolum Athanasii_. Edited by Victor Cousin and Charles Jourdain, _Petri Abaelardi opera_ Vol. 1 (Paris 1849) 615–617.
* _Hexaëmeron_. Edited by Mary F. Romig with the assistance of David Luscombe, in _Corpus christianorum continuatio mediaevalis_ Vol.15. Brepols: Turnhout 2004.
* _Historia calamitatum_. Edited by Jacques Monfrin in _Abélard, Historia calamitatum: texte et commentaires_, J. Vrin: Paris 1974 (fourth edition), 62–109.
* _Hymnarius Paraclitensis_. Edited by Chrysogonus Waddell in _Hymn Collections from the Paraclete_ Vol. 2. Trappist Monastery, Ky.: Gethsemani Abbey (Cistercian Liturgy series) 1987.
* _Introductiones parvulorum_. Edited by Mario Dal Pra in _Pietro Abelardo: Scritti di logica_, Firenze 1969 (second edition).
* _Logica_ ‘_ingredientibus_’ (LI):
  * LI 1: Commentary on Porphyry’s _Isagoge_. Edited by Bernhard Geyer in _Beiträge zur Geschichte der Philosophie und Theologie des Mittelalters_ 21 (1). Aschendorff: Munster 1919.
  * LI 2: Commentary on Aristotle’s _Categories_. Edited by Bernhard Geyer in _Beiträge zur Geschichte der Philosophie und Theologie des Mittelalters_ 21 (2). Aschendorff: Munster 1921.
  * LI 3: Commentary on Aristotle’s _De interpretatione_. Edited by Klaus Jacobi and Christian Strub, _Corpus christianorum continuatio mediaevalis_ Vol.206. Brepols: Turnhout 2010.
  * LI 7: Commentary on Boethius’s _De topicis differentiis_. Edited by Mario Dal Pra in _Pietro Abelardo: Scritti di logica_, Firenze 1969 (second edition).
* _Logica_ ‘_nostrorum petitioni sociorum_’. Commentary on Porphyry’s _Isagoge_. Edited by Bernhard Geyer in _Beiträge zur Geschichte der Philosophie und Theologie des Mittelalters_ 21 (4). Aschendorff: Munster 1933.
* _Planctus_. Planctus 1, 4, 6: edited by Peter Dronke, _Poetic Individality in the Middle Ages_ (London 1986). Planctus 2, 5: edited by Giuseppe Vecchi, _Pietro Abelardo, I “Planctus”_ (Modena 1951). Planctus 3: edited by Wolfram von den Steinen, _Mittellateinisches Jahrbuch_ 4 (1967), 122–144. There are several modern recordings.
* _Problemata Heloïssae cum Petri Abaelardi solutionibus_. Edited by Victor Cousin and Charles Jourdain, _Petri Abaelardi opera_ Vol. 1 (Paris 1849): 237–294.
* _Sententiae secundum Magistrum Petrum_. Edited by Lorenzo Minio-Paluello in _Twelfth-Century Logic: Texts and Studies_ Vol. 2 (Abaelardiana inedita), Roma 1958.
* _Sermones_. Edited by Paola De Santis in _I sermoni di Abelardo per le monache del Paracleto_, Leuven University Press 2002. (Mediaevalia Lovaniensa ser. 1, studia 31.)
* _Sic et non_. Edited by Blanche Boyer and Richard McKeon in _Peter Abailard: Sic et Non. A Critical Edition_. University of Chicago Press 1977.
* _Soliloquium_. Edited by Charles Burnett in “Peter Abelard’s ‘Soliloquium’: A Critical Edition” in _Studi Medievali_ 25 (1984), 857–894.
* _Theologia_ ‘_summi boni_’. Edited by Eligius M. Buytaert and Constant Mews in _Petri Abaelardi opera theologica_. Corpus christianorum (continuatio mediaevalis) Vol. 13. Brepols: Turnhout 1987.
* _Theologia christiana_. Edited by Eligius M. Buytaert in _Petri Abaelardi opera theologica_. Corpus christianorum (continuatio mediaevalis) Vol. 12. Brepols: Turnhout 1969.
* _Theologia_ ‘_scholarium_’. Edited by Eligius M. Buytaert and Constant Mews in _Petri Abaelardi opera theologica_. Corpus christianorum (continuatio mediaevalis) Vol. 13. Brepols: Turnhout 1987.
* _Tractatus de intellectibus_. Edited by Patrick Morin in _Abélard: Des intellections_. Paris: J. Vrin 1994.

### Primary texts in English translation

* Fairweather, E. R., 1995, _A Scholastic Miscellany_, Westminster: John Knox Press. (Excerpt from Abelard’s commentary on _Romans_.)
* King, Peter, 1982, _Peter Abailard and the Problem of Universals in the Twelfth Century_, Ph.D. Dissertation, Philosophy Department, Princeton University. (Volumes 2 contains a complete translation of Abelard’s _Tractatus de intellectibus_.)
* Luscombe, David, 1971, _Ethics_, Oxford: Oxford University Press. (Complete translation of Abelard’s _Ethica_.)
* Marenbon, John and Giovanni Orlandi (eds. and trans.), 2001, _Peter Abelard: Collationes_, Oxford: Clarendon. (Complete translation of Abelard’s _Conversations_.)
* McCallum, James Ramsay, 1948, _Abelard’s Christian Theology_, Oxford: Blackwell. (Includes substantial selections from Abelard’s _Theologia christiana_.)
* Minnis, A. and Scott, A. B. (eds.), 1988, _Medieval Literary Theory and Criticism 1100–1375_, Oxford: Oxford University Press. (Includes Abelard’s preface to the _Sic et non_.)
* Payer, Pierre, 1979, _Peter Abelard: A Dialogue of a Philosopher with a Jew and a Christian_, Toronto: The Pontifical Institute of Mediaeval Studies Publications.
* Radice, Elizabeth, 1974, _The Letters of Abelard and Heloise_, New York: Penguin Books.
* Spade, Paul Vincent, 1994, _Five Texts on the Mediaeval Problem of Universals_, Indianapolis: Hackett Publishing Company. (Abelard’s discussion of the problem of universals from his _Logica_ ‘_ingredientibus_’.)
* Spade, Paul Vincent, 1995, _Peter Abelard: Ethical Writings_, Indianapolis: Hackett Publishing Company. (Complete translations of Abelard ‘s _Ethics_ and _Conversations_.)
* Tweedale, Martin and Bosley, Richard, 1997, _Issues in Medieval Philosophy_, Peterborough: Broadview Press. (Includes selections from Abelard on foreknowledge, universals, and ethics.)

### Selected Secondary Literature in English

* Allen, Julie, 1996, _A Commentary on the Second Collatio of Peter Abailard’s Dialogus_, Ph.D. Dissertation, Philosophy Department, University of Toronto.
* Arlig, Andrew, 2007, “Abelard’s Assault on Everyday Objects”, _American Catholic Philosophical Quarterly_, 81: 209–227.
* –––, 2012, “Peter Abelard on Material Constitution”, _Archiv fur Geschichte der Philosophie_, 94: 119–146.
* –––, 2013, “Some Twelfth-century Reflections on Mereological Essentialism”, _Oxford Studies in Medieval Philosophy_, 1: 83–112.
* –––, 2022, “Abelard and Other Twelfth-Century Thinkers on Social Constructions”, _Philosophies_, 7(4), 84. \[[Arlig 2022 available online](https://www.mdpi.com/2409-9287/7/4/84)]
* Astroh, Michael, 2001, “Abelard on Modalities _de re_ and _de dicto_”, in _Potentialität und Possibilität. Modalaussagen in der Geschichte der Metaphysik_, Thomas Buchheim, C. H. Kneepkens, and Kuno Lorenz (eds.), Stuttgart: Frommann Holzboog, 79–95
* Bejczy, I., 2003, “Deeds Without Value: Exploring a Weak Spot in Abelard’s Ethics”, _Recherches de théologie et philosophie médiévale_, 70: 1–21.
* Binini, Irene, 2022, _Possibility and Necessity in the Time of Peter Abelard_, Leiden/Boston: Brill.
* Blackwell, Daniel, 1988, _Non-Ontological Constructs: The Effects of Abaelard’s Logical and Ethical Theories on his Theology_, Berne, Paris, New York: Peter Lang.
* Boler, John, 1963, “Abailard and the Problem of Universals”, _The Journal of the History of Philosophy_, 1: 104–126.
* Brower, Jeff, 1998, “Abelard’s Theory of Relations: Reductionism and the Aristotelian Tradition”, _The Review of Metaphysics_, 51: 605–631.
* –––, 2004, “Trinity”, in _The Cambridge Companion to Abelard_, J. Brower and K. Guilfoy (eds.): 223–257.
* Brower, Jeff and Guilfoy, Kevin (eds.), 2004, _The Cambridge Companion to Abelard_, New York: Cambridge University Press.
* Freddoso, Alfred, 1978, “Abailard on Collective Realism”, _The Journal of Philosophy_, 75: 527–538.
* Gracia, Jorge, 1984, _Introduction to the Problem of Individuation in the Early Middle Ages_, Washington, D.C.: Catholic University of America Press.
* Guilfoy, Kevin, 1999, _Abelard’s Theory of the Proposition_, Ph.D. Dissertation, Philosophy Department, University of Washington.
* –––, 2004, “Mind and Cognition”, in J. Brower and K. Guilfoy (eds.), 200–222.
* Henry, D. P., 1985, “Abelard’s Mereological Terminology”, in _Mediaeval Semantics and Metaphysics_, E. P. Bos (ed.), Ingenium: Nijmegen, 65–92.
* Hause, Jeff, 2007, “Abelard on Degrees of Sinfulness”, _American Catholic Philosophical Quarterly_, 81: 251–270.
* Jacobi, Klaus, 1983, “Abelard and Frege: the Semantics of Words and Propositions”, in _Atti del Convegno Internazionale di Storia della logica_, V. Abrusci (ed.), Bologna: Ed. CLUEB, 81–96.
* –––, 1986, “Peter Abelard’s Investigations into the Meaning and Function of the Speech Sign ‘Est’”, in _The Logic of Being_, Simo Knuutila and Jaakko Hintikka (eds.), Dordrecht: D. Reidel, 145–180.
* –––, 2004, “Philosophy of Language”, in J. Brower and K. Guilfoy (eds.) 2004, 126–157.
* King, Peter, 1982, _Peter Abailard and the Problem of Universals in the Twelfth Century_, Ph.D. Dissertation, Philosophy Department, Princeton University.
* –––, 1992, “Peter Abelard (1079–1142)”, in _The Dictionary of Literary Biography_ (Volume 115: Medieval Philosophers), Jeremiah Hackett (ed.), Detroit/London: Gale Research: 3–14.
* –––, 1995, “Abelard’s Intentionalist Ethics”, _The Modern Schoolman_, 72: 213–231. \[[Preprint available online](http://individual.utoronto.ca/pking/articles/Abelard\*on\*Ethics.pdf)].
* –––, 2004, “Metaphysics”, in _The Cambridge Companion to Abelard_, in J. Brower and K. Guilfoy (eds.), 65–125. \[[Preprint available online](http://individual.utoronto.ca/pking/articles/Abelard\*on\*Metaphysics.CC.pdf)].
* –––, 2007a, “Abelard on Mental Language”, _The American Catholic Philosophical Quarterly_, 81: 169–187.
* –––, 2007b, “Abelard’s Answer to Porphyry”, in _Documenti e studi sulla tradizione filosofica medievale_, 18: 249–70. \[[Preprint available online](http://individual.utoronto.ca/pking/articles/Abelard\*on\*Porphyry.pdf)].
* Kretzmann, Norman, 1982, “The Culmination of the Old Logic in Peter Abelard”, in _Renaissance and Renewal in the Twelfth Century_, R. L. Benson and J. Constable (eds.), Cambridge, MA: Harvard University Press, 488–511.
* Lenz, Martin, 2005, “Peculiar Perfection: Peter Abelard on Propositional Attitudes”, _Journal of the History of Philosophy_, 43: 377–386.
* –––, 2007, “Are Thoughts and Sentences Compositional? A Controversy between Abelard and a Pupil of Alberic on the Reconciliation of Ancient Theses on Mind and Language”, _Vivarium_, 45: 169–188.
* Lewis, Neil, 1987, “Determinate Truth in Abelard”, _Vivarium_, 25: 81–109.
* Luscombe, David, 1969, _The School of Peter Abelard_, Cambridge: Cambridge University Press.
* Mann, William, 2004, “Ethics”, in J. Brower and K. Guilfoy (eds.), 279–304.
* Marenbon, John, 1997, _The Philosophy of Peter Abelard_, Cambridge: Cambridge University Press.
* –––, 2006, “The Rediscovery of Peter Abelard’s Philosophy”, _Journal of the History of Philosophy_, 44: 331–351.
* –––, 2013, _Abelard in Four Dimensions: A twelfth-century philosopher in his context and ours_, Notre Dame: University of Notre Dame Press.
* Martin, Christopher J., 1986, “William’s Wonderful Machine”, _Journal of Philosophy_, 83: 564–572.
* –––, 1987, “Something Amazing About the Peripatetic of Le Pallet”, _Argumentation_, 1: 420–436.
* –––, 2001, “Abaelard on Modality: Some Possibilities and Some Puzzles”, in _Potentialität und Possibilität. Modalaussagen in der Geschichte der Metaphysik_, Thomas Buchheim, C. H. Kneepkens, and Kuno Lorenz (eds.), Stuttgart: Frommann Holzboog, 97–122
* –––, 2004, “Logic”, in J. Brower and K. Guilfoy (eds.), 158–199.
* Mews, Constant, 1987, “Aspects of the Evolution of Peter Abelard’s Thought on Signification and Predication”, in _Gilbert de Poitiers et ses contemporains_, J. Jolivet and A. de Libera (eds.), Naples: Bibliopolis.
* –––, 2005, _Abelard and Heloise_, New York: Oxford University Press.
* Pinziani, Roberto, 2003, _The Logical Grammar of Abelard_. Dordrecht: Kluwer Academic Publishers. (Translation of _La grammatica logica di Abelardo_, Parma 1992.)
* De Rijk, L. M., 1980, “The Semantical Impact of Abailard’s Solution of the Problem of Universals”, in _Petrus Abaelardus: Person, Wirk, und Wirkung_, Rudolf Thomas (ed.), Trier: Paulinus-Verlag, 139–152.
* –––, 1986, “Peter Abailard’s Semantics and his Doctrine of Being”, _Vivarium_, 24: 85–127.
* Tweedale, Martin, 1976, _Abailard on Universals_, Amsterdam: North-Holland.
* Wilks, Ian, 1993, _The Logic of Abelard’s Dialectica_, Ph.D. Dissertation, Philosophy Department, University of Toronto.
* –––, 1997, “The Role of Virtue Theory and Natural Law in Abelard’s Ethical Writings”, _Proceedings of the American Catholic Philosophical Association_, 71: 137–149.
* –––, 1998, “Peter Abelard and the Metaphysics of Essential Predication”, _Journal of the History of Philosophy_, 36: 356–385.
* –––, 2008, “Peter Abelard and his Contemporaries”, in _Handbook of the History of Logic_ (Volume 2: Medieval and Renaissance Logic), Dov Gabbay and John Woods (eds.), Amsterdam: Elsevier, 85–155.

## Academic Tools

| ![sep man icon](https://plato.stanford.edu/symbols/sepman-icon.jpg) | [How to cite this entry](https://plato.stanford.edu/cgi-bin/encyclopedia/archinfo.cgi?entry=abelard).                                                                      |
| ------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ![sep man icon](https://plato.stanford.edu/symbols/sepman-icon.jpg) | [Preview the PDF version of this entry](https://leibniz.stanford.edu/friends/preview/abelard/) at the [Friends of the SEP Society](https://leibniz.stanford.edu/friends/). |
| ![inpho icon](https://plato.stanford.edu/symbols/inpho.png)         | [Look up topics and thinkers related to this entry](https://www.inphoproject.org/entity?sep=abelard\&redirect=True) at the Internet Philosophy Ontology Project (InPhO).   |
| ![phil papers icon](https://plato.stanford.edu/symbols/pp.gif)      | [Enhanced bibliography for this entry](https://philpapers.org/sep/abelard/) at [PhilPapers](https://philpapers.org/), with links to its database.                          |

## Other Internet Resources

* [Pierre Abelard of Le Pallet](https://www.abelard.org/abelard/abel-hi.htm) (information and short biography)
* [Abelard’s Logic and the Origins of Nominalism](http://www.ontology.co/abelard.htm), by Raul Corazzon, which includes an annotated bibliography.
* [Peter Abelard](http://justus.anglican.org/resources/bio/142.html), by James E. Kiefer.
* [Prologue](http://www.fordham.edu/halsall/source/Abelard-SicetNon-Prologue.html) to Abelard’s _Sic et non_, by W.J. Lewis (and S. Barney), online at the Internet History Sourcebooks Project (Fordham).
* [First Lecture on Abelard](https://webarchive.nla.gov.au/wayback/20120503141653/http://pandora.nla.gov.au/pan/98441/20120504-0000/www.humanities.mq.edu.au/Ockham/x52t05.html) and [Second Lecture on Abelard](https://webarchive.nla.gov.au/wayback/20120503141653/http://pandora.nla.gov.au/pan/98441/20120504-0000/www.humanities.mq.edu.au/Ockham/x52t06.html), by R. J. Kilcullen (Politics and International Relations, Macquarie University).
* [Some older print editions and manuscripts](https://gallica.bnf.fr/accueil/?mode=desktop) (Gallica (gallica.bnf.fr), the website of the Bibliothèque nationale de France, now has images of several older print editions (including Cousin’s still valuable editions) and a few manuscripts, which in some cases are the only known surviving copies of those works)
* [Logicalia Medievalia](https://pric.unive.it/projects/logicalia-medievalia/home#c2459), website maintained by Caterina Tarlazzi (Ca’ Foscari University of Venice), which aims to draw together recent research on Latin logical texts before 1220 with particular attention to research on William of Champeaux and Peter Abelard.

## Related Entries

[Aristotle, General Topics: logic](https://plato.stanford.edu/entries/aristotle-logic/) | [mereology: medieval](https://plato.stanford.edu/entries/mereology-medieval/) | [relations: medieval theories of](https://plato.stanford.edu/entries/relations-medieval/) | [syllogism: medieval theories of](https://plato.stanford.edu/entries/medieval-syllogism/) | [universals: the medieval problem of](https://plato.stanford.edu/entries/universals-medieval/) | [William of Champeaux](https://plato.stanford.edu/entries/william-champeaux/)

[Copyright © 2022](https://plato.stanford.edu/info.html#c) by\
[Peter King](http://individual.utoronto.ca/pking/)\
[Andrew Arlig](http://www.brooklyn.cuny.edu/web/academics/faculty/faculty\*profile.jsp?faculty=641) <[_aarlig@brooklyn.cuny.edu_](mailto:aarlig%40brooklyn%2ecuny%2eedu)>
