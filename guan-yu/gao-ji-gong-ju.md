# 高级工具

## 搜索工具

以下是一些可帮助您快速在SEP中查找内容的搜索工具：

- 将“SEP搜索书签”添加到您的网络浏览器中（将图标放在书签栏中），这将允许您直接将搜索查询发送到我们的搜索引擎。要在Firefox、Safari和OmniWeb上安装，请简单地将以下链接拖放到您的书签栏中：

  > SEP搜索

  [针对Mac OS X Safari用户的说明：出于某种原因，当书签窗口处于打开状态时，此书签将无法使用。将此书签放在“书签菜单”文件夹或“书签栏”中可使其在不打开书签窗口的情况下被激活。]

- 适用于符合Open Search标准的浏览器（如Firefox、Mozilla、Internet Explorer）的搜索插件，允许用户在搜索栏中选择SEP作为要使用的搜索引擎。这是由Simon R. Ives编写的：

  > [下载搜索插件](http://mycroftproject.com/search-engines.html?author=Simon+Ives)

- 适用于Mac OS X/Dashboard.app的SEP搜索小部件，由Hakim Jonas Ghoula编写：

  > [下载（压缩的）SEP搜索小部件适用于Mac OS X/Dashboard](https://plato.stanford.edu/tools/SepSearchWidget.zip)
