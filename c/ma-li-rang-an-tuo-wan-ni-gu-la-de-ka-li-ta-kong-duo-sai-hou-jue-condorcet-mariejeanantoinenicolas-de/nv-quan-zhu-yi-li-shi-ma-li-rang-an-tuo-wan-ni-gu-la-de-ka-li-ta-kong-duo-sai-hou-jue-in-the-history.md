# 女权主义历史：马里·让·安托万·尼古拉·德·卡里塔，孔多塞侯爵 in the history of feminism (Joan Landes)

*首次发表于 2009 年 3 月 10 日；实质性修订于 2022 年 1 月 11 日*

孔多塞侯爵（1743 年 9 月 17 日至 1794 年 3 月 28 日）通常被称为最后的启蒙思想家之一，或者是早期社会科学的倡导者之一。作为人权的积极支持者，孔多塞从数学领域的首次成就转向公共服务，旨在将他所称的“社会算术”科学模型应用于社会和政治事务。通过教育和宪法改革，他希望创造一个自由、理性和民主的政体。他主张统计学和概率论的社会效用，并将数学计算应用于财政危机、医院护理改革、陪审团决策和投票程序。他最为人所铭记的是他在世后出版的最后著作《人类精神历史画像草图》（1794 年），在这部作品中，他诊断了人类进步的阶段，包括未来的发展。然而，较少人知道的是孔多塞对妇女权利的非凡倡导。在这方面，他甚至对于一个启蒙思想家来说也是异常的。

孔多塞侯爵在一篇 1790 年的论文中所称的“女性获得公民权利的承认”遭到了广泛反对，理由是女性具有独特的天性，非常适合履行她们的家庭职责。女性被认为不适合参与公共事务，因为据称她们更容易受到感官的影响，理性有缺陷，正义感较弱。在法国大革命期间，女性没有获得选举权，但她们受益于许多关于婚姻、离婚、继承以及未婚母亲及其子女法律地位的变革。然而，随着拿破仑统治期间许多这些改革被撤回或削减，她们最终遭受挫折。（关于革命记录，请参见 Traer 1980；Landes 1988, 1996；Fraisse 1994；Hunt 1992；Hufton 1999；Scott 1996；Verjus 2002；Desan 2004；Heuer 2005；Beckstrand 2009。）革命的另一个后果是，性别首次被引入作为拥有政治权利的宪法条件，尽管权利被宣称为普遍和不可剥夺的。与这种虚伪相反，孔多塞侯爵基于理性和正义坚称女性拥有平等的人性。尽管从未完全否认支持女性差异的有影响力的论点，孔多塞拒绝将此视为妨碍她们平等享有公民权利的障碍。他认为女性的局限，如果存在的话，不是由于她们的性别，而是由于她们较差的教育和环境。他意识到在反驳当时最根深蒂固的偏见之一时面临的风险，他恳求有机会与对手进行理性对话。

> 我希望任何攻击我的论点的人都能够不使用嘲讽或慷慨之词，最重要的是，有人能够向我展示一个自然的男女之间的差异，以此作为合理排除的基础。(孔多塞侯爵，1790 年，见麦克林和休伊特，1994 年，338-339 页)

* [ 政治背景](https://plato.stanford.edu/entries/histfem-condorcet/#PolCon)
* [生命与思想](https://plato.stanford.edu/entries/histfem-condorcet/#LifIde)
* [3. 婚姻与智识伙伴关系](https://plato.stanford.edu/entries/histfem-condorcet/#MarIntPar)
* [4. 妇女的权利](https://plato.stanford.edu/entries/histfem-condorcet/#RigWom)

  * [4.1 共和宪法中的妇女权利](https://plato.stanford.edu/entries/histfem-condorcet/#WomRigWitRepCon)
  * [4.2 对女性权利的大胆捍卫](https://plato.stanford.edu/entries/histfem-condorcet/#BolDefWomRig)
  * [4.3 评价孔多塞侯爵的立场](https://plato.stanford.edu/entries/histfem-condorcet/#AppConSta)
* [ 参考文献](https://plato.stanford.edu/entries/histfem-condorcet/#Bib)

  * [ 主要文献](https://plato.stanford.edu/entries/histfem-condorcet/#PriLit)
  * [ 次级文献](https://plato.stanford.edu/entries/histfem-condorcet/#SecLit)
  * [ 相关文献](https://plato.stanford.edu/entries/histfem-condorcet/#RelLit)
* [ 学术工具](https://plato.stanford.edu/entries/histfem-condorcet/#Aca)
* [其他互联网资源](https://plato.stanford.edu/entries/histfem-condorcet/#Oth)
* [ 相关条目](https://plato.stanford.edu/entries/histfem-condorcet/#Rel)

---

## 政治背景

孔多塞侯爵支持的争议性事业不仅仅是性别平等：在公开讨论妇女问题之前，他就激烈地主张非洲奴隶的人道主义和权利，并提议废除法国海外殖民地的奴隶制度。他于 1781 年著作《关于黑人奴隶制度的反思》（Réflexions sur l’esclavage des nègres）帮助激发了法国的废奴运动，该运动于 1788 年初在新成立的“黑人之友协会”（Société des Amis des Noirs）中汇聚，孔多塞侯爵于 1789 年 1 月成为该协会的主席，这是对具有影响力的亲种植园主的 Massiac 俱乐部的反对派。孔多塞侯爵在 1780 年代积极出版著作，并后来为国民议会起草了许多关于殖民地改革和奴隶贸易的立法提案。此外，他主张自由贸易、宗教少数派的权利和刑法改革。他认为同性恋和自杀不构成犯罪，因为它们“不侵犯其他人的权利”，不像强奸那样“侵犯了每个人对自己身体的所有权”（见《伏尔泰笔记[1789]》，收录于孔多塞侯爵的 O’Connor 和 Arago 1968 年版[原版 1847-9]第四卷，引自 McLean 和 Hewitt 1994 年，第 56 页）。他相信妇女有权规划自己的怀孕。他对女性教育的看法在当时尤为进步，他提议女孩应该与男孩一起在普遍的男女同校机构接受教育；他还主张为展现才华的妇女提供进入所有职业的机会。

康多塞侯爵是一位女权主义者、废奴主义者，在他的晚年成为一位民主共和派，他在公共生活中致力于扩大正义、道德和人权的主张。他是让·勒龙·达朗贝尔的朋友、门徒和盟友，是伏尔泰的门徒，是《百科全书》的合作者，是科学院的常任秘书，是法兰西学院和许多欧洲学术机构的成员，是著名的数学家，也是安妮-罗伯特-雅克·图尔戈和伏尔泰等当时许多知名知识分子的传记作者。他还积极参与政治事务领域——首先在图尔戈的短暂内阁（1774 年至 1776 年）下，然后在法国大革命期间。在法国大革命之前，他发表了关于将概率理论应用于普通选举、美国革命和制宪大会的论文；他还积极辩论，支持图尔戈试图改革经济和政治生活。他或许是唯一一位参与到狄德罗和达朗贝尔著名《百科全书》的贡献者中，活得足够长以参与法国大革命的人，帮助起草了 1789 年《人权和公民权宣言》。不幸的是，他也是法国大革命中最显赫的牺牲品之一：他参与了准备召开三级会议的工作，被选为 1791 年立法议会的代表，后来又当选为国民公会的代表，他撰写了一份关于公共教育的报告，并起草了一部体现他理想选举程序的法国宪法，但这部宪法从未被采纳。1793 年对康多塞侯爵来说是一个命运攸关的一年。1793 年，他因公开反对废除他起草的宪法，支持由日益主导的雅各宾派支持的匆忙准备的版本，对新闻审查和逮捕吉伦丹派的抗议以及他对 1793 年雅各宾派与 1791 年王党派的尖锐比较，而被国民公会下令通缉。在藏匿了八个月后，他写下了他未完成的《概述》（其中包括 1804 年首次发表的《新大西洋岛片段》或《人类为促进科学进步而做出的共同努力》），他逃离了巴黎，但于 1794 年 3 月 27 日被捕，并被关押在布尔格拉-雷讷，他于 1794 年 3 月 29 日在监狱牢房中被发现死亡，其死因至今仍不明。用他 19 世纪的一位崇拜者的华丽措辞来说：

> 在法国，那些杰出的思想家和作家们在两代人的时间里积极地播撒革命的种子，只有孔多塞侯爵幸存下来，目睹了第一次苦涩的收获。那些播下风的人已经不在了；只有他留下来看到风暴的收割，并迅速而残酷地被卷走。伏尔泰和狄德罗、卢梭和埃尔韦修斯都已消失，但孔多塞侯爵既参与了《百科全书》的编纂，又在国民公会中任职；他是那些栽种树木的人中唯一的杰出人物，也是在适当的时候来分享果实的人；既是先驱，又是实现者的共享者。 (Morley 1871, 37)

## 2. 生命与思想

孔多塞侯爵生于 1743 年 9 月 17 日，出生在皮卡第地区里本蒙-苏尔艾讷镇，父亲是已故的玛丽-玛德琳·高德里和她的丈夫孔多塞骑士安托万，一名骑兵上尉，他在儿子出生几周后的军事演习中丧生。孔多塞家族是来自多芬省的古老贵族家族。他的祖先亨利·德·卡里塔是 1561 年最早接受改革信仰的人之一，在 1598 年《南特敕令》正式容许之前。然而，在路易十四对胡格诺派的打压和 1685 年《南特敕令》的废除期间，家族中没有移民的成员被强制重新皈依罗马教会。在他出生时，像这个家族的其他男性继承人一样，人们本来期望孔多塞会在军队或教会中任职。在父亲不幸去世后，孔多塞被他虔诚的母亲独自抚养长大，母亲将他奉献给圣母保护，直到八岁时才穿上白色礼服。在他父亲的叔叔、传统派的里绍主教的怂恿下，孔多塞在九岁时开始接受耶稣会教师的正规教育；十一岁时，他在兰斯的耶稣会学校就读四年，取得了他的第一个学术成功，在十三岁时获得了第二名。

尽管他在学校取得了成功，但在后来的岁月里，他谴责了旧制度法国学院中竞争所起的作用；他与其他开明的批评者一道，强烈反对宗教对教育的控制。在他 1791 年关于公共教育的著作之一中，他强调了一种合作模式的教育，指出：在《公共人的图书馆》中。

> 人类的生活并不是竞争对手争夺奖品的斗争。它是兄弟们共同踏上的一段航程：每个人都为了整体的利益而发挥自己的力量，并通过相互仁爱的甜蜜、通过获得他人的感激或尊重而得到回报……相比之下，在我们的学院里颁发的冠冕——这些冠冕让学童相信自己已经是一个伟大的人——只会激起一种幼稚的虚荣心，而这种虚荣心是一个明智的教育体系应该设法保护我们免受其害的，如果不幸的话，它的根源不是我们的本性，而是我们犯错的制度。争夺第一名的习惯对那些被灌输这种观念的个人来说，要么是荒谬的，要么是不幸的。对于那些被命运迫使与他共同生活的人来说，这是一场真正的灾难。而追求值得尊重的需要，则导致内心的平静，这种平静是唯一能够带来幸福和使美德变得容易的。《论公共教育的性质和目的》（1791 年）孔多塞侯爵在贝克尔（1976 年）139-140 页。

他在耶稣会学校所经历的教条和体罚的混合同样令孔多塞侯爵感到震惊。在一份未发表的手稿中，他评论道：

> 他们教导孩子们，他们没有恩典就不能行善，而且有两种罪过：轻罪，为此你将被烧烤几个世纪，和重罪，为此你将永远被烧烤... 耻辱和侮辱是基督徒的自然状态。 （未发表的手稿，约 1778 年，巴丹特 1988 年引用，第 19 页；由麦克林和休伊特 1994 年翻译和引用，第 3 页）

他主张建立一个世俗国家；考虑到宗教观点是一个人的良心问题，一个人是唯一合法的裁判，“显然，维护这种崇拜的费用应该由那些相信它的人自愿承担”（孔多塞侯爵，《伏尔泰传》（1789 年），翻译并引用自罗文（1984 年），第 19 页）。早在 1774 年，毫无疑问是在 1770 年与著名的伏尔泰初次会面的影响下，他就在一部匿名作品中讨论了宗教不宽容的问题，这部作品经常被归因于伏尔泰本人。尽管他钦佩他年轻的追随者，伏尔泰（或许是恶作剧地）抱怨道：“这是一场可怕的战争宣言……我既不想得到写下它的荣耀，也不想承受随之而来的惩罚”（引自贝克（1976 年），x 页）。然而，孔多塞侯爵坚持捍卫一个更加世俗化的社会。1789 年后，他公开倡导宽容原则，反对宗教干涉新国家公立学校的做法。正如他所说，

> 宗教观念不能成为共同教育的一部分，因为它们必须是独立良心的选择。没有权威有权偏袒其中任何一种。（孔多塞侯爵，“公共教育”[1791]，见贝克尔 1976 年，127 页）

在 1758 年至 1760 年间，孔多塞侯爵在巴黎大学一部分的著名纳瓦尔学院继续他的伦理学、形而上学、逻辑学和数学研究，那里牛顿派的牧师让-安托万·诺莱特担任法国第一位实验物理学教授。在纳瓦尔学院哲学监督乔治·吉罗德·德·克鲁杜的指导下，孔多塞侯爵的数学和哲学才华得以展现。在纳瓦尔学院之后，他在里勃蒙逗留了两年，期间克服了家庭对他从事科学事业的反对意见，随后在巴黎与他的前任老师克鲁杜进行了一段时间的住宿，并继续研究积分微积分问题。他向法国科学院提交的第一篇正式论文被拒绝，尽管数学家亚历西斯-克洛德·克莱罗和亚历西斯·方丹认可了他的数学才能。在第二篇关于同一主题的论文在 1764 年被法国科学院接受后，他得到了让·勒龙·达朗贝尔和艾蒂安·贝祖的支持，发表了《积分微积分论文》（Essay on Integral Calculus），这使他在 1765 年的法国科学院年度科学史中获得了一席之地，尽管他当时还不是会员。天文学家约瑟夫-杰罗姆·德·拉朗德，柏林和法国科学院的成员，将当时年仅 21 岁的孔多塞侯爵评为欧洲十大数学家之一；他对积分微积分的进一步应用也给约瑟夫-路易·拉格朗日、莱昂哈德·欧拉和丹尼尔·贝努利等著名数学家留下了深刻印象。通过达朗贝尔，孔多塞侯爵获得了与伏尔泰的介绍，从此伏尔泰成为这位年轻人的另一位重要影响者；他开始参加朱莉（让娜·朱莉·埃莱奥诺尔）·德·莱斯皮纳斯的沙龙，这是当时法国哲学家的聚会场所。在那里，他结识并与法国经济学家兼政治家安妮-罗伯特-雅克·图尔戈（1727-1781）结为朋友，后者是重农主义经济理论和启蒙管理的倡导者，与伏尔泰和达朗贝尔一样，他在这位年轻数学家逐渐成为日益突出的公共知识分子的演变中发挥了重要作用。在莱斯皮纳斯的沙龙上，他还遇见并与另一位文学家、文学沙龙女主人阿梅丽·苏瓦尔结下了亲密友谊，她是出版商查尔斯-约瑟夫·潘库克的姐姐，也是学者让-巴蒂斯特·苏瓦尔的妻子。莱斯皮纳斯和苏瓦尔不仅鼓励这位年轻人的智力需求，而且似乎还就他们认为相当粗糙的心灵和社交礼仪向他提供建议（见巴丁特 1988）。在这个圈子里，他因为脾气暴躁但又非常害羞、社交不安和内向而赢得了声誉。莱斯皮纳斯称他为“被雪覆盖的火山”，而图尔戈则认为他是“疯狂的羊”，冷静但总是一触即发（威廉姆斯 2004 年，13 页）。

在专业领域，孔多塞侯爵在数学微积分科学方面取得了早期成功，导致他于 1769 年被任命为皇家科学院院士。1777 年，他成为科学院的常任秘书。1782 年，他被任命为法兰西学院院士，以表彰他对文学界的贡献。因此，正如基思·迈克尔·贝克所揭示的那样，

> 孔多塞侯爵在近二十年的时间里，不仅是法国有组织科学的主要发言人，而且（考虑到巴黎科学院的权力和声望... [以及他的永久秘书职位]）在整个欧洲也是如此。最重要的是，他被视为社会科学理论家，他在旧制度下发展的科学塑造了启蒙思想的一些基本假设。（Baker 1975, 385）

在 18 世纪 70 年代，孔多塞侯爵首次展现出自己是一位富有才华和激情的辩论家，旨在将公共管理转向公共利益，同时敏锐地意识到在实现后者方面权力和地位有多重要。正如他在写给当时是利摩日的皇家官员的图尔戈的一封早期信中所写道：“要做好事，就必须有足够的权力和善意”（引自 Baker 1976，x）。在图尔戈升任王室财政管理总管（实际上是财政部长）后，孔多塞侯爵自愿提供他的服务参与舆论战。他捍卫了图尔戈引入粮食自由贸易、废除行会和公司以及废除苛役的立场：苛役是与领主权利和王室特权相关的强制劳动制度或实物税收。他主张改革国家的度量衡制度，并参与了水力学实验，以确定运河建设的工程原理。与图尔戈的其他短暂改革一样，孔多塞侯爵也支持后者未成功的提议，即通过引入从地方到国家层面的一系列议会来改革法国的政治代表制度。这位部长在 1776 年 5 月突然失势，使他深感绝望：“这一事件已经改变了我的整个人生”，他从里贝蒙写信给伏尔泰说。“我再也不能在这美丽的乡村中感受到同样的快乐……我们已经跌得多么低，我亲爱的和杰出的导师，从如此高处跌落”（引自 Baker 1976，xii）。

孔多塞侯爵在发展自己关于通过选举和宪法修订实现政治正义的想法时，继续尊重图尔戈的改革努力，正如他在其重要著作《论宪法和省级议会的职能》（1788 年）中所表达的那样。在《图尔戈传》（1786 年）中，他赞美了他已故朋友对公共管理和自由贸易的贡献。此外，与他的导师一样，孔多塞侯爵拒绝将赤裸裸的自私行为视为人类行为的唯一动机，坚持爱和同情所起的作用。在有关图尔戈拒绝克洛德·阿德里安·埃尔韦修斯《论心灵》（1759 年）中未经修改的功利主义的通信中，孔多塞侯爵提出了自己关于同情和伦理的思考。

> 我刚刚收到了你对信仰的表白：这是我的信仰。当我离开大学时，我开始思考关于正义和美德的道德观念。我感到我看到我们对正义和美德的兴趣是源于一个敏感的个体意识到另一个个体所遭受的痛苦而产生的。从那时起，[出于对其他利益可能使我变坏（méchant）的恐惧]，我努力保持这种情感的自然能量。我放弃了我喜欢的打猎，甚至不会杀死一只昆虫，除非它非常有害。（摘自 McLean 和 Hewitt 1994 年，第 7 页）[11]

在 18 世纪 80 年代及早期的革命时期，孔多塞侯爵越来越致力于“le bien public” [公共利益]。例如，他关注了医疗保健问题，希望结束长期与贫困和基督慈善联系在一起的体制。在他 1786 年的《关于医院的备忘录》中，他建议关闭巴黎市一千年历史的市立医院——圣母酒店，并用由小型社区医院组成的世俗、本地化的市政卫生服务系统取而代之。他也不排斥改变自己早期制定的立场。例如，1793 年，他改变了自己 1788 年反对渐进税制的论点，转而支持相同观点（参见 Greenbaum 1984 和 Perkins 1984）。

然而，最重要的是他政治观点的演变，从支持改革的君主立宪制到捍卫民主共和制，从捍卫基于财产的选举权到普选权。他被选为选举会议代表他所在国家马恩特地区的贵族。虽然他没有在 1789 年革命爆发后的第一届国民议会中任职，但他被他所在居住地区选为第一次巴黎公社的大会代表。1791 年 10 月，他当选为国民立法议会议员，他在那里任职并主持了公共教育委员会。在同一年，他对未来充满乐观。他以笔名“Un Vieux Bramine” [一个老婆罗门] 在《La Bouche fer, Bulletin du Cercle Social》[铁嘴，社交圈子的公报] 中写道，他向其他自由之友发出宣言：

> 我相信人类是无限完善的，因此应致力于实现和平、自由和平等，其终点无法确定。我也相信这种进步必须是理性的工作，要有冥想的支持，要有经验的支持。根据这些原则，我的哲学必须是冷静和耐心的……我不会说“一切都好”，而是“一切都会好”，为此，我将得罪双方。（见罗宾内 1968 [原著 1893]，101）。

他不断发展的共和主义观点在 1791 年 6 月瓦伦努的国王逃跑并被捕后得到确认，随后在 1791 年 7 月 17 日发生的马尔斯广场上对和平示威者的袭击（要求废黜路易十六国王），由拉法耶特将军指挥的部队发动，拉法耶特是孔多塞侯爵的早期盟友，这一事件孔多塞视为个人攻击，因为他的妻子和婴儿女儿当天也在人群中。1792 年秋季，他当选为新成立的第一共和国国民公会的一员，他通过选举成为九人宪法委员会的成员之一，后来被任命为主席。在孔多塞看来，

> 基于平等的共和宪法是唯一符合自然、理性和正义的宪法：唯一能保护公民自由和人类尊严的宪法。 （Rosenblum 1984, 188; 有关《宪法项目》的文本，请参见康多塞侯爵、奥康纳和阿拉戈，第 12 卷，335-415 页）

然而，孔多塞侯爵的宪法计划被称为“吉伦丁派”，这是根据在该委员会任职的大多数吉伦丁派议员（尽管孔多塞本人不是吉伦丁派成员）。然而，这一计划从未被国民公会采纳。雅各宾宪法于 1793 年 6 月 24 日被国民公会接受，但从未实施。然而，孔多塞对后者的激烈抨击以及在《致法国公民，关于新宪法》中对自己观点的辩护导致了 1793 年 6 月 8 日对其逮捕的命令。

## 3. 婚姻与智识伙伴关系

1786 年，42 岁的孔多塞侯爵与 22 岁的 Sophie de Grouchy（1764-1822）结婚，他们建立了一段充满爱的关系，拥有相似的政治信念和坚实的智力伙伴关系。与丈夫一样，de Grouchy 致力于在法国推动重大的司法和政治改革；她在修道院的经历让她对教会怀有同样强烈的厌恶，并致力于世俗价值观。两人通过对三名农民受害者的辩护利益的共同关注结识，这三人因司法错误和法律滥用而受害，这一事业得到了 de Grouchy 的叔叔、波尔多议会议长查尔斯·杜帕蒂（Charles Dupaty）的支持（见 Perrod 1984）。除了经常合作撰写孔多塞的著作外，他的妻子还翻译了亚当·斯密和托马斯·潘恩的作品，并在孔多塞于 1775 年被任命为造币总检察官后的住所——蒙纳酒店（l’Hotel des Monnaies）举办了一场重要的沙龙。这个沙龙吸引了许多外国访客，包括托马斯·杰斐逊、本杰明·富兰克林的孙子、托马斯·潘恩、查尔斯·斯坦霍普（第三代斯坦霍普伯爵）和贝卡利亚侯爵（《犯罪与刑罚论》（1764）的作者，反对酷刑和死刑），以及作家皮埃尔-奥古斯特·卡伦·德·博马谢、剧作家和传单作者奥林普·德·古热（《妇女权利和女性公民宣言》的作者）以及作家和女主人斯塔埃尔夫人。在革命初期，孔多塞的沙龙是吉伦派追随者的重要场所，它举办了先前提到的社会圈的会议，这是革命俱乐部中最支持妇女参与和妇女权利的团体之一。1789 年后，孔多塞还经常出席与吉伦派有关的其他沙龙，如皮埃尔·维克图尼安·韦尔尼奥和夫人（玛丽·简）罗兰的沙龙（见 Williams 2004，13）。

康多塞夫人是一位有成就的翻译家和作家；她本身也分享了丈夫的自由主义和共和主义观点，特别是在刑事司法、政治改革以及少数群体和妇女权利等问题上。因为她在康多塞教授数学的中学里学习，其他人则教授历史和科学课程，康多塞夫人被称为“中学女神”。她特意学习英语，以便阅读原版并翻译亚当·斯密的《道德情感理论》第七版，该书于 1792 年在伦敦出版，正如她的许多同代人一样，她对现有的法语翻译感到不满。1791 年，康多塞夫妇与托马斯·潘恩一起创立了“共和社会”，有时被认为是法国第一个共和社会；康多塞夫人为该社会的期刊《共和者或代议政府的捍卫者》翻译了潘恩的著作。在流亡期间，他们相互分离，受到妻子的鼓励，哲学家开始撰写他的最后作品，而她也在创作被称为《给卡巴尼的同情书》的文本，其中她阐述了自己关于实现“幸福社会”的想法。正如她在第八封信中所写道，

> 谁能够不总是超越自然寻找新的享受或滥用其恩惠，而是每天都能在改变周围所有的责任和奴役关系为慈善、诚信和善意的关系中找到新的快乐的人在哪里呢？（Grouchy 1994, 183; 关于 Grouchy 对亚当·斯密的翻译、她对道德哲学的独立贡献以及她对孔多塞侯爵的思想影响的学术评价，请参见 Dawson 2004; Brown 2008; Grouchy 2010）

在孔多塞侯爵的最后一篇著作中，他向他的幼女（出生于 1790 年）写下了一份遗嘱，展现了他对妻子智慧和道德品质的极大尊重。“当正义的时刻到来时”，他写道，“她将在我的著作中找到帮助。我为她写下的建议，以及她母亲关于友谊的信件，将提供道德教育。她母亲的其他著作也提供了关于同一主题非常有用的观点”（见 1994 年 McLean 和 Hewitt 的《孔多塞的遗嘱（1794 年 3 月）》，第 290 页）。在他对女儿的建议中，他告诫她不要压抑对其他生物的感情，也反映了他妻子对感性的看法。他坚定地认为不应该让怨恨淹没灵魂与他人同情的自然倾向；在一段引人注目的文字中，他警告女孩，即使对动物的残忍行为也可能导致她原本善良天性的残忍化：“这种温和的敏感性”，他说道，

> 情感的源泉，可能是幸福的根源，起源于使我们分享所有有情众生悲伤的自然感情。保持它的纯净和力量。不要将其局限于人类的痛苦，甚至要将你的人性延伸至动物。不要让任何属于你的东西感到不快乐；不要忽视它们的福祉；不要对它们天真而真诚的感激心存麻木；不要给它们带来不必要的痛苦。任何此类行为都是真正的不公正，是对自然的侮辱，自然会以习惯性残忍所产生的冷酷惩罚你。动物缺乏远见是那条残忍的法律的唯一借口，这条法律将它们定罪为彼此的食物。让我们忠于自然，不要超越这个借口所允许的范围。 (McLean and Hewitt 1994, 287)

与此同时，他劝告他的女儿不要夸大自己的敏感性，因为那也可能是一个陷阱

> 我不会给你无用的建议，要避免激情，要小心过于敏感，但我会告诉你要对自己真诚，不要夸大自己的敏感，无论是为了虚荣心，欺骗自己的想象力，还是刺激他人的想象力。 (McLean and Hewitt 1994, 287)

他告诫她要“确保平等和正义的感觉成为你的第二天性”，并要求她的监护人抚养她

> 热爱自由和平等，拥有共和价值观和美德。确保她不怀个人复仇之情，并教导她保护自己免受敏感和冲动天性的危险。请代表我向她提出这个要求；告诉她我从未沦为这些事情的牺牲品。 (McLean and Hewitt 1994, 290).

也许这对夫妇最引人注目的地方在于他们如何在一起生活中最危险的时刻保持乐观。1793 年，尽管被迫分离和面临的危险，她致力于写信，他致力于他的《概述》，这两部作品表达了对人类进步和善良的坚定信念。正如 A. Pons（《序言，A. de G. 孔多塞侯爵 1994, 10》）所指出的，

> 孔多塞侯爵被追捕并被迫隐藏自己的时刻，他写下了他的进步赞歌，也就是他的《概述》。而苏菲在担心丈夫、女儿和自己的时刻，被雅各宾革命者和阶级成员的仇恨包围，指责她叛国，她却吹嘘同情心的甜蜜效果！

1795 年，孔多塞侯爵夫人在丈夫去世后出版了《Esquisse》，1799 年出版了他的《Éloges des academicians》[学术家的颂辞]；她在奥托伊的一个沙龙中重新开设了自己的沙龙，这个沙龙位于另一位沙龙女主人（安妮-卡特琳·德·利尼维尔）赫尔维修斯夫人（1722-1800）的前住所。在执政期间和帝国时期，孔多塞侯爵夫人主持了另一个沙龙，这是持不同政见的共和派人士的聚会场所。她努力捍卫孔多塞的声誉，并出版了他的全部作品，其中一部分于 1801 年至 1804 年间出版，得到了她的姐夫、法国著名医生和杰出的思想家皮埃尔·卡巴尼斯的协助，以及革命政治家和作家约瑟夫·加拉特，以及编目学家和图书馆馆长安托万·亚历山大·A.-A. 巴比耶的帮助。这个项目在她去世后继续进行，导致了 1847 年至 1849 年第二次汇编版的出版，由她的女儿伊丽莎（路易丝·亚历山德琳·德·孔多塞，生于 1790 年）和女婿、流亡的爱尔兰革命家亚瑟·奥康纳负责，得到了弗朗索瓦·阿拉戈的协助，后者是孔多塞的继任者，担任法兰西科学院（法国学院）的秘书。（第二版《Oeuvres completes》的第一卷包括了阿拉戈对哲学家的同情传记。A. 孔多塞·奥康纳和 F. 阿拉戈 1968 [原始出版 1847-9]。关于孔多塞的其他经典法国作品，请参见：Vial 1970 [原始出版 1902]；Cahen 1904；Alengry 1971 [原始出版 1904]；Koyré 1948；以及 E.和 R. 巴丹特尔撰写的有影响力的知识传记，1988 年）。

孔多塞侯爵与这位杰出的女性的合作无疑使他坚定了对女性能力的慷慨看法，加强了他对女性独立和自由的承诺；而且，可以肯定地说，这驱使他切实解决当时所有女性在一个据称更光明的时代中遭受的不公正待遇的需要。

## 4. 妇女的权利

孔多塞侯爵在女权问题上的最广泛论证出现在两篇论文中。第一篇是在 1787 年撰写的，革命之前；第二篇发表于 1790 年的《1789 年社会杂志》，是在新法国国家适当宪法安排的辩论背景下撰写的。此外，对女权的承诺贯穿于他写给女儿的遗嘱中，并且在《概述》中被称为《新大西洋碎片》的部分中也没有被遗忘，在这里他重申了他反对使用关于生理或智力劣势的指控来证明政治排斥的观点。在他 1791 年的《公共教育备忘录》中，他要求公共教育对男女开放，并且不应该排除妇女参与任何课程，包括科学。尽管 18 世纪被著名地描述为“女性时代”，在很大程度上是因为 19 世纪由冈库尔兄弟描绘，但大多数情况下，无论是启蒙思想家还是他们最具智力的女性同时代人（在法国，如兰贝尔夫人、舍蒂莱夫人、格拉菲尼夫人、里科博尼夫人、兰贝尔夫人、莱斯皮纳斯夫人和热尼丽夫人；在意大利，博洛尼亚大学数学教授弗朗索瓦·阿涅西和解剖学教授劳拉·巴西）都没有像孔多塞那样清晰、直接和有力地探讨女权问题。此外，关于自然人类平等的启蒙主张被关于男性和女性互补性属性的根深蒂固的假设所调和，这为现代时代妇女公民和政治权利的延迟扩展提供了支持。正如斯坦布吕格（1995 年，4）所提出的，18 世纪“是一个看到女性本性形象出现的时代，这种形象正好允许这些排斥被认为是‘自然的’”。因此，在“启蒙时代”，女权的捍卫者面临的问题是，对女性平等的否定是以世俗而非宗教考虑为基础，并得到伪科学主张的支持。

然而，孔多塞侯爵在支持妇女权利的同时从未放弃科学；他也没有将科学主张视为更大平等的障碍（安萨尔[2009]进一步提出他的女权主义与将数学应用于社会调查的开创性努力之间存在联系）。此外，他将自己的辩护基础放在了启蒙思想中的一种反思潮流上，这种潮流可以追溯到 17 世纪初，当时前神学家、笛卡尔的门徒拉巴尔挑战男性至上主义，主张性别和种族平等（参见拉巴尔 2002 年；斯图尔曼 2004 年）。与许多哲学家主导的哲学人类学相对立的是，拉巴尔和其他参与者提出了一种理性主义的替代方案，这种方案被称为“女权之争”，与标准的厌恶女性立场不同，它指向的不是身体而是头脑。孔多塞侯爵正是受到这种理性主义传统的影响。然而，鉴于启蒙思想圈内对妇女权利的讨论不足以及在 1789 年前夕法国（或其他地方）没有任何有组织的妇女权利运动，孔多塞对妇女权利的考虑尤为引人注目。正如这位哲学家深知的那样，人们还存在着他所称的舒适思维习惯，即将妇女视为二等公民。此外，孔多塞不仅扩展了拉巴尔早期的贡献，而且他在 1787 年和 1790 年的论点大胆地预见并强化了那些在革命期间敢于要求妇女权利的相对较少人士所持的立场，并最终以越来越多的人数，以及更成功地，在随后的几个世纪中提出了相同的要求（关于讨论妇女状况的启蒙男性思想家，请参见布鲁克斯 1980 年；兰德斯 1988 年；帕帕斯 1991 年；纳尔 2008 年和 2010 年）。

### 4.1 女性权利在共和宪法中

《纽黑文市民致维吉尼亚公民的信函》第二封信（1787 年）捍卫了单一立法机构的公正性，反对美国人偏好的两院制立法机构（McLean 和 Hewitt，1994 年，292-334 页）。在这里，孔多塞侯爵探讨了“和平、自由和持久宪法”的范围和限制，考虑到自然权利原则，他随后对其进行了定义：

> 这些权利被称为自然权利，因为它们源自人的本性；因为这是一个明显且必然的结论，即一个有理性和道德观念的有感知能力的存在必须享有这些权利，而且不能被公正地剥夺。（McLean and Hewitt 1994, 297）

从自然权利中，作为一个道德意识存在的每个人固有的权利，孔多塞侯爵推导出公民权利，包括“在共同利益事务上投票的权利，无论是亲自还是通过自由选举产生的代表”，以及共和政府的真正含义，其中公民的利益与总体利益相一致。因此，他认为，

> 一些居民，或至少一些地主被剥夺这些权利的国家不再是自由的...它不再是一个真正的共和国[而且]可以这样说，从来没有真正的共和国存在过。

然而，孔多塞侯爵在定义共和宪法的条件后，进一步讨论了性别歧视问题，指出如果政府要与理性和正义原则保持一致，那么就没有理由否认妇女享有平等权利

> 如果我们认为人类仅凭理性和道德观念的能力就拥有权利，那么女性应该拥有完全相同的权利。然而，在任何所谓的自由宪法中，女性从未拥有过公民权利。(McLean and Hewitt 1994, 297)

此外，他表示，如果

> 公民权利要求一个人能够根据自己的自由意志行事，[然后，在他看来]，任何确立足够男女之间不平等的民法，以至于后者被认为无法自由意志，只会增加不公正。(McLean and Hewitt 1994, 297–98)

基于自然权利，孔多塞侯爵立即着手处理政治代表问题，因为这关系到所有妇女以及已婚妇女的特殊情况。提到英国的“无代表不纳税”原则，这一原则在让-路易·德洛尔姆（Jean-Louis Delolme）于 1771 年所著的《英格兰宪法》中得到普及，孔多塞坚持认为“所有妇女有权拒绝支付议会征收的税款”；并补充说，他没有找到“针对这些观点的实质性论据，至少就寡妇和未婚妇女而言”（McLean 和 Hewitt，1994 年，297 页）。然而，虽然未婚妇女和寡妇可能被允许享有政治权利，但已婚妇女权利这个更加棘手的问题仍待解决。孔多塞直面这个问题，知道即使是他刚刚援引的英国法律也对已婚妇女持有强烈偏见。根据英国普通法，未婚成年人被视为法律上的独身女性（法律书中的法语术语），而已婚妇女则被视为有配偶的女性。口语中，丈夫和妻子在法律上被视为一人，而这个人就是丈夫。妇女被限制不能拥有财产、签署法律文件、订立合同、接受教育或保留自己的薪水，都需要得到丈夫的许可。正如历史学家多米尼克·戈迪诺（Dominique Godineau）（1998 年，xix 页）所指出的，在法国大革命前，妇女同样被定义为母亲和配偶，因此“被置于公共之外，因此也被置于城市之外”。她观察到，在《百科全书》中也是如此，狄德罗认为（男性的）公民这个词只适用于“妇女、幼儿或仆人，就像[指]公民家庭成员一样；他们并不真正是公民”。因此，在法国大革命前，政治个体代表家庭（包括家庭仆人，如果适用），而不仅仅代表自己。简言之，如果权利属于独立个体，那么根据传统和法律推理，妇女严格来说并不是法律上的个体。妇女的法律权利与丈夫（以及婚前的父亲）的权利合并在一起。在法律面前，已婚妇女毫无地位；因此，她不能被视为一个必须受到国家尊重其自然权利的公民。

关于谁有权代表家庭以及只有男性被授予这种特权的问题，孔多塞侯爵做出了一个简单的观察：如果婚姻是“两个人的社会”，它只允许一种情况下允许必要的不平等，即“在不同意见不能同时行动的罕见情况下需要有人拥有决定性投票权，同时需要迅速行动的需要意味着我们不能等待各方达成一致意见”（McLean 和 Hewitt，1994 年，298 页）。然而，即使在这些情况下，孔多塞侯爵坚持认为在配偶之间不需要引入永久的不平等。相反，他会分配和轮换这种特权，让“男性或女性在某些事项上拥有决定性投票权，其中一方更有可能表达符合理性的意愿”。他还提醒读者，配偶之间的更大平等“并不像我们想象的那样新颖”，因为罗马妇女被皇帝朱利安授予了对其配偶提起离婚诉讼的权利。在这一点上，孔多塞侯爵无法抑制自己的机智：“也许最不殷勤的凯撒对妇女最公正”，他提出（McLean 和 Hewitt，1994 年，298 页）。

正义原则要求“我们停止剥夺妇女公民权利”。那么，妇女是否有资格担任公职呢？在《第二封信》的这一部分中，孔多塞侯爵考虑了排除妇女作为选民和公职人员的理由，并准备了将在他 1790 年的论文中再次出现的论点。他首先提到效用原则：“只有在理由清楚证明其效用时，才应该进行法律排除”（McLean 和 Hewitt，1994 年，298 页）。他坚持认为，“一个好的选举方法”应该允许排除那些经过审判并被判有罪的人，以及两类受抚养人员，即家庭服务人员和未达到法定成年年龄的人。除了一个重要方面外，他对这些人类别排除的标准与 18 世纪共和国的假设一致，这些假设最终指导了 1791 年宪法的起草者，他们将人口分为积极和消极公民。只有前者——约占法国人口的 15%和男性的 61%（约 4,298,360 人）——拥有完整的公民权利。他们必须是 25 岁或以上的男性，至少在同一住所居住了一年，并支付相当于 3 天工资的费用。根据法国 1789 年后的第一部宪法，所有妇女被指定为消极公民的身份；法国妇女直到 1944 年才获得完整的公民权利。相比之下，即使在法国大革命之前，孔多塞侯爵也反对明确排除妇女，甚至是在军队或司法机构的职位。他不是寻求法律禁令，而是寻求民法和教育来支持妇女的参与。他强调了教育在对抗人们归因于妇女身体和智力局限的限制方面所起的作用，主张：

> 女性的体质意味着她们不适合成为士兵，并且在她们的一生中的某些阶段，使她们无法担任需要每天辛苦工作的职位。怀孕、分娩和哺乳会阻止她们履行这些职能。但我相信男性和女性之间的所有其他差异仅仅是教育的结果。 (McLean and Hewitt 1994, 299)

教育而非天性被认为是导致女性劣势及其被认为不适合特定社会角色和任务的原因。只有在某些有限的情况下，或者在某些时期，身体上的限制才是一个因素。孔多塞侯爵认为，女性在智力上并不逊色于男性；相反，她们是受到劣质教育的受害者。“即使我们同意女性可能仍然没有与男性相同的智力或体力，这只意味着最优秀的女性与第二优秀的男性相等，比第三优秀的男性更好，依此类推”（McLean 和 Hewitt，1994 年，299 页）。对于孔多塞侯爵认为伏尔泰对女性普遍开明的观点表示赞赏，但他仍然对后者对女性天才和创造力的保留持异议。他坚持认为，“如果职位只能由有发明能力的男性担任，那么许多职位甚至在学术界也将空缺。相反，对于许多职位，甚至对公众来说，牺牲一个天才男性的时间并不符合利益”。进一步引用两位文学女性的天才，塞维涅夫人和拉斐特夫人，他只承认女性可能在科学和哲学的最高领域不及男性——这是一个奇怪的承认，因为孔多塞侯爵在这方面可能会提到伏尔泰自己的情人，杰出的牛顿学派者夏特莱夫人的例子。

在《第二封信》中还有另外两处提到妇女的参考。在讨论可以称之为军事和外交事务的内容时，孔多塞侯爵将对妇女的不公正对待与共和国内部的被征服人口的处境进行了比较：在这种情况下，“他们无法将自由视为一种权利”（McLean 和 Hewitt，1994 年，312 页）。在另一段中，孔多塞侯爵总结并为自己辩护，反驳了有关他为妇女权利辩护所提出的论点微不足道的指控：“这个审查可能看起来很长”，他写道，“但我们正在讨论被所有立法者忽视的人类一半的权利。此外，展示如何克服对共和国唯一可能的反对意见，以及标明它们与那些不自由的国家之间的真正区别，对于男人的自由并无害处”。但值得注意的是，孔多塞担心嘲笑他的论点的不仅仅是男性听众：他还担心妇女的反应，因为她们被让-雅克·卢梭的引诱误导，使她们留在家庭领域（McLean 和 Hewitt，1994 年，299 页）。

> 即使是一位哲学家，在讨论女性问题时也很难不有些失态。然而，我担心如果她们阅读了这篇文章，我可能会惹上麻烦。我讨论的是她们的平等权利，而不是她们的影响力，因此可能会被怀疑暗地里希望减少这种影响力。由于卢梭通过说她们只是为了照顾我们而存在，只适合折磨我们而获得了她们的支持，我不指望得到她们的支持。但事实是一件好事，即使我因说出真相而让自己暴露于被嘲笑的风险中。（McLean 和 Hewitt，1994，299）

### 4.2 对女性权利的大胆辩护

与他 1787 年的作品形成鲜明对比，其中妇女权利是对公正宪法要素的更大考量的一部分，孔多塞侯爵于 1790 年撰写的论文《关于妇女获得公民权利的入场》（On the Admission of Women to the Rights of Citizenship）被视为整个女权主义思想史上最简洁而有力的妇女权利辩护之一。这也需要放在从 1789 年开始并导致新选举产生的国民议会于 1791 年通过宪法的重大事件背景下来看。根据 1791 年新君主立宪制宪法的规定，如前所述，人口根据财富被划分为积极公民和被动公民，从而排除了大多数男性公民参与完整的政治参与。复杂的选举规定进一步限制了民众的影响力。然而，新法律也将所有妇女（无一例外）归类为被动公民，这与旧制度法律实践有所不同，旧制度下妇女有时可以投票或担任摄政。除了禁止妇女参与投票程序或担任法官或当选代表外，只有男性被授予在新成立的民兵中服役的特权，这在这些年里成为共和国公民身份日益重要的职能。性别分歧在 1792 年君主制被废除，法国成为共和国后，如果说有什么变化，那就是男性积极公民和被动公民之间的分歧被消除，建立了普选权，但在法律之前并未采取任何措施改变妇女的次要政治地位。然而，从 1789 年革命开始之初，实际上妇女远非完全“被动”。许多妇女为权利而斗争，并积极参与革命公共领域的俱乐部和社团。

孔多塞侯爵并不是唯一认识到自然权利和理性原则与排除妇女享有完全政治权利之间明显矛盾的人。他也不是唯一将女性公民权问题与犹太人、新教徒和混血儿等其他群体要求权利联系起来的人，这些群体在旧政权下的政治和公民权利受到了限制，甚至与激进的奴隶解放要求联系在一起。一些政治活跃的妇女抗议改善妇女状况，甚至要求妇女拥有携带武器的权利。《真理之友联盟》是第一个接纳妇女为常规会员并建立独立妇女部门的俱乐部。该俱乐部倡导支持离婚，并就会员埃塔·帕姆·达尔德的演讲展开激烈辩论，呼吁男性全力关注妇女权利问题，坚持“我们是你们的伴侣，而不是你们的奴隶”（埃塔·帕姆·达尔德，《呼吁法国妇女关于道德重建和妇女在自由政府中影响力的必要性》）。帕姆和她的同伴们在妇女部门提出的其他问题包括废除长子继承权、保护妻子免受殴打、全面离婚法案以及妇女政治平等。帕姆构想了一个巴黎和全国范围内的附属俱乐部系统，以照顾和教育儿童，并为贫困妇女提供诊所。早在 1789 年 10 月，剧作家奥兰普·德·古日提出了一项改革方案，涵盖了法定性别平等、允许妇女从事所有职业以及通过国家提供的替代方案消除嫁妆制度。在她 1791 年的《妇女权利和女性公民宣言》中，古日欣喜于所有人类的自然平等，并谴责基于种族和性别否认人们应有的固有权利的虚伪行为。尽管始终是少数派立场，但在法国内外仍有其他声音为完全平等而发声，也许最著名的是玛丽·沃斯通克拉夫特在她同样雄辩的 1792 年著作《妇女权利辩护》中。

孔多塞侯爵在他 1790 年的论文中，对他那些“最开明”的同胞提出了强烈的控诉，尤其是那些哲学家和立法者，他们代表人权原则发言和立法，却否定这些权利属于人类一半的人

> 习惯可以使人们对其自然权利的侵犯如此熟悉，以至于那些失去这些权利的人既不会考虑抗议，也不认为自己受到不公正对待。甚至有些侵犯逃脱了那些热情地确立了人类共同权利并将其作为政治制度唯一基础的哲学家和立法者的注意。毫无疑问，通过剥夺妇女公民权利，他们都在违反平等权利的原则，从而平静地剥夺了人类一半的权利，即参与法律制定的权利。对于习惯对开明人的影响力，难道还有比他们为三四百名被荒谬偏见剥夺平等权利的男性辩护更强有力的证据吗？然而，却忘记了对于 1200 万妇女的情况。 (McLean and Hewitt 1994, 335)

孔多塞侯爵认为，如果不包括妇女，国家将无法诉诸于“自由宪法”。他认识到法国在将权利基础放在个人财产上发生的转变，指出“我们的领地选举集会将封建权利赋予了那些他们拒绝赋予自然权利的东西。我们的贵族代表中有几位能够坐在国民代表之间，这要归功于妇女。”他建议，与其剥夺女性封建财产所有者的旧权利，不如将所有女性财产所有者和家庭负责人扩展到男性现在已经获得的同等权利。“为什么，如果我们认为通过代理行使公民权利是荒谬的，我们应该剥夺妇女这种权利，而不是给予她们亲自行使的自由呢？”他回忆道，讽刺的是，在 1776 年之前，“一个女人可以统治法国[作为摄政王？]，然而，在 1776 年之前（当图尔戈废除行会时），她不能在巴黎成为一个裁缝[没有丈夫的帮助]”（McLean 和 Hewitt，1994 年，339 页）。他坚持说，除非能证明“妇女的自然权利与男性的完全相同”，否则新国家就构成了一种“暴政行为”的罪行（McLean 和 Hewitt，1994 年，337 页，335 页）。

在这一尖锐的指控之后，他继续以他在 1787 年讨论中已经提出的方式为妇女的权利辩护，尽管在这里他明确将妇女的权利与宗教和种族少数群体的权利联系在一起——毫无疑问，他希望理性的人会支持他的事业，即使奴隶主和天主教极端分子可能会反对

> 人的权利完全源自于他们是有感知能力的生物，能够获得道德观念并对其进行推理。由于女性具有相同的品质，她们必然也拥有相同的权利。要么人类没有任何真正的权利，要么他们都拥有相同的权利；任何投票反对他人权利的人，无论其宗教、肤色或性别如何，都自动放弃了自己的权利。 (McLean and Hewitt 1994, 335)

孔多塞侯爵再次被迫考虑有关女性身体社会影响的强烈观点。他认为，女性的职责和身体都不应该使她们失去参与公共领域的资格。他从琐碎的事情开始，逐渐转向更严重的反对意见，首先将母性的不便与“每月的不适”（如痛风和感冒）进行比较。更严肃的是，他观察到，“人们争论说，除了教育上的差异外，男性仍然比女性更聪明，但这还远未被证明，而且必须在女性可以公正地被剥夺天赋权利之前得到证实”（McLean 和 Hewitt，1994 年，335 页）。他再次重述了关于女性天才和天才与合法权利行使之间不恰当联系的旧论点：即使女性缺乏表现出的天才这一指控是真实的，他对此表示怀疑，“我们几乎不会试图将公民权利仅限于天才男性”（McLean 和 Hewitt，1994 年，335 页）。承认存在一小部分拥有真正天才的男性，他指出，“除了这个小类别，两性都拥有优劣思维的平等份额”，因此没有理由排除女性参与权利行使，就像没有理由排除绝大多数男性一样。他举例英格兰的伊丽莎白女王、奥地利的玛丽·特蕾莎和俄罗斯的两位叶卡捷琳娜，充分证明“女性既不缺乏思想力，也不缺乏坚定信念的勇气”。在一个富有趣味的段落中，他巧妙地揭穿了一大批据说伟大的男性所犯下的罪恶，问道像凯瑟琳·麦考利、玛丽·勒贾·德·古尔奈、玛丽-安娜·德·拉·特雷莫伊尔、乌尔桑斯公主、沙特莱夫人或兰贝尔夫人这样的女性是否会破坏良心自由或公民权利，攻击自由宪法，或通过诸如针对新教徒、偷窃仆人、走私者和黑人的“荒谬和野蛮的法律”？总之，他观察到，“当男性审视那些统治他们的人名单时，他们没有真正的理由感到自豪”（McLean 和 Hewitt，1994 年，336 页）。

孔多塞侯爵反对“女性从未根据所谓的理性行事”的说法，坚持认为“她们可能从未按照男性的理性行事；但她们确实按照自己的理性行事”（McLean 和 Hewitt，1994 年，336 页）。尽管承认女性可能有自己的理性，与男性不同，孔多塞仍然通过倡导提高教育标准、改善法律以及使两性所忍受的社会环境平等来应对同胞强烈反对女性理性不同或甚至根本不理性的可能性。他坚持认为，只有通过这些改革，女性才能摆脱目前所受的虚荣和自私的诱惑，并开始回应正义和积极法律的要求。至于两性之间可能仍然存在的任何残余差异，孔多塞在其中找到了一种可理解的逻辑：

> 她们（女性）基于不同原则并设定不同目标并不意味着她们是不理性的。一个女性关注她的面部魅力就像德摩斯特尼关注培养他的声音和手势一样是合理的。 （McLean 和 Hewitt，1994，336-7）

无论是关于令人钦佩还是鄙视的品质，孔多塞侯爵并不责怪女性的本性，而是指出她们的教养，认为这导致了她们的无知和迷信。由于女性被阻止行使真正的权力，她们不得不诉诸使用非法影响力。如果女性比男性更少自私和冷酷，更温和和敏感，他将这归因于她们的社会化以及她们过度被保护的生活。

> 他们对商业或任何由积极法律或严格正义原则决定的事务没有经验；他们关心并活跃的领域恰恰是那些由感情和自然体面所统治的领域。仅仅因为她们没有享有这些权利，就以仅仅因为她们没有享有这些权利而合理的理由继续拒绝妇女享有她们的自然权利是非常不公平的。 (McLean and Hewitt 1994, 337)

在对他所处时代的父权安排进行深刻观察时，孔多塞侯爵断言：如果女性受其丈夫过度影响，而她们又依赖于丈夫，这并不足以成为排除她们的理由，“因为我们可以废除这种暴虐的民法……[而且]一个不公正决不应成为犯另一个不公正的理由”（McLean and Hewitt 1994, 337）。

孔多塞侯爵认为，通过教育和宣传，可以消除皇室情妇和其他旧制度妇女所施加的秘密影响。他认为，无论妇女有何种影响力，如果这种影响力是秘密的，那么它就更具威胁性，而不是在公开辩论中行使（McLean 和 Hewitt，1994 年，337 页）。通过公开化，一个人对另一个人的不当影响必然会失去其影响力。与此同时，他指出，用来剥夺妇女权利的同样论点很容易被用来反对劳工——“任何被迫不断工作，因此既无法开明也无法行使理性的人。不久之后，公民权将只对已完成公法课程的男性开放……”他采纳了在革命期间迅速成为共和党人最具毁灭性口号的说法，指责“所有的贵族都是通过这种借口形成或被证明合理的”（McLean 和 Hewitt，1994 年，337 页）。当然，他并不是唯一一个将贵族指控与性别平等联系起来的人。例如，早在 1789 年，一份名为《女士们向国民议会的请愿》的激进传单出现，抗议议会所建立的“男性贵族”，并建议宣布“在整个法国，男性的所有特权都完全且不可撤销地废除；[而且]女性将永远享有与男性相同的自由、优势、权利和荣誉”（A. Soboul，1982 年；在 K. Offen，2000 年，54-55 页中引用和翻译）。同样，奥兰普·德·古日呼喊道：

> 人类，你能够公正吗？提出这个问题的是一个女人；至少你不会剥夺她这个权利。告诉我，是什么赋予你至高无上的权力来压迫我的性别？……人类独自将他特殊的情况提升为原则。在一个充满启蒙和智慧的世纪里，他变得古怪、盲目、自负科学并堕落为最愚昧，他想要像专制者一样统治一个完全掌握智力的性别；他假装享受革命，并声称要求平等权利，却不愿多说一句。（Gouges 1791）。

最后，孔多塞侯爵提出了对妇女自由最不妥协的反对意见，这些意见基于功利主义。如果妇女获得权利后会被诱使放弃家务事呢？如果女性公民会超越相对被动的权利行使，而执掌政府呢？孔多塞坚持认为，对功利的诉求经常被滥用，往往只是暴君拒绝“真正权利”的借口，导致诸如奴役非洲人、巴士底狱囚禁无辜者、审查新闻、以及在贸易和工业中剥削工人等罪行。然而，他也发现有必要超越仅仅回应功利主义对妇女平等的反对意见，以安抚男性的恐惧。因此，即使只是出于战术考虑，他最终不得不做出对他的对手具有决定性意义的让步。因此，他安抚男性，

> 不必害怕，仅仅因为妇女成为国民议会的成员，她们就会立即抛弃自己的孩子、家庭和针线活。事实上，这只会使她们更有能力抚养自己的孩子，培养他们成为男子。一个女人哺乳她的孩子、在他们年幼时照料他们是自然的。由于这个原因被迫呆在家里，比男人更虚弱，她过着更隐蔽、更家庭化的生活也是自然的。因此，妇女与需要每天工作几个小时的男人属于同一类别。这可能是不选举她们的一个理由，但不能成为法律排斥的基础。骑士精神可能会因这种变化而失去，但家庭生活将从这种平等中获益，正如从其他一切平等中获益一样。 (McLean and Hewitt 1994, 338)

### 4.3 评价孔多塞侯爵的立场

毫无疑问，孔多塞侯爵提出了一些代表当时时代最引人注目的有关妇女的主张，这些主张又是他对个人自由和社会平等的深刻承诺的一部分。在结束他对女性公民权的辩护时，他挑战批评者：“请告诉我一个自然的男女之间的差异，可以合理地作为排斥的基础”（McLean 和 Hewitt，1994 年，339 页）。然而，即使是这位理性和性别平等的倡导者也引入了男女之间的不对称性，并将这种不均衡直接定位在女性的生殖和母性身体上。尽管他反对自然差异的论点，孔多塞承认性别差异仍将在一个更合理组织的社会中继续产生社会影响。然而，他表明自己对让·雅克·卢梭和其他志同道合的改革者对更高阶层婴儿和幼儿使用奶妈这一广泛实践的批评以及这些改革者对雇佣奶妈的女性的虚荣和自我主义的抱怨有所认识（参见 Landes，1988 年，第 3 章）。因此，很可能孔多塞的安抚不仅是为了男性，也是为了女性。在当时，公众舆论明显倾向于反对任何女性自愿选择忽视她的家庭责任或完全否认这些责任，尤其是如果她出于社会或纯粹自私的原因这样做。尽管卢梭和其他人确实坚持要求父亲更多地参与和承担对家庭的责任，但这并不意味着要解除女性的主要家庭角色及其伴随的负担。如果有什么，改革者坚持要求父母双方在家庭生活中做更多事情，同时保持劳动的性别分工。

在他的辩护中，然而，很明显，直到康多塞侯爵生命的悲剧性终结，他从未放弃过这样一个观念：女性不仅可以，而且必须为自己的独立做职业准备。在他的遗嘱中，他建议他的幼女“养成工作的习惯，这样你就能自给自足，不需要外界帮助……即使你变得贫穷，也永远不会依赖他人”。而且这样的工作不应是例行公事或琐碎的：“选择一种工作，不仅仅是动动手，而是让头脑参与而不感到紧张；选择一些通过给予你快乐来补偿你努力的事情”（McLean 和 Hewitt，1994 年，284 页）。作为有头衔的贵族出身，对于一个女性的有偿就业的想法可能看起来不合适，甚至是奇怪的，这是非凡的建议。但康多塞侯爵坚持要求他的女儿为各种情况做好准备。在母亲和父亲的双重失去的悲惨前景中，他要求监护人为他的女儿做好比“通常的淑女技能”更多的准备，建议：“我希望我的女儿学会画画、绘画和雕刻得足够好，以便能够轻松或不感到厌恶地谋生。我希望她学会阅读和说英语”（McLean 和 Hewitt，1994 年，290 页）。同样，在他的《人类思维进展的草图》中的第十阶段，他大胆地肯定说

> 在对人类思想进步的原因中，对普遍幸福至关重要的因素之一，我们必须包括完全消除那些导致性别权利不平等的偏见，这种不平等甚至对其所支持的一方都是致命的。（孔多塞侯爵 1955 [原著 1795]，193）

总的来说，正如政治秩序中的暴政会使暴君和受害者一样丑陋一样，孔多塞侯爵相信，一旦人们接受了妇女的完全平等，他们将会过得更加幸福。

事实上，人们在孔多塞侯爵 1790 年的论文中发现的许多矛盾情绪，更多地属于那个时代，不仅仅是在当时的偏见方面——正如我们所看到的，他强烈反对这些偏见——而且还涉及到实现妇女权利机会被日益消失的程度，甚至是对那些被认为过于“公开”、不够谦逊和“家庭化”的妇女表现出的越来越消极甚至公开敌对的气氛。面对妇女在大众革命中日益增强的政治参与，那些仍然支持妇女政治平等的少数代表似乎已经收回了他们以前的支持。尽管孔多塞侯爵早期明显且公开承诺支持妇女选举权，但在 1793 年向公会提交宪法草案的开场报告时，他对这个问题的公开沉默仍然令人困惑。宪法辩论发生在取消被动选民和主动选民之间区别之后，因此否定妇女权利更明确地成为性别差异而不是财产或阶级地位的问题。此外，从 1792 年 9 月宣布共和国到 1793 年秋季公会代表们禁止妇女政治参与之前，大众妇女在街头和公会议事厅的活动加剧，但同时革命报纸也展开了一场针对妇女的运动。（关于这一时期，特别参见 Levy 等人 1979 年和 Godineau 1998 年）。

尽管孔多塞侯爵保持沉默，但他的一些朋友和政治盟友发表了看法。出生于威尔士、入籍法国的大卫·威廉姆斯（David Williams）——吉伦丹派的朋友，参与了索菲·孔多塞（Sophie de Condorcet）的沙龙活动，著有《政治自由书信》（Lettres sur la liberté politique），并参与了宪法的准备工作——写下了他的《关于最新宪法的观察》（Observations sur la dernière constitution）（1793 年在巴黎出版）。威廉姆斯支持妇女接受教育，支持她们在涉及同性别成员的案件中作证的权利，以及单身妇女、未婚女子和寡妇的政治权利。此外，代表皮埃尔·吉约马尔（Pierre Guyomar）提出的一项关于妇女选举权的呼吁，名为《平等权利的支持者和事实上的不平等》（le Partisan de l'égalité des droits et de l'inégalité en fait），在宪法起草委员会上进行了讨论。吉约马尔似乎借鉴了孔多塞的观点，将性别问题中的偏见与种族问题相提并论，并呼吁彻底废除这种偏见。与孔多塞（以及 1789 年《妇女请求》的作者）一样，他指出了国家的彻头彻尾的虚伪，其中权利宣言旨在延续男性的贵族制度并夹带旧制度的原则。然而，官方意见的压倒性力量并不支持将妇女纳入完全的公民身份。委员会于 1793 年 4 月得出结论，认为妇女缺乏足够的教育参与国家政治生活（参见 Roudinesco 1991, 129–130）。到 1793 年秋季，妇女也被禁止参加俱乐部和社团。法国妇女直到 1944 年才获得选举权，而 1790 年代通过的许多民法进步措施在拿破仑时期被废除，直到 20 世纪下半叶才再次得到充分保障。

在某种意义上，法国妇女的生活几乎长期以来完全被孔多塞侯爵抗议的那个机构所塑造。具有讽刺意味的是，这位慈爱的父亲和丈夫也许是唯一一个从未养过情妇的启蒙思想家，然而他可以说是

> 对于 18 世纪所知的家庭制度最为批判的人。教会赋予婚姻不可解除的特征在他看来是一种真正的种子，会滋生出通奸、卖淫和私生子等恶行。 (Rowe, in Rosenfield 1984, 25)

他主张控制生育，支持妇女合理规划怀孕，并主张男人在孩子出生后对孩子的福利负有责任。他设想了私生子的更美好未来，并支持未婚怀孕妇女有机会生育子女而不受社会惩罚（孔多塞侯爵，1968 年，VI: 256–9，VIII: 465–466; 威廉姆斯，2004 年，168–169; 沙皮罗，1963 年，192–193）。他提议对待妓女采取职业培训而非监禁，反对警察骚扰妓女和同性恋者，并谴责针对同性恋者的野蛮法律和做法，比如法国活活烧死同性恋者以及英国诉诸暴力行为（孔多塞侯爵，1968 年，VIII: 469–70，IV: 561; 沙皮罗，1963 年，192–195; 威廉姆斯，2004 年，170）。在《概述》中，他主张将婚姻定为一种民事而非宗教契约，正如他的同志们在 1790 年代法国正式实现的那样。他谴责父母在安排子女婚姻中的专制角色。他支持离婚，并认为在离婚事件中应保护子女的监护权和教育。孔多塞侯爵认为，在旧制度法国家庭生活的私人专制主义之处，以及对妇女扩大独立性的希望中，更多像他与妻子之间所建立的那样充满爱和平等的联合体将会出现。

> 一切能够促使个体更加独立的因素也会增加他们相互赋予幸福的能力；当个体行动更加自愿时，他们的幸福也会更加显著。 (Bibliothèque nationale ms. N.a. fr 4586, fol. 18, 引自并翻译自 Manuel 1962, 99)

如果他活得更久，他会看到许多失望，看着早期革命中立法改革在婚姻和个人生活方面，以及他为之奋斗的整个人权体系被撤销、修改或暂停，而政治参与在执政官和更强硬地在拿破仑统治下被镇压。在他去世后，孔多塞侯爵并没有被完全遗忘，他的贡献在 19 世纪的法国、英国和其他地方得到了尊重，这些男人和女人为了推翻由民主派和共和派强化的对妇女权利的障碍而奋斗，这些民主派和共和派在其他方面认为自己是前政权的自由主义对手。

## Bibliography

### Primary Literature

#### Works by Condorcet

* Baker, K. M., 1976, *Condorcet: Selected Writings*, Indianapolis: Bobbs Merrill.
* Badinter, E. (ed.), 1988, *Correspondance inédite de Condorcet et Madame Suard, M. Suard et Garat (1771–1791)*, Paris: Fayard.
* Condorcet, J.-A.-N., 1790a, “On giving Women the Right of Citizenship (1790)” in McLean and Hewitt 1994: 335–340.
* –––, 1790b, *The Life Of Voltaire: Translated From the French: In Two Volumes*. London: Robinson.
* –––, 1791, *Cinq mémoires sur l’instruction publique* , C. Coutel and C. Kintzler (eds.), Paris: Garnier-Flammarion, 1994.
* –––, 1795, *Sketch for a Historical Picture of the Progress of the Human Mind*, J. Barraclough (trans.), S. Hampshire (intro.), New York: Noonday Press, 1955 [.
* –––, *La mathématique sociale de Marquis de Condorcet*, G. G. Granger (ed.), Paris: Presses Universitaires de France, 1956.
* –––, *Écrits sur les États-Unis*, G. Ansart (ed.), Paris: Classiques Garnier, 2012.
* –––, *Writings on the United States*, edited, translated, with an introduction by G. Ansart, University Park: Penn State University Press, 2012.
* Condorcet, J.-A.-N., et al., *French Liberalism and Education in the Eighteenth Century; The Writings of La Chalotais, Turgot, Diderot, and Condorcet on National Education*, New York: McGraw-Hill, 1932.
* Condorcet, J.-A.-N. and F.-G. La Rochefoucauld-Liancourt, *Mémoires de Condorcet sur la révolution française: extraits de sa correspondance et de celles de ses amis*. Paris: Ponthieu, 1824.
* Condorcet O’Connor, A. and F. Arago (eds.), 1968 [orig. 1847–9], *Oeuvres complètes de Condorcet*, 12 vols, Paris: Didot; repr. Stuttgart: F. Frommann.
* Lukes, S. and N. Urbinati (eds.), 2012, *Condorcet: Political Writings* (Cambridge Texts in the History of Political Thought), Cambridge: Cambridge University Press.
* McLean, I. and F. Hewitt (eds.), 1994, *Condorcet: Foundations of Social Choice and Political Theory*, Aldershot, England and Brookfield, Vermont: Edward Elgar.
* Williams, D. (ed.), 2003, *Condorcet*: *Réflexions sur l’esclavage des nègres, et autres textes abolitionnistes*, Paris: Harmattan.

#### Works by Sophie de Grouchy, Marquise de Condorcet

* Grouchy, Marie-Louise-Sophie, marquise de Condorcet, 1994, *Lettres sur la sympathie, suivies des Lettres d’amour*, J.P. de Lagrave, (ed.), A. Pons (intro.), Montréal: Etincelle.
* –––, 2008, *Letters on Sympathy (1798): A Critical Edition*, K. Brown (ed.), J. E. McClellan III (trans.), *Transactions of the American Philosophical Society*, Volume 98, Part 4, Philadelphia: American Philosophical Society.
* –––, 2010, *Les ‘Lettres sur la sympathie’ (1798) de Sophie de Grouchy, marquise de Condorcet: philosophie morale et réforme sociale*, M. A. Bernier and D. Dawson (eds), SVEC 2010:08, Oxford: Voltaire Foundation, SVEC Collection.
* –––, 2019, *Sophie de Grouchy’s Letters on Sympathy: A Critical Engagement with Adam Smith’s The Theory of Sentiments*, S. Bergès, (ed. and trans.), S. Bergès and E. Schliesser (intro. and commentary), New York: Oxford University Press.
* Smith, A., 1798, *Théorie des sentimens moraux, ou Essai analytique sur les principes des jugements […] suivi d’une dissertation sur l’origine des langues*, 7th and last ed., M.-L.-S. de G. Condorcet (trans.); Elle y a joint huit lettres sur la sympathie, Paris: Buisson.

### Secondary Literature

#### Works on Condorcet

* Alengry, F., 1971 [orig. 1904], *Condorcet: Guide de la Révolution française, théoricien du droit constitutionnel et précurseur de la science sociale*. Geneva: Slatkine Reprints.
* Ansart, G., 2009, “Condorcet, Social Mathematics, and Women’s Rights”, *Eighteenth-Century Studies* 42(3): 347–62.
* Badinter, É. and R., 1988, *Condorcet (1743–1794): Un intellectuel en politique*, 2nd edition. Paris, Fayard.
* Baker, K. M., 1975, *Condorcet: From Natural Philosophy to Social Mathematics*. Chicago: University of Chicago Press.
* Brookes, B., 1980, “The Feminism of Condorcet and Sophie de Grouchy”, *Studies on Voltaire and the Eighteenth Century* 189, 297–362, Oxford: The Voltaire Foundation at the Taylor Institution.
* Buisson, F., 1929, *Condorcet*, Paris: Félix Alcan.
* Burlingame, A. E., 1930, *Condorcet: the Torch Bearer of the French Revolution*, Boston: Stratford.
* Cahen, L., 1904, *Condorcet et la Révolution française*, Paris: F. Alcan.
* Chouillet, A.-M. and P. Crépel*, 1997, Condorcet: homme des Lumières et de la Révolution*. Fontenay-aux-Roses: ENS éditions.
* Crépel, P. and C. Gilain, 1989, *Condorcet, mathématicien, économiste, philosophe, homme politique. Colloque international Condorcet*. [Paris]: Minerve.
* Didier, B., 1989, “Condorcet et les droits de la femme” in *Écrire la Révolution: 1789–1799*, Paris: Presses Universitaires de France.
* Fricheau, C., 1989, “La Femme dans la cite de l’Atlantide” in *Condorcet: mathématicien, eeconomiste, philosophe, homme politique*, ed. P. Crépel and C. Gilain, 1989.
* Greenbaum, L. S., 1984, “Condorcet’s *Mêmoire sur les Hôspitaux (1786*): An English Translation and Commentary” in “Condorcet’s ”Memoire sur les hôpitaux“ in L. C. Rosenfield, 1984, 83–98.
* Kintzler, C., 1984, *Condorcet: L’Instruction publique et la naissance du citoyen*. [Paris]: Le Sycomore.
* Koyré, A., 1948, ”Condorcet“, *Journal of the History of Ideas*, 9: 131–52.
* Manuel, F., 1962, *Prophets of Paris*, Cambridge, MA: Harvard University Press.
* Morley, J., 1871, ”Condorcet“ in *Critical Miscellanies*, London: Chapman and Hall.
* Nall, J., 2008, ”Condorcet’s Legacy Among the Philosophes and the Value of His Feminism for Today’s Man“, *Essays in the Philosophy of Humanism*, 16(1): 39–57.
* –––, 2010, ”Exhuming the History of Feminist Masculinity: Condorcet, 18th Century Radical Male Feminist“, *Culture, Society & Masculinities*, 2(1, Spring): 42–61.
* Pappas, J., 1991 ”Condorcet, ‘Le Seul’ et ‘Le Premier’ Féministe du 18 Siècle?“ *Dix-Huitième Siècle*, 23: 441–443.
* Perkins, J. A., 1984 Condorcet and Progressive Taxation: Theory and Practice” in L. C. Rosenfield, 1984, 99–113.
* Perrod, P. A., 1984, “Une Contribution de Condorcet à la Reforme de la Législation Pénale” in L. C. Rosenfield, 1984, 171–186.
* Robinet, J. F. E., 1968 [orig. 1893], *Condorcet, sa vie, son oeuvre, 1743–1794*. Geneva: Slatkine Reprints.
* Rosenfield, L. C. (ed.), 1984, *Condorcet Studies I*, Atlantic Highlands, N.J.: Humanities Press.
* Rosenblum, V. G., 1984, “Condorcet as Constitutional Draftsman: Dimensions of Substantive Commitment and Procedural Implementation” in L. C. Rosenfield, 1984, 187–206.
* Rothschild, E., 2001, *Economic Sentiments: Adam Smith, Condorcet, and the Enlightenment*. Cambridge, Mass: Harvard University Press.
* Rowe, C., 1984, “The Present-Day Relevance of Condorcet”, in L. C. Rosenfield, 1984, 15–34.
* Schandeler, J.-P., 2000 *Les Interprétations de Condorcet. Symboles et concepts (1794–1894)*, SVEC 2000:03, Oxford: Voltaire Foundation.
* Schapiro J. S., 1963, *Condorcet and the Rise of Liberalism*. New York: Octagon Books.
* Vial, F., 1970 [orig. 1902], *Condorcet et l’education démocratique*, Geneva: Slatkine reprints.
* Williams, D., 1976, “Condorcet, Feminism and the Egalitarian Principle”, *Studies in Eighteenth-Century Culture*, 5: 151–163.
* ––– (ed.), 1987, *Condorcet Studies II*, New York: Peter Lang.
* –––, 2004, *Condorcet and Modernity*, Cambridge: Cambridge University Press.
* –––, 2006, “Condorcet and the Abolition of Slavery”, in *Enlightenment and Emancipation*, Susan Manning and Peter France (eds.), Lewisburg: Bucknell University Press.

#### Works on Sophie de Grouchy, Marquise de Condorcet

* Albertone, M., 2003, *Una scuola per la rivoluzione, Condorcet e il dibattito sull’istruzione 1791–1792*, Naples.
* Arnold-Tétard, M., 2003, *Sophie de Grouchy, Marquise de Condorcet: La Dame de Cœur*, Paris: Christian.
* Boissel, T., 1988, *Sophie de Condorcet: Femme des Lumières (1764–1822)*, Paris: Renaissance.
* Dawson, D., 2004, “From Moral Philosophy to Public Policy: Sophie de Grouchy’s Translation and Critique of Smith’s *Theory of Moral Sentiments”* in *Scotland and France in the Enlightenment*, D. Dawson and P. Morère (eds.), Lewisburg, PA: Bucknell University Press.
* Forget, E. L., 2001, “Cultivating Sympathy: Sophie Condorcet’s Letters on Sympathy”, *Journal of the History of Economic Thought*, 23 (3): 319–337.
* LaGrave, J.-P. de, 2004, “Femmes des lumières: Sophie de Condorcet, légeerie du bonheur”, *Dix-huitième siècle*, 36: 87–97.
* Schliesser, E., 2019, “Sophie de Grouchy, Adam Smith, and the Politics of Sympathy” in *Feminist History of Philosophy: The Recovery and Evaluation of Women’s Philosophical Thought*, E. O’Neill and M.P. Lascano (eds.), Cham, Switzerland: Springer.

### Related Literature

* Alembert, Jean Le Rond d’, 1774, “Des Femmes”, in *Pensées de Monsieur D’Alembert*, Paris: Dufour, 34–38.
* Arrow, K. J., 1963 [orig. 1951], *Social Choice and Individual Values*, 2nd ed. New York: Wiley.
* Beckstrand, L., 2009, *Deviant women of the French Revolution and the Rise of Feminism*, Madison [N.J.]: Fairleigh Dickinson University Press.
* Black, D., 1958, *The Theory of Committees and Elections*, Cambridge: University Press.
* Desan, S., 2004, *The Family on Trial in Revolutionary France*, Berkeley: University of California Press.
* Diderot, D., 1772, “On women”, in *Dialogues*, Francis Birrell (trans.), London: George Routledge, 1927.
* Fauré, C., 1991, *Democracy Without Women: Feminism and the Rise of Liberal Individualism in France*, Claudia Gorbman and John Berks (trans.), Bloomington: Indiana University Press.
* Fraisse, G., 1994, *Reasons’s Muse: Sexual Difference and the Birth of Democracy*, Jane Marie Todd (trans.), Chicago: University of Chicago Press.
* Godineau, D., 1998, *The Women of Paris and Their French Revolution*, Katherine Streip (trans.), Berkeley: University of California Press.
* –––, 2003, *Les Femmes dans la Société française*, Paris: Armand Colin.
* Goncourt, E. and J. de, 1982, *La Femme au 18e siècle*, É. Badinter (ed.), Paris, Flammarion.
* Harth, E., 1992, *Cartesian Women: Versions and Subversions of Rational Discourse in the Old Regime*, Ithaca, N.Y.: Cornell University Press.
* Heuer, J. N., 2005, *The Family and the Nation: Gender and Citizenship in Revolutionary France, 1789–1830*, Ithaca, N.Y.: Cornell University Press.
* Higonnet, P., 1998, *Goodness Beyond Virtue: Jacobins During the French Revolution*, Cambridge, Mass.: Harvard University Press.
* Hufton, O., 1999, *Women and the Limits of Citizenship in the French Revolution*, Toronto: University of Toronto Press.
* Hunt, L., 1992, *The Family Romance of the French Revolution*, Berkeley: University of California Press.
* Jordan, W. D., 1968, *White over Black: American Attitudes towards the Negro, 1550–1812*, Chapel Hill: University of North Carolina Press.
* Kates, G., 1985, *The Cercle Social, the Girondins, and the French Revolution*, Princeton, N.J.: Princeton University Press.
* Landes, J. B., 1988, *Women and the Public Sphere in the Age of the French Revolution*, Ithaca, N.Y.: Cornell University Press.
* –––, 1996, “The Performance of Citizenship: Democracy, Gender and Difference in the French Revolution”, in *Democracy and Difference*, Seyla Benhabib (ed.), Princeton, NJ: Princeton University Press.
* Levy, D. G., H. B. Applewhite, and M. D. Johnson (eds.), (trans.), 1979, *Women in Revolutionary Paris, 1789–1795, Selected Documents*, Urbana: University of Illinois Press.
* Moses, C. G., 1984, *French Feminism in the Nineteenth Century*, Albany, State University of New York Press.
* Offen, K., 2000, *European Feminisms, 1700–1950: A Political History*, Stanford, CA: Stanford University Press.
* Porter, R., 2001, *The Enlightenment*, 2nd ed., New York: Palgrave.
* Poulain de la Barre, F., 2002, *Three Cartesian Feminist Treatises*, Vivien Bosley (trans.), Chicago: University of Chicago Press.
* Roudinesco, É., 1991, *Theroigne de Mericourt: A Melancholic Woman during the French Revolution*, Martin Thom (trans.), London: Verso.
* Schapiro, J. S., 1963, *Condorcet and the Rise of Liberalism*, New York: Octagon.
* Schiebinger, L., 1991, *The Mind Has No Sex: Women in the Origins of Modern Science*, Cambridge, MA: Harvard University Press.
* Silvestre de Sacy, J., 1953, *Le Comte d’Angiviller: dernier directeur général des bâtiments du roi*, Paris: Plon.
* Scott, J. W., 1996, *Only Paradoxes to Offer: French Feminists and the Rights of Man*, Cambridge, MA: Harvard University Press.
* Soboul, A. (ed.), 1982, *Les Femmes dans la Révolution française*, 1789–1794, Volume 1, Number 19, Paris: EDHIS.
* Spencer, S.I., 1984, *French Women and the Age of Enlightenment*, Bloomington: Indiana University Press.
* Steinbrügge, L., 1995, *The Moral Sex: Woman’s Nature in the French Enlightenment*, P. E. Selwyn (trans.), New York, Oxford: Oxford University Press.
* Stuurman, S., 2004, *François Poulain de la Barre and the Invention of Modern Equality*, Cambridge, MA: Harvard University Press.
* Traer, J. S., 1980, *Marriage and the Family in Eighteenth-Century France*, Ithaca, NY: Cornell University Press.
* Trouille, M., 1994, “Sexual/Textual Politics in the Enlightenment: Diderot and d’Epinay Respond to Thomas’s Essay on Women,” *Romanic Review*, 85 (2): 191–210.
* Verjus, A., 2002, *Le cens de la famille: les femmes et le vote, 1789–1848*, Paris: Belin.

## Academic Tools

> | ![sep man icon](https://plato.stanford.edu/symbols/sepman-icon.jpg) | [How to cite this entry](https://plato.stanford.edu/cgi-bin/encyclopedia/archinfo.cgi?entry=histfem-condorcet). |
> | --- | --- |
> | ![sep man icon](https://plato.stanford.edu/symbols/sepman-icon.jpg) | [Preview the PDF version of this entry](https://leibniz.stanford.edu/friends/preview/histfem-condorcet/) at the [Friends of the SEP Society](https://leibniz.stanford.edu/friends/). |
> | ![inpho icon](https://plato.stanford.edu/symbols/inpho.png) | [Look up topics and thinkers related to this entry](https://www.inphoproject.org/entity?sep=histfem-condorcet&redirect=True) at the Internet Philosophy Ontology Project (InPhO). |
> | ![phil papers icon](https://plato.stanford.edu/symbols/pp.gif) | [Enhanced bibliography for this entry](https://philpapers.org/sep/histfem-condorcet/) at [PhilPapers](https://philpapers.org/), with links to its database. |

## Other Internet Resources

### Works by Condorcet Online

* Condorcet, 1791, [*Cinq mémoires sur l’instruction publique*](http://classiques.uqac.ca/classiques/condorcet/cinq_memoires_instruction/cinq_memoires.html), hosted at Les classiques des sciences sociales (Université du Québec)
* Condorcet, Year 6 (1798), URL: [*Esquisse d’un tableau historique des progrès de l’esprit humain*](http://socserv.mcmaster.ca/~econ/ugcm/3ll3/condorcet/cindex1.htm), 4th edition, Paris: Agasse (hosted at U. McMaster).
* Condorcet, 1789, [*Idées sur le despotisme, à l’usage de ceux qui prononcent ce mot sans l’entendre*](http://gallica.bnf.fr/ark:/12148/bpt6k41724z), extract from *Oeuvres de Condorcet*, vol. 9, A. Condorcet O’Connor and M.F. Arago (eds.), Paris: Didot, 1847.
* Marie-Jean-Antoine-Nicolas Caritat, Marquis de Condorcet, (1796), [1795], [*Outlines of an historical view of the progress of the human mind*](http://oll.libertyfund.org/index.php?option=com_staticxt&staticfile=show.php%3Ftitle=1669&Itemid=28), a posthumous work of the late M. de Condorcet. (Translated from the French), in The Online Library of Liberty.
* Condorcet, 1781, [*Reflexions sur l’esclavage des nègres*](http://gallica.bnf.fr/ark:/12148/bpt6k823018), by M. Schwartz, pastor at Bienne.
* Condorcet, 1790, [Sur l’admission des femmes au droit au cité](http://oll.libertyfund.org/index.php?option=com_staticxt&staticfile=show.php%3Ftitle=1014&Itemid=99999999), Journal de la Société de 1789, 3 juillet 1790, no. V.

### Web Resources on Condorcet

* [Marie Jean Antoine Nicolas Caritat, marquis de Condorcet](http://agora.qc.ca/mot.nsf/Dossiers/Marie_Jean_Antoine_Nicolas_Caritat_marquis_de_Condorcet), Encyclopedia entry, in French (L’Encyclopédie de L’Agora).
* [Condorcet, l’instruction et la cité](http://www.mezetulle.net/article-1560495.html), blog entry by Catherine Kintzler, 3 parts and references.
* [Condorcet: le fondateur des systèmes scolaires modernes](http://classiques.uqac.ca/contemporains/massot_alain/condorcet/condorcet.html), text by Alain Massot, presented to the 70e Congrès de l’Acfas, Histoire, Université Laval, QUÉBEC, 16 May 2002.
* [Condorcet, un mathématicien engagé](http://pagesperso-orange.fr/fabien.besnard/condorcet.htm), by Fabien Besnard, EPF école d’ingénieurs, Sceaux.
* [*De Condorcet à Gramsci: un élan à rénouveler*](http://classiques.uqac.ca/contemporains/fossaert_robert/de_condorcet_a_gramsci/de_Condorcet.html), text by Robert Fossaert, Paris, 1996.

### Related Web Resources

* Diderot, Denis, 1772, “[Sur les Femmes](https://fr.wikisource.org/wiki/Sur_les_femmes)”, in *Oeuvres complètes de Diderot*, vol. ii: 251–262, edited by J. Assézat and M. Tourneux, Garnier.
* Dupont-Chatelain, M., 1911, *[Les encyclopédistes et les femme: Diderot–D’Alembert–Grimm–Helvetius–D’Holbach–Rousseau–Voltaire](https://archive.org/details/lesencyclopdis00dupo)*, Paris: H. Daragon.
* Fauré, C., 2008, “[A nom de l’égalité entre femmes et hommes-dix-huitième siècle](https://halshs.archives-ouvertes.fr/halshs-00274877/document)”.
* [International Society for Eighteenth Century Studies](http://www.c18.org/).
* [*Declaration of the Rights of Woman*](https://revolution.chnm.org/d/293/), by Olympe De Gouges, 1791.
* [The Partisan of Political Equality between Individuals](http://chnm.gmu.edu/revolution/d/597/), Excerpt of a pamphlet by Pierre Guyomar (written in April 1793), from *The French Revolution and Human Rights: A Brief Documentary History*, translated, edited, and with an introduction by Lynn Hunt (Bedford/St. Martin’s: Boston/New York), 1996, 133–135; reproduced at Center for History and New Media, George Mason University.
* [*Les Femmes de la Révolution*](http://www.gutenberg.org/etext/18738), by Jules Michelet (1798–1874), ebook at Project Gutenberg.
* Paine, Thomas, 1775, “[An occasional letter on the female sex](https://teachingamericanhistory.org/document/an-occasional-letter-on-the-female-sex/)”.
* [*A Vindication of the Rights of Woman, with Strictures on Political and Moral Subjects*](http://www.bartleby.com/144/), by Mary Wollstonecraft (Boston: Peter Edes, 1792), hosted at Great Books Online Series at bartleby.com.

## Related Entries

[Diderot, Denis](https://plato.stanford.edu/entries/diderot/) | [feminist philosophy](https://plato.stanford.edu/entries/feminist-philosophy/) | [feminist philosophy, interventions: liberal feminism](https://plato.stanford.edu/entries/feminism-liberal/) | [feminist philosophy, interventions: political philosophy](https://plato.stanford.edu/entries/feminism-political/) | [liberalism](https://plato.stanford.edu/entries/liberalism/) | [Montesquieu, Charles-Louis de Secondat, Baron de](https://plato.stanford.edu/entries/montesquieu/) | [republicanism](https://plato.stanford.edu/entries/republicanism/) | [rights: human](https://plato.stanford.edu/entries/rights-human/) | [Rousseau, Jean Jacques](https://plato.stanford.edu/entries/rousseau/) | [Voltaire](https://plato.stanford.edu/entries/voltaire/) | [Wollstonecraft, Mary](https://plato.stanford.edu/entries/wollstonecraft/)

[Copyright © 2022](https://plato.stanford.edu/info.html#c) by  
Joan Landes <[*joanlandes@mac.com*](mailto:joanlandes%40mac%2ecom)>
