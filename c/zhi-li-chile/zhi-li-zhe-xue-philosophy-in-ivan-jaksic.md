# 智利哲学 philosophy in (Ivan Jaksic)

_首次发表于 2015 年 4 月 2 日星期四；实质性修订于 2020 年 3 月 31 日星期二_

智利的哲学，就像拉丁美洲其他地区一样，自从 16 世纪初西班牙和葡萄牙征服和定居新大陆以来，一直是一项学术追求。在整个殖民时期，很少有哲学家冒险走出学术或宗教机构的围墙（通常是同一所机构），这段时期在大多数西班牙殖民地国家结束（古巴和波多黎各除外）的早 19 世纪。即使在独立之后，哲学仍然是一项基本的学术事业。与该地区其他国家相比，智利表现出了显著的制度连续性，除了 20 世纪最后一个季度的军事统治时期。

然而，学科发展的学术背景并没有阻止哲学家参与社会和政治活动，尤其是关于新兴国家的历史、文化和发展方向的辩论。自早期共和国以来，哲学家一直是重要的政治人物，也是在该国现代史上进行的大学改革过程中的领导者。在军事统治时期（1973 年至 1990 年），一些人成为政权的重要反对者。事实上，对政治的关注主导了哲学家的议程，包括那些谴责政治侵入学科的人。

政治的中心地位，即使在直接参与其中的情况下，也没有让哲学家们忘记他们对探索不同哲学（主要是欧洲的）学派、学科内的一些特定领域（如逻辑和形而上学）以及一些主题性关注（如宗教在世俗国家中的角色、现代性对自我和社会的影响以及大学在国家建设中的作用）的基本承诺。智利哲学以学术追求与对政治和社会进行更紧密的哲学参与要求之间的持续紧张关系为特征。%%

* [1. 殖民背景](https://plato.stanford.edu/entries/philosophy-chile/#ColBac)
* [2. 独立与国家认同：形成时期](https://plato.stanford.edu/entries/philosophy-chile/#IndNatForYea)
* [3. 实证主义时代](https://plato.stanford.edu/entries/philosophy-chile/#EraPos)
* [4. 反对实证主义的反应.](https://plato.stanford.edu/entries/philosophy-chile/#ReaAgaPos)
* [5. 哲学专业化的接受与批评](https://plato.stanford.edu/entries/philosophy-chile/#RecCriPhiPro)
* [6. 军事统治时期](https://plato.stanford.edu/entries/philosophy-chile/#PerMilRul)
* [参考文献](https://plato.stanford.edu/entries/philosophy-chile/#Bib)
* [主要来源](https://plato.stanford.edu/entries/philosophy-chile/#PriSou)
* [次要来源](https://plato.stanford.edu/entries/philosophy-chile/#SecSou)
* [学术工具](https://plato.stanford.edu/entries/philosophy-chile/#Aca)
* [其他互联网资源](https://plato.stanford.edu/entries/philosophy-chile/#Oth)
* [相关条目](https://plato.stanford.edu/entries/philosophy-chile/#Rel)

***

## 1. 殖民背景

尽管在 1818 年 2 月智利这个前殖民地从西班牙宣布独立之前，我们不能恰当地谈论“智利哲学”，但忽视西班牙殖民时期几位哲学家的活动也是不合适的。哲学活动首次出现在 1595 年圣地亚哥的多明我会修道院，随后迅速传播到十七世纪的其他中学，并随着 1758 年创建的圣菲利普皇家大学而逐渐发展壮大。在这段时间内，以及直至十九世纪初，著名学者包括神职人员阿隆索·布里塞诺、米格尔·德·维尼亚斯、曼努埃尔·奥瓦列和曼努埃尔·安东尼奥·塔拉维拉，他们大多在学术传统内撰写了重要的哲学论著。1767 年耶稣会士被驱逐意味着包括米格尔·德·奥利瓦雷斯、费利佩·戈麦斯·德·维多尔、胡安·伊格纳西奥·莫利纳和曼努埃尔·拉昆萨在内的几位才华横溢的作者在其他地方出版了他们的作品。这两种传统之间的主要区别在于前者将哲学培育为解决神学问题的工具，而后者更倾向于实际关注，比如理解新世界的条件，因此对神学问题不太感兴趣。在晚殖民时期涵盖了一些反映启蒙运动关切的哲学主题的世俗作者，包括曼努埃尔·德·萨拉斯和胡安·埃加纳，他们成为独立后时期的桥梁。

但是，智利作为一个独立共和国的建立，智利哲学开始在高等教育机构中以系统的方式培育，这些机构肩负着明确的国家使命。五个主要时期刻画了这一学科的历史：从独立到 1865 年的形成时期；从 1865 年到 1920 年的实证主义的兴起和衰落；从 1920 年到 1950 年的反实证主义反应；从 1950 年到 1973 年的专业化的兴起和批评；以及从 1973 年到 1990 年的军事统治下哲学的命运。这些时期大致与该地区其他国家哲学发展的主要阶段相吻合。自 1990 年以来，智利恢复民主统治，追踪和定义这一时期要困难得多。然而，该学科的主要基础并没有发生实质性的变化。

## 2. 独立与国家主权：形成时期

在独立后的哲学家们，直至 19 世纪 60 年代实证主义的到来，不得不面对教会与国家之间关系的复杂性。智利人民之间的独立斗争是智利人民对抗任意和不稳定的殖民统治，而不是对抗天主教会。相反，教会被视为建立新共和国制度中不可或缺的盟友。因此，19 世纪所有智利宪法文件都宣称天主教是国家的官方宗教。因此，包括教育机构在内的公共机构必须遵守天主教义。由于大多数哲学家是信徒，这种限制并不一定意味着问题。但作为哲学家，他们被迫审查那些世俗的或甚至对天主教教义敌对的学说和学派。在这种情况下，他们讨论了这些学说，但压制了那些可能被视为超出天主教会庇护范围的内容。因此，像大卫·休谟这样的思想家可能会被评论，但最终却因其怀疑论而受到严厉批评。法国的_唯心论_这样的学派可以被讨论，但再次剥夺了其最唯物主义的边缘。哲学作品的出口主要是中学教科书，这些教科书必须经过适当的政府机制批准，这通常意味着应用天主教会的标准来确定哲学上的可容许性和神学上的可接受性。没有明显反天主教的小册子，比如弗朗西斯科·毕尔巴奥的《智利社会性》（1844 年，1866 年汇编），这些小册子最终因亵渎和不道德而被焚烧。

哲学家们仍然找到了忠于他们学科的方法，同时也认识到天主教作为国家官方宗教所施加的限制。也许最好的例子是安德烈斯·贝略（1781–1865）的作品，他探索了广泛的哲学来源，特别是苏格兰启蒙运动的那些。然而，他从未质疑宗教的重要性，尤其是天主教。他的《理解的哲学》于 1881 年在他去世后出版，但这部作品的重要部分在 19 世纪 40 年代就已出现。

贝略的《哲学》展示了他对托马斯·里德、托马斯·布朗和达格尔德·斯图尔特的作品有重要的了解和很多的认同。贝略在这部作品中的主要重点是思想的获取，他认为这是该学科的核心努力。他的作品分为两部分，即心理学和逻辑学。对于贝略来说，哲学的任务是适当理解思想的起源，并引导人类的行动。在这个过程中，贝略认真地涉及了约翰·洛克和乔治·伯克利的思想，尽管并非毫无批判地。贝略哲学作品的影响起初有限，但在 20 世纪迅速传播到该地区的其他国家，并成为拉丁美洲哲学家的作品中少数几部被完全翻译成英文的作品之一（1984 年）。

然而，也许最重要的，从智利最广泛使用的哲学文本的意义上来看，是拉蒙·布里塞诺（1814-1910）的《现代哲学课程》，于 1845-1846 年分两卷出版。与贝略的贡献相比，布里塞诺的作品更倾向于天主教，因此更容易进入公立和私立学校的课程。在 19 世纪余下的时间里，出现了多个版本。布里塞诺对与宗教密切相关的哲学理念的偏爱在他对伦理学和自然法的涵盖中显而易见，他认为宗教实践是人类最重要的职责，并得出结论：“任何不能导致基督教结论的哲学都是危险和虚假的”（1846: 216）。虽然安德烈斯·贝略并不完全反对这项工作对宗教的强调，但他对逻辑的覆盖范围不足提出了异议，在布里塞诺的处理中，逻辑被限制在演绎逻辑中。因此，可以观察到关于宗教角色的更大争论体现在选择学科教学的子领域上。

尽管哲学在独立后的学术背景中得到发展，并在智利大学成立于 1842 年时帮助定义了其使命（安德烈斯·贝略制定了该机构的章程，并成为其校长长达 23 年），其他哲学家将这门学科更加接近政治行动。最重要的例子是何塞·维克托里诺·拉斯塔里亚（1817-1888），他主张消除仍然根深蒂固在教会中的殖民遗留，并消除阻碍自由个人主动性和表达的威权主义做法。无论是通过他的国会席位，他作为自由党（1849 年）创始人的角色，还是通过他大量的著作，拉斯塔里亚推动了自由主义的主要原则，并阐明了一个基于启蒙思想的历史哲学。其他为智利自由观念的发展做出贡献的思想家包括阿根廷流亡者多明戈·福斯蒂诺·萨门蒂奥（1811-1888）和胡安·鲍蒂斯塔·阿尔贝迪（1819-1884），他们通过各种广泛传播的著作（其中一些具有哲学性质）和在新闻界的激烈辩论，提出了他们的观点。

## 3. 实证主义时代

与拉丁美洲其他地方一样，实证主义在智利在许多领域产生了重大影响，包括哲学。实证主义的到来与智利自 19 世纪 60 年代至 19 世纪 80 年代兴起的反教权主义潮流相吻合，但其影响并不仅限于针对教会的攻击。事实上，它在教育方面产生了深远影响，促成了中等和高等教育课程的实质性转变。然而，最初，它是针对教会在智利社会中的影响的一种及时武器。

Positivism 的基本吸引力在于其“进步”的概念。基于奥古斯特·孔德（Auguste Comte）的工作，实证主义允许反教权思想家将“神学”阶段视为人类进化中的原始状态，这个阶段必将被“形而上学”和“科学”阶段所取代。这种进化观的政治含义和用途是显而易见的：要达到最高水平，智利人需要摆脱天主教教会的宗教影响。正如实证主义的主要早期倡导者何塞·维克多里诺·拉斯塔里亚（José Victorino Lastarria）所阐述的，他放弃了基于抽象或“形而上学”原则的自由主义承诺，科学能够以最有效的方式解决智利社会的问题。其他人响应了这一号召，并集中精力解决诸如教育等重要优先事项。自由主义历史学家迭戈·巴罗斯·阿拉纳（Diego Barros Arana）在这方面做了很多工作，通过改革智利国立学院的课程，米格尔·路易斯·阿穆纳特吉（Miguel Luis Amunátegui）也是如此，他在担任教育部长时，于 1879 年正式在公立学校引入了科学教育。但最重要的实证主义人物是瓦伦丁·莱特利尔（Valentín Letelier，1852–1919），他的《教育哲学》（_Filosofía de la educación_，1927 \[1891]）成为智利哲学研究深刻重新定位的基础。在他看来，自独立以来主导智利教育的神学和形而上学强调对围绕共同信念核心统一国家的作用微乎其微。在他看来，只有科学的确定性才能做到这一点。实现这一更高阶段的工具是逻辑，他成功地在 1893 年的教育计划中引入了逻辑，代价是伦理学和神学。与这种强调一致，胡安·塞拉皮奥·洛伊斯（Juan Serapio Lois，1844–1913）从孔特的角度出版了最完整的逻辑论著，即《实证哲学要素》（_Elementos de filosofía positiva_，1906–1908），将逻辑分析应用于一系列科学的方法论中。

在莱特利尔的影响下，哲学获得了科学特性，特别强调实验心理学，最终引发了一场反应，不满于智利对欧洲和美国新思潮的反应迟钝的思想家之间。然而，实证主义有助于巩固哲学的研究，无论是在更高层次（通过智利大学的教育学院）还是在中学，哲学都是必修科目。

尽管影响力较小，但重要的是要指出，实证主义还有另一个分支，由胞兄胞弟胡安·恩里克（1852-1927）和豪尔赫·拉加里格（1854-1894）代表，他们接受了晚期奥古斯特·孔德的宗教思想，并将人类宗教引入智利。尽管这种实证主义的变体一直延续到 20 世纪，但在哲学领域几乎没有什么影响力。在政治上，它与总统制和据称专制的何塞·曼努埃尔·巴尔马塞达政府结盟，后者在 1891 年的内战中被击败。与孔德支持路易·波拿巴的情况类似，拉加里格兄弟的实证主义品牌开始与专制主义联系在一起。

## 4. 反对实证主义。

智利对实证主义的反叛由恩里克·莫利纳（1871-1964）领导，他是智利大学实证主义启发的教育学院的毕业生。莫利纳发现哲学研究停滞不前，并认为对科学的强调令人窒息。他最初赞同学校对教授多个学科（包括哲学）采用科学方法的强调。然而，在接触到威廉·詹姆斯和尤其是亨利·博格森的作品后，莫利纳发起了一场哲学运动，将形而上学确立为该学科中最重要的领域。在他看来，实证主义关于“进步”的概念已经变成了物质，特别是技术上的进步。在他最重要的哲学著作《_论人类生活中的精神_》（1937）中，莫利纳认为科学和技术对推动人类幸福做出了很少贡献。因此，对精神生活的强调（虽然不是宗教意义上的）应该成为该学科的中心努力以及对社会的主要贡献。他建立了一个价值观的等级制度，其中精神性质的价值高于庸俗的物质主义关注。

莫利纳的论点绝非没有政治倾向。他认为，在实证主义之后，马克思主义已成为传播物质主义关注的主要渠道。在 20 世纪中叶的智利，马克思主义确实在该国取得了重大进展，莫利纳是一代哲学家中的一员，他对马克思主义意识形态所代表的威胁感到担忧。他的回应是推进将形而上学置于各学科等级制度之首的哲学观点。然后，他着手通过自己的著作和机构活动（包括他于 1919 年创立的康塞普西翁大学的校长职务）重新调整该国的哲学研究。

在学科上，莫利纳的论点在天主教思想家中引起共鸣，比如克拉伦斯·芬利森（1913–1954），以及更倾向于世俗的豪尔赫·米利亚斯（1917–1982），他的《个体性的观念》（1943）完全摆脱了与实证主义的任何联系，而是强调个体自由作为该学科的主要关注点。尽管米利亚斯在整个职业生涯中保持着高度的政治参与，但他将个体自由视为人类存在的更高阶段，超越了社会和政治的外部性。

这些观点的制度表达体现在哲学课程的一系列变化中，包括引入新的作者和新的思想流派，如现象学、存在主义和新托马斯主义。1929 年，佩德罗·莱昂·洛约拉在智利大学创立了一个哲学研究中心，1931 年，一场重大的大学改革将哲学研究的所在地——教育学院，置于更加注重学术的哲学系。哲学也在康塞普西翁大学和智利天主教大学（成立于 1888 年）教授。后者于 1923 年成立了一个哲学学院，1943 年成立了一个教育学院，开设了本体论、神辩学和伦理学课程。到 1950 年，它增设了认识论、形而上学和美学课程。哲学活动的增加导致了 1948 年成立智利哲学学会（Sociedad Chilena de Filosofía），以及次年推出《哲学杂志》。西班牙内战和第二次世界大战的爆发意味着 20 世纪 40 年代有几位哲学家作为难民来到智利，增加了哲学关注的多样性。

尽管多样性，智利哲学家之间的主要共识是，这门学科是自主的，尤其是与政治无关。个别哲学家可能会有政治承诺，但他们都认同这个领域是一个高度专业化的学术努力。他们进入了阿根廷哲学家弗朗西斯科·罗梅罗所称的“哲学正常化”阶段，这一阶段以大学级别职位、在专业期刊上发表文章、国际会议、在外国机构任教，以及最重要的是，将学术哲学活动与社会和政治关注分隔开来的强烈分歧为特征。

## 5. 哲学专业化的接受与批评

建立在对实证主义的反应之上的哲学模式在 20 世纪 50 年代蓬勃发展，这要归功于智利哲学协会的活动、《哲学杂志》的定期出版以及哲学界的国际化。该协会得到了美洲哲学协会联合会、国际哲学研究所和国际哲学协会的认可。外国教师经常在该国主要哲学系任教，1956 年，第一届美洲哲学协会国际大会在圣地亚哥举行，出席的知名人物包括米格尔·雷阿莱、爱德华多·尼科尔、里西埃里·弗龙迪齐、弗朗西斯科·罗梅罗、何塞·高斯和何塞·费拉特尔·莫拉。逻辑、科学哲学和认识论领域的演讲者包括威拉德·范·奥曼·奎因、罗德里克·奇索姆、马里奥·邦格、弗朗西斯科·米罗·克萨达和胡安·大卫·加西亚·巴卡。当地的哲学界可以自豪地庆祝这门学科如何有效地成为一个在国内外都得到认可的专业事业。

最初，一批新一代哲学家接受了智利哲学界宣扬的高国际标准的哲学工作。除了豪尔赫·米拉斯，新的名字不断在出版物和其他专业场合中出现：费利克斯·施瓦茨曼（1913–2014）、马可·安东尼奥·阿连德斯（1925–2000）、洪贝托·詹尼尼（1927–2014）、胡安·德·迪奥斯·维亚尔·拉赖因（1924–2019）、加斯顿·戈麦斯·拉萨（1926–2019）和胡安·里瓦诺（1926–2015）。然而，并非所有人认为专业模式是当时智利条件下唯一或最合适的模式。一些人来自不同的学科背景或兴趣领域，因此对一种看似过分依赖国际（主要是欧洲）潮流的哲学活动模式持怀疑态度，甚至认为这种模式对该国可能是无关紧要的。尤其是胡安·里瓦诺，他对哲学界中发现的抽象水平提出了毁灭性的批评，并质疑同事们与国家关切的距离。作为逻辑学领域的一员，他质疑同事们在使用几个关键哲学概念时的不一致性。他还介绍了英国新黑格 elian 主义者如弗朗西斯·H·布拉德利和哈罗德·乔奇姆的作品，但很快转向研究黑格尔和马克思，如《在黑格尔和马克思之间》（1962）和《从宗教到人道主义》（1965）。他主张一种既批判社会经济状况又更加响应智利和拉丁美洲正在发生的复杂变化的哲学工作。

哲学界广泛拒绝了里瓦诺的主张，并驳回了他呼吁使该学科符合国家需求的要求。他们不仅加强了哲学专业化的基础，而且继续发展了一种有力的辩护立场，支持大学模式，使专业人士的工作免受更大社会的要求。选民的迅速扩大，左翼强大政党的出现以及高等教育入学人数的增加，都带来了对大学改革的压力，而哲学专业化的捍卫者则予以抵制。在哲学学派方面，他们继续探讨现象学主题，特别是马丁·海德格尔的作品。对现代性的批判是专业哲学家在他们的专业工作中的主题。

专业主义阵营中最有口才的声音来自豪尔赫·米利亚斯，他在西班牙哲学家何塞·奥尔特加·伊·加塞特的影响下，是大众社会的强烈批评者。在《西方精神史论文》（1960）和《大众社会的精神挑战》（1962）等著作中，米利亚斯重申了精神居于比唯物主义，尤其是马克思主义更高位置的主张。在 20 世纪 60 年代的背景下，他所做的新尝试是将该学科的目标与大学的目标联系起来：两者都有义务抵制大众社会的压力。其他追随这一论点的人，如费利克斯·马蒂尼斯·博纳蒂（1929 年）和胡安·德·迪奥斯·维亚尔·拉赖因，强调了大学在培养受哲学更高目标启发的知识精英方面的作用。在许多方面，他们预见了艾伦·布鲁姆在《美国心灵的封闭》（1987 年）中的论点。

这种关于高等教育性质和哲学家责任的辩论与国家最重要大学内部的政治竞争融为一体。自称为不涉政或反对政治侵入大学校园的哲学家们反而成为公开政治辩论的一部分。1960 年代后半叶引入了重要的大学改革，但关于高等教育性质和哲学目标的辩论并未减少。减少的是哲学产出水平，这在萨尔瓦多·阿连德政府执政期间（1970 年至 1973 年）持续下降。《哲学评论》于 1970 年停刊，课程经常中断，哲学类书籍减少到了几乎绝迹。也许最重要的例外可以在少数几位哲学家中找到，尤其是豪尔赫·米利亚斯，他的两卷本《哲学的理念》于 1970 年出版。由于政治动荡和大学目标争议，哲学界深受分裂。

## 6. 军事统治时期

1973 年 9 月 11 日由奥古斯托·皮诺切特将军领导的军事政变后，智利经历了长达十六年半的独裁统治。政变宣称的意图是在阿连德政府混乱的几年后恢复秩序。然而，几年后很明显，政权打算改变智利社会和政治，关闭国会，禁止政党，特别是左翼政党，并于 1980 年颁布新宪法。在高等教育领域，军事政权扭转了多年的大学自治，并直接干预了该国主要大学的管理：任命现役军官为校长，关闭了几所学校和系，清洗了被视为异见者或潜在对手的学者和学生。军队作为一个机构并不孤立行动，因为它拥有大量的民间支持者。

智利大学哲学系或许是全国哲学系中受打击最严重的。作为一所历史可追溯至 1842 年的旗舰公立大学，智利大学从军事政权开始就成为了严厉和持续镇压的目标。像胡安·里瓦诺（Juan Rivano）这样的批评家被逮捕并流亡，而其他人，比如埃迪森·奥特罗（Edison Otero，1946 年出生），则被无罪解雇。军方首先将智利大学置于空军的保护下，随后（1976 年）改为陆军。但是，军方没有依靠那些反对大学改革的专业人士，比如豪尔赫·米利亚斯（Jorge Millas）、洪贝托·吉安尼尼（Humberto Giannini）或加斯顿·戈麦斯·拉萨（Gastón Gómez Lasa），而是在关键的学术和行政职位上任命了大多数不知名但持同情态度的哲学家。结果，专业哲学家与政权日益疏远，一些人变成了公开的反对者。

也许最重要的例子是豪尔赫·米亚斯（Jorge Millas）。最初，他对军方确实寻求实现他对大学的理想看法，即作为思考和理性之地，抱有希望。然而，到了 1976 年，他的希望变成了幻灭：米亚斯抨击了军方对大学的干预，宣称上世纪 60 年代和 70 年代初高等教育政治化没有改善；大学已成为一家“受监视”的机构（1981 年：84）。他自己的哲学工作越来越多地关注暴力主题，正如他在与批判哲学家爱迪生·奥特罗（Edison Otero）合著的《暴力及其面具》（1978 年）中所展示的，这种合作标志着该国两种历史哲学传统之间的和解。米亚斯受到政府审查的孤立和日益疏远，他撤退到瓦尔迪维亚南部的南部大学，最初受到欢迎，但很快因公开批评政权而被撤职（1980 年）。在他的最后一部作品《大学的理念》（1981 年）中，他重申了自己终身致力于大学作为学术和思考之地，并重申了对军事干预的批评。被压制、孤立和疾病缠身，他于 1982 年 65 岁时去世。

其他哲学家没有像米亚斯那样直言不讳，但显然对军方处理大学和国家的方式感到不满。像洪贝托·詹尼尼（Humberto Giannini）和加斯顿·戈麦斯·拉萨（Gastón Gómez Lasa）这样的知名哲学家专注于他们的哲学工作，并偶尔发表可能被视为批评的声明。但镇压环境不利于他们认为该领域需要的对话和自由探究。像胡安·里瓦诺（Juan Rivano）这样被流放的哲学家甚至更难找到国家舞台发表自己的观点。他被禁止进入该国，直到 1988 年全民公决后才能返回。那是引发向民主过渡的转折点，最终在 1989 年总统选举和 1990 年帕特里西奥·艾尔温（Patricio Aylwin）政府就职典礼上达到高潮。

这段时期的平衡对参与其中的哲学家来说是负面的，除了那些占据被军事统治迫害者职位的人，即使在民主恢复后仍留任的人。然而，由于独裁统治的岁月，发生了重要的变化：那些对政治持敌意的哲学家现在完全拥护民主的价值观，包括容纳不同观点的所有含义。由于大学中存在的敌对氛围，许多人移居到独立研究中心，从而与其他学科的学者进行对话。哲学家因此能够摆脱二十世纪该领域基础的哲学与政治分离。

从军事统治时期以及或许更早时期出现的另一个重要因素是，许多哲学家因政治动荡而移居国外，比如罗伯托·托雷蒂（1930 年）和卡拉·科尔杜亚（1925 年），或因明目张胆的迫害，如胡安·里瓦诺和雷纳托·克里斯蒂（1941 年）。在智利领域历史的长期考察中，必须包括大量的哲学著作。还有一些年轻一代在国外接受培训，回国后的工作开始在该领域产生影响。

自 1990 年恢复文职统治以来，哲学领域面临着诸多挑战，如新的国家优先事项（人权、经济增长、宪法改革、机构合法性的侵蚀），由于新私立大学的蓬勃发展，领域的分裂，以及国家教育委员会未能成功将哲学从中学课程中移除（2018 年）。与此同时，在其他方面，哲学在某些方面蓬勃发展，这要归功于前一代人的持续贡献（Roberto Torretti、Carla Cordua、Renato Cristi 和 Marcos García de la Huerta）或者后来出版的 Jorge Millas、Juan Rivano 和 Humberto Giannini 的著作。直到 2017 年去世，出生于 1927 年的 Jorge Eduardo Rivera 仍然是海德格尔最重要的专家。Pablo Oyarzún（1950 年出生）在美学和文学批评方面做出了重要贡献。最近，Andrés Claro（1968 年出生）已成为一个强大的新哲学声音，关注语言的诗意维度，如《Las vasijas quebradas》（2012 年）和《Tiempos sin fin》（2018 年）等作品。一种新趋势正在兴起，即对现代经济哲学基础的审查，最好的例证是 Leonidas Montes（1966 年出生）的《亚当·斯密在背景下》（2017 年）。

## Bibliography

### Primary Sources

* Alberdi, Juan Bautista, 1900, “Ideas para presidir la confección del curso de filosofía contemporánea”, in _Escritos póstumos de Juan Bautista Alberdi_ (Volume 15), Buenos Aires: Imprenta Juan Bautista Alberdi.
* Allendes, Marco Antonio, 1980, “La imaginación creadora en la ciencia y el arte”, _Atenea: Revista de Ciencia, Arte y Literatura_, 442: 37–44.
* –––, 1965, “Relación entre religión y filosofía en el pensamiento hindú”, _Anales de la Universidad de Chile_, 135: 131–152.
* Bello, Andrés, 1881 \[1981], _Filosofía del entendimiento y otros escritos filosóficos_, in, 1981, _Obras completas_ (Volume 3), Caracas: Fundación La Casa de Bello.
* –––, 1984, _Philosophy of the Understanding_, trans. by O. Carlos Stoetzer, Washington, D.C.: General Secretariat, Organization of American States.
* Bilbao, Francisco, 1866, _Obras completas de Francisco Bilbao_, Manuel Bilbao (ed.), Buenos Aires: Imprenta de Buenos Aires.
* Briseño, Ramón, 1845–1846, _Curso de filosofía moderna, para el uso de los colegios hispano-americanos, y particularmente para el de los de Chile: extractado de las obras de filosofía que gozan actualmente de celebridad_ (Volumes 1–2), Valparaíso: Imprenta del Mercurio.
* Claro, Andrés, 2012, _Las vasijas quebradas: Cuatro variaciones sobre_ “_la tarea del traductor_,” Santiago: Ediciones Universidad Diego Portales.
* –––, 2018, _Tiempos sin fin_, Santiago: Ediciones Bastante.
* Cordua, Carla, 2010, _Once ensayos filosóficos_, Santiago: Ediciones Universidad Diego Portales.
* Cordua, Carla, and Roberto Torretti, 2017, _Perspectivas_, Santiago: Ediciones Universidad Diego Portales.
* Egaña, Juan, 1827, _Tractatus de re logica, metaphisica, et morali pro filiis et alumnis Instituti nacionalis Jacobo politanae erudiendis J.E._, Santiago: Raimundo Rengifo.
* Finlayson, Clarence, 1969\*, Antología\*, Tomás MacHale (ed.), Santiago: Editorial Andrés Bello.
* Frei Montalva, Eduardo, 1946, _La política y el espíritu_, Santiago: Editorial del Pacífico, 2nd edition.
* García de la Huerta, Marcos, 1985, _Crítica de la razón tecnocrática_, Santiago: Editorial Universitaria.
* Giannini, Humberto, 1965\*, Reflexiones acerca de la convivencia humana\*, Santiago: Facultad de Filosofía y Educación, Universidad de Chile.
* –––, 1968, _El mito de la autenticidad_, Santiago: Ediciones de la Universidad de Chile.
* –––, 1981, _Desde las palabras_, Santiago: Ediciones Nueva Universidad.
* –––, 1982, _Tiempo y espacio en Aristóteles y Kant_, Santiago: Editorial Andrés Bello.
* –––, 1987, _La “reflexión” cotidiana: Hacia una arqueología de la experiencia_, Santiago: Editorial Universitaria.
* –––, 2007, _La metafísica eres tu: Una reflexión ética sobre la intersubjetividad_, Santiago: Catalonia.
* Gómez Lasa, Gastón, 1978a, _Platón: El periplo dialógico_, Valdivia: Ediciones de la Universidad Austral de Chile.
* –––, 1978b, _Platón: Aporías dialógicas_, Valdivia: Universidad Austral de Chile.
* –––, 1979, _Platón: Primera agonía_, Valdivia: Departamento de Extensión Académica, Universidad Austral de Chile.
* –––, 1980a, _La institución del diálogo filosófico_, Valdivia: Universidad Austral de Chile.
* –––, 1980b, _El expediente de Sócrates_, Valdivia: Universidad Austral de Chile, 1980.
* –––, 2011, _El periplo de la metafísica_, Santiago: Ediciones Universidad Diego Portales.
* Lagarrigue, Jorge, 1875, “La filosofía positiva”, _Revista Chilena_, 2: 632–645.
* –––, 1879, “Una conversión a la Religión de la Humanidad”, _Revista Chilena_, 14: 228–246.
* Lagarrigue, Juan Enrique, 1926, _La Religión de la Humanidad_, Santiago: Fundación Juan Enrique Lagarrigue, 5th edition.
* Lastarria, José Victorino, 1875, _Lecciones de política positiva_, Paris: Librería de A. Bouret e Hijo.
* –––, 1885, _Recuerdos literarios. Datos de la historia literaria de la América española y del progreso intelectual de Chile_, Santiago: Librería de M. Servat, 2nd edition.
* –––, 1843–1844, “Investigaciones sobre la influencia social de la conquista i del sistema colonial de los españoles en Chile”, _Anales de la Universidad de Chile_, 1: 199–271.
* Letelier, Valentín, 1895, _La lucha por la cultura: Miscelánea de artículos políticos y estudios pedagógicos_, Santiago: Imprenta y Encuadernación Barcelona.
* –––, 1927 \[1891], _Filosofía de la educación_, Buenos Aires: Cabaut, 2nd edition.
* Lois, Juan Serapio, 1906–1908, _Elementos de filosofía positiva_, Copiapó: Imprenta de la Tribuna, 2nd edition.
* Loyola, Pedro León, 1954, _Una oposición fundamental en el pensamiento moderno: Causalidad y evolución_, Santiago: Editorial Jurídica de Chle.
* Mann, Wilhelm, 1915, “El espíritu general y los ramos de la enseñanza filosófica en el Liceo”, _Anales de la Universidad de Chile_, 137: 643–707.
* Marchant, Patricio, 2014, _Escritura y temblor_, Pablo Oyarzún and Willy Thayer (eds.), Santiago: Editorial Cuarto Propio.
* Marín, Ventura, 1834–1835, _Elementos de la filosofía del espíritu humano escritos por Ventura Marín para el uso de los alumnos del Instituto Nacional de Chile_, Santiago: Imprenta de la Independencia.
* Martínez Bonati, Félix, 1965, _La situación universitaria_, Santiago: Prensas de la Editorial Universitaria.
* –––, 1960, “La misión humanística y social de nuestra universidad”, _Anales de la Universidad de Chile_, 119: 114–137.
* Millas, Jorge, 1943, _Idea de la individualidad_, Santiago: Prensas de la Universidad de Chile.
* –––, 1960, _Ensayos sobre la historia espiritual de Occidente_, Santiago: Editorial Universitaria.
* –––, 1962, _El desafio espiritual de la sociedad de masas_, Santiago: Editorial Universitaria.
* –––, 1963, “Discurso sobre la universidad y su reforma”, _Anales de la Universidad de Chile_, 127: 249–261.
* –––, 1970, _Idea de la filosofía_, Santiago: Editorial Universitaria.
* –––, 1981, _Idea y defensa de la universidad_, Santiago: Editorial del Pacífico; Coedición Corporación de Promoción Universitaria.
* Millas, Jorge, 2010, _Filosofía del derecho_, Juan O. Cofré (ed.), Santiago: Ediciones Universidad Diego Portales.
* Millas, Jorge and Edison Otero, 1978, _La violencia y sus máscaras_, Santiago: Ediciones Aconcagua.
* Molina, Enrique, 1914, _Filosofía americana: Ensayos_, Paris: Casa Editorial Garnier Hermanos.
* –––, 1921, _De California a Harvard: Estudio sobre universidades norteamericanas y algunos problemas nuestros_, Santiago: Sociedad Imprenta y Litografía Universo.
* –––, 1925, _Dos filósofos contemporáneos: Guyau–Bergson_, Santiago: Editorial Nascimento.
* –––, 1942, _Confesión filosófica y llamado de superación a la América hispana_, Santiago: Editorial Nascimento.
* –––, 1945, _Discursos universitarios_, Santiago: Editorial Nascimento.
* –––, 1947 \[1937], _De lo espiritual en la vida humana_, Santiago: Editorial Nascimento, 2nd edition.
* Montes, Leonidas, 2017, _Adam Smith en contexto: Una revaluación crítica de algunos aspectos centrales de su pensamiento_, Madrid: Tecnos.
* Mora, José Joaquín de, 1832, _Cursos de lógica y ética según la escuela de Edimburgo_, Lima: Imprenta de José Masías.
* Otero, Edison, 1979, _Los signos de la violencia_, Santiago: Editorial Aconcagua.
* –––, 1982, “El pensador en la caverna”, _Estudios Sociales_, 31(1): 79–108.
* –––, 1985, _Los derechos de la inteligencia_, Santiago: Instituto Chileno de Estudios Humanísticos.
* –––, 1986, “Reivindicación de la filosofía”, _Estudios Sociales_, 48(2): 195–200.
* –––, 1986, “Los filósofos y el poder: Un anecdotario”, _Estudios Sociales_, 50(4): 47–56.
* Otero, Edison and Ricardo López, 1989, _Pedagogía del terror: Un ensayo sobre la tortura_, Santiago: Editorial Atena.
* Oyarzún, Luis, 1967, _Temas de la cultura chilena_, Santiago: Editorial Universitaria.
* Oyarzún, Pablo, 1996, _El dedo de Diógenes: La anécdota en filosofía_, Santiago: Editorial Dolmen.
* –––, 2009, _La letra volada: Ensayos sobre literatura_, Santiago: Ediciones Universidad Diego Portales.
* Rivano, Juan, 1962, _Entre Hegel y Marx: una meditación ante los nuevos horizontes del humanismo_, Santiago: Comisión Central de Publicaciones de la Universidad de Chile.
* –––, 1964, _Curso de lógica antigua y moderna_, Santiago: Editorial Universitaria.
* –––, 1965, _Desde la religión al humanismo_, Santiago: Editorial Universitaria.
* –––, 1965, _El punto de vista de la miseria_, Santiago: Facultad de Filosofía y Educación, Universidad de Chile.
* –––, 1966, _Contra sofistas_, Santiago: Encuadernadora Hispano-Suiza.
* –––, 1969, _Cultura de la servidumbre: Mitología de importación_, Santiago: Editorial Santiago.
* –––, 1985, _Lógica elemental_, Santiago: Editorial Universitaria, 2nd edition.
* –––, 1986a, _Perspectivas sobre la metáfora_, Santiago: Editorial Universitaria.
* –––, 1986b, “Globalización y estrategias lógicas”, _Cuadernos Americanos_, 265(2): 72–85.
* –––, 1987, _Mitos: Su función social y cultural_, Santiago: Pehuén.
* –––, 2017, _El Eclesiastés, Diógenes, Montaigne_, Santiago: Ediciones Tácitas.
* Rivera, Jorge Eduardo, 2016, _De asombros y nostalgias_, Santiago: Ediciones Universidad Católica de Chile.
* Schwartzmann, Félix, 1950, _El sentimiento de lo humano en América; ensayo de antropología filosófica_ (Volume 1), Santiago: Universidad de Chile.
* –––, 1953, _El sentimiento de lo humano en América; antropología de la convivencia_ (Volume 2), Santiago: Universidad de Chile.
* –––, 1967, _Teoría de la expresión_, Santiago: Ediciones de la Universidad de Chile.
* Stahl, Gerold, 1964, _Elementos de metalógica y metamatemáticas_, Santiago: Editorial Universitaria.
* Torretti, Roberto, 1967, _Manuel Kant: Estudio sobre los fundamentos de la filosofía crítica_, Santiago: Ediciones de la Universidad de Chile. 2nd edition, 2005.
* –––, 2006, _Estudios filosóficos, 1957–1987_, Santiago: Ediciones Universidad Diego Portales.
* –––, 2007, _Estudios filosóficos, 1986–2006_, Santiago: Ediciones Universidad Diego Portales.
* Varas, José Miguel, 1828, _Lecciones elementales de moral_, Santiago: Imprenta de la Independencia.
* Varas, José Miguel and Ventura Marín, 1830, _Elementos de ideología_, Santiago: Imprenta de la Independencia.
* Vial Larraín, Juan de Dios, 1965, “Idea de la universidad”, in _La universidad en tiempos de cambio_, Santiago: Editorial del Pacífico, pp. 7–12.
* –––, 1971, _Metafísica cartesiana_, Santiago: Editorial Universitaria.
* –––, 1979, _Tres ideas de la filosofía y una teoría_, Santiago: Facultad de Ciencias Físicas y Matemáticas, Universidad de Chile.
* –––, 1980, _La filosofía de Aristóteles como teología del acto_, Santiago: Editorial Universitaria.
* –––, 1987, _Una ciencia del ser_, Santiago: Ediciones Universidad Católica.

### Secondary Sources

* Abellán, José Luis, 1967, _Filosofía española en América, 1936–1966_, Madrid: Ediciones Guadarrama.
* Ardao, Arturo, 1986, _Andrés Bello, filósofo_, Caracas: Biblioteca de la Academia Nacional de la Historia.
* –––, 1963, “Assimilation and Transformation of Positivism in Latin America”, _Journal of the History of Ideas_, 24(4): 515–522.
* Astorquiza, Fernando (ed.), 1982, _Bio-Bibliografía de la filosofía en Chile desde el siglo XVI hasta 1980_, Santiago: Universidad de Chile—Instituto Profesional de Santiago.
* ––– (ed.), 1985, _Bio-Bliblografía de la filosofía en Chile desde 1980 hasta 1984_, Santiago: Universidad de Chile-Instituto Profesional de Santiago.
* Atria, Manuel, 1970, “El pensamiento metafísico de Clarence Finalyson”, _Mapocho_, 23: 71–82.
* Bader, Thomas, 1970, “Early Positivistic Thought and Ideological Conflict in Chile”, _The Americas_, 26(February): 376–393.
* Bazán, Armando, 1954, _Vida y obra del maestro Enrique Molina_, Santiago: Editorial Nascimento.
* Bloom, Allan, 1987, _The Closing of the American Mind: How Higher Education Has Failed Democracy and Impoverished the Souls of Today’s Students_, New York: Simon & Schuster.
* Collier, Simon, 1967, _Ideas and Politics of Chilean Independence, 1808–1833_, Cambridge: Cambridge University Press.
* Cristi, Renato, 1975, “El gesto filosófico de Lastarria”, _Teoría_, 5–6(December): 3–14.
* Cristi, Renato y Carlos Ruiz, 1981, “¿Hacia una moral de mercado?”, _Mensaje_, 30(June): 244.
* Da Costa Leiva, Miguel, 1978, “El pensamiento filosófico de Enrique Molina”, Ph.D. Dissertation, Universidad Complutense de Madrid.
* Donoso, Ricardo, 1946, _Las ideas políticas en Chile_, (Colección Tierra Firme), Mexico: Fondo de Cultura Económica.
* –––, 1964, “El Instituto Pedagógico: Tres generaciones de maestros”, _Journal of Inter-American Studies_, 6(January): 5–16.
* Echeverría Yáñez, José R., 1965, _La enseñanza de la filosofía en la universidad hispanoamericana_, Washington, D.C.: Unión Panamericana.
* Escobar, Roberto, 1970, “Clarence Finlayson: el filósofo que regresó del silencio”, _Revista Interamericana de Bibliografía_, 20(4): 459–463.
* –––, 1976, _La filosofía en Chile_, Santiago: Universidad Técnica del Estado.
* Figueroa, Maximiliano, 2011, _Jorge Millas: El valor de pensar_, prologue by Humberto Giannini, Santiago: Ediciones Universidad Diego Portales.
* Frondizi, Risieri, 1948, “¿Hay una filosofía Iberoamericana?” _Realidad_, 3: 158–170.
* Frondizi, Risieri and Jorge J. E. Gracia, 1975, _El hombre y los valores en la filosofía lainoamericana del siglo XX_, Mexico: Fondo de Cultura Económica.
* Fuentealba H., Leonardo, 1959, “Filosofía de la historia en Letelier”, _Revista Chilena de la Historia y Geografía_, 127(January-December): 313–351.
* Góngora, Mario, 1986, _Ensayo histórico sobre la noción de Estado en Chile en los siglos XIX y XX_, Santiago: Editorial Universitaria, 2nd edition.
* Gracia, Jorge J. E. (ed.), 1986, _Latin American Philosophy in the Twentieth Century: Man, Values, and the Search for Philosophical Identity_, Buffalo, N.Y.: Prometheus Books.
* –––, 1984, “Philosophical Analysis in Latin America”, _History of Philosophy Quarterly_, 1(1): 111–122.
* Gracia, Jorge J. E. and Iván Jaksic, 1984, “The Problem of Philosophical Identity in Latin America”, _Inter-American Review of Bibliography_, 34(1): 53–71.
* Gracia, Jorge J. E., Eduardo Rabossi, Enrique Villanueva y Marcelo Dascal (eds.), 1984, _Philosophical Analysis in Latin America_, Dordrecht, Boston, Lancaster: D. Reidel Publishing Co.
* Hale, Charles A., 1986, “Political and Social Ideas in Latin America, 1870–1930”, in _The Cambridge History of Latin America_ (Volume 4), Leslie Bethell (ed.), Cambridge: Cambridge University Press, pp. 367–643.
* Hanisch Espíndola, Walter, S. J., 1963, _En torno a la filosofía en Chile, 1594–1810_, Santiago: Universidad Católica de Chile.
* –––, 1968, _Rousseau, la ideología y la escuela escocesa en la filosofía chilena, 1828–1830_, Santiago: Instituto de Historia, Universidad Católica de Chile.
* Huneeus Gana, Jorge, 1910, _Cuadro histórico de la producción intelectual de Chile_, Santiago: Biblioteca de Escritores de Chile.
* Jaksic, Iván, 1984, “Philosophy and University Reform at the University of Chile, 1842–1973”, _Latin American Research Review_, 19(1): 57–86.
* –––, 1989, _Academic Rebels in Chile: The Role of Philosophy in Higher Education and Politics_. Albany, N.Y.: State University of New York Press.
* Jobet, Julio César, 1970, _Doctrina y praxis de los educadores representativos chilenos_, Santiago: Editorial Andrés Bello.
* Jorrín, Miguel and John D. Martz, 1970, _Latin American Political Thought and Ideology_, Chapel Hill: The University of North Carolina Press.
* Kilgore, William J., 1969, “The Development of Positivism in Latin America”, _Inter-American Review of Bibliography_, 19: 23–42.
* Krebs, Ricardo (ed.), 1981, _Catolicismo y laicisimo: Las bases doctrinarias del conflicto entre la Iglesia y el Estado en Chile, 1875–1885_, Santiago: Ediciones Nueva Universidad, Pontificia Universidad Católica de Chile.
* Lipp, Solomon, 1975, _Three Chilean Thinkers_, Waterloo, Ontario: Wilfrid Laurier University Press.
* Milliani, Domingo, 1963, “Utopian Socialism, Transitional Thread from Romanticism to Positivism in Latin America”, _Journal of the History of Ideas_, 24: 523–538.
* Molina, Enrique, 1953, _La filosofía en Chile en la primera mitad del siglo XX_, Santiago: Editorial Nascimento.
* Ortega y Gasset, José, 1965, _Misión de la universidad y otros ensayos afines_, Madrid: Editorial Revista de Occidente, 4th edition.
* –––, 1983, “Recuerdo del paso por Chile del filósofo José Ortega y Gasset”, _Occidente_, 301(May–June 1983): 32–38.
* Oyarzún, Luis, 1953, _El pensamiento de Lastarria_, (Colección de Estudios Jurídicos y Sociales), Santiago: Editorial Jurídica de Chile.
* –––, 1949, “Lastarria y los comienzos del pensamiento filosófico en Chile durante el siglo XIX”, _Revista de Filosofía_, 1(August): 27–56.
* Pontificia Universidad Católica de Chile, 1982, _La presencia de la filosofía en la Universidad Católica (1888–1973)_, Santiago: Pontificia Universidad Católica de Chile.
* Rodríguez, Rogelio, 1983, “Un filósofo en el exilio”, _Pluma y Pincel_, 10: 32–36.
* –––, 1985, “Un filósofo que no calla”, _Pluma y Pincel_, 16: 12–14.
* Sacks, Norman P., 1972, “José Victorino Lastarria: Un intelectual comprometido en la América Latina”, _Revista Chilena de Historia y Geografía_, 140: 153–193.
* Salazar Bondy, Augusto, 1968, _¿Existe una filosofía en nuestra América?_, Mexico: Siglo XXI.
* Sarti, Sergio, 1976, _Panorama della filosofía Ispanoamericana contemporanea_, Milan: Cisalpino-Golliardica.
* Schultz, Margarita and Jorge Estrella, 1978, _La antropología de Félix Schwartzmann_, Santiago: Editorial Universitaria.
* Schutte, Ofelia, 1987, “Toward an Understanding of Latin American Philosophy: Reflections on the Formation of a Cultural Identity”, _Philosophy Today_, 31(Spring): 21–34.
* Schwartzmann, Félix, 1971, “La Philosophie au Chili”, in _La Philosophie Contemporaine_, Raymond Klibanski (ed.), Florence: La Nuova Editrice, pp. 635–642.
* Sehlinger, Peter Joseph, 1969, “The Educational Thought and Influence of Valentín Letelier”, Ph.D. Dissertation, University of Kentucky.
* Spindler, Frank M., 1980, “Francisco Bilbao: Chilean Disciple of Lamennais”, _Journal of the History of Ideas_, 41(3): 487–496.
* Stabb, Martin S., 1967, _In Quest of Identity: Patterns in the Spanish American Essay of Ideas, 1890–1960_, Chapel Hill: The University of North Carolina Press.
* Varona, Alberto, 1973, _Francisco Bilbao: Revolucionario de América_. Buenos Aires: Ediciones Excelsior.
* Vial Larraín, Juan de Dios, 1983, “Situación de la Filosofía en Chile”, in _Las ciencias sociales en Chile, 1983: Análisis de siete disciplinas_, Santiago: Corporación de Promoción Universitaria, pp. 117–125.
* Vicuña Subercaseaux, Benjamín, 1909, _Memoria sobre la producción intelectual en Chile_, Santiago: Sociedad Imprenta y Litografía Universo.
* Vidal Muñoz, Santiago, 1956, “Apuntes sobre la filosofía en Chile”, _Cursos y Conferencias_, 48(March): 39–60.
* –––, 1977, “La filosofía en Chile”, _Cuadernos de Filosofía_, 6: 19–44.
* Woll, Allen L., 1982, _A Functional Past: The Uses of History in Nineteenth-Century Chile_, Baton Rouge and London: Louisiana State University Press.
* Woodward, Ralph Lee (ed.), 1971, _Positivism in Latin America, 1850–1900: Are Order and Progress Reconcilable?_ Lexington, Mass.: D. D. Heath and Company.
* Zea, Leopoldo, 1949, _Dos etapas del pensamiento en Hispanoamérica: Del romanticismo al positivismo_, Mexico: El Colegio de México.
* ––– (ed.), 1980, _Pensamiento positivista latinoamericano_, Caracas: Biblioteca Ayacucho.

## Academic Tools

| ![sep man icon](https://plato.stanford.edu/symbols/sepman-icon.jpg) | [How to cite this entry](https://plato.stanford.edu/cgi-bin/encyclopedia/archinfo.cgi?entry=philosophy-chile).                                                                      |
| ------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ![sep man icon](https://plato.stanford.edu/symbols/sepman-icon.jpg) | [Preview the PDF version of this entry](https://leibniz.stanford.edu/friends/preview/philosophy-chile/) at the [Friends of the SEP Society](https://leibniz.stanford.edu/friends/). |
| ![inpho icon](https://plato.stanford.edu/symbols/inpho.png)         | [Look up topics and thinkers related to this entry](https://www.inphoproject.org/entity?sep=philosophy-chile\&redirect=True) at the Internet Philosophy Ontology Project (InPhO).   |
| ![phil papers icon](https://plato.stanford.edu/symbols/pp.gif)      | [Enhanced bibliography for this entry](http://philpapers.org/sep/philosophy-chile/) at [PhilPapers](http://philpapers.org/), with links to its database.                            |

## Other Internet Resources

* [Chilean Association of Philosophy](http://www.achif.cl/) (in Spanish).

## Related Entries

[Comte, Auguste](https://plato.stanford.edu/entries/comte/) | [Latin American Philosophy](https://plato.stanford.edu/entries/latin-american-philosophy/)

### Acknowledgments

Acknowledgments: The author wishes to thank Andrés Estefane, Francisco Gallegos, Susana Gazmuri, Sergio Missana, Juan Luis Ossa for their helpful comments.

[Copyright © 2020](https://plato.stanford.edu/info.html#c) by\
Ivan Jaksic <[_ijaksic@stanford.edu_](mailto:ijaksic%40stanford%2eedu)>
