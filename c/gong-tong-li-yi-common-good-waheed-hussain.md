# 共同利益 common good (Waheed Hussain)

*首次发表于 2018 年 2 月 26 日星期一*

在普通的政治话语中，“共同利益”指的是社区成员为了履行彼此对共同利益的关心义务而向所有成员提供的那些设施——无论是物质的、文化的还是制度的。一些现代自由民主国家中共同利益的经典例子包括：道路系统；公共公园；警察保护和公共安全；法院和司法系统；公立学校；博物馆和文化机构；公共交通；诸如言论自由和结社自由之类的公民自由；财产制度；清洁空气和清洁水；以及国防。这个术语本身既可以指成员们共同拥有的利益，也可以指服务共同利益的设施。例如，人们可能会说，“新的公共图书馆将服务于共同利益”或者“公共图书馆是共同利益的一部分”。

作为一个哲学概念，共同利益最好被理解为政治社区成员之间实践推理的一个包容性模型的一部分。该模型默认公民之间存在“政治”或“公民”关系，并且这种关系要求他们在某些设施上创造和维护，因为这些设施符合某些共同利益。相关设施和利益共同构成共同利益，并作为政治商讨的共同立场。当公民面临关于立法、公共政策或社会责任的各种问题时，他们通过诉诸于相关设施和相关利益的概念来解决这些问题。换句话说，他们讨论哪些设施特别值得关注，如何扩展、缩减或维护现有设施，以及他们应该在未来设计和建造哪些设施。

共同利益是政治哲学中的重要概念，因为它在关于社会生活的公共和私人维度的哲学反思中起着核心作用。假设在一个政治社区中，“公共生活”包括成员之间共同努力维护某些设施以谋求共同利益。而“私人生活”则包括每个成员追求一套独特个人项目。作为政治社区的成员，我们每个人都参与到社区的公共生活和自己的私人生活中，这引发了一系列关于这两种活动的性质和范围的问题。例如，我们应该何时基于共同利益做出决定？大多数人会同意，当我们作为立法者或公务员时，我们有义务这样做。但作为记者、公司高管或消费者呢？更根本的问题是，我们为什么要关心共同利益？一个成员退出公共生活，专注于自己的私人生活，这有什么问题？这些是激发对共同利益哲学讨论的一些问题。

本文审视了哲学文献，涵盖了传统共同利益概念中的各种一致点，如柏拉图、亚里士多德、约翰·洛克、卢梭、亚当·斯密、黑格尔、罗尔斯和沃尔泽等人所青睐的观点。它还涵盖了一些重要的分歧，特别是“共同体”和“分配”观之间的分歧。最后，文章考虑了文献中的三个重要主题：民主、共同分享和竞争市场。为了理解这些问题，从将共同利益与在福利经济学和政治道德福利后果主义中起着重要作用的各种善的概念区分开来是有帮助的。

* [第一对比：福利后果主义](https://plato.stanford.edu/entries/common-good/#FirsContWelfCons)
* [第二对比：公共物品](https://plato.stanford.edu/entries/common-good/#SecoContPublGood)
* [3. 为什么政治哲学需要这个概念？“私人社会”中的缺陷](https://plato.stanford.edu/entries/common-good/#WhyDoesPoliPhilNeedConcDefePrivSoci)
* [4. 共同利益的核心特征](https://plato.stanford.edu/entries/common-good/#StruFeatCommGood)

  * [4.1 实践推理的共同立场](https://plato.stanford.edu/entries/common-good/#SharStanForPracReas)
  * [4.2 一组共同设施](https://plato.stanford.edu/entries/common-good/#SetCommFaci)
  * [4.3 一种特权阶级的共同利益](https://plato.stanford.edu/entries/common-good/#PrivClasCommInte)
  * [4.4 一种团结关怀](https://plato.stanford.edu/entries/common-good/#SoliConc)
  * [4.5 一种非聚合性关注](https://plato.stanford.edu/entries/common-good/#NonaConc)
* [5. 共同利益 (i): 共同活动](https://plato.stanford.edu/entries/common-good/#CommInteIJoinActi)
* [6. 共同利益（ii）：私人个性](https://plato.stanford.edu/entries/common-good/#CommInteIiPrivIndi)
* [7. 共同利益视角：共同的还是分配的？](https://plato.stanford.edu/entries/common-good/#CommGoodPersCommDist)
* [8. 政治中的共同利益：民主与集体决策](https://plato.stanford.edu/entries/common-good/#CommGoodPoliDemoCollDeciMaki)
* [9. The Common Good in Civic Life: Burden Sharing and Resource Pooling 共同利益在公民生活中：分担负担与资源共享](https://plato.stanford.edu/entries/common-good/#CommGoodCiviLifeBurdSharResoPool)
* [10. 市场、竞争和看不见的手](https://plato.stanford.edu/entries/common-good/#MarkCompInviHand)
* [11. 结论：社会正义与共同利益](https://plato.stanford.edu/entries/common-good/#ConcSociJustCommGood)
* [ 参考文献](https://plato.stanford.edu/entries/common-good/#Bib)
* [ 学术工具](https://plato.stanford.edu/entries/common-good/#Aca)
* [其他互联网资源](https://plato.stanford.edu/entries/common-good/#Oth)
* [ 相关条目](https://plato.stanford.edu/entries/common-good/#Rel)

---

## 第一对比：福利后果主义

共同利益属于与善而非正确相关的概念家族（Sidgwick 1874）。共同利益与该家族中其他概念的不同之处在于，它是一种被理解为内在于社会关系要求的善的概念。在任何社区中，共同利益包括成员有特殊责任关心的设施和利益，这是因为他们彼此之间处于某种特定关系。例如，在一个家庭中，家庭住所是共同利益的一部分，因为家庭纽带要求成员照顾住所，作为共同努力的一部分，以照顾彼此在住所和安全方面的利益。在大学中，校园内的学术自由氛围是共同利益的一部分，因为大学社区成员之间的特殊关系要求他们照顾这种氛围，作为共同努力的一部分，以照顾彼此在教学、学习和探究方面的利益。

共同利益与在政治道德福利后果主义账户中扮演基础角色的各种善的概念有所不同。在后一类概念中，我们可以包括：快乐超过痛苦的总和，理性欲望的完全满足，考虑分配因素后的总体福利，福利优先主义，福利平等（在某些表述中），帕累托最优性等等。与共同利益不同，这些概念并不基本参考社会关系的要求。它们为行动、动机和事态的善提出了完全独立的标准，这些标准的独立性使它们能够作为具有后果主义结构的规范理论中的基础要素。

根据古典功利主义，例如，正确的行动方式是从对所有有感情生物的快乐和痛苦持中立关注的立场判断出的最佳行动方式（Sidgwick 1874）。假设一种关系包括一组关于处于关系中的人应该如何相互行事的要求——例如，父母应该喂养他们的孩子，父母应该给他们的孩子穿衣服，孩子应该听从父母的判断等。根据古典功利主义，一个行动者应该执行满足关系要求的行动，只有当这样做会导致快乐超过痛苦的总和时。这里的善的概念——即快乐超过痛苦的总和——是独立于任何关系要求而定义的，因此它为善的标准提供了一个准则，可以告诉我们，当人们遵守任何特定的关系要求时，什么时候是好的。

一些福利后果主义关于善的概念包含了一个分配要素，例如福利优先主义，这一特征可能使得将这些概念视为与关系要求内在相关更加合理。例如，有些人可能认为福利优先主义可能是内在于家庭关系的，家庭关系被理解为要求家庭成员执行从最不利群体成员的立场来看是最佳的行动。但请记住，即使是更具分配敏感性的善的概念，比如福利优先主义，仍保留了使其难以看出这些概念如何能够在相关意义上内在于关系的其他后果主义对善的理解特征。

考虑代理人中立性。就福利优先主义而言，它是一个真正的后果主义概念，它表明正确的行动是从一个立场来判断的，这个立场不会随着代理人的位置或代理人所处的关系而改变（Williams，1973 年；Nagel，1986 年；参见 Sen，1993 年）。以这种方式理解，福利优先主义并不要求代理人执行从她自己家庭中最不利成员的立场来看是最优的行动。相反，它要求代理人执行从代理人中立立场来看是最优的行动，比如说，世界上最不利人的福利或者那些在各自家庭中最不利人群中的人的平均福利。如果人们有理由特别关注他们自己家庭中最不利的成员，根据这种观点，这是因为沿着这些思路推理的模式会导致世界上最不利人的福利水平最高，或者对于那些属于相关人群的人来说，平均福利水平最高。

因为福利优先主义是一种中立代理人的概念，所以在某些情况下，福利优先主义可能要求父母伤害自己的孩子，如果这样做可以从世界上处境最糟糕的人的福利角度或相关类别中人的平均福利角度来看取得最佳结果。即使降低自己孩子的福利只会为其他受影响的人带来略微更高水平的福利，父母可能也需要这样做。这些推论显然与我们对关系要求的代理人相对性特征的普通理解相矛盾。

结论是，政治道德的福利后果主义账户在最基本的层面上并非基于对共同利益的概念。相反，它们基于被理解为先于和独立于任何社会关系的善的概念。然而，值得强调的是，政治道德的福利后果主义账户可能将对共同利益的概念纳入更具体的公民在公共生活中的道德义务的账户之中。毕竟，作为公民之间的一种特定模式的动机，可能是从总体福利的角度（或其他适当的与代理人无关的视角）判断为最佳模式。约翰·斯图尔特·密尔在《代议政府考虑》（1862）中阐述了这方面的理论。在他看来，公民应该对其社区的公共事务感兴趣，社会机构应设计成促使公民产生这种动机模式。原因在于，公民对其社区的共同事务的取向是整体上最佳政治安排的一部分，从效用原则的角度来看。

## 2. 第二个对比：公共物品

另一个重要的对比是共同利益和公共物品之间的区别。在经济理论中，公共物品是社区成员如果仅仅受到自身利益驱使，就不会拥有的一种特定物品。举个例子，想象一下，如果一个镇上的居民每个人都用杀虫剂处理自己的草坪，那么他们就可以享受一个没有蚊子的夏天。这种喷雾是需要花钱的，但每个居民付钱购买喷雾，享受没有蚊子的生活会更好。如果几乎每个居民都给自己的草坪喷洒杀虫剂，那么镇上的每个人都会受益，即使是那些没有给自己的草坪喷洒杀虫剂的居民也会受益。但是没有可行的方法来排除那些没有喷洒杀虫剂的居民享受这种好处。

公共物品所带来的问题在于，从个体的利己理性角度来看，每个人的最佳行动方案是不为提供公共物品做出贡献，即使如果每个人都这样做的话，所有人都会受益（参见奥尔森，1965 年）。以我刚刚描述的小镇的任何居民为例。从她自身利益的角度来看，她不应该给自己的草坪喷洒杀虫剂：如果其他居民给他们的草坪喷洒杀虫剂，她将得到好处而不用承担成本。如果其他居民不给他们的草坪喷洒杀虫剂，她将节省给自己喷洒杀虫剂的成本。因此，只要居民们只受到自身利益的驱使，他们就不会创造一个没有蚊子的夏天的美好。

在学术和非学术讨论中，人们经常将共同利益与公共利益或一组公共物品混淆。但是保持这两种概念的区别是很重要的。构成共同利益的设施类似于公共物品，因为它们通常是应该对所有人开放和可利用的设施（例如，公共图书馆）。这意味着无法排除那些不做出贡献的人享受这些好处。然而，构成共同利益的设施在概念上与公共物品不同，因为这些设施可能对社区的每个成员都不是净利益。构成共同利益的设施服务于所有公民共同拥有的一类特殊利益，即公民关系的客体。但是，每个公民除了这些共同利益之外还会有各种私人利益，因此对于任何特定公民来说，某些设施影响的私人利益可能从她的利己理性角度来看比属于共同利益特殊类别的利益更为重要。因此，这些设施对她来说可能不是净利益。

考虑一个公共图书馆的案例。假设某个图书馆是政治共同体中共同利益的一部分，因为它服务于特权阶级的共同利益。假设相关利益是对人类知识宝库的保证访问的利益。某个个体 X 拥有一家面包店。她的面包店是盈利的，但如果人们无法在图书馆阅读某些烹饪书籍，因此无法在家里做出她的胡萝卜松饼，那么她的面包店将会更加盈利。X 对人类知识宝库的保证访问有兴趣，但她也对她的面包店的盈利有兴趣。如果从她的利己主义角度来看，她对松饼垄断的私人利益比她对人类知识宝库的保证访问的利益更重要，那么她实际上会因为公共图书馆而变得更糟。在这种情况下，公共图书馆是共同利益的一部分，但它不是公共物品，因为社区中有人因为图书馆的存在而变得更糟。

在继续之前，请注意，人们有时会使用“公共利益”一词来指代经济理论中的公共物品的技术概念之外的东西。在学术和非学术讨论中，人们有时会以一种或多或少与“共同利益”同义的方式使用“公共利益”。这种术语的使用在政治哲学家中尤为普遍，大致从 16 世纪到 19 世纪。例如，在《政府论》（1698 年）中，约翰·洛克（John Locke）将政治权力定义为制定具约束力法律的权利以及动员社区捍卫这些法律的权利，这两种权力都是

> 以无他目的，而是人民的和平、安全和公共利益。 (1698 [1988: 353]).

在这里，洛克使用术语“公共利益”来指代所有政治社区成员共同拥有的利益（例如，对身体安全和财产的兴趣），其中成员有关系义务来关心这些共同利益。在这个意义上，“公共利益”基本上指的是共同利益，尽管使用术语“公共利益”的哲学家通常更倾向于对政治关系的概念有更薄弱的理解，对政府权力的看法也更为有限。

## 3. 为什么政治哲学需要这个概念？“私人社会”中的缺陷

为什么政治哲学需要共同利益的概念？除了福利、正义或人权等其他概念之外，为什么还需要这个概念？要理解共同利益的重要性，有助于思考私人社会中的道德缺陷。

一个私人社会是一个其成员只关心他们作为私人个体的生活的社会（托克维尔，1835-1840 年；黑格尔，1821 年；罗尔斯，1971 年；另见杜威，1927 年）。成员不一定是理性的利己主义者——他们可能关心自己的家人和朋友。重要的是，他们的动机视野不会超出个人生活的焦点，即人和项目。作为一个私人社会中的个体，我可能对为家人购置更好的住所或改善本地学校，使孩子们和邻里的其他孩子受益感兴趣。我甚至可能会参加全国选举，因为选举结果可能会影响我的家庭或本地学校。但我对全国选举不感兴趣，因为结果会影响我不认识的公民，以及其他州或省的人。我对全国选举不感兴趣，因为结果会影响我社会法律和制度的基本公平性。在私人生活中，我只关心社区的共同事务，只要这些事务触及我的私人世界。

许多哲学家认为，私人社会在道德上存在某种缺陷。一种缺陷特别涉及由理性利己主义者组成的私人社会的情况。正如我在最后一节中所指出的，一群理性利己主义者不会采取必要的行动来产生公共产品。由于这些产品是令人向往的，缺乏公共产品可能是次优的，无论是从总体福利的角度还是从每个成员的利己理性的角度。因此，人们有很好的工具性理由来创建一个公共机构——即一个国家——这个国家可以使用税收、补贴和强制威胁来吸引人们进入互惠合作的模式。

然而，共同利益指向私人社会中的一种不同类型的缺陷。在这种情况下，这种缺陷延伸到所有形式的私人社会，而不仅仅是理性利己主义者的社会，而且这种缺陷是非工具性的。在这种情况下，政治共同体的成员有一种关系义务，即关心他们的共同事务，因此，他们仅关心自己的私人生活这一事实本身就是社区的道德缺陷，无论这种关注模式是否导致次优结果。

为了理解这一观点，考虑人们在自由民主制中可能扮演的各种公共角色（参见黑格尔，1821 年；杜威，1927 年；J.科恩，2010 年：54-58）。显而易见的是，公民在担任立法者、公务员、法官、检察官、陪审员、警察、士兵、教师等职位时以公共身份行事。当他们参与政治过程、在选举中投票并参与公共领域的政策讨论时，他们也是以公共身份行事。许多哲学家认为，公民在担任大型商业企业的执行官、大学和学院的高级官员、记者、律师和学者、从事公民不服从抗议的抗议者以及具有社会意识的消费者时，也是以公共身份行事，或至少在某种程度上是以公共身份行事。

当公民担任公共角色时，政治道德要求他们以与作为私人个体时不同的方式思考和行动。如果您是刑事审判中的法官，如果被告被判有罪，您可能会从个人利益中获益。但政治道德不允许您像私人个体一样做出决定，以谋求自己的私人目标。作为法官，您必须根据审判中提出的证据和法律规定的标准做出决定。这些法律标准本身应该符合共同利益。因此，实际上，政治道德要求您从共同利益的角度思考和行动。

担任公共角色的公民可能也需要做出个人牺牲。考虑一个历史例子。在水门事件丑闻期间，美国总统理查德·尼克松命令美国司法部长埃利奥特·理查森解雇水门特别检察官，以阻止对尼克松滥用职权的调查。理查森没有执行尼克松的命令，而是辞去了自己的职位。许多人会认为理查森做了正确的事情，实际上，他有义务拒绝尼克松的命令，即使这导致他的职业生涯受到重大挫折。作为司法部长，理查森有责任维护美国的法治，这是为了共同利益的实践，即使这意味着在职业抱负方面做出重大牺牲。

现在考虑以下可能性。想象我们生活在一个自由民主制度下，拥有各种社会角色，人们在公共场合扮演不同的角色。但是想象一下，我们的社会是一个私人社会：公民只关心自己的私人事务。为了确保各种公共角色得到填补，我们的制度为人们提供私人激励来承担这些责任。高薪吸引人们担任法官和立法者等职位，相互监督给这些人提供私人激励来履行职责。假设我们的制度结构良好，私人激励足以填补所有重要的公共职位。我们的社会是否有什么缺失？我们的社会是否存在某种道德缺陷？

共同利益传统中的哲学家们认为答案是肯定的：我们的社会中确实缺少一些道德上重要的东西。缺少的是对共同利益的真正关注。我们社会中没有人真正关心共享设施，比如法治，或者这些设施所服务的共同利益。公民仅仅出于从中获得的私人利益而担任各种公共角色。根据政治道德的共同利益观念，即使私人激励导致人们填补所有相关职位，对共同利益的关注不足本身就是政治社区中的道德缺陷。

共同利益传统中理论家们面临的一个核心挑战是解释为什么真正致力于共同利益是重要的。为什么市民是否真正关心共同利益很重要呢？传统中的一些哲学家提到了一个实际问题。即使在一个设计良好的安排中，情况很可能会出现，社会制度并不能为人们提供足够的私人激励，让他们以公共导向的方式行事。例如，政治道德可能要求公职人员维护法治，即使在这样做会损害他们的职业生涯的情况下。或者政治道德可能要求市民抗议不公正的法律，即使这意味着私人面临被监禁或列入黑名单的风险。政治道德甚至可能要求市民冒生命危险，捍卫宪法秩序抵御外部威胁（参见沃尔泽 1970 年；卢梭 1762b [1997: 63–4]）。在这些情况下，无论制度设计得多么完善，市民可能没有足够的私人激励来做政治道德所要求的事情，因此对共同利益的真正关注可能是至关重要的。

一种不同的解释——也许是共同利益传统中最重要的解释之一——强调了社会关系的概念。想想父母与子女之间的关系。这种关系不仅要求参与其中的人以某种方式相互行事，还要求他们以某种方式关心彼此。例如，父母不仅需要喂养和给孩子穿衣服，也许是为了避免被儿童和家庭服务部罚款。父母还需要关心他们的孩子：他们必须在实际推理中给予孩子的利益一定的地位。许多哲学家认为，我们与其他公民的关系具有类似的特征。政治纽带不仅要求我们以某种方式行事，还要求我们在实际推理中给予其他公民的利益一定的地位。在这种观点下，公民仅仅为了私人激励而履行某些公共角色是不可接受的。例如，最高法院大法官必须关心法治和这一实践所服务的共同利益。如果她只是为了每两周领取薪水而做出一致的裁决，那么她就不会以正确的方式回应那些为了共同利益而行事的其他公民，这些公民会做出投票、遵守法律和准备捍卫宪法秩序等行为。

许多哲学家认为，即使是一个私人社会，即使私人激励推动人们填补所有重要的公共角色，也存在道德上的缺陷。共同利益的概念为我们提供了一个解释，说明了在私人社会中公民的实践推理中缺少了什么，并将这一点与一种更广泛的观点联系起来，即关系义务要求公民以这种方式进行推理。

## 4. 共同利益的核心特征

根据政治道德的共同利益概念，政治社区的成员彼此之间处于社会关系中。这种关系虽然不像家庭成员或教会成员之间的关系那样亲密，但仍然是真正的社会关系，它要求成员不仅以某种方式行动，而且在实际推理中给予彼此的利益一定的地位。这种基本观点导致大多数共同利益概念具有一些共同特征。

### 4.1 实践推理的共同立场

大多数概念共享的第一个特征是，它们描述了一种实际推理模式，旨在在政治社区成员的实际思维过程中实现。共同利益的概念不仅仅是正确行动的标准，以至于公民只要执行了正确的行动，就会满足这一概念，而不考虑他们这样做的主观原因。共同利益概念的要点在于定义一种实际推理模式，一种思考和行动方式，构成成员之间适当形式的相互关注。为了满足这一概念，社区成员的活动必须在某种程度上由体现相关模式的思维过程组织。

### 4.2 一组共同设施

大多数关于共同利益的概念确定了一组设施，公民有责任特别维护这些设施，因为这些设施服务于某些共同利益。相关设施可能是自然环境的一部分（例如大气层、淡水含水层等）或人类制品（例如医院、学校等）。但文献中最重要的设施是社会制度和实践。例如，私人财产制度存在于社区成员遵守规则，这些规则赋予个人对外部物体的某些形式的权威。私人财产作为一个社会制度，服务于公民对其物质环境能够主张私人控制的共同利益，因此许多概念将这一制度包括在共同利益的范畴内。

### 4.3 一种特权阶级的共同利益

一个共同利益的概念将定义一类特权的抽象利益。公民被理解为有一种关系义务，即创造和维护某些设施，因为这些设施符合相关利益。特权类别中的利益“共同”是指每个公民被理解为在相似程度上拥有这些利益。这些利益在“抽象”意义上是指它们可以通过各种物质、文化或制度设施来实现。文献中涉及各种利益，包括：参与最值得选择的生活方式的兴趣（亚里士多德 Pol. 1323a14–1325b31）；身体安全和财产的兴趣（例如，洛克 1698 年；卢梭 1762b）；过上负责任和勤奋的私人生活的兴趣（亚当·斯密 1776 年）；拥有一个完全充分的平等基本自由方案的兴趣（罗尔斯 1971 年和 1993 年）；拥有公平机会达到社会中更有吸引力的职位的兴趣（罗尔斯 1971 年）；以及安全和福利的兴趣，其中这些利益被理解为社会公认的需要，受持续政治决定的影响（沃尔泽 1983 年）。

### 4.4 一种团结关怀

大多数关于共同利益的概念定义了一种符合团结模式的实践推理形式。许多社会关系需要在处于关系中的人之间建立一种团结形式。这里的团结基本上涉及一个人在推理中赋予另一个人的某些利益子集与她自己的利益在推理中的地位类似（例如，参见亚里士多德《尼各马可伦理学》1166a1–33）。例如，如果我的朋友今晚需要一个睡觉的地方，友谊要求我应该提供给他我的沙发。我必须这样做，因为友谊要求我推理关于影响我朋友基本利益的事件，就好像这些事件在以类似的方式影响我的基本利益。一个关于共同利益的概念通常要求公民维护某些设施，因为这些设施服务于某些共同利益。因此，当公民按照这一概念要求的方式推理时，他们有效地赋予了他们的同胞的利益在推理中的地位，这与他们在推理中赋予自己的利益的地位类似。

一个例子会使这个观念更直观。根据卢梭的观点，一个适当有序的政治共同体是“一种协会形式，将以全部共同力量保卫和保护每个成员的人身和财产”（1762b [1997: 49]）。这个共同体的公民是通过一种团结的相互关怀形式而团结在一起，这种关怀主要集中在（其他方面）他们在身体安全和财产方面的共同利益上。这种相互关怀形式要求每个公民对其他公民身体或财产的攻击作出反应，就好像这是对自己身体和财产的攻击一样。当扩展到所有成员时，这种相互关怀形式要求整个共同体对任何个体成员的攻击作出反应，就好像这是对每个成员的攻击一样。从这个意义上讲，“全部共同力量”支持每个人的身体安全和财产。或者，正如卢梭有时所说，“不能伤害其中一个成员而不攻击身体，更不能伤害身体而不影响成员”（1762b [1997: 52]）。

### 4.5 一种非聚合性关注

一个密切相关的特征是，大多数关于共同利益的概念并不采取个人利益的聚合观点。聚合观点将个人利益的满足视为可比较的价值，并指导公民最大化这些价值的总和。由于它专注于总体，聚合观点可能要求公民对一些同胞施加损害条件，以便为其他人带来足够的收益。

团结排除了聚合观。从适当的自身利益观出发，团结要求每位公民在推理中赋予她的同胞某些利益与她自己的利益类似的地位。这种思维方式不允许公民为了整体利益而抛弃任何同胞的利益。例如，团结不会允许公民将一些同胞置于奴役之下，即使这可能为其他人带来巨大利益，因为奴役将意味着每位公民在推理中未能给予她被奴役同胞的利益正确的地位。

## 5. 共同利益 (i): 共同活动

让我们现在转向共同利益观念在某些方面的差异。其中一种方式与它们如何定义政治关系对象的特权共同利益有关。我们可以将文献中的重要观点分为两大类：(a) 共同活动观念和 (b) 私人个性观念。

一个共同活动的概念将特权阶级的共同利益定义为成员参与涉及社区所有或大多数成员的复杂活动时所拥有的利益。支持这种观点的人包括古代哲学家，如柏拉图（《理想国》）和亚里士多德（《政治学》），世俗自然法理论家约翰·芬尼斯（1980 年），以及天主教传统中的大多数自然法理论家。共同活动观的一些方面在社群主义思想家查尔斯·泰勒（1984 年）的作品中也很重要，而在迈克尔·桑德尔（2009 年）的作品中则较少。最重要和有影响力的观点是亚里士多德的。

亚里士多德认为，政治共同体的成员不仅仅参与军事联盟或特别密集的契约网络（Pol. 1280b29–33）。成员还参与一种他描述为友谊形式的关系（NE 1159b25–35）。这种友谊包括公民彼此祝福，他们意识到其他公民也祝福他们，并且他们参与一种共同生活，以回应这种相互关心（Pol. 1280b29–1281a3）。在关心彼此并祝福彼此时，公民特别关心的是他们和其他公民过上美好的生活，也就是过上最值得选择的生活。

在亚里士多德看来，最值得选择的生活是一种完全参与并表达人类本性理性部分的活动模式。这种活动模式是一种共同活动的模式，因为像戏剧一样，它有各种相互依存的部分，只有通过团体成员共同实现才能实现。这种模式以一系列有价值的休闲活动为中心，包括哲学、数学、艺术和音乐。但这种模式还包括协调社会努力参与休闲活动的活动（即治国之道），以及各种支持活动，如公民教育和资源管理。

根据亚里士多德的观点，一个有序的社会将拥有一系列物质、文化和制度设施，以满足公民在过上最值得选择的生活中的共同利益。这些设施构成了一个环境，让公民可以参与休闲活动，并进行各种协调和支持活动。亚里士多德论述中涉及的一些设施包括：共同的餐厅和集体用餐，提供休闲活动的场所（《政治学》1330a1–10; 1331a19–25）；共同的教育体系（《政治学》1337a20–30）；共有土地（《政治学》1330a9–14）；共同拥有的奴隶来耕种土地（《政治学》1330a30–3）；共享的政治职位（《政治学》1276a40–3; 1321b12–a10）和行政建筑（《政治学》1331b5–11）；共享的武器和防御设施（《政治学》1328b6–11; 1331a9–18）；以及官方的祭司系统、神庙和公共祭祀（《政治学》1322b17–28）。

亚里士多德的观点可能与现代的感知有所距离，但一个很好的类比是他所考虑的社区形式与我们今天所关联的某些大学相似。想象一所像普林斯顿或哈佛这样的学院。大学社区的成员通过一种特定形式的相互关注而联系在一起：成员们关心他们自己和其他成员的生活，其中生活得好被理解为参与繁荣的大学生活。这种生活方式围绕着物理学、艺术史、曲棍球等智力、文化和体育活动展开。成员们共同努力维护一系列设施，以满足他们共同参与这一共同活动的利益（例如图书馆、计算机实验室、宿舍、橄榄球场等）。我们可以将大学社区中的公共生活看作是大多数成员参与的一种共享实践推理形式，重点是为了维护共同设施，以促进他们的共同利益。

## 6. 共同利益 (ii): 私人个性

私人个性观念提供了一个不同的解释，关于共同利益的特权阶级。根据这些观点，政治共同体的成员有一种关系义务，要关心他们作为私人个体能够过上私人生活的共同利益。公民们各自都有一种兴趣，即能够通过自己的私人选择来塑造自己的生活，选择从事什么活动以及建立什么关系。当公民在做出这些选择时不需要与任何人商议，也不需要通过任何形式的共同协商达成决定时，这些选择在相关意义上是“私人的”。在支持这种观点的哲学家中，包括自由主义传统中许多重要的思想家，如约翰·洛克（1698 年）、卢梭（1762b）、亚当·斯密（1776 年）和黑格尔（1821 年）。最近支持这种观点的人包括约翰·罗尔斯（1971 年）和迈克尔·沃尔泽（1983 年）。

一个私人个性观念的复杂例子是 Rawls 的观点。根据 Rawls 的观点，政治社区的成员有一种关系义务，即关心与所有公民共享的“平等公民地位”相关的利益（1971 [1999: 82–83]）。这些利益包括（a）对充分充分的平等基本自由方案的兴趣和（b）获得社会中更有吸引力的职位的公平机会的兴趣。Rawls 使用术语“共同利益”来指代符合与平等公民地位相关的利益的社会条件的总和（1971 [1999: 217]）。以这种方式理解，共同利益包括但不限于：提供公民言论自由、信仰自由和其他自由的法律秩序；提供公民政治自由的民主政府体制，如投票自由、担任公职和参与集体决策的自由；法院系统以执行法治；以及警察保护和国防以保护基本自由。共同利益还包括对职业自由选择的法律保护；收集和传播有关工作机会的大众传媒机制；为人们提供通往工作的交通系统；以及确保具有相似才能和动机的人们具有相似前景的教育系统（无论是公立还是私立），而不考虑其阶级或家庭背景。

Rawls 的概念具有私人个性观的核心特征。符合共同利益的设施确保公民享有平等自由和公平机会，使他们能够以私人身份参与或退出各种活动和组织，从而能够做出独立选择。例如，良心自由赋予公民法律上的权利，根据自己的私人信仰加入或离开宗教组织。他们无需就这些选择与其他公民商议，也无需将这些选择作为涉及其他公民的更广泛的审议过程的一部分。

Rawls 的观点认为，共同利益在于一种包括身体安全、私人财产和公民自由的体系。从这个角度看，他的观点类似于卢梭的观点，后者也侧重于这些共同利益。Rawls 的观点与卢梭的不同之处在于，它将共同利益的特权阶层扩展到包括对更广泛的基本自由和对获得社会中更有吸引力职位的公平机会的兴趣。这些利益涉及更广泛的制度和社会条件，特别是在教育、交流和经济再分配方面。但值得强调的是，无论是 Rawls 还是卢梭，在他们对共同利益的概念中都没有包含对分配正义的完整阐述。我将在下一节中详细介绍这一点。

## 7. 共同利益视角：共同的还是分配的？

不同共同利益构想之间最重要的区别之一在于它们如何将私人和部门利益纳入考虑，以确定公民之间的关系义务。在这里，我们可以区分两种主要观点：(a) 共同利益的社区构想和 (b) 共同利益的分配构想。

政治社区的成员有一种关系义务，要关心他们共同拥有的某些利益。对于共同利益的“共同体”概念认为这些利益是公民作为公民所拥有的利益，公民身份及与此身份相关的利益被理解为优先于构成每个成员作为私人个体身份的各种身份和利益。当公民们就他们的法律和制度进行社会审议时，一个共同体的概念通常会引导他们抽象出他们的私人利益以及作为某个子群体成员可能拥有的部分利益，而是专注于作为公民的共同利益。

例如，想象一下，公民们正在考虑对社会贸易规则进行变更。他们可能倾向于根据作为某个职业成员或某个行业参与者的部门利益来评估提案。但共同利益的社会观念指引公民搁置这些利益，根据它们对共同公民利益的回应程度来评估提案，比如国家安全或生产型经济的利益。

一种“分配”概念的共同利益与“共同体”概念不同之处在于，它不要求公民像前者那样摆脱他们的私人和部门利益。分配概念始于公民属于具有不同部门利益的各种群体的想法。这些利益在社区的物质、文化和制度设施上提出了部分竞争性要求。分配概念包含一个分配原则，确定社会设施应如何回应这些部门利益，并且该概念表示成员有一种关系义务，即维护一套符合每个人部门利益的设施，方式是符合分配原则规定的。

作为分配观点的一个例子，考虑许多哲学家持有的观点，其将共同利益定义为 Rawls 的差异原则（参见，例如，J. Cohen 1996 [2009: 169–170]；另请参见下面的第 8 节）。根据这一观点，我们可以将公民视为属于各种子群体，每个子群体由所有出生于社会生活中某一特定“起始位置”的人组成。每个群体中的公民共享某些与选择无关的特征，例如他们出生时的阶级地位和天生的才能水平。群体成员对更好的生活前景（以初级商品为衡量标准）有部分竞争性要求，这些要求在社会基本结构上提出了部分竞争性主张。差异原则指出，社会制度应该平等地回应每个群体的利益，但制度应该包含任何能够最大程度地增加最不利群体前景的不平等。然后，公民被理解为有一种关系义务，即维持一套符合差异原则规定方式关注每个人群体利益的制度。

共同利益与分配概念之间的分歧可能是不同概念之间最重要的分歧，它引发了一些关于政治关系性质的重要问题。让我提出两个一般观点。

第一点与共同观点的道德基础有关。将共同利益的共同账户视为对某种社会生活概念的吸引是有帮助的（例如，卢梭，1762b; 黑格尔，1821; 沃尔泽，1983）。根据这一概念，公民在更基本的努力框架内形成各种私人和部门利益，以共同维护某些社会条件。政治联系在某种程度上优先于他们的私人利益，因此政治关系有时可能要求公民搁置他们的私人利益，以便共同行动来维护相关的社会条件。也许最明显的例子是国防（见下面的第 9 节）。在捍卫宪法秩序免受外部威胁时，政治道德要求公民共同行动捍卫共同利益，而不是以一种特别回应不同保护水平的竞争私人利益的方式组织他们的努力。

一个类比可能有所帮助。家庭成员各自作为私人个体拥有不同的利益，比如发展自己的才能，追求人际关系，培养职业前景等。在某种程度上，家庭必须以一种能够满足这些私人利益的方式进行组织。但在某些事务上，家庭关系要求成员以一种方式共同行动，将彼此竞争的私人利益放在一边。如果家庭着火，成员需要拯救家园，而不特别考虑资源如何被配置以更有可能拯救一个成员的房间而不是另一个成员的房间。在某些领域，成员应该从一个关注社会联系中至关重要的共同利益的共同视角行动，而不是从他们作为私人个体的独特且潜在竞争的利益出发。共同利益的共同概念将政治关系视为具有类似性质的关系。

第二点是，令人惊讶的是，罗尔斯本人更倾向于共同利益的实质性共同体概念，而不是分配概念。在《正义论》中，他并没有根据他对社会正义的完整概念来定义共同利益。相反，他将其定义为“共同利益原则”。这一原则从平等公民的立场评估社会制度。正如他所说，“尽可能地，基本结构应该从平等公民的立场来评估”，而这个立场“由平等自由原则和公平机会原则所要求的权利和自由来定义”（1971 年[1999 年：82-83]）。罗尔斯认为，广泛的政策问题可以通过诉诸共同利益原则来解决，包括“合理的规定以维护公共秩序”，“公共卫生和安全的有效措施”，以及“在正义战争中进行国家防御的集体努力”（1971 年[1999 年：83]）。

罗尔斯（Rawls）认为，社会审议应尽可能在一个以所有公民共同利益为焦点的推理框架内展开，当仅仅依靠对共同利益的呼吁无法妥善决定问题时，差异原则才会主要进入讨论。但为什么政治审议应该以这种方式展开呢？罗尔斯为什么认为，“尽可能地，基本结构应该从平等公民的立场评估”？

一种可能的理由与公民通过其共同身份为“公民”所实现的团结有关。当社会成员根据共同利益原则进行推理时，他们会尽可能将私人和部门利益放在一边，以便专注于作为公民的共同利益。将他们的部门利益放在一边（例如，作为最不利群体的成员，第二不利群体的成员，第三不利群体的成员等），公民将他们作为“公民”的共同利益视为比作为私人个体的不同且可能相互竞争的利益更为基本。每位公民有效地告诉她的公民同胞，“团结我们的东西比分裂我们的东西更重要”。将“公民”身份置于公民在公共生活中相互关系的中心对 Rawls 来说尤为重要，因为基于这一共同身份的相互认可对于他关于一个公正社会秩序如何防止嫉妒和地位竞争破坏基本自由的论述至关重要（1971 年[1999 年：476-9]）。

一个与之密切相关的观念与相互关联有关（见上文的第 4.4 节和 4.5 节）。当社会成员从共同利益的角度推理自由和机会时，他们评估政策的立场并不区分一个公民和另一个公民。他们在推理中将自己的同胞的利益视为与自己的利益一样重要。当公民在符合共同利益的社会安排中尽责，并且基于这种安排符合共同利益的理由行事时，公民实现了一种完全相互的团结形式：每个公民都以与她的同胞公民为她的利益工作的方式，与她的同胞公民为她的利益工作的方式完全相同。

基于差异原则的社会合作并不体现相同类型的相互性。想象一下，公民们正在推理他们的制度。从创造对每个起始位置出生的人平等前景的安排开始，他们考虑会产生帕累托改进的不同安排。公民们现在必须在不同可能性之间进行选择：一种安排将最大化最不利群体的前景；另一种安排将最大化次不利群体的前景；第三种安排将最大化第三不利群体的前景；依此类推。在这些可能性下，差异原则要求公民选择从特定群体的立场来看最好的安排，即处于最不利地位的人。

想象一下，我们现在生活在一个满足差异原则的社会秩序中。社会中有某些设施——比如说，某些教育设施——明显地符合处于最不利群体中的人的利益。涉及的资源本可以以对第二不利群体、第三不利群体等更有利的方式进行配置，因此整体安排偏向于特定群体。由于它是以这种方式倾斜的，互动模式缺乏完美互惠的特性：每个公民并不以完全相同的方式为她的每位同胞的利益工作，就像每位同胞为她的利益工作一样。每个人的工作方式都明显地朝向最不利群体的利益。

当然，公民在社会合作以差异原则为基础组织的情况下实现一种团结形式；关键在于公民在社会合作以共同利益原则为基础组织的情况下实现一种独特的团结形式。在后一种情况下，他们实现了一种更加共同体的团结形式，因为公民将个人利益置于一边，专注于共同利益，公民不特别看重不同群体之间的区别。更加共同体的团结形式更好地回应了政治关系的社会维度，这可能是为什么罗尔斯倾向于一种公共推理形式，其中共同利益原则统治着“涉及每个人利益并且在分配效果方面无关紧要或无关紧要的事项”（1971 [1999: 82–83]）的原因之一。

## 8. 政治中的共同利益：民主与集体决策

在关于共同利益的广泛文献中，有几个主题凸显为关注的重要议题。其中一个重要主题是民主。民主在关于共同利益的哲学反思中占据重要地位，因为哲学家们普遍一致认为——尽管并非普遍一致！——一个私人社会在成员进行集体决策方面会存在缺陷。政治社区中的集体决策必须在其公共生活中展开，也就是说，在公民超越自己的私人关注并从共同利益的立场出发进行推理的互动领域中。

根据某些民主观点，公民并非必须站在共同利益的角度。例如，根据多元主义，民主最好被理解为一种将权力和影响力分散在社会中许多不同群体之间的集体决策过程（参见达尔（Dahl）1956 年和 1989 年）。每位公民都有自己的私人利益，具有类似利益的公民群体在各种制定规则的论坛中推进这些利益。总体过程本质上是一种交易形式，每个群体都在策略上与其他群体交换让步，以最大程度地满足其政策偏好。一个秩序良好的民主政权将维持公平的交易条件，使所有重要群体能够对影响其利益的集体决策行使有意义的影响。但在多元主义观点中，没有人需要对社区的共同事务感兴趣：每位公民可能只关心自己的私人事务，进入公共论坛以推进自己的私人利益，与他人的利益相抗衡。

许多哲学家批评多元主义和其他类似私人化的民主推理观点，因为这些观点未能捕捉政治生活的一个重要方面。正如杰里米·沃尔德隆（Jeremy Waldron）所指出的，公民经常基于除了自己的私人利益之外的其他事情进行投票：

> 人们经常根据他们认为对社会的共同利益投票。 他们关心赤字，或堕胎，或东欧，这种方式反映了他们对这些问题有利害关系，而不仅仅是关于他们自己的个人利益。 同样，他们投票的方式通常会考虑到他们对某些利益和自由的特殊重要性的理解。 （Waldron 1990 [1993: 408]）

许多批评者还认为，多元主义没有恰当区分适用于民主决策的实践推理形式和适用于市场环境的形式。公司中的经理可能会以这种策略将改善公司底线为由，而不考虑这种策略可能会损害竞争对手或其他群体，从而为一种商业策略辩护而不是另一种。但在民主过程中的公民不应该以这种方式推理：

> 在民主社会中，呼吁共同利益是一种政治惯例。没有任何政党公开承认要推动对任何公认的社会群体不利的立法。（Rawls 1971 [1999: 280]）

如果将民主决策私有化的方法在道德上存在缺陷，问题究竟出在哪里？公民根据法律对其私人利益的服务程度进行评估并投票表决，这样做有什么问题？

民主理论中一个突出的推理线索是对民主的认识论概念的呼吁（例如，卢梭 1762b；J. 科恩 1986）。根据这一观点，立法有一个独立的正确性标准，即法律必须符合共同利益。民主决策是政治道德的要求，因为立法过程更有可能生成符合标准的法律，当过程是民主的时候。此外，当参与过程的人实际上试图确定符合标准的法律时，民主过程更有可能生成符合标准的法律。因此，参与民主过程的公民应该根据这些提案如何服务共同利益来评估立法提案，因为这是识别和制定合理法律的最佳方式。

民主理论中的另一主要推理线索是对民主的审议概念的呼吁（J. Cohen 1996, 2009; Habermas 1992; Gutman & Thompson 1996）。根据 Joshua Cohen 的审议概念，政治道德要求公民通过公共推理的过程做出具有约束力的集体决策，在这个过程中，公民将彼此视为政治共同体中的平等成员（J. Cohen 1989, 1996）。公共推理的过程要求每个公民应提出理由说服他人采纳立法提案，这些理由是她可以合理期望他人接受的理由，考虑到合理多元主义的事实。

Cohen 认为，他理解的审议民主理想提供了对民主决策共同利益取向的令人信服的描述（1996 [2009: 168–170]）。没有公民可以合理地期望其他人接受立法提案，仅仅因为它符合她自己的利益，因此任何立法提案必须对所有公民的利益做出响应是一个基本要求。此外，公民作为政治共同体的平等成员的背景思想还提出了额外的要求。公民

> 可以拒绝，作为一个过程中的理由，认为有些人比其他人更不值得或者一个群体的利益应该比其他群体的利益少计算。(1996 [2009: 169])

这种对可接受理由的限制导致了一个实质性要求，即立法必须与公众对共同利益的理解一致，将人们视为在相关意义上的平等。

Cohen 引用 Rawls 的差异原则作为满足相关要求的共同利益的公共理解的一个例子。

> 将平等视为基准，[差异原则] 要求由国家行动建立或认可的不平等必须对最不利者的最大利益发挥作用。该基准[即平等] 是对公民背景平等地位所产生的理由约束的自然表达：它不会被视为政策体系的理由，即该体系使得特定社会阶层、天赋或其他区分平等公民的特征的成员受益。[此外，该原则] 大致上坚持，任何人都不应比任何人需要的更不好—这本身就是审议概念的自然表达。(J. Cohen 1996 [2009: 169–170])

注意，科恩在这里主张“分配”而不是“共同体”对共同利益的概念（见上文第 7 节）。在科恩看来，政治共同体的成员有一种关系义务，即为彼此提供一套设施，以满足每个人的部门利益，这与某种分配原则规定的方式相符（即差异原则）。这与共同体的概念不同，后者并不以分配原则来构想公民的关系义务。

科恩（Cohen）可能是对的，差异原则是对决策理想的自然表达，背景是所有公民都是政治共同体中平等成员的假设。但是，一个共同概念的捍卫者可能会主张，公民之间的政治关系具有超越政治共同体中平等成员身份的社会维度。就像朋友之间或体育团队成员之间的关系一样，政治关系必须被理解为对人们施加义务，体现诸如团结和相互关系等关系理想。这意味着政治关系可能要求公民以体现这些价值观的方式彼此推理。例如，政治关系可能要求公民在某些决策背景下搁置他们的私人和部门利益，以便专注于作为公民的共同利益。对社会理想（如团结和相互关系）的隐含关注可能是罗尔斯（Rawls）将共同利益与共同利益原则等同，并赋予这一原则在政治推理中发挥特殊作用的原因之一。

## 9. The Common Good in Civic Life: Burden Sharing and Resource Pooling 9. 共同利益在公民生活中：分担负担与资源共享

许多哲学家都同意，公民在参与政治过程时必须超越他们的私人关注。但一些哲学家认为，在社会生活的其他方面，公民有义务超越他们的私人关注。文献中特别突出的两个例子涉及负担分享和资源汇集。迈克尔·沃尔泽（Michael Walzer）关于征兵和国防的讨论突出了几个重要问题（1983 年：64-71、78-91、97-99 和 168-70；另请参见 Walzer 1970）。

当外国势力威胁自由民主体制时，政治道德似乎指引公民以特定方式捍卫秩序。公民必须将国防视为一项共同事业，通过各种形式的负担分担和资源共享来共同实现一定水平的安全。在这种情况下，负担分担要求社区的每个成员以某种方式参与承担对抗威胁的集体负担。一些公民将进行实际战斗，但其他人将通过治疗伤员、研发武器、照顾儿童、向士兵发送关爱包裹、对必需资源进行配给等方式做出贡献。

在考虑某些高度私有化的国防组织方式时，分担负担的道德重要性最为明显。例如，考虑一种基于市场的方法。一个政治共同体可能允许企业家建立“保护机构”，这些机构将作为公司，雇佣雇佣兵，购买武器，并根据个人公民的偏好和支付能力出售不同级别的保护（参见诺齐克（Nozick）1974 年）。即使通过这种机制捍卫人们的宪法自由是可能的，政治道德似乎排除了这种可能性。一个原因是，市场方案将允许有足够财富的公民为自己购买保护服务，然后让其他人面对实际的战斗危险。这将违反所有公民必须以某种方式分享共同防御社区的集体负担的共同理想（参见沃尔泽（Walzer）1983 年：98-9 和 169）。

高度私有化的国防方式存在另一个问题，与伤员有关。当士兵在战斗中受伤时，他们的伤势与他们以私人身份决定骑摩托车或在马戏团工作时可能遭受的伤害具有不同的道德地位。不同之处在于，战斗伤害不是公民必须以私人身份承担的私人伤害。即使士兵自愿参加战斗，他们也在履行公共服务，我们将他们的伤害视为整个社区必须承担的集体负担的一部分，例如通过免费提供医疗护理和康复服务来帮助伤员。

公共服务和分担负担的共同理想可能不仅限于国防，还可能延伸到其他形式的社会必要工作，这些工作可能困难或危险。

> 今天的矿工是自由公民，但我们可以将他们视为为国家服务的公民。然后，我们可以像对待征兵者一样对待他们，不是分享他们的风险，而是分享治疗的成本：矿山安全研究，为他们的紧急需求设计的医疗保健，提前退休，体面的养老金等等。 (Walzer 1983: 170)

更广泛地应用共同理想可能需要公民将与其他职业相关的负担视为共同社会负担的一部分，包括警察、消防员、教师、托儿所工作人员、护士、养老院工作人员等所面临的负担（参见 Brennan & Jaworski 2015）。

除了分担负担之外，资源汇集是公民为了共同利益而组织活动的另一种方式。在现代自由民主国家，许多设施都为共同利益提供服务，包括军队、公共卫生服务和教育系统。这些设施需要物质资源，这带来了一系列关于如何生成这些资源并将其纳入为共同利益提供服务的资产池的问题。

亚里士多德倾向于通过私人所有权的方式进行处理。在柏拉图的《理想国》中，几乎所有卫士所持有的资源都作为集体资产，卫士可以为社区的共同利益而使用。重要的是，由于卫士几乎没有私人财产，他们不会像一群朋友在野营旅行中自愿汇集资源以谋求共同利益那样做出选择。换句话说，卫士不通过礼物、捐赠或其他形式的私人贡献表达对社区成员的关心。部分原因是，亚里士多德倾向于一种安排，即公民拥有资产的私人所有权和控制权，并有义务将这些资产汇集起来为了共同利益（参见 Kraut 2002: 327–56）。例如，如果社区面临海军威胁，亚里士多德理想社区中的富裕公民将负责建造战舰并将这些船只贡献给战争努力。

亚里士多德的观点引起了当代市场社会中一系列重要问题的关注。他所考虑的公民义务最接近我们对私人慈善的概念。但私人慈善真的是社区为了共同利益而维持共同设施的正确方式吗？2015 年，Facebook 的亿万富翁创始人马克·扎克伯格宣布将捐赠公司 99%的股份给慈善事业，包括公共教育（Kelly 2015）。从亚里士多德的观点来看，这对我们的社会是有益的：我们的制度将财富置于私人手中，从而使公民能够做出有意义的选择，将他们的财富汇集到共同利益中。但许多人会认为，我们的安排存在严重缺陷，因为它使一些个人有能力控制价值超过 450 亿美元的私人财富，即使这些个人最终会将这些资源用于共同利益。柏拉图在谈到政治团结时有所指，社会机构需要直接将一些财富引导到公共领域。但柏拉图似乎在另一个方向上走得太远，这给我们留下了一系列重要问题，即社会何时应通过国家集中资源，何时应通过私人慈善集中资源。

## 10. 市场、竞争和看不见的手

在关于共同利益的哲学反思中的第三个重要主题是市场。公民对关心某些共同利益有一种关系义务，通过市场进行的社会协调可以使公民参与到一种生产活动和消费活动的模式中，以满足这些利益。例如，市场可以引导公民更好地利用社会中的土地和劳动力，从而为每个人追求各自目标提供更多资源。问题在于市场协调涉及一种私有化的推理形式，市场的正常运作可能要求公民不要从共同利益的立场出发进行推理。

为了说明，假设一个社会利用市场来协调公民的教育（参见弗里德曼，1962 年）。 一套以盈利为目的的学校体系将作为企业运作，雇佣教师，购买计算机，并向公众销售教育服务。 父母则会充当消费者，以最低成本购买最好的教育给他们的孩子。 在这种安排中，每个公民都会从自己的私人关注点出发进行推理：作为学校管理者，公民将致力于最大化利润，作为父母，公民将致力于以最低成本为他们的孩子获得最好的教育。 没有人会出于对作为服务共同利益的共享设施的教育系统的关注而行动。 实际上，市场可能要求公民避免这种观点。 毕竟，为了有效降低成本，学校管理者不能对学生的教育表现出太多关注。 为了提高自己孩子的教育，父母也不能对他人孩子的教育表现出太多关注。

我们可以将哲学辩论分为两个阵营。第一个阵营认为，市场社会——即在很大程度上依赖市场来协调社会生活的社会秩序——与政治关系的要求是相容的。这个阵营的理论家包括亚当·斯密（1776 年）、黑格尔（1821 年）、约翰·罗尔斯（1971 年）、迈克尔·桑德尔（2009 年）以及也许迈克尔·沃尔泽（1983 年）。我们还可以包括辩论民主主义者，如于尔根·哈贝马斯（1992 年）和约书亚·科恩（科恩和萨贝尔，1997 年）。

作为第一阵营中的一个例子，考虑黑格尔（1821）及其对市场的看法。黑格尔追随亚当·斯密的思路，认为市场将公民引入一种为共同利益服务的专业化模式。市场通过价格实现这一点。每个公民发现，通过发展自己的才能，以市场价格出售劳动力，然后从他人那里购买所需的商品，她可以为自己做得更好。但遵循价格信号涉及一种只关注私人利益而非共同利益的推理形式。因此，在黑格尔看来，市场活动领域必须被整合到更广泛的政治共同体中是至关重要的。作为政治共同体的成员，公民（或至少一些公民）在公共领域讨论他们的共同利益，在选举中投票，并在立法讨论中找到他们的观点被代表，从而形成一个官方的共同利益概念。这一官方概念塑造了法律，并指导政府管理经济。因此，即使公民不像市场参与者那样从共同利益的立场推理，他们的整体生活也是通过一种专注于维护共同利益而组织的推理形式来管理共享设施。

在这场分歧中的另一派认为，市场社会与政治关系的要求不相容。该派理论家包括亚里士多德（见《政治学》1256b39–1258a17）、卢梭（1762b）、马克思（1844 年，1867 年）和 G.A.科恩（2009 年）。马克思的观点与黑格尔的观点形成了有趣的对比。

马克思赞同黑格尔的观点，即政治共同体的成员必须根据共同利益的概念组织他们的活动。但他认为，如果大多数成员实际上从这个立场出发从未进行推理，那么他们就无法达到理想状态。政治共同体在某种意义上必须是“根本民主的”，即普通公民通过对共同利益的概念的呼吁直接参与到组织社会生活的集体努力中（马克思，1844 年）。通过市场进行社会协调的问题在于市场参与者通过价格被吸引到某种活动模式中，这意味着他们从未真正根据共同利益进行推理。在马克思看来，一个秩序良好的政治共同体将超越这种不透明的社会协调形式。

> 社会的生命过程，基于物质生产过程，直到被自由结合的人们视为生产，并根据一个确定的计划有意识地加以调节，才会剥去其神秘的面纱。 (Marx 1867 [1967:84])。

在一个秩序良好的政治社区中，成员将超越价格协调的权威主义神秘主义，通过一个开放透明的推理过程来组织他们的生产和消费活动，向每个人明确展示他们的活动如何为共同利益服务。

当代政治哲学中的许多问题围绕着市场和共同利益的立场展开。大多数理论家今天持有的观点介于我刚才描述的两个阵营之间：他们主张对于公民何时应该采取私有化视角以及何时必须从共同利益的立场进行推理有更为细致入微的看法。

当涉及到公司和公司高管时，例如，Thomas Christiano (2010) 主张一种特定类型的社会意识取向：公司领导人至少在限制其私人目标的战略追求方面必须从共同利益的立场出发，以符合民主多数人确立的更广泛社会目标。Joseph Heath (2014) 则主张一种更为有限的观点，即市场参与者在外部性和其他市场失灵会阻止市场过程产生有吸引力结果的情况下，不应采取纯粹私有化的视角。在其他市场生活方面仍有大量工作需要完成，这可能需要公民从更具社会意识的角度来推理，特别是在涉及劳工权利、政治自由和气候变化时。

另一个重要的当代问题集合与竞争有关。市场协调通常通过一个过程运作，其中公民相互竞争以获取重要商品。例如，在美国，劳动力市场参与者竞争工作，这些工作在很大程度上决定了谁能获得不同水平的收入，以及由此带来的不同水平的医疗保健、警察保护、司法系统中的考虑和政治影响。当公民们相互对抗时，每个人都努力为自己获取重要商品，知道自己的行动如果成功的话，将有效地剥夺其他一些公民获得这些相同商品的机会。这样，劳动力市场竞争要求公民以一种极端的无视方式行事，不考虑他们的行动如何影响彼此的基本利益。

许多哲学家认为，市场竞争的对抗性结构与政治共同体成员对关心某些共同利益的关系义务不一致。G.A. Cohen (2009: 34–45) 用“社区社会主义原则”来阐述这一问题，该原则排除了需要人们将彼此视为必须克服的障碍的社会安排。Hussain (即将发表) 持更温和的观点，认为“友好竞争”和“生死搏斗”之间存在差异。政治关系允许公民之间一定程度的竞争，但在涉及共同利益的商品（例如医疗保健、教育和自尊的社会基础）时，它限制了机构如何严重地让公民相互对抗。

## 11. 结论：社会正义与共同利益

本文涵盖了不同共同利益概念之间的主要一致和分歧点，以及一些关注的中心话题。让我总结一下关于共同利益与社会正义之间关系的一些观点。

考虑友谊的情况。友谊是一种社会关系，要求处于这种关系中的人以体现一种特定形式的相互关心的方式思考和行动。相关的关心形式包括道德的基本要求——即，Scanlon（1998）所称的“对与错的道德”——因为朋友们不能彼此撒谎、攻击彼此或利用彼此。但即使是陌生人也必须遵守这些基本的道德标准。友谊的区别在于，它涉及的相互关心形式超越了基本的道德，要求朋友们基于这些模式有利于某些共同利益而保持一定的行为模式。

政治社区的成员处于一种社会关系中，这种关系也要求他们以体现某种形式的相互关注的方式思考和行动。共同利益定义了这种关注的形式。共同利益包含了社会正义的某些基本要求，因为公民必须向彼此提供基本的权利和自由，他们不得相互剥削。但共同利益超越了正义的基本要求，因为它要求公民根据这些模式服务于某些共同利益来维持某些行为模式。

类比友谊应该清楚地表明，共同利益与社会正义是不同的，但仍然密切相关的。根据大多数主要传统观点，政治共同体成员有责任关心的设施和利益在一定程度上是以社会正义的术语定义的。例如，卢梭（1762b）、黑格尔（1821）和罗尔斯（1971）都认为，私有财产的基本制度既是正义的要求，也是共同利益的要素。同样，在《自然法与自然权利》中，芬尼斯认为，尊重人权是正义的要求，“维护人权是共同利益的基本组成部分”（1980: 218）。但共同利益超越了正义的要求，因为（1）它描述了内在动机的模式，而不仅仅是外在行为的模式，以及（2）它可能包含并非是正义的一般要求的设施和利益。

所有这些都给我们留下了一些重要的问题。许多当代社会问题取决于公民何时可以采取私人化的视角，何时必须从共同利益的立场进行推理。社会正义通常对这些问题保持沉默，因为原则上，人们可以按照正义要求的方式行事，无论他们是受私人激励计划的影响还是出于对共同利益的关注。这些社会问题最好被理解为围绕政治关系的性质和所需的相互关注形式的分歧。哲学反思在阐明这种关系以及它对我们的要求方面发挥着重要作用，超越了我们作为正义问题的一部分所应承担的责任。

## Bibliography

* Aristotle, 1984 [*Pol.*], *Politics*, Carnes Lord (trans.), Chicago: University of Chicago Press.
* –––, 1985 [*NE*], *Nichomachean Ethics*, Terence Irwin (trans.), Indianapolis: Hackett.
* Axelrod, Robert, 1981, “The Emergence of Cooperation Among Egoists”, *American Political Science Review*, 75(2): 306–318. doi:10.2307/1961366
* –––, 1984, *The Evolution of Cooperation*, New York: Basic Books.
* Barbieri, William J., Jr., 2001, “Beyond the Nations: The Expansion of the Common Good in Catholic Social Thought”, *The Review of Politics*, 63(4): 723–754. doi:10.1017/S0034670500032149
* Brennan, Jason and Peter Martin Jaworski, 2015, “Markets Without Symbolic Limits”, *Ethics*, 125(4): 1053–1077. doi:10.1086/680907
* Cahill, Lisa Sowle, 1987, “The Catholic Tradition: Religion, Morality, and the Common Good”, *Journal of Law and Religion*, 5(1): 75–94. doi:10.2307/1051018
* Christiano, Thomas, 2010, “The Uneasy Relationship between Democracy and Capital”, *Social Philosophy and Policy*, 27(1): 195–217. doi:10.1017/S0265052509990082
* Cohen, G.A., 2009, *Why Not Socialism?*, Princeton: Princeton University Press.
* Cohen, Joshua, 1986, “An Epistemic Conception of Democracy”, *Ethics*, 97(1): 26–38. doi:10.1086/292815
* –––, 1989 [2009], “Deliberation and Democratic Legitimacy”, in *The Good Polity: Normative Analysis of the State*, Alan Hamlin and Phillip Petit (eds), New York: Blackwell, pp. 17–34. Reprinted in J. Cohen 2009: ch. 1.
* –––, 1996 [2009], “Procedure and Substance in Deliberative Democracy”, in *Democracy and Difference: Contesting the Boundaries of the Political*, Seyla Benhabib (ed.), Princeton, NJ: Princeton University Press, pp. 95–119. Reprinted in J. Cohen 2009: ch. 5.
* –––, 2009, *Philosophy, Politics, Democracy: Selected Essays*, Cambridge, MA: Harvard University Press.
* –––, 2010, *Rousseau: A Free Community of Equals*, (Founders of modern political and social thought), Oxford: Oxford University Press. doi:10.1093/acprof:oso/9780199581498.001.0001
* Cohen, Joshua and Charles Sabel, 1997 [2009], “Directly Deliberative Polyarchy”, *European Law Journal*, 3(4): 313–342. Reprinted in J. Cohen 2009: ch. 6.
* Cooper, John M., 1990, “Political Animals and Civic Friendship”, Aristoteles “Politik”: Akten des XI. Symposium Aristotelicum (Proceedings of the XIth Symposium Aristotelicum), Gunther Patzip (ed.), Friedrichshafen/Bodensee: Vandenhoeck & Ruprecht, pp. 220–241. Reprinted in *Aristotle’s Politics: Critical Essays*, Richard Kraut and Steven Skultety (eds), New York: Rowman & Littlefield Publishers, 2005.
* Dahl, Robert A., 1956, *A Preface to Democratic Theory*, Chicago: University of Chicago Press.
* –––, 1989, *Democracy and Its Critics*, New Haven, CT: Yale University Press.
* Dewey, John, 1927 [2012], *The Public and its Problems*, University Park: Penn State Press.
* Dworkin, Ronald, 1986, *Law’s Empire*, Cambridge, MA: Harvard University Press.
* Friedman, Milton, 1962, *Capitalism and Freedom*, Chicago: University of Chicago Press.
* Finnis, John, 1980, *Natural Law and Natural Rights*, Oxford: Oxford University Press.
* Gauthier, David P., 1986, *Morals by Agreement*, Oxford: Clarendon. doi:10.1093/0198249926.001.0001
* Gutman, Amy and Dennis Thompson, 1996, *Democracy and Disagreement*, Cambridge, MA: Harvard University Press.
* Habermas, Jürgen, 1981a [1984], *A Theory of Communicative Action* (*Theorie des kommunikativen Handelns*), vol. 1, *Reason and the Rationalization of Society*, Thomas McCarthy (trans.), Boston: Beacon Press. Originally published in German in 1981.
* –––, 1981b [1989], *A Theory of Communicative Action* (*Theorie des kommunikativen Handelns*), vol. 2, *Lifeworld and System: A Critique of Functionalist Reason*, Thomas McCarthy (trans.), Boston: Beacon Press. Originally published in German in 1981.
* –––, 1992 [1996], *Between Facts and Norms* (*Faktizität und Geltung*), William Rehg (trans.), Cambridge, MA: MIT Press. Originally published in German in 1992, Frankfurt am Main: Suhrkamp.
* Heath, Joseph, 2001, *Communicative Action and Rational Choice*, Cambridge, MA: MIT Press.
* –––, 2006, “The Benefits of Cooperation”, *Philosophy & Public Affairs*, 34(4): 313–351. doi:10.1111/j.1088-4963.2006.00073.x
* –––, 2014, *Morality, Competition and the Firm: The Market Failures Approach to Business Ethics*, New York: Oxford University Press. doi:10.1093/acprof:osobl/9780199990481.001.0001
* Hegel, G.W.F., 1821 [1991], *Elements of the Philosophy of Right* (*Grundlinien der Philosophie des Rechts*), Allen W. Wood (ed.), H.B. Nisbet (trans.), Cambridge: Cambridge University Press.
* Hobbes, Thomas, 1651 [1991], *Leviathan*, Richard Tuck (ed.), Cambridge: Cambridge University Press.
* Hussain, Waheed, 2012, “Is Ethical Consumerism an Impermissible Form of Vigilantism?”, *Philosophy & Public Affairs*, 40(2): 112–143. doi:10.1111/j.1088-4963.2012.01218.x
* –––, forthcoming. “Why should we care about competition?”, *Critical Review of International Social and Political Philosophy*, first published online: 08 Nov 2017. doi:10.1080/13698230.2017.1398859
* Kelly, Heather, 2015, “Zuckerberg pledges 99% of Facebook stock to charitable causes”, *CNN Money*, 2 December 2015. [[available online](http://money.cnn.com/2015/12/01/technology/zuckerberg-facebook-stock-daughter/)]
* Kraut, Richard, 2002, *Aristotle: Political Philosophy*, (Founders of modern political and social thought), New York: Oxford University Press.
* Locke, John, 1698 [1988], *Two Treatises of Government*, Peter Laslett (ed.), Cambridge: Cambridge University Press.
* Lewis, David K., 1969, *Convention: A Philosophical Study*, Cambridge, MA: Harvard University Press.
* Marx, Karl, 1844 [1978], “On the Jewish Question” (*Zur Judenfrage*), in *The Marx-Engels Reader*, 2nd edition, Robert C. Tucker (ed.), New York: W. W. Norton.
* –––, 1867 [1967], *Capital* (*Das Kapital*), volume one, New York: International Publishers
* McMahon, Christopher, 2013, *Public Capitalism: The Political Authority of Corporate Executives*, Philadelphia: University of Pennsylvania Press.
* Mill, John Stuart, 1859, “On Liberty”, Reprinted in Mill 2015.
* –––, 1862, “Considerations on Representative Government”, Reprinted in Mill 2015.
* –––, 2015, *On Liberty, Utilitarianism, and other Essays*, Mark Philp and Frederick Rosen (eds), Oxford: Oxford University Press.
* Nagel, Thomas, 1986, *The View from Nowhere*, Oxford: Oxford University Press.
* Nagle, John Copeland, 2015, “Pope Francis, Environmental Anthropologist”, *Regent University Law Review*, 28(1): 7–47.
* Nozick, Robert, 1974, *Anarchy, State, and Utopia*, New York: Basic Books.
* Olson, Mancur, 1965, *The Logic of Collective Action: Public Goods and the Theory of Groups*, revised edition, Cambridge, MA: Harvard University Press. Revised edition, 1971.
* Ostrom, Elinor, 1990, *Governing the Commons: The Evolution of Institutions for Collective Action*, Cambridge: Cambridge University Press.
* Plato, *The Republic of Plato*, Allan Bloom (trans.), 2nd edition, New York: Basic Books, 1991.
* Rawls, John, 1971 [1999], *A Theory of Justice*, Cambridge, MA: Harvard University Press. Page numbers from the revised edition 1999.
* –––, 1982 [2005], “The Basic Liberties and Their Priority”, *Tanner Lectures on Human Values*, volume 3, Salt Lake City: University of Utah Press, pp. 3–87. Reprinted in Rawls 1993 [2005].
* –––, 1988 [2005], “Priority of Right and Ideas of the Good”, *Philosophy & Public Affairs*, 17(4): 251–276. Reprinted in Rawls 1993 [2005].
* –––, 1993 [2005], *Political Liberalism*, New York: Columbia University Press. Expanded edition 2005.
* Rousseau, Jean-Jacques, 1758, “Lettre a M. D'Alembert sur les spectacles”. Translated in *Politics and the Arts: Letter to M. D’Alembert on the Theatre*, Alan Bloom (trans.), New York: Free Press, 1960.
* –––, 1762a [1979], *Émile, ou De l’éducation*, Translated as *Emile: or, On Education*, Alan Bloom (trans.), New York: Basic Books.
* –––, 1762b [1997], *Du contrat social; ou Principes du droit politique* (On the Social Contract), France. Translated in Rousseau 1997: 39–152.
* –––, 1772 [1997], “Considérations sur le gouvernement de Pologne” (Considerations on the Government of Poland). Translated in Rousseau 1997: 177–260.
* –––, 1997, *‘The Social Contract’ and Other Later Political Writings*, (Cambridge Texts in the History of Political Thought), Victor Gourevitch (ed./trans.), Cambridge: Cambridge University Press.
* Sandel, Michael J., 2005, *Public Philosophy: Essays on Morality in Politics*, Cambridge, MA: Harvard University Press.
* –––, 2009, *Justice: What’s the Right Thing to Do?*, New York: Farrar, Straus and Giroux.
* Scanlon, Thomas M., 1977 [2003], “Due Process”, *NOMOS*, 18: 93–125. Reprinted in his *The Difficulty of Tolerance: Essays in Political Philosophy*, Cambridge: Cambridge University Press, 2003, pp. 42–69.
* –––, 1998, *What We Owe to Each Other*, Cambridge, MA: Harvard University Press.
* Schofield, Malcolm, 2006, *Plato: Political Philosophy*, (Founders of modern political and social thought), New York: Oxford University Press.
* Sen, Amartya, 1993 [2002], “Positional Objectivity”, *Philosophy & Public Affairs*, 22(2): 126–145. Reprinted in his *Rationality and Freedom*, Cambridge, MA: Harvard University Press.
* Sidgwick, Henry, 1874, *The Methods of Ethics*, London: Macmillan & Co.; 7th edition, 1907, London: Macmillan; 7th edition reprinted (with a foreword by John Rawls) by Indianapolis: Hackett, 1981.
* Smith, Adam, 1776 [2000], *The Wealth of Nations*, New York: Modern Library.
* Smith, Thomas W., 1999, “Aristotle on the Conditions for and the Limits of the Common Good”, *American Political Science Review*, 93(3): 625–636. doi:10.2307/2585578
* Taylor, Charles, 1984 [1985], “Kant’s Theory of Freedom”, in *Conceptions of Liberty in Political Philosophy*, Zbigniew A. Pelczynski and John Gray (eds), New York: St. Martin's Press: 100–121. Reprinted in *Philosophy and the Human Sciences*, (Philosophical Papers, volume 2), Cambridge: Cambridge University Press, pp. 318–338. doi:10.1017/CBO9781139173490.013
* Taylor, Michael, 1987, *The Possibility of Cooperation*, Cambridge: Cambridge University Press.
* Tocqueville, Alexis de, 1835–1840, *Democracy in America* (*De la démocratie en Amérique*), J.P. Mayer (ed.) and George Lawrence (trans.), 2 volumes, New York: Harper & Rowe, 1966.
* Vlastos, Gregory, 1999, “The Individual as an Object of Love in Plato”, in *Plato 2: Ethics, Politics, Religion, and the Soul*, Gail Fine, Oxford: Oxford University Press, ch. 5.
* Waldron, Jeremy, 1988, “When Justice Replaces Affection: The Need for Rights”, *Harvard Journal of Law & Public Policy*, 11(3): 625–648. Reprinted in Waldron 1993.
* –––, 1990 [1993], “Rights and Majorities: Rousseau Revisited”, *NOMOS*, 32: 44–75. Reprinted in Waldron 1993.
* –––, 1993, *Liberal Rights: Collected Papers 1981–1991*, Cambridge: Cambridge University Press.
* Walzer, Michael, 1970, *Obligations: Essays on Disobedience, War, and Citizenship*, Cambridge, MA: Harvard University Press.
* –––, 1983, *Spheres of Justice*, New York: Basic Books.
* Williams, Bernard, 1973, “A Critique of Utilitarianism”, in *Utilitarianism: For and Against*, J.J.C. Smart and Bernard Williams, Cambridge: Cambridge University Press. pp. 82–118. Reprinted as “Consequentialism and Integrity” in *Consequentialism and its Critics*, Samuel Scheffler (ed.), Oxford: Oxford University Press, 1988, pp. 20–50.

## Academic Tools

> | ![sep man icon](https://plato.stanford.edu/symbols/sepman-icon.jpg) | [How to cite this entry](https://plato.stanford.edu/cgi-bin/encyclopedia/archinfo.cgi?entry=common-good). |
> | --- | --- |
> | ![sep man icon](https://plato.stanford.edu/symbols/sepman-icon.jpg) | [Preview the PDF version of this entry](https://leibniz.stanford.edu/friends/preview/common-good/) at the [Friends of the SEP Society](https://leibniz.stanford.edu/friends/). |
> | ![inpho icon](https://plato.stanford.edu/symbols/inpho.png) | [Look up topics and thinkers related to this entry](https://www.inphoproject.org/entity?sep=common-good&redirect=True) at the Internet Philosophy Ontology Project (InPhO). |
> | ![phil papers icon](https://plato.stanford.edu/symbols/pp.gif) | [Enhanced bibliography for this entry](http://philpapers.org/sep/common-good/) at [PhilPapers](http://philpapers.org/), with links to its database. |

## Other Internet Resources

* [The Concept of the Common Good](https://www.britac.ac.uk/sites/default/files/Jaede.pdf), working paper by Maximilian Jaede (University of Edinburgh), at the British Academy project.
* [The Common Good](http://www.vatican.va/archive/ccc_css/archive/catechism/p3s1c2a2.htm), Section II of Article 2, from Part Three, Section One, Chapter Two of *Catechism of the Catholic Church*, maintained by the Vatican. (Contains an important religious statement about the common good.)
* [Catechism Commentary: The Common Good](https://catholicmoraltheology.com/catechism-commentary-the-common-good/), post by David Cloutier (Theology, Catholic University of America) at Catholic Moral Theology website.
* [Economy for the Common Good](https://www.ecogood.org/en/), a volunteer organization that advocates a model for a market economy organized around the idea of the common good.
* [The Economy for the Common Good](https://thenextsystem.org/the-economy-for-the-common-good), paper by Christian Felber (Vienna University of Economics and Business) and Gus Hagelberg (Coordinator for International Expansion of the Economy for the Common Good), at the Next System Project website.
* [Catholic Healthcare and the Common Good](https://www.chausa.org/publications/health-progress/article/may-june-1999/catholic-healthcare-and-the-common-good), by Rev. Charles E. Bouchard, OP, S.T.D., at the Catholic Health Association website. (Catholic statement about the common good in the context of doctor-patient relations.)
* [Government And The Common Good](https://www.aft.org/resolution/government-and-common-good), a resolution by the American Federation of Teachers.

## Related Entries

[Aristotle](https://plato.stanford.edu/entries/aristotle/) | [communitarianism](https://plato.stanford.edu/entries/communitarianism/) | [democracy](https://plato.stanford.edu/entries/democracy/) | [friendship](https://plato.stanford.edu/entries/friendship/) | [justice](https://plato.stanford.edu/entries/justice/) | [justice: distributive](https://plato.stanford.edu/entries/justice-distributive/) | [public goods](https://plato.stanford.edu/entries/public-goods/) | [Rawls, John](https://plato.stanford.edu/entries/rawls/) | [republicanism](https://plato.stanford.edu/entries/republicanism/) | [Rousseau, Jean Jacques](https://plato.stanford.edu/entries/rousseau/)

[Copyright © 2018](https://plato.stanford.edu/info.html#c) by  
Waheed Hussain
