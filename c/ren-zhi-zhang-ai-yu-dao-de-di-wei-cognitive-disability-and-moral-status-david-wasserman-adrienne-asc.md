# 认知障碍与道德地位 cognitive disability and moral status (David Wasserman, Adrienne Asch, Jeffrey Blustein, and Daniel Putnam)

_首次发布于 2012 年 7 月 6 日；实质性修订于 2017 年 8 月 11 日_

为什么认知障碍和道德地位被认为有足够的关联性，值得单独讨论呢？原因在于，患认知障碍的个体在关于自我意识和实践理性等智力属性对道德具有重要意义的讨论中被用作案例。如果相当大比例的人类缺乏自我意识和实践理性，那么这些属性本身就不能区分我们对待认知发育良好的人类与我们对待非人类动物和人类胎儿的方式。如果我们不能对缺乏这些属性的人类进行实验或杀害，那么仅仅缺乏这些属性本身不能成为正当化动物实验或堕胎的理由。

在很大程度上，考虑这些观点的哲学家们并不是主要关心认知障碍人类的对待或道德地位，他们试图挑战现有对待胎儿或动物的做法，或者这些做法的理由。但是这些观点对认知障碍人类有着重要的实际影响。如果对待生命存在某种方式的正当化在某种程度上取决于它们是否具有智力属性，那么对待认知障碍人类的方式可能是可以接受的，而这种方式对待认知非障碍人类则是不可接受的。这一含义，是对动物权利和堕胎的讨论中的一种哲学反击，已经成为应用伦理学中持续争议的主题。

哲学家们经常将具有最显著认知障碍的人与声称具有类似或更大认知能力的动物进行比较（McMahan 1996, 2002, 2009; Singer 1993, 2009; 以及 Wilkinson 2008）。一些批评者认为这些比较是不必要且令人反感的（例如，Carlson 2009; Carlson 和 Kittay 2009）。提出这种比较的哲学家强调以下对比：大量的黑猩猩和其他“较高级”的灵长类动物被用于痛苦且常常致命的研究，以造福人类。尽管针对特定灵长类动物研究项目和特定灵长类动物的研究存在强烈反对意见，但普遍认为，如果大部分灵长类动物研究有可能对人类健康产生重大贡献，并且对动物实验对象的伤害和风险被最小化，那么这样的研究是可以接受的。相比之下，认知障碍的人类不能参加潜在有害的研究，除非他们可能受益，伤害风险微乎其微，并且他们的法定代表同意他们的参与。即使他们不比非人类灵长类动物更能理解研究的目的或同意参与，他们也享有这些保护措施。

辩论关于认知障碍最严重个体的道德地位也引发了关于依赖直觉、信念和深思熟虑的方法论问题，以评估道德论证。一些哲学家会否认任何论证应该说服我们放弃我们的信念，即让一个认知能力无法同意的人遭受对他毫无益处的痛苦和危险实验是非常错误的（例如，Kittay 2008）。两位哲学家改编了 G.E. Moore 关于外部世界存在的“证明”（参见[George Edward Moore](https://plato.stanford.edu/entries/moore/)条目）来主张我们对所有人类在道德上平等且道德上优于其他动物这一命题的真实性更有信心，而不是对否定它的论证的有效性（Curtis 和 Vehmas，2016a，2016b）。其他哲学家拒绝将 Moore 的论证应用于道德地位（Lachlan，2016；Roberts 即将发表）；更广泛地说，他们坚持认为即使是关于道德地位的坚定信念也不能免受批判性审查，特别是如果它们似乎与其他深刻的信念相冲突的话（McMahan 2007）。还有一些人否认这种信念具有任何推定的重量或权威（Singer 2005）。

最后，在单独的条目中讨论认知障碍人类的道德地位，而不是在残疾的一般条目中，我们并不支持有关认知障碍的有争议的“例外论”——一种认为认知障碍与其他类型的障碍根本不同的观点。我们之所以限制讨论认知障碍，是 dialectical 的：目前没有关于非认知障碍个体的道德地位的争论。我们不知道有哪位认真的哲学家会认为那些看不见、听不见、或无法使用他们的腿，或者经常感到抑郁或幻觉的人，比没有这些障碍的人道德地位更低。诚然，一些哲学家声称认为身体或精神障碍的人与无障碍的人具有相同的道德地位，但同时持有一些其他哲学家认为与坚持平等道德地位不一致的立场。一个显著的例子是 Rawls（1971）排除患有身体残疾的人不符合完全合作社会成员的假设，因此不被纳入原始立场。另一个例子是在分配有限的医疗资源时捍卫“质量调整”，这种做法折算残疾人的寿命年限，以反映他们据称较低的生活质量（Williams 1987）。但无论这些立场是否与承认完全道德地位一致，它们的支持者坚持认为是一致的；他们并不试图反对认为身体或（大多数）精神障碍的人与无障碍人具有相同的道德地位。相比之下，具有认知障碍的人类的道德地位已经成为哲学家、应用伦理学家和残疾学者之间激烈辩论的话题。

我们将按以下步骤进行。我们将首先描述道德地位辩论的主体——那些我们称之为“患有极端认知障碍”的人类。在讨论人类被归类为认知障碍的方式之后，我们将描述辩论涉及的人类更狭窄的范畴。需要明确的是，这是一组被规定存在的人类，而不是通过实证程序分类的。我们将指出很难区分关于这些被规定个体的说法和关于一些实际个体是否满足该规定的说法。接下来，我们将描述道德地位的概念，描述其结构和功能。然后，我们将概述对该概念理解的主要差异，特别是其“包含标准”——用于归因我们所称的“完全道德地位”的标准，这种地位被归属于认知非障碍的成年人类。 （在称该道德地位为“完全”时，我们并不打算在关于是否存在更高道德地位的争论中站队。）我们将确定一类道德地位观点家族——那些将拥有视为个体属性的人作为基础的观点——作为本文所讨论的挑战的主要来源，尽管不是唯一来源：即一些人类缺乏完全道德地位的说法。在概述这些观点之后，我们将回顾几种应对挑战的方式：1）将完全道德地位基于更多人类共享的个体属性；2）采用关于拥有完全道德地位的次要依据——“礼貌”或“代理”；3）拒绝个体属性观点，支持将所有人类的完全道德地位基于其物种成员资格或人类身份的观点。

* [2. 什么是道德地位?](https://plato.stanford.edu/entries/cognitive-disability/#WhaMorSta)
* [2.1 道德地位的概念](https://plato.stanford.edu/entries/cognitive-disability/#ConMorSta)
* [2.2 道德地位的标准](https://plato.stanford.edu/entries/cognitive-disability/#CriForMorSta)
* [3. 基于个体的观点](https://plato.stanford.edu/entries/cognitive-disability/#IndBasAcc)
* [4. 基于个体的观点和激进认知障碍: 排斥的挑战](https://plato.stanford.edu/entries/cognitive-disability/#IndBasAccRadCogDisChaExc)
* [4.1 确定更具包容性的属性](https://plato.stanford.edu/entries/cognitive-disability/#IdeIncAtt)
* [4.2 代理、派生和推定道德地位](https://plato.stanford.edu/entries/cognitive-disability/#ProDerPreMorSta)
* [5. 基于群体的道德地位观](https://plato.stanford.edu/entries/cognitive-disability/#GroBasAccMorSta)
* [5.1 物种为基础的观点](https://plato.stanford.edu/entries/cognitive-disability/#SpeBasAcc)
* [5.2 人性作为完全道德地位的基础](https://plato.stanford.edu/entries/cognitive-disability/#HumGroFulMorSta)
* [6. 结论](https://plato.stanford.edu/entries/cognitive-disability/#Con)
* [参考文献](https://plato.stanford.edu/entries/cognitive-disability/#Bib)
* [学术工具](https://plato.stanford.edu/entries/cognitive-disability/#Aca)
* [其他互联网资源](https://plato.stanford.edu/entries/cognitive-disability/#Oth)
* [相关条目](https://plato.stanford.edu/entries/cognitive-disability/#Rel)

***

## 1. 辩论的对象是谁？

有关如何定义认知或智力障碍的争议并不令人意外。（我们将这些术语互换使用，忽略它们在广度和重点上的明显差异，除非特别相关。）存在竞争的心理测量和功能性定义，分别基于智力测试得分与平均值的标准偏差，以及“智力功能和适应行为方面存在显著限制，涵盖许多日常社交和实用技能”（美国智力和发展残疾协会，2011 年）。在本条目中，我们将考虑在功能方面被定义为认知障碍的个体，因为我们感兴趣的是关键认知功能的缺失或显著限制是否具有道德相关性。这些限制更可能被功能性测试捕捉到，尽管正如我们后面讨论的那样，前者和后者都可能无法检测到重要的智力能力。我们不会假设或检查那些在心理测量上被定义为“严重”或“深度”智力障碍的个体是否以这种方式在功能上存在障碍。

正如所指出的，本文将重点关注那些具有“根本认知障碍”的人类 — 即智力功能和能力受限或无法发展一个或多个被认为赋予完全道德地位的属性。这些属性包括意识到自己作为一个时间延伸的存在；实践理性 — 通过思考如何行动来管理自己行为的能力；以及制定和回应道德要求的能力。这些属性，以及其他被认为是获得完全道德地位所必需的属性，可能被不同子群的人类所拥有，而这些属性之间的关系是一个颇具争议的问题。然而，这种争议最好推迟到对道德地位基础的更充分探讨；我们将仅讨论它们对具有重大认知障碍的人类的道德地位产生影响的程度。

The category of “radical cognitive disability” is stipulative. We do not start with the assumption that any specific human being falls into that category, or even that some human beings do. Eva Kittay (2005) has argued that there is no reason to assume that any human beings are radically cognitively disabled, in the sense we are using the term. Jeff McMahan (2009) has countered that the existence of such human beings is very likely: given the continuous nature of fetal and infant neurological development, it is very likely that some human beings are radically disabled because their development has ceased or been interrupted at points where they have not yet acquired morally relevant capacities. Even if McMahan’s “existence argument” for radical impairment is correct, however, it does not remove the daunting uncertainty of attributing radical cognitive impairment to any actual human being.

This uncertainty arises in part from a lack of clarity and consensus about what would count as adequate evidence of self- and other-awareness and practical rationality. To resolve that question, it is necessary to confront not only interpretive ambiguities in making inferences about the content of other minds, but conceptual issues about what it means to possess critical cognitive functions. Similar issues are raised in the debate about animal consciousness (see the Entry on [animal consciousness](https://plato.stanford.edu/entries/consciousness-animal/)), but there is also disagreement about whether the kinds of evidence adduced for and against the cognitive capacities of animals are equally relevant in assessing the cognitive capacities of human beings (Kittay 2005; McMahan 2002, 2005, 2008).

The primary reason for restricting the entry to radical cognitive disability, like the reason for restricting it to cognitive disability, is dialectical. Few contemporary philosophers would deny that human beings with mild or moderate cognitive disabilities have the attributes required for what we call “full moral status.” Philosophers who see self-consciousness and practical rationality as necessary for that status generally recognize that mildly and moderately disabled humans possess those attributes, and possess them to the extent necessary to reach the threshold set by their accounts of full moral status (see [Sec. 2.1](https://plato.stanford.edu/entries/cognitive-disability/#ConMorSta)).

By stipulating a category of human beings with radical cognitive disabilities, we seek to avoid difficult empirical issues about the extent to which individuals classified as having serious cognitive disabilities actually lack the psychological functions held to confer moral status. Even the most direct assessments of these functions may fail to recognize atypical, especially nonverbal, forms of cognitive functioning. And with the possible exception of extreme cases like anencephaly, there are formidable difficulties in inferring a lack of cognitive capacity from a lack of specific behavior or brain activity. As recent work on intellectually conscious states has revealed (Wannez et al. 2017, Real et al. 2015), some psychological capacities are difficult to detect neurologically or behaviorally.

我们意识到有人反对使用规定来回避这些棘手的经验问题。残障学者坚持认为哲学家必须意识到他们使用的术语将不可避免地被视为实际的人类，因此他们无法规定消除有关伤害和误解的担忧。此外，实际满足他们规定的个体的普遍性与道德上重要的政策问题相关，比如更具包容性的教育实践的成本和收益。然而，我们将依赖于我们对激进认知障碍所做的规定，部分原因是一些认为某些人类存在激进残障的人也认同这一假设，他们主张所有人类都应享有完整的道德地位。

我们将仅讨论先天性或早发的激进认知障碍。道德地位如何受到重要认知功能丧失的影响是一个独立的问题，无论是对于那些认为这些功能赋予了道德地位的人，还是对于那些不这样认为的人。对于前者，问题是道德地位是否能够在赋予其地位的基础功能丧失后继续存在。对于那些否认道德地位需要拥有这些功能的人来说，这些功能的丧失仍可能引发重要问题，涉及个人身份的问题，以及早期非残障自我的第一人称权威与后来残障自我的关系。对于处于极度昏迷状态或严重痴呆症的“曾有能力”的个体的处理，因此引发了关于心理不连续性或精神功能丧失对人类个体与其过去或未来自我的关系的不同问题。关于这些问题已经存在长期的争议和大量的文献，它们值得单独讨论。

## 2. 什么是道德地位？

### 2.1 道德地位的概念

“道德地位”并非所有伦理学理论共享词汇的一部分。例如，一个行为功利主义者对于这个概念的用处不比对“尊重”、“权利”或“不可侵犯”更有用。对于一个只按照它们享受或产生的效用比例给予生物权重的理论来说，道德地位的概念只在间接意义上相关，因为它的使用会以各种方式影响总体效用。例如，否定某些人类拥有完全道德地位可能导致的负效用可能支持一项政策，即将所有人类都视为拥有完全道德地位（见[第 4 节](https://plato.stanford.edu/entries/cognitive-disability/#IndBasAccRadCogDisChaExc)）。在这样的理论中，一个个体最显著的道德属性是其能够处于可被描述为“好”或“坏”的状态的能力，其中最明显的是快乐和痛苦。拥有这种能力使个体在占据或避免这些状态方面拥有一种在道德上值得考虑的利益。道德值得考虑性是：1）连续的，即一个生物所拥有的道德值得考虑程度与其利益的强度、性质和数量成比例地变化；以及 2）不对称的，即一个生物可能对其他生物有道德要求，而其他生物却没有对它有道德要求。

当非功利主义哲学家讨论某种生物的“道德地位”时，他们通常使用的是比道德可考虑性更为范畴化的概念。他们通常将道德地位理解为一个阈值概念和一个范围概念。那些低于某种赋予地位属性（如理性）的最低水平——阈值——的生物，尽管在某种程度上具备这种属性，却缺乏某种道德地位。而所有落在“范围”内——达到属性的阈值水平的生物——无论超过阈值多远，都具有相同的道德地位。（“范围概念”一词来自罗尔斯；他的例子是圆圈内的各点，尽管距离圆周的距离不同，但它们都同样“在内部”）。更具争议的是，道德地位有时被视为对称的：一个生物必须能够对其提出道德要求（因此具备责任能力），也必须能够对其他生物提出道德要求。对称条件将排除任何无法对其提出道德要求的人类——不仅是患有严重认知障碍的个体，还包括婴儿和幼儿。

并非所有关于道德地位的作家都将其视为一个阈值和范围概念。这个概念的两个特征都受到了质疑，并且这个概念本身被批评为等级制度和精英主义（Birch 1993）。一些哲学家主张将道德地位视为一个程度问题，因此一个生物的道德地位与其具有的与道德相关的属性成比例地变化（Perring 1997; DeGrazia 2008）。这些提议避免了令人不安的尖锐二分法，但它们具有修正性质。即使常识道德承认道德地位存在一些渐变，但已经确立的社会实践，如动物研究，却假定了不连续性。此外，即使常识可以与阈值以下的渐变相调和（从而阈值变得不那么明显），它也会对识别超过阈值的渐变感到犹豫，从而放弃道德地位作为一个范围概念。

更全面地说，玛丽·安·沃伦提出了一个道德地位的多标准账户，其中它既不是一个范围概念，也不是一个阈值概念。对于沃伦来说，一个生物的道德地位随着它拥有赋予地位的属性的程度而变化（Warren 1997）。但沃伦坚持认为，“完全的道德地位”既是一个阈值概念，也是一个范围概念；具有道德代理能力的生物享有相同的、最高的道德地位，无论它们拥有道德代理所需属性的程度如何。正如前面所指出的，我们将使用术语“完全的道德地位”来指代通常赋予认知障碍的人类成年人的道德地位，而不假设没有更高的地位。因此，“完全的道德地位”在关于胎儿或更高级灵长类动物是否为人的争论中，大致等同于“人格”。

道德地位的问题，以及哪些生物拥有完全的道德地位，已经变得重要，因为这关系到这些生物的权利和待遇。“道德地位”和“完全的道德地位”并非作为尊称；将其归于某个生物意味着它享有其他缺乏该地位的生物所没有的权利。完全道德地位所蕴含的特定权利可能会因不同的道德理论而有所不同。具有相同道德地位概念和相同归属标准的两个理论（见[Sec. 2.2](https://plato.stanford.edu/entries/cognitive-disability/#CriForMorSta)）可能会将完全道德地位与不同的权利或不同强度的权利联系起来。例如，一个理论可能认为拥有完全道德地位的生物永远不能被当作单纯手段使用，而另一个理论可能认为在某些紧急情况下可以这样使用。然而，在道德地位的形式特征和标准之间，以及由此类地位蕴含的权利之间，只存在部分独立性。例如，一个理论似乎不太可能承认道德地位的范畴概念，同时又拒绝任何权利概念。对道德地位的描述不必否认低于其设定门槛的生物的所有权利；它们只是缺乏与高于该门槛的生物所拥有的相同权利包。

两位哲学家（Sachs, 2011; Silvers, 2011）质疑了“道德地位”概念的实用性。Sachs 认为，关于道德地位的主张是不必要且令人困惑的，因为这场辩论实际上是关于证明或确立特定权利所需的具体属性。Silvers 提出了更严厉的批评。她指责那些支持基于心理和生物标准的人（见[Sec. 4](https://plato.stanford.edu/entries/cognitive-disability/#IndBasAccRadCogDisChaExc)，[Sec. 5](https://plato.stanford.edu/entries/cognitive-disability/#GroBasAccMorSta)，以下）忽视了相关特征的渐变，并忽略了展示或发展这些特征的潜力，导致了不应排除的结果。作为一种替代方案，Silvers 提出了一个关于正义的观点，避免了关于能力的先入之见，试图在社会合作方案中包容尽可能多的生命体。

虽然道德地位主张往往更多地是模糊的而不是启示性的，但它们并不仅仅涉及将特定权利基于特定属性。首先，正如上文所指出的，那些共享完全道德地位标准的理论在他们所关联的权利方面可能存在差异，反之亦然。此外，即使 Silvers 关于道德地位概念的排他倾向是正确的，先前的问题仍然是它们的门槛和范围特征是否能够被证明是合理的。在将一个存在置于门槛之上或之下的在道德上相关属性中的微小差异如何能够构成该存在所拥有的权利和豁免权包裹中的巨大差异？尽管在将他们置于门槛之上的属性上存在巨大差异，所有认知正常的人类如何能够拥有相同的权利和豁免权包裹？那些捍卫道德地位标准概念的人需要一个道德理论来支持其范围和门槛特征；那些攻击它的人需要否认任何合理的理论可以这样做。

### 2.2 道德地位的标准

辩论关于患有严重认知障碍的人类是否具有完整道德地位的核心问题在于道德地位的基础属性类型。大多数否认或质疑所有人类完整道德地位的人坚持认为道德地位必须仅基于个体的属性或特征；这些属性必须能够通过对该个体的生物学或心理学清单进行识别，而不能参考其所处的生物学或社会环境。例如，可以评估一个个体的自我意识或实践理性，而不知道它的同类（尽管评估本身可能需要由具有自我意识和实践理性的个体进行）。相比之下，我们无法在不了解其所处世界的情况下确定一个个体的物种成员资格或社会关系。我们将基于个体属性的解释称为“基于个体”；我们将基于个体在生物或社会群体中的成员资格或与该群体其他成员的关系的事实的解释标记为“基于群体”。这些事实可能涉及个体的生物起源，她所属生物或社会群体的典型或平均特征，或者她与其他个体的实际关系。尽管一些基于个体的解释涉及个体与其他个体形成某种关系的能力或潜力，但正是这种能力或潜力赋予了道德地位，而不是这种关系的存在。

那些坚持道德地位必须基于个体自身属性的人，认为道德地位不能取决于出生的偶然性或生物分类的变幻，也很可能坚持认为个体对他人的要求不能取决于那些他人恰好是同一物种或社区的成员。在这种观点中，完整的道德地位不仅必须独立于外部环境，而且必须得到普遍承认。尽管一些基于群体的观点可能要求一个生物的完整道德地位得到普遍承认，但其他人将完整的道德地位视为相对于群体的（见[Sec. 5.1](https://plato.stanford.edu/entries/cognitive-disability/#SpeBasAcc)）。

在比较道德地位的描述时，有两个可能的差异需要注意。首先，它们可能提出不同的条件作为完全道德地位的充分条件。例如，一些描述声称，作为人类物种的成员就足以获得这种地位。其次，它们可能对如何满足这些条件形成或证明完全道德地位提出不同的解释。例如，即使作为智人就足以拥有完全道德地位，也可能有不同的理由解释为什么它足够：1）因为作为智人的本性是理性的；2）因为该物种的理性本质要求尊重其所有成员，无论其是否理性；或者 3）因为同一物种的成员对彼此有一种偏爱的责任，无论其个体属性如何。因此，这两种描述将道德地位基于相同的关系属性——作为人类物种的成员，并且使成员成为完全道德地位的充分条件。但它们在证明道德地位时对物种成员资格的使用方式有所不同。它们也可能将不同的权利与完全道德地位联系起来，与它们不同的基础方式相一致。正如在[Sec. 4](https://plato.stanford.edu/entries/cognitive-disability/#IndBasAccRadCogDisChaExc)中讨论的那样，它们对于谁有责任承认或尊重个体地位可能会有不同的含义。

## 3. 基于个体的描述

个体为基础的观点通常以认知正常的成年人类为典范，并挑选出他们的一个或多个属性作为他们所享有的道德地位的充分条件。这些观点确定了心理和认知属性的重叠群集——自我意识，意识到并关心自己作为一个时间延伸的主体；实践理性，理性代理，或自主性；道德责任；一种能够认识其他自我并且被激励为向他们证明自己行为的能力；有能力被道德上追究，以及追究他人的能力。这些属性挑选出人类的不同子集。例如，一些有自我意识的人可能缺乏被道德上追究的能力。但是，个体为基础的观点在给予完全道德地位的生物范围上的差异可能不如它们认为这些属性作为地位基础的方式那样大。

一种方法认为心理属性通过其引发的利益赋予道德地位。这种方法将心理属性视为我们有责任促进的利益的基础，或者至少不阻挠的利益——这种责任的强度随着这些利益的强度和其他特征而变化。感受疼痛的能力构成了避免施加疼痛的责任；预期和害怕以及感受疼痛的能力可能构成更强的责任。但是，如果额外的心理能力仅仅增加了利益的强度，以及相应的促进或不阻挠它的责任，那么一个利益账户将无法为道德地位的门槛提供基础。

一个根据利益强度赋予道德重要性的观点不需要道德地位的概念，即使某种类型的存在往往具有更强的利益。因此，那些用不平等利益来解释我们对不同物种存在的不同义务的哲学家往往认为“道德地位”是一个不必要或无效的概念（Harman，2003 年，187 页），或者得出结论认为所有具有利益的存在（其中通常认为需要有感知能力）具有相同的道德地位（Rossi，2010 年）。然而，对不同道德地位的否定使得如果假定认知障碍最严重的人存在利益较弱，尤其是在生存方面，就可以不平等地对待他们。因此，这对所有人类的道德平等辩护者来说并不令人欣慰。

更加分类的基于利益的方法将道德地位建立在作为一个时间延伸存在并且生活可以变得更好或更糟的能力上（Singer 1993; Tooley 1983），或者价值自己存在的能力上（Harris 1985; Newson 2007）。具有这些能力的个体可以关心和珍视他们未来生活的方式，而那些缺乏这些能力的个体则不能，这使得他们对这些生活具有更重要的利益（McMahan 2002）。无论这些能力是否能解释完整道德地位的门槛和范围特征，将它们作为标准的采纳似乎否定了将最严重认知障碍的人类排除在完整道德地位之外。

一种源自康德或受其启发的第二种方法认为，道德地位是根据拥有自主权或道德责任等一个或多个属性所要求的尊重来界定的（例如，Korsgaard 1996）。这种方法认为，拥有自主意志赋予尊严并要求尊重，因此具有这种意志的存在不得被视为一种手段，而应被视为一种目的。康德对完全道德地位的这种构想通常被视为一种范例，因为它确定了一个不连续变化的属性，并且其拥有似乎具有明显的道德含义。许多康德主义观点设定的道德地位门槛很高。如果这些观点认为自主能力是完全道德地位的门槛，并且如果他们理解这种能力是基于道德责任的道德地位，那么由于“我们不会让人类婴儿或严重认知障碍的成年人为其行为负道德责任，因此，据说这样的人类缺乏康德式的道德地位”（Kain 2009, 66）。即使人类不需要为享有完全道德地位而负道德责任，许多人仍然缺乏似乎在任何康德主义观点中都是必不可少的自主能力。

一种与契约主义相关的第三种方法认为，道德地位取决于成为道德共同体成员所需的属性，或者参与相互承认和关注关系所需的属性。[\[7\]](https://plato.stanford.edu/entries/cognitive-disability/notes.html#note-7)关键在于具备建立这种关系的能力，而不是实际建立这种关系，这是道德地位的基础。即使一个认知正常的人被遗弃在荒岛上，他也会拥有这样的地位。这种方法最为对称，将某些属性视为道德地位所必需的，并非仅因为拥有这些属性就产生道德义务，而是因为它们是这种方法的提倡者理解道德义务的关系的必要条件。这一要求似乎赋予认知和心理属性比第二种方法赋予它们的更多的工具性角色。并不是因为仅仅拥有这些属性就要求尊重，而是因为它们使其拥有者能够建立相互尊重是其中一个组成部分的关系。

虽然基于关系的观点在概念上与基于尊重的观点有所不同，但在实际方面它们之间的差异很小。只有当人类具有尊重所必需的认知或其他心理能力，同时缺乏加入道德共同体所必需的共情或动机时，它们才会将不同的人类视为具有完全的道德地位（或反之亦然）。例如，基于道德关系的观点可能会排除精神病患者。但如果基于尊重的观点否定精神病患者的自主权，因为他们缺乏被责任感驱使的能力，或者更广泛地说，缺乏认识和行动道德理由的能力（参见 Shoemaker 2007），那么基于尊重的观点也可能会排除精神病患者。如果将他们的道德缺陷归因于实际理性方面的严重障碍，基于尊重的观点也可能会排除精神病患者。基于关系的观点将会像康德主义的观点一样，对完全的道德地位设定非常高的门槛。关系所要求的道德问责度越大，要声称患有严重认知障碍的人类有能力参与可能就越困难。

一个挑战是要确定一个或多个属性，这些属性可以解释，或至少与当前道德地位概念的阈值和范围特征相协调。阈值带来的困难在于它对心理连续属性施加了道德不连续性。与拥有灵魂相比，例如，实践理性、道德责任以及当代账户声称作为道德地位基础的大多数其他个体属性，似乎是有程度之分的。看着一个婴儿的发展，这些属性的获得似乎是逐渐的，即使其步伐不均匀。然而，我们对道德地位的判断是范畴性的——一个个体要么具有完全的道德地位，要么缺乏道德地位。道德地位的范畴性特征在阈值之上也是明显的。我们不认为更聪明、更深刻的自我意识，或者比我们其他人更完全自治的人，在道德地位上比其他人具有更高的地位，即使是那些接近边缘的人也是如此。证明完全道德地位的这种范围特征的挑战与证明阈值的挑战密切相关——为什么在阈值之上的差异在道德上是无关紧要的，当阈值本身是如此重要时，为什么在道德上是无关紧要的？

## 4. 基于个体的账户和激进认知障碍：排斥的挑战

如果完全的道德地位是由最后一节讨论的任何认知属性的拥有来决定的，那么这种地位将被一些非人类动物所享有，而且——更具问题性——几乎可以肯定地被一些人类所缺乏。大多数支持基于个体属性的观点的人都欢迎这样一个含义，即我们无法证明我们对“更高级”动物和认知障碍人类之间的待遇差异的存在，许多人认为，这些差异最好通过提高我们对非人类动物的标准来减少，而不是通过降低对认知障碍人类的标准来减少。

> 收敛的最佳点…要求对动物的传统信念进行比对传统对严重智障的看法更广泛的修订。(McMahan 2002, 230)

但正如麦克马汉所认识到的那样，即使完全收敛，也会产生令人不安的影响，即使完全通过提升对非人类动物的对待来实现：

%%

Convergence at a significantly lower level would have equally unacceptable implications. For example, it would permit the use of radically impaired human beings in any research, however harmful, for which the use of animals with comparable cognitive capacities was permitted. Any view about moral status that aspires to reflective equilibrium with our deeply held moral convictions must confront the abhorrence with which most thoughtful people would regard the practical implications of treating humans with radical cognitive disabilities as having even slightly lower moral status than the rest of us.

A variety of approaches seek to address that abhorrence: by identifying criteria for full moral status that include a wider range of humanity ([Sec. 4.1](https://plato.stanford.edu/entries/cognitive-disability/#IdeIncAtt)); by expanding the ways in which an individual can possess or achieve full moral status without denying the primacy of individual attributes ([Sec. 4.2](https://plato.stanford.edu/entries/cognitive-disability/#ProDerPreMorSta)); and by rejecting individual attribute accounts of moral status ([Sec. 5](https://plato.stanford.edu/entries/cognitive-disability/#GroBasAccMorSta)).

### 4.1 确定更具包容性的属性

有几种观点确定了一些属性，比如价值或关怀的能力，这些属性被更多人类所共享，而不是自我意识、实践理性、自治或道德问责能力。这些观点试图承认所有或几乎所有人类，包括儿童以及具有重大认知和心理障碍的成年人，拥有完整和平等的道德地位。提出的更具包容性标准包括沟通的能力，或者与其他人最低限度的沟通（分别由 Berube 1996; Francis 和 Norman 1978 提出）；价值或关怀（Jaworska 1999, 2007）；以及给予和接受爱（Kittay 1999）；建立以关怀相互回报为特征的关系的能力（Mullin 2011）。Jaworska 和 Tannenbaum（2016a，2016b）也许提供了最详细的解释，将道德地位基于形成具有道德意义的关系的能力。他们认为，如果一个生物有能力通过抚养关系实现“人类具有典型心理属性的活动的不完全实现”（他们称之为“独立人”（SSP））（2016b，1098），那么它就“应该获得更高的道德地位”。实际的抚养关系并非必需，但只有能够模仿典型 SSP 活动的生物才具备必要的能力。这排除了“能够进行基本意识活动但仍然无法以任何形式模仿 SSP 活动的人类”（2016b，1101）——不仅包括早期胎儿，还包括那些具有最严重认知障碍的儿童和成年人，他们永远不会具备那种能力。

这些观点在确定可能提供比实践理性或自我意识更直观吸引人的道德地位基础方面具有吸引力，并在不同程度上减少了被排除在完全道德地位之外的人类比例。但是，由于它们排除了一些人类，并可能包括一些非人类动物，它们并没有完全捕捉到关于道德地位的常识观点，因此对一些哲学家来说是不可接受的。此外，这些替代属性并没有解决完全道德地位的门槛和范围特征的问题，而只是将这个问题转移到了其他地方。

一个属性——任何其他被认为足以构成完全道德地位的属性的_潜力_——更接近完全包容（Kumar 2008）。但它面临三个严峻的问题。正如乔尔·费因伯格（1986）所说，某人仅仅具有某种属性的潜力并不足以使我们将其视为实际拥有该属性。如果潜力具有道德意义，那么它不能直接从其实现的那个具有道德意义的属性中推断出来。此外，一些人类永远不会具有任何一个被认为足以构成完全道德地位的单个属性的潜力，无论从何种“潜力”的意义上来区分它们与许多非人类动物（McMahan 2008, 91–92）。正如关于胎儿道德地位的辩论所暗示的，何时一个生命体具有某种属性的“潜力”并不清楚。声称某人具有潜力是一种反事实论：在其他情况下，也许是个体的后续生命阶段，这个人将具有这个属性。这带来了关于这种反事实论的范围的困难问题。在足够广泛的解释下，每个生命体都有潜力在一个足够不同的可能世界中发展相关属性，因此具有完全的道德地位。

Matthew Liao 提出了“道德代理的遗传基础”作为充分条件，这一条件比潜力标准更具说服力和包容性(Liao 2010)。它赋予那些失去代理能力的人以及从未具备过这种潜力的极度残障人士完全的道德地位，在任何传统意义上的术语中。但是，这包括这些个体的同时也包括几乎所有的人类胚胎——许多人认为这是一个过高的代价。此外，Liao 的观点需要在具有和缺乏“道德代理基础”的基因组之间进行概念上困难、道德上不确定的区分(例如，Wasserman 2002; McMahan 2002, 2008)。潜力理论面临类似的划界问题；问题在于从“潜力”到“遗传基础”的转变是否使这些问题更容易解决。遗传基础理论也存在许多人认为的“过度包容性”的问题——它们赋予早期胚胎道德地位，因为这些胚胎具有道德代理的遗传基础，至少和它们具有这种潜力一样清晰。为了证明堕胎的正当性，这两种观点的支持者必须依赖于妇女撤销妊娠援助的权利。

这些挑战也被两种试图以“模态属性”为基础赋予道德地位的观点所继承。Shelley Kagan (2016) 和 Jonathan Surovell (即将发表) 认为，那些具有或可能具有典型人类心理能力的存在是他们的道德平等者。根据这两种观点，由于疾病或损伤而失去或从未具备这些能力的人仍然具有他们本可以拥有这些能力的模态属性，而这一属性赋予他们完全的道德地位。大多数其他物种的成员并不具备这种属性，他们只能在相当遥远的可能世界中获得这些能力。正如 Surovell 所说，“\[残障是一种运气，而不是物种成员资格”(21)。

这些观点面临着与廖的观点类似的挑战，即确定一个给定的生物是否“有能力”发展成为人类的心理能力，当他们未能做到这一点是由于遗传差异时。他们还面临着其他物种成员提出的具有保持身份认同的认知升级能力的挑战。但或许最强烈反对将道德地位建立在模态属性上的是德格拉齐亚提出的：即使深度认知障碍的人类被恰当地描述为倒霉的受害者，为什么他们的受害会赋予道德地位呢？这种地位肯定不能被证明是一个差一点就获得的安慰奖。虽然这一反对意见并不直接适用于基因基础的观点，但该观点面临一个问题，即为什么具有道德重要属性的生物学基础本身具有道德意义。

### 4.2 代理、派生和推定道德地位

第二种容纳有关所有人类的完整道德地位的强烈直觉的一般方法是承认存在替代方案，以取代实际拥有赋予地位的属性。这种方法的一个版本将患有严重认知障碍的人类视为实际上能够通过与其他人类的关系代理获取必要属性的能力。例如，Thomas Scanlon（1998）确认所有人类的完整道德地位，同时将完整道德地位（“可证明性的要求”）基于“对判断敏感”的态度的个体能力。尽管他承认患有严重障碍的人类缺乏这些属性，但他提出他们可以通过受托人间接获取这些属性：

> 出生的纽带使我们有充分理由希望“将\[缺乏对判断敏感态度能力的人类]视为人类”，尽管他们的能力有限。由于这些限制，对他们的可证明性的想法必须在反事实条件下理解，即根据如果他们能够理解这样一个问题而能够合理拒绝的内容。这使得在他们的情况下使用受托人的概念是合适的，无论在非人类动物的情况下是否合适。这也表明了这样一个受托人可以根据其反对所提原则的基础提出异议。严重残疾的人类有理由希望任何人类有理由希望的东西，因为那些是他们有益处的东西。（185-186）

Scanlon 本人可能仅将受托人理解为实现或尊重具有根本障碍的人类的完整道德地位的一种方式，这种地位基于“血缘纽带”。然而，其他哲学家则认为，受托人可以通过帮助满足其他情况无法满足的标准来确保完整的道德地位。因此，Francis 和 Silvers（Francis 2009；Silvers 和 Francis 2009）认为，认知正常的人类可以作为激进受损个体的“心智假肢”：

> \[A]s a prosthetic arm or leg executes some of the functions of a missing fleshly limb without being confused with or supplanting the usual fleshly limb, so, we propose, a trustee’s reasoning and communicating can execute part or all of a subject’s own thinking processes without substituting the trustee’s ideas as if it were the subject’s own. (485)

例如，受托人可以通过仔细引导和推断她的态度和偏好来帮助主体构建自己对善的理解。

Francis 和 Silvers 并不声称具有最严重认知障碍的人类的道德地位可以建立在这种受托关系上，或者建立在这种潜力上。但考虑到他们所承认的两个挑战是有益的。第一个挑战是关于作者身份或真实性的问题：不清楚如何说受托人的推理可以“执行”所有，而不是部分，“主体自己的思考”。如果一个受托人完全执行了思考，那么这种思考如何能成为主体的思考就不清楚了。第二个挑战，与心理义肢的概念和 Scanlon 关于代理表达与判断敏感态度的概念相同，是为非人类动物提供这种支持的可能性——Scanlon 在上述段落中留下了这种可能性。如果可以将其扩展到动物身上，那么这种代表性的潜力将为大量灵长类动物和其他哺乳动物奠定完整的道德地位。为了关闭这扇闸门，似乎有必要争辩说这种支持对于智能动物，甚至是驯养的动物来说，比对于严重受损的人类更不可行。显然，需要有一个论点，即按照 Francis 和 Silvers 的术语，为前者打造心理义肢将会更加困难。

一种代理方法也引发了一个棘手的问题，即受托人如何获得道德和认知上的权威，代表一个患有严重认知障碍的个体发言。法律体系指定受托人或监护人代表那些过于不成熟或受损无法做出或已经做出自己判断的个体的“最佳利益”。但是，目前尚不清楚某人如何被“任命”为确保道德地位的受托人。此外，即使是那些与患有严重认知障碍的个体关系最密切且最为承诺的人，也可能很难辨别他的利益，并将其与自己的“判断敏感”态度区分开来。

另一种容纳对所有人类的完全道德地位有强烈信念的方式是将这种地位建立在认知正常和认知严重受损的人类之间的实际关系中。由于他们的偏袒责任，认知严重受损个体的近亲必须将他们视为具有完全道德地位。这可以被视为一种代理人相关的地位：对于近亲而言，认知严重受损的人实际上具有完全道德地位。但由于这些近亲的完全道德地位，其他人类必须尊重他们将其残障亲属视为完全道德平等的责任。然而，他们自己并不必须承担那种责任。一些个体属性账户的支持者，比如麦克马汉，似乎持有这种观点。相比之下，下一节中一些基于群体的账户认为，“血缘纽带”要求所有认知非受损的人类赋予认知严重受损的人类完全的道德地位，无论他们的关系如何。这种派生或礼貌立场的较弱版本并不声称认知严重受损的人实际上具有完全的道德地位，即使对于近亲也是如此，只是他们必须将她视为如同具有完全道德地位一样。这在家庭成员和第三方之间减少了一些（但并未消除）差距，但只是通过降低认知严重受损的人类的道德地位，即使是对于他们最重要的他人。

对于一些哲学家来说，这种观点的任何版本都是不令人满意的，原因有两个相关的。首先，这种观点太狭隘了，因为它并没有给患有严重认知障碍的人完全的道德平等。只有少数其他人类需要将他们视为道德平等；其余人只需要尊重这种特殊关系。其次，这种观点太过依赖——一个患有严重认知障碍的人甚至他的部分平等也是依赖于亲密关系的存在。如果他的父母和其他亲戚抛弃他或去世，他只能勉强要求被其他人类对待得比具有类似属性的非人类动物更好一些。

我们建议另一种“证据”方法来适应这样一种信念，即每个人都具有完全的道德地位，这种方法要求对所有人类采取强有力甚至“不可反驳”的完全道德地位的假设。这种方法基于评估人类认知潜力的困难，人们倾向于低估任何程度认知异常的人类的能力和潜力，以及对那些应该但被剥夺完全道德地位的个体造成的可怕代价。这种方法可以被视为规则后果主义，因为它要求我们有时要忽略我们的具体案例判断，因为错误的高概率和巨大代价。但它可能吸引许多拒绝将规则后果主义作为一般方法的人。与此同时，对完全道德地位的这种理由可能看起来令人不安，也太过依赖。它似乎暗示着，通过足够准确的评估工具和足够可靠的评估者，我们可以否定许多我们现在被迫视为具有道德地位的人类的道德地位。此外，他们的排除将代表道德进步。

这种对患有根本认知障碍的人赋予完全道德地位的假设可能需要更有力的理据。这种理据将生物学差异、物种规范或“血缘纽带”视为赋予完全道德地位的独立根据，而是视为赋予这一假设非常强有力的理由。也许我们之所以假定如此是出于认识论的原因，因为很难得出这样的结论：看起来像人类的个体实际上缺乏人类的能力。但这并非全部解释。当认知能力正常的人类遇到另一个外表像人类的存在时，他们通常会以一种与非人类动物不同的方式对待这个存在；他们使用独特的手势、面部表情、触摸、语言和其他行为。这些反应假定了一种可能并非总是存在的互惠交流能力。但即使这种能力不存在，这些反应也不是毫无意义的手势。它们可能促进沟通，并引发认知和社会发展，否则这些发展可能不会发生。与患有认知障碍的人一起工作的家庭成员、朋友、专业人士和学者报告说，他们与最初似乎无法沟通或有意义地回应的个体在一起的时间越长，他们就能够更多地了解他们的兴趣、欲望和情绪（Brown 和 Gothelf 1996 年；Goode 1994 年）。事实上，这些人经常展现出在服装、食物、社交和其他活动方面的类群典型偏好。在与将他们视为同一道德共同体成员的认知能力正常的人持续互动中，认知障碍个体在社会和心理上得以发展，沿着其他人类的发展轨迹。因此，将患有认知障碍的人视为具有典型人类感受、欲望和反应潜力，可能会变成自我实现。 这种方法为假定所有人类拥有完整道德地位提供了一种务实（和后果主义）的理由：不仅仅是因为错误地否认那种地位会带来可怕的后果，而且因为把我们的人类同胞视为有能力加入我们道德共同体的成员，这样做更有可能使他们真的能够这样做。

这一提议并不声称，如果针对海豚或黑猩猩采取这种对待方式就永远不会有效。但是，我们有更强烈的理由以这种方式对待我们的人类同胞，无论他们的认知障碍有多显著。我们共同的体质和遗传基因使我们能够把他们视为具有典型人类互动和活动能力或潜力，并且这样做会使他们更有可能对我们的对待作出回应，而不像一个具有类似认知能力的非人类动物那样。其他智能生物，与我们的体质不同，会有同样的理由以这种方式对待_他们_的同类。在承认这种有限的偏袒时，我们并没有把物种视为具有类似于家庭甚至国家的道德意义。显然，我们也没有假设人类特别具有特殊的道德地位。

尽管这项提议表现出坚定的乐观主义，但仍然排除了一些人类拥有完全道德地位的人。它假定了一种几乎可以肯定在先天性无脑儿患者中缺乏的社会责任感的最低水平，并且可能也缺乏在其他激进认知障碍的人类中。然而，在对同一物种成员表现出有限偏爱的务实理由中，它为那些赋予物种成员身份更为核心角色的观点铺平了道路。

## 5. 基于群体的道德地位账户

一些哲学家主张所有人类拥有完整的道德地位，而不寻求确定所有人类所拥有的任何属性来作为该地位的基础。这些哲学家大致可分为两组。第一组的哲学家认为作为_智人_物种的成员足以获得完整的道德地位，并将这种地位基于物种属性（见[第 5.1 节](https://plato.stanford.edu/entries/cognitive-disability/#SpeBasAcc)）。在这第一组中的一些哲学家看来，所有智人属于一种本质或规范是具有理性或类似属性的类别（[第 5.1.1 节](https://plato.stanford.edu/entries/cognitive-disability/#SpeBasAcc)）。对于另一些人来说，所有智人通过“血缘纽带”与其他人类相连（[第 5.1.2 节](https://plato.stanford.edu/entries/cognitive-disability/#SpeBasAcc)）。对于前者，任何一种本质上具有理性等属性的生物都具有完整的道德地位；对于后者，任何与其他人类通过血缘关系相连的人都具有完整的道德地位。尽管这两种方法都涉及相同的个体人类，但它们确定道德地位的方式对于具有严重认知障碍的人类的地位产生不同的影响。

前一种确立道德地位的方式赋予了更广泛的“权威”，因为个体的道德地位并非基于他与特定他人的关系。相反，它是基于她所属群体的规范。该规范要求任何人（无论是否是该群体的成员）都能够认可它。如果患有严重认知障碍的人类因属于具有理性规范的群体而拥有完整的道德地位，那么一个理性的火星人，与一个理性的人类一样，应该认可那些患有这些障碍的人类的完整道德地位。也许可以提出这样的论点：一个群体的在道德上相关的规范对于群体外的人并不具约束力，但我们尚未见到这样的论证。

相比之下，血缘关系可能不会约束那些缺乏相同生物联系的人；其他物种的成员无需认可患有严重认知障碍的人类的完整道德地位。从这个意义上讲，这些人类的道德地位并不像其他人类那样“完整”，因为它只能被其他人类认可。如果完整的道德地位是基于对同一群体成员的偏袒义务，那么它将是相对于群体的，对其他群体的成员并不具约束力。

第二种基于群体的账户（[Sec. 5.2](https://plato.stanford.edu/entries/cognitive-disability/#HumGroFulMorSta)）承认了第一种基于群体属性的账户所依赖的道德意义。但它否认人类的完全和平等的道德地位可以建立在任何特定属性上，即使间接地，也不能用道德中立的术语来描述。这些账户将“人类”视为一个厚重的规范概念，根植于语言和社会实践，不一定与“智人”这一生物类别完全重合，并且具有道德内容，这种道德内容不能从与之相关联的任何描述性属性中推导出来。认定一个存在是人类，因此必须受到尊重的判断并不包括一个价值中立的生物分类，以及一个证明这些被分类存在的道德地位的论证。对于某些待遇的要求，以及对其他待遇的禁止，是“人类”这一概念的含义的一部分，并且隐含在承认某个个体是人类的过程中。\[9]这些账户的支持者因此拒绝了试图确定所有具有认知正常人类成年人道德地位的标准属性的尝试（Diamond 1978; Edwards 1997; Byrne 2000）。这些哲学家反对将认知非残障成年人作为完全道德地位的典范，将婴儿、幼儿和认知严重受损的成年人视为需要通过延伸来证明其道德地位的“边缘案例”\[10]。

对于两种基于群体的账户，完全道德地位的范畴性质是通过这种地位的基础方式来解释的。对于这两种类型的账户来说，人类物种的成员资格是这种地位的充分条件，这是一个范畴性质，而不是一个连续的“变量”（尽管由于不精确或相互冲突的成员资格标准可能存在一些模糊或歧义）。对于这两种类型的账户，无论个体属性如何，所有人类的完全道德地位的基础是相同的。

### 5.1 基于物种的账户

### 5.1.1 物种规范

在第一个关系方法中，一些被内在属性账户确定为完全道德地位所必需的属性，在获得道德地位时也可以发挥不同的作用。尽管在这个观点中，自我意识和实践理性并非对于完全道德地位是必要的，但它们是人类的规范。这个规范不能以统计数据来理解；即使大多数或所有人类不再具有自我意识或实践理性，这个规范也不会改变。相反，这个规范捕捉了物种的特征。一个正常的属性并不是每个成员都必须拥有的本质。相反，它是一个关系属性：每个个体作为拥有这种属性为规范的群体的成员具有道德地位。例如，斯坎隆（Scanlon）声称，我们必须为我们的行为辩护的那些人“至少包括那些通常能够产生与判断敏感态度相关的生物的类别”（1998 年，186 页）。

物种规范理论关注人类物种，但并不局限于人类。可以假设，如果我们发现海豚或火星人是具有类似认知规范的物种，那么该物种的成员无论是否符合该规范，都将具有完整的道德地位。我们将被束缚去承认（如果它们能够的话）所有海豚或火星人的完整道德地位，它们也将被束缚去承认所有人类的完整道德地位。

在以某些但并非所有成员遵守的规范中建立完整和平等的道德地位可能存在紧张关系。即使实践理性的规范赋予所有群体成员平等的道德地位，似乎那些实际拥有该属性的成员会比缺乏该属性的成员更“平等”。对这一顾虑的回应，我们在[Sec. 5.2](https://plato.stanford.edu/entries/cognitive-disability/#HumGroFulMorSta)中讨论，即拥有该属性的人拥有更好的运气，但并没有更高的道德地位。

不足为奇的是，像麦克马汉（McMahan）这样的内在属性账户的支持者，并不同情于声称“关于某些个体的本质的事实可能决定缺乏该本质的其他个体应该如何被对待”（麦克马汉，2008 年，85 页）。像其他批评者一样，麦克马汉认为这种说法需要一种将关于某些个体的事实性主张转化为关于其他人的道德要求的“道德炼金术”。但对于那些将人性视为“厚重”概念的人来说，不需要炼金术。人类的概念是一种规范性概念，赋予所有符合条件的人权利，并对那些理解并应用它的人提出道德要求。

#### 5.1.2 (Co-)Humanity as a Special Relationship

一种声称赋予道德地位的物种关系的第二种类型并不是个体人类与物种规范之间的关系，而是个体人类与其他人类之间的关系，特别是那些认知正常的人。该主张是，基于作为物种的共同成员，人类有理由将彼此视为道德平等。正如斯坎隆（Scanlon）（1998）所断言，

> “\[ ]仅仅一个生物‘生于人类’的事实就足以成为赋予它与其他人类相同地位的有力理由。有时这被描述为偏见，称为物种主义。但认为我们与这些生物的关系使我们有理由接受我们的行动需要对他们进行证明，并不是偏见。”（185）

这一立场将所有人类的完整道德地位基于人类之间的亲属关系，这在过去被称为“人类大家庭”。尽管这种亲属关系目前取决于出生于人类母亲，但它被所有人类分享，并且不随血缘关系的程度而变化。

这种方法避免了以物种规范作为道德地位来源的可疑概念。但与物种规范账户不同的是，它不要求除人类之外的任何其他生物来承认所有人类的完整道德地位。在物种规范账户中，麦克马汉（2002）观察到，

\[智能和道德敏感的火星人将被要求以与对待我们相同的方式对待智力严重低下的人类。... 但如果我们必须给予智力严重低下者与其他人类相同的道德地位的原因是我们通过'血缘关系'与他们有关，那么火星人就没有这个理由。] (217)

这在目前实际上是一个微不足道的限制，但对于那些坚持道德地位应该得到普遍认可的人来说，这是一个表达上重要的限制。

一个支持人类亲属关系方法的辩护者可能会很容易地接受这一限制，特别是因为我们和火星人仍然会受到认知正常成员对其根本受损关系的依恋的约束（如上文[Sec. 4.2](https://plato.stanford.edu/entries/cognitive-disability/#ProDerPreMorSta)所讨论的）。但她仍然必须捍卫这样一种主张，即在物种中的共同成员身份建立了需要甚至是这种相对于物种的完全道德地位的亲属关系。麦克马汉（2002）认为，即使在某些团体（如国家）的成员身份可以赋予其成员完全平等的地位，但在同一物种中的成员身份却不能：

> 与国家成员身份不同，物种成员身份不是集体身份的焦点。作为人类并不能明显区别我们与其他任何事物；因此，它未能激发我们的自豪感或增强我们的身份认同感。正如没有人会因意识到自己是动物而不是植物而扩大自己的身份认同感一样，因此没有人的身份认同感会因意识到自己是人类而不是，例如，一只兔子而受到重要的塑造。（221）

辩护“血缘纽带”观点的人可以有两种回应方式。首先，她可能会否认物种身份和自豪感对于确立物种成员的完整道德地位是必要的。相反，这种地位是建立在人类之间的相似之处上的，即使是极度残障的人，这种相似之处源于他们独特的具身性，并且产生了一种强烈的奋斗感（对于那些足够自我意识到有奋斗感的人来说），甚至掩盖了在智力能力上的巨大差异。这些相似之处包括感受方式、交流方式、移动方式，以及对待和与物种其他成员互动的方式。这些亲和力受到文化的影响，其他物种的成员也分享其中的一些，而并非所有该物种的成员——即使是认知正常的人——都分享所有这些。然而，它们可能构成一个足够独特的整体，为偏袒提供基础。或者辩护者可能会主张，正如伯纳德·威廉姆斯（2006）所做的那样（参见[Sec. 5.2](https://plato.stanford.edu/entries/cognitive-disability/#HumGroFulMorSta)），物种认同和自豪感_可能_在当前被缺乏合适的比较类别所掩盖的人类奋斗中发挥作用。尽管，正如罗伯特·诺齐克（1974）所观察到的，当代人类没有人会夸耀自己拥有可对立的拇指或说一种语言，但我们的物种自豪感和认同感可能会在另一个先进物种的存在下结晶化，这将突显出我们独特的共同历史和成就。

站在一边，然而，这两种回应似乎都容易受到这样的质疑，即与物种成员身份相关的相似之处可能可以解释，但不能证明将所有其他人类视为道德平等的对待。然而，为什么一种强烈的归属感会成为道德地位的来源呢？如果我们的归属感反映了道德上重要的事情，为什么我们不会对麦克马汉（McMahan）那个聪明、道德敏感的火星人产生更强烈的归属感，而不是对一个发育障碍使他似乎对其他人类无动于衷的人类婴儿呢？

一个回应是，正义于物种内部的偏爱的不是分享的能力，而是获益的能力。因此，冈纳森（Gunnarson，2008）提出，我们自己物种的成员有能力从与其他人类的关系中获得独特的内在好处。依赖这种能力可能为完全的道德地位提供一个直观上更具吸引力的基础，胜过基于生物相似性的基础。但它仍可能否认某些人类的完全道德地位 —— 不仅仅是无脑儿婴儿，还有其他缺乏从与其他人类的关系中获益的能力的人。此外，一些非人类动物，尤其是驯养的动物，可能会从与人类的关系中获得巨大且独特的内在好处（Townley，2010）。而其他认知先进物种的成员可能会从与人类的互动中以高度特定、可能是独特的方式获益。

### 5.2 人性作为完整道德地位的基础

为了解释物种共同成员身份的道德意义，一些哲学家接受了一种强烈的人类中心观，否认我们可以超越人类来评估世界居民的道德地位。在这种观点下，人类的概念优先于且与人的概念不可分割。正如斯蒂芬·穆尔霍尔（2002）所主张的：

> 我们对人的概念是我们对人类的概念的延伸或方面；而这个概念不仅仅是生物学的，而是我们对我们独特物种本质的一切所凝结的。把另一个人看作是一个人类，就是把她看作是一个同类生物——另一个生命体，其具体化身将她嵌入具有语言和文化的独特共同生活形式中，其存在构成了对我们一种特殊要求的主张。（7）

这种观点，我们将其称为“人道主义”，有两种变体。第一种，根据 Williams（2006），是语言或概念的，受到 Wittgenstein（1958）的影响。我们通过其他人类的行为来理解思考、决定和感受等概念，尽管我们可以将这些能力或状态归因于其他生物，但这只是通过延伸或类比（Hanfling 2001）。我们还学习了对待其他人类的适当方式，学习了这个概念本身：例如，人类应该被命名，即使他们已经死亡也不应该被吃掉。我们并不_得出_人类必须以这种方式对待的结论；他们必须被这样对待的认知已经是这个概念含义的一部分（Diamond 1978；Gleeson 2008）。这种“厚重”的人类概念并不是生物学的，也不必具有与_智人_类相同的范围。对于这种类型的一些人道主义者来说，新生的胚胎不是人类，并且新受精卵的生命必须得到与新生婴儿生命同等程度的保护并不是人类概念的一部分（Crary 2007）。但是，其他人道主义者_确实_将早期胚胎视为人类，这表明有必要解释这样一个根深蒂固的概念如何在边缘处如此模糊或有争议。

由于我们的语言和概念在我们的道德理解中的作用，根据人道主义观点，我们对道德实践的理解和批判只能在其中内部进行。正如伯恩所坚持的，

> 当理性内在运作时，道德才能得以恰当发挥作用。提出一个扎实的道德论证来批评我们的任何一种道德实践，将涉及从我们道德生活的其他部分汲取洞察力。(2000, 70–71)

反对食用动物的论据，例如，不能通过列举它们与我们共享的属性来证明，而只能通过揭示与我们其他做法的紧张或矛盾来证明（Diamond 1978）。据推测，任何排除某些生物人类或否认其中一些人类拥有完全道德地位的观点都将面临类似但更严重的紧张，这使得种族主义和性别歧视无法持续下去，而不会对人类本身的概念施加任何压力。

第二种观点否认了评估道德地位的客观基础的可能性。试图以绝对的标准对世界的居民进行分级或评估，就是把宇宙视为具有观点的东西——一个神明或功利主义理想观察者的视角（Williams 2006）。没有这样的观点，人类只能根据自己的关注点、价值观和文明来评判世界其他部分。事实上，只有在这些条件下，我们才会重视那些被个体属性理论家视为完整道德地位标准的特征，比如理性和自我意识（Grau, 2016）。因此，我们的人性为我们提供了一个不可或缺的评估世界其他部分的参照框架。它还奠定了我们彼此之间可辩护的偏爱，这与种族主义或性别歧视几乎没有共同之处。几乎所有人都接受对另一个生命的人性的裸露呼吁，而不是她的性别或种族，作为行动或克制的基础，我们在拒绝种族主义和性别歧视时也会呼吁人性。几乎没有最不道歉的种族主义者或性别歧视者会仅仅因为种族或性别而做出不受道德相关属性支持的裸露呼吁。这种“人类偏见”更类似于共享文化的参与者之间的亲近感和忠诚。如果很难以这种方式看待“人类偏见”，那可能是因为“人类不必与任何其他生物打交道，后者无论在论据、原则、世界观或其他方面都无法回应”（Williams 2006, 148）。在一个没有竞争对手的世界中，一种独特的人类“文化”既无处不在又几乎看不见。Williams 并没有声称我们对那种文化的承诺必然会胜过参与更先进的普遍社区并放弃我们自己文化中珍视但偏见的方面的呼吁。然而，这种承诺将为我们提供一个道德上可辩护的理由，而不仅仅是一种偏见，反对这种同化。

人道主义者认识到物种规范具有道德意义，但他们并没有赋予这些规范在将道德地位建立在关系属性上的观点中所起的决定性作用。在我们之前引用的段落中，穆赫尔(Mulhall)认为患有严重残疾的人类在缺乏人类特有的能力方面遭受了严重的不幸：

> 我们并不（当我们努力时）努力将人类婴儿和儿童、老年人和严重残障者视为完全的人类，是因为我们错误地将能力归因给了他们实际缺乏的能力，或者是因为我们对物种界限的纯生物学意义视而不见。我们这样做（当我们这样做时）是因为他们是我们的同类人类，是具有肉体的生物，他们将会分享我们共同的生活，或者已经分享过，或者由于所有人类血肉所遭受的冲击和疾病而无法这样做——因为上帝的恩典降临在我身上。（穆赫尔，2002 年，第 7 页）

对于像穆哈尔这样的人道主义者来说，参与独特的人类生活形式的能力既不是充分的道德地位的必要条件，也不是充分的条件（正如麦克马汉（2005）所假设的）。这种地位仅仅通过我们将一个个体视为人类，并认为他们是与我们一样承受着相同的“冲击和疾病”的继承人而建立起来。患有严重认知障碍的人类遭受着“严重的不幸”，如果他们无法分享他们的具体形式的共同生活，那么他们的地位并不会降低，因为他们的具体形式的共同生活是他们的体现所“嵌入”的。具有类似属性的非人类动物具有较低的道德地位，但并不会遭受类似的疏远或损失。他们参与自己物种的独特生活形式（如果有的话）并不取决于，而且很可能会受到，他们是否具有正常成年人类的认知属性的影响。

批评者可能会承认，事实上我们对待人类和非人类动物持有这些不同的态度，但会质疑它们的道德意义：为什么我们应该认为某些能力的先天缺失是一种悲剧，_对于那些缺乏这些能力的个体_，只有当这个个体是人类时（麦克马汉 1996 年）？人道主义者会回应说，问题本身揭示了批评者对“人类”的概念并不清楚 - 这个概念包括了一个基于共同体现的共同生活的概念，而这种共同生活是基于共享的，而患有严重障碍的人类被排除在外。

Humanists are wary of grounding the full moral status of radically-impaired humans in their relationship to a species-norm like rationality. Thus, Byrne (2000) argues that appeal to the rational nature of human beings as the basis for respect is too reliant on external justification and too narrow. It is too reliant on claims about the respect owed to beings that are rational-by-nature, which Byrne doubts are any more self-evident or plausible than claims about the respect owed human beings. It is too narrow because it ignores other aspects of humanity that make the concept of “human being” so rich and powerful.

Humanists 对于将患有严重障碍的人类的完整道德地位建立在其与理性等物种规范的关系上持谨慎态度。因此，拜恩（2000）认为，诉诸于人类作为尊重基础的理性本质过于依赖外部理由且过于狭隘。它过于依赖于关于应尊重天生理性的存在的主张，而拜恩怀疑这些主张并不比关于应尊重人类的主张更不言自明或更合理。它过于狭隘，因为它忽视了使“人类”概念如此丰富和强大的人性的其他方面。

相比之下，威廉姆斯的观点以及其他不太依赖于维特根斯坦语言和概念观的观点，更能够更容易地应对与其他智能生物的密切接触的可能性。威廉姆斯本人考虑到这种相遇，并承认相互认可的可能性，尽管他认为这种可能性可能会受到对自己物种及其文化成员的偏爱的合理限制。

这两种人道主义观点对“人类”这一厚重概念的界限提出了关键问题。这一概念是否包括早期胚胎，或者由未来的合成生物学产生的类人生物？我们能否根据所提出的界限与与该概念相关的其他信念和实践的契合来回答这些问题，或者这些问题是否是在我们实际实践中逐渐转变而不太自觉地决定的？如果我们没有更好地了解如何解决界限问题，那么人道主义观点将如何应对排斥的挑战就不太清楚了。

## 6. 结论

对于那些患有最严重认知障碍的人的道德地位，似乎很难达成共识。关于人类的道德地位如何建立，甚至是否能够建立，存在着严重分歧，以及对我们最坚定、最深思熟虑的道德信念应该给予多大重视也存在分歧。那些将完全的道德地位建立在个体心理属性上的观点必然会排除一部分人类，而且似乎会对被排除的人类的对待产生一些我们中很少人愿意接受的影响。那些将完全的道德地位建立在我们的物种成员资格上——在物种的本质或者我们与其他成员的生物关系中的观点可以避免这些影响。那些否认需要将人类的道德地位建立在我们或我们群体拥有的任何属性上的观点也可以避免这些影响。但是，这些避免排斥的方式都存在着重大的代价。它们要求对那些拥有共同生物特征、外貌或起源的人表现出强烈的偏爱，这种偏爱与我们对待他人行为的正当性的同样坚定但更抽象的信念相冲突。而且，它们对我们对待其他生物的道德约束以及人类本身的界限留下了深深的不确定性。

尽管两种方法面临严峻挑战，达成任何共识的障碍重重，但对于患有严重认知障碍的人类的道德地位的讨论是应用伦理学中的一个核心问题。这一讨论需要继续。

## Bibliography

* American Association on Intellectual and Developmental Disabilities. (2011). “Definition of Intellectual Disability.” [available online](http://www.aamr.org/content\_100.cfm?navID=21) \[accessed 12 August 2011].
* Berube, M., 1996. _Life as We Know It: A Father, a Family, and an Exceptional Child_, New York: Pantheon.
* Birch, T., 1993. “Moral Considerability and Universal Consideration,” _Environmental Ethics_, 15: 313–332.
* Brown, F. and Gothelf, C., 1996. “Community Life for all Individuals,” in _People with Disabilities Who Challenge the System_, D.H. Lehr and F. Brown (eds.), Baltimore: Paul H. Brookes Publishing Co., 175–188.
* Byrne, P., 2000. _Philosophical and Ethical Problems in Mental Handicap_, New York: Palgrave.
* Carlson, L., 2009. “Philosophers of Intellectual Disability: A Taxonomy,” _Metaphilosophy_, 40: 552–566.
* Carlson, L. and Kittay, E., 2009. “Introduction: Rethinking Philosophical Presumptions in Light of Cognitive Disability,” _Metaphilosophy_, 40: 307–330.
* Crary, A., 2007. “ Humans, Animals, Right and Wrong,” in _Wittgenstein and the Moral Life: Essays in Honor of Cora Diamond_, A. Crary (ed.), Cambridge, MA: MIT Press, 381–404.
* Curtis, B. and Vehmas, S., 2016. “A Moorean argument for the full moral status of those with profound intellectual disability,” _Journal of Medical Ethics_, 42: 41–45.
* DeGrazia, D., 2008. “Moral Status as a Matter of Degree?” _Southern Journal of Philosophy_, 46: 181–198.
* Diamond, C., 1978. “Eating Meat and Eating People,” _Philosophy_, 53: 465–479.
* DiSilvestro, R., 2010. _Human Capacities and Moral Status_, Dordrecht: Springer.
* Dresser, R., 1995. “Dworkin on Dementia: “Elegant Theory, Questionable Policy,” _Hastings Center Report_, 25: 32–38.
* Dworkin, R., 1994. _Life’s Dominion: An Argument about Abortion, Euthanasia, and Individual Freedom_, New York: Vintage Press.
* Edwards, S., 1997. “The Moral Status of Intellectually Disabled Individuals,” _The Journal of Medicine and Philosophy_, 22: 29–42.
* Feinberg, J., 1986. “Abortion” in _Matters of Life and Death_, 2nd edition, T. Regan (ed.), New York: Random House.
* Francis, L., 2009. “Understanding autonomy in light of intellectual disability,” in _Disability and Disadvantage_, K. Brownlee and A. Cureton (eds.), New York: Oxford University Press, 200–215.
* Francis, L. and Norman, R., 1978. “Some Animals are More Equal than Others,” _Philosophy_, 53: 518.
* Gleeson, A., 2008. “Eating Meat and Reading Diamond,” _Philosophical Papers_, 37: 157–175.
* Goode, D.A., 1994. _A World Without Words: The Social Construction of Children Born Deaf_, Philadelphia: Temple University Press.
* Grau, C., 2015. “McMahan on Speciesism and Deprivation,” _The Southern Journal of Philosophy_, 53(2): 216–226.
* –––, 2016. “A Sensible Speciesism?” _Philosophical Inquiries_, 4(1): 49–70.
* Gunnarson, L., 2008. “Great Apes and the Severely Disabled: Moral Status and Thick Evaluative Concepts,” _Ethical Theory and Moral Practice_, 11: 305–326.
* Hacker-Wright, J., 2007. “Moral Status in Virtue Ethics,” _Philosophy_, 82: 449–473.
* Hanfling, O., 2001. “Thinking,” in _Wittgenstein: A Critical Reader_, H. Glock (ed.), Malden, MA: Blackwell.
* Harman, E., 2003. “The potentiality problem,” _Philosophical Studies_, 114(1–2): 173–198.
* Harris, J., 1985. _The Value of Life_, London: Routledge.
* Jaworska, A., 1999. “Respecting the Margins of Agency: Alzheimer’s Patients and the Capacity to Value,” _Philosophy and Public Affairs_, 28: 105–138.
* –––, 2007. “Caring and Full Moral Standing,” _Ethics_, 117: 460–97.
* Jaworska, A. and Tannenbaum, J., 2015. “Who Has the Capacity to Participate as a Rearee in a Person-Rearing Relationship?” _Ethics_, 125(4): 1096–1113.
* Kagan, S., 2016. “What’s Wrong with Speciesism? (Society for Applied Philosophy Annual Lecture 2015).” _Journal of Applied Philosophy_, 33: 1–21.
* Kain, P., 2009. “Kant’s Defense of Human Moral Status,” _Journal of the History of Philosophy_, 47: 59–102.
* Kittay, E., 1999. _Love’s Labor: Essays on Women, Equality, and Dependency_, New York: Routledge.
* –––, 2005. “At the Margins of Personhood,” _Ethics_, 116: 100–31.
* –––, 2008. “Ideal Theory in Bioethics and the Exclusion of People with Severe Cognitive Disabilities,” in _Naturalized Bioethics: Toward Responsible Knowing and Practice_, H. Lindemann, M. Verkerk and M.U. Walker (eds.), Cambridge: Cambridge University Press, 218–237.
* Korsgaard, C., 1996. _The Sources of Normativity_, Cambridge: Cambridge University Press.
* Kumar, R., 2008. “Permissible Killing and the Irrelevance of Being Human,” _The Journal of Ethics_, 12: 57–80.
* Liao. M., 2010. “The Basis of Human Moral Status,” _Journal of Moral Philosophy_, 7: 159–179.
* McMahan, J., 1996. “Cognitive Disability, Misfortune, and Justice,” _Philosophy and Public Affairs_, 25:3–31.
* –––, 2002. _The Ethics of Killing_, New York: Oxford.
* –––, 2005. “Our Fellow Creatures,” _The Journal of Ethics_, 9: 353–380.
* –––, 2007. “Infanticide,” _Utilitas_, 19: 131–159.
* –––, 2008. “Challenges to Human Equality,” _The Journal of Ethics_, 12: 81–104.
* –––, 2009. “Radical Cognitive Limitation,” in _Disability and Disadvantage_, K. Brownlee and A. Cureton (eds.), New York: Oxford University Press, 240–259.
* –––, 2016. “On ‘Modal Personism’,” _Journal of Applied Philosophy_, 33: 26–30.
* Mulhall, S., 2002. “Fearful Thoughts,” _London Review of Books_, 24: 18.
* Mullin, A., 2011, “Children and the Argument from ‘Marginal’ Cases,” _Ethical Theory and Moral Practice_, 14: 291–305.
* Newson, A.J., 2007. “Personhood and Moral Status,” in _Principles of Health Care Ethics_, 2nd edition, R.E. Ashcroft, A. Dawson, H. Draper and J.R. McMillan (eds.), Chichester: John Wiley & Sons, Ltd.
* Nozick, R., 1974. _Anarchy, State, and Utopia_, New York: Basic Books.
* Perring, C., 1997. “Degrees of Personhood,” _Journal of Medicine and Philosophy_, 22: 173–197.
* Rawls, J., 1971. _A Theory of Justice_, Cambridge, MA.: Harvard University Press.
* Roberts, A.J., 2016. “Pessimism About Motivating Modal Personism,” _Journal of Applied Philosophy_, published online 3 July 2016, doi: 10.1111/japp.12235
* Rossi, J., 2010. “Is equal moral consideration really compatible with unequal moral status?” _Kennedy Institute Ethics Journal_, 20(3): 251–76.
* Savulescu, J., 2009. “The Human Prejudice and the Moral Status of Enhanced Beings: What Do We Owe the Gods?” in _Human Enhancement_, J. Savulescu and N. Bostrom (eds.), New York: Oxford University Press, 211–250.
* Sachs, B., 2011. “The Status of Moral Status,” _Pacific Philosophical Quarterly_, 92: 87–104.
* Scanlon, T., 1998. _What We Owe to Each Other_, Cambridge, MA: Harvard University Press.
* Shoemaker, D., 2007. “Moral Address, Moral Responsibility, and the Boundaries of the Moral Community,” _Ethics_, 118: 70–108.
* Silvers, A. 2012. “Moral status: what a bad idea!” _Journal of Intellectual Disabily Research_, 56(11): 1014–25.
* Silvers, A. and Francis, L., 2009. “Thinking about the good: reconfiguring liberal metaphysics (or not) for people with cognitive disabilities,” _Metaphilosophy_, 40: 475–498.
* Singer, P., 1993. _Practical Ethics_, 2nd ed. Cambridge: Cambridge University Press.
* –––, 2005. “Ethics and Intuitions,” _The Journal of Ethics_, 9: 331–352.
* –––, 2009. “Speciesism and Moral Status,” _Metaphilosophy_, 40: 567–581.
* –––, 2016. “Why Speciesism is Wrong: A Response to Kagan,” _Journal of Applied Philosophy_, 33(1): 31–35.
* Stone, J., 2007. “Pascal’s Wager and the Persistent Vegetative State,” _Bioethics_, 21(2): 84–92.
* Surovell, Jonathan, forthcoming. “But for the Grace of God: Abortion and Cognitive Disability, Luck and Moral Status” _Ethical Theory and Moral Practice_, published online 29 September 2016, doi:10.1007/s10677-016-9755-0
* Townley, C., 2010. “Animals and Humans: Grounds for Separation?” _Journal of Social Philosophy_, 41: 512–526.
* Tooley, M., 1983. _Abortion and Infanticide_, Oxford: Clarendon.
* Walker, R.L. and King, N.M.P., 2011. “Biodefense Research and the U.S. Regulatory Structure: Whither Nonhuman Primate Moral Standing?” _Kennedy Institute of Ethics Journal_, 21: 277–310.
* Warren, M.A., 1997. _Moral Status: Obligations to Persons and Other Living Things_, Oxford: Clarendon.
* Wasserman, D., 2002. “Personal Identity and the Moral Appraisal of Prenatal Genetic Therapy,” in _Mutating Concepts, Evolving Disciplines: Genetics, Medicine, and Society_, L. Parker and R. Ankeny (eds.), Dordrecht: Kluwer, 235–264.
* Wasserman, D. and McMahan, J., 2012. “Cognitive Surrogacy, Assisted Participation, and Moral Status,” in _Medicine and Social Justice_, 2nd edition, R. Rhodes, M. Battin, and A. Silvers (eds.), Oxford: Oxford University Press.
* Williams, A., 1987. “Response: QUALYfying the Value of Life,” _Journal of Medical Ethics_, 13: 123.
* Williams, B., 2006. “The Human Prejudice,” in _Philosophy as a Humanistic Discipline_, Princeton: Princeton University Press. 135–152.
* Wittgenstein, L., 1958. _Philosophical Investigations_, Oxford: Oxford University Press.
* Wong, S., 2007. “The Moral Personhood of Individuals Labeled ‘Mentally Retarded’: A Rawlsian Response to Nussbaum,” _Social Theory and Practice_, 33: 579–594.

## Academic Tools

| ![sep man icon](https://plato.stanford.edu/symbols/sepman-icon.jpg) | [How to cite this entry](https://plato.stanford.edu/cgi-bin/encyclopedia/archinfo.cgi?entry=cognitive-disability).                                                                      |
| ------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ![sep man icon](https://plato.stanford.edu/symbols/sepman-icon.jpg) | [Preview the PDF version of this entry](https://leibniz.stanford.edu/friends/preview/cognitive-disability/) at the [Friends of the SEP Society](https://leibniz.stanford.edu/friends/). |
| ![inpho icon](https://plato.stanford.edu/symbols/inpho.png)         | [Look up topics and thinkers related to this entry](https://www.inphoproject.org/entity?sep=cognitive-disability\&redirect=True) at the Internet Philosophy Ontology Project (InPhO).   |
| ![phil papers icon](https://plato.stanford.edu/symbols/pp.gif)      | [Enhanced bibliography for this entry](http://philpapers.org/sep/cognitive-disability/) at [PhilPapers](http://philpapers.org/), with links to its database.                            |

## Other Internet Resources

* Wilkinson, D., 2008. [Answering the challenge: moral philosophy and the cognitively disabled](http://www.academia.edu/151381/Answering\_the\_challenge\_moral\_philosophy\_and\_the\_cognitively\_disabled), online manuscript, accessed Novemer 22, 2011.

## Related Entries

[animal: consciousness](https://plato.stanford.edu/entries/consciousness-animal/) | [disability: and justice](https://plato.stanford.edu/entries/disability-justice/) | [disability: definitions and models](https://plato.stanford.edu/entries/disability/) | [disability: health, well-being, personal relationships](https://plato.stanford.edu/entries/disability-health/) | [feminist philosophy, topics: perspectives on disability](https://plato.stanford.edu/entries/feminism-disability/) | [moral status, grounds of](https://plato.stanford.edu/entries/grounds-moral-status/)

### Acknowledgments

We have received invaluable editorial assistance from Dorit Barlevy, Ari Schick, and William Chin.

[Copyright © 2017](https://plato.stanford.edu/info.html#c) by\
David Wasserman\
Adrienne Asch\
Jeffrey Blustein\
[Daniel Putnam](https://www.danielmputnam.com/)
